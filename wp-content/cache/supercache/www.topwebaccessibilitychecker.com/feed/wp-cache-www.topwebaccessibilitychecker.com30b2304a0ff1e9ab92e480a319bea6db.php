<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Top Web Accessibility Checker</title>
	<atom:link href="https://www.topwebaccessibilitychecker.com/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.topwebaccessibilitychecker.com</link>
	<description>Check Your Web Accessibility</description>
	<lastBuildDate>Thu, 02 Sep 2021 07:36:14 +0000</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=5.7.2</generator>
</channel>
</rss>
