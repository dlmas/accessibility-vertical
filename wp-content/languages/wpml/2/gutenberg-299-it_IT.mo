��    �      �      �	      �	  �  �	      ^            �      �      �            $  �   E  �  4  ?  "    b  V  h    �  (   ?      h      �      �      �    �  �  �  �   ~    g  �   }      s      �      �  �  �  �  �$  �  �'    ]+  k  o,  j  �.    F0  �  ]2     S5  �   T9  \  :  �   w>      W?      x?    �?      �A      �A      �A      B      ,B      MB    nB      �C      �C      �C      �C    D  ^   G  �  I  F  HL  5  �N    �R    �T  H   �X      4Y      UY      vY  �  �Y      $]      E]      f]      �]      �]      �]  �   �]  �   �^  E  i_      �`      �`      �`  �   a  %   �a      �a       b      !b      Bb      cb  B   �b     �b     �b     �b  
   c  "   c  
   <c     Gc  �  ^c  &   &f     Mf  (   bf  4   �f     �f     �f  �   �f  *   �g  +   �g  �  (h     �i    �i  "   �j  8  �j  o   p    �p  j   �s     t  �   �t  �  uu     5w      Aw      bw      �w      �w      �w      �w      x      (x      Ix      jx      �x      �x      �x      �x      y      0y      Qy      ry      �y      �y      �y      �y      z      8z      Yz      zz      �z      �z      �z      �z      {      @{      a{      �{  <  �{  �   �|  Y   �}  �   ~  O  �~  z   �      r�  �  s�  l   E�  K  ��  �  ��  �  �  5  ܕ  �  �     �  y  �  �  {�  �  F�  v  �  �  f�  �  I�  %   &�  �  L�  �  �  c  ��  8   ��  \  #�    ��  B  ��  �  ��  �  `�  �  ^�  w  H�  x  ��   	  9�  �  :�  K  ,�  h  x�  �  ��  �  � n  � �  *
 �  �   s �   5  i  y  �#   ' b  %*    �.   �. i   �1 !  2 �   75 �  6 �  ; {   �>    ? 
   )? \  4? �  �B �  �H x  �M   �R �  V w  �\ �  Ia �  ;h .   �j p   �j h  ok   �n �   �t �  \u (   Uz '   ~z  	  �z -   �� I  Ճ   � �  %� �  ō F  q� �  �� !  �� (   �� 4  ՜ �  
� �  �    ��    	� B   � -   Z�    ��    ��    ��    �� 
   ӯ "   ޯ   � .   �    <� 2   V� 8   ��    ³ %   ڳ    � (   � 3   H� �  |�     �   .� '   F� 4  n� p   �� 8  � l   M� �   �� c  ;� �  ��    ��   ��   �� �  ��   _� �  w�   a� �   ��   0� 8  7� n  p� (   �� 2   �    ;�    S� %   g� �  ��    �� F  �� �  �� �  l� b   v  o �  � I  � B   � �  / %   �   � 3   � �  ) �  � "   �# B  �# �  �& F  �* �   , i   �, �   X- F  . {   O/  (a) administer and make available the Site and Services to you; <br/>(b) further develop and generally improve the Site and the Services; <br/>(c) counting and tracking purposes of referrals and ads; <br/>(d) validation of users as human and anti-fraud monitoring <br/>(e) respond to your inquiries and other communications with you; <br/>(f) identify or authenticate your access and use of the Site. 004b6710fc6af22cfb0b75c79de3cda4 011e852f7d0b8b1236aff0d537879012 055275172dfb2ea6ced325e032bbc10a 082df888b96aff3fe34cc71283a121d5 085eb26929ce9d1296a97091a9c73288 0aedd55307fec0b37a9d46d6e58b7bc5 0ebb149d34b753775e32c32a9fb82c89 1.    <strong>Terms of Use</strong>. This Privacy Policy forms an integral part of the Site's Terms of Use (the "Terms"). Any capitalised term in this Privacy Policy that we don't define, will have the meaning given to it in the Terms. 10.   <strong>Information Security</strong>. We have put in place appropriate security measures to prevent your personal data from being accidentally lost, used or accessed in an unauthorised way, altered or disclosed. For example, the Site is encrypted by using SSL and so as any other information at transport, we periodically review our information and data collection practices to ensure constant monitoring, we use third-party processing and serving services that implement various information security standards and certifications (at rest and transmission), and we monitor and filter access to data when it comes to personnel in our organization. Such systems and procedures reduce the risk of security breaches, but they do not provide absolute security, as there is no such thing when it comes to the world-wide network. Therefore, we cannot guarantee that the Site and our servers are immune to unauthorized access to the information stored therein and to other information security risks.   11.    <strong>Children’s Privacy</strong>. The Site or the Services are not designed to attract children under the age of 16. Accordingly, we do not intend to collect Personal Information from anyone we know to be under 16. If we learn that we have collected Personal Information from a child under 16, we will use commercially reasonable efforts to delete that information as quickly as possible. If you believe that we might have any such information, please contact us at: <a href="mailto:hello@bestwebsiteaccessability.com">hello@topwebaccessibilitychecker.com</a>. 12.    <strong>Commitment</strong>. Protecting your privacy online is an evolving practice, and we use our best efforts to evolve our Site in a manner that meets these requirements and best practices as per all jurisdictions in which the Site is available.  13.1.    <em>General Data Protection Regulation (GDPR)</em> - European Representative. Pursuant to Article 27 of the General Data Protection Regulation (GDPR), Top Web Accessibility. has appointed European Data Protection Office (EDPO) as its GDPR representative in the EU. You can contact EDPO regarding matters pertaining to the GDPR by: 13.2.     <em>We try to make this Privacy Policy simple and easy to understand</em>. However, if you have any questions you may contact us at: <a href="mailto:hello@bestwebsiteaccessability.com">hello@topwebaccessibilitychecker.com</a> is for data protection inquiries in accordance with your legal rights listed above. Any other inquiries sent to this email box will be ignored. 13.    <strong>Contact Us. </strong> 1695a23bbc1a867c4e02e3e11fc0c216 1dad1a236045027566851157612ad6e5 1e48d055bd48ef69651782a0e809a563 1f8726f32238604735b7b56b25aa021e 2.1.   <em> Performance of a Contract</em>. By accessing or using the Site, you ask us to provide the Services to you in accordance with our Terms of Use. In order to do that, we need to process some of your Personal Information (such as your IP address). 2.2.    <em>Legitimate Interests</em>. We may use some of your Personal Information where we have legitimate business grounds to do so. We use this Personal Information to operate, improve and promote the Services (for example, when we customize your experience of the Services or for analytics purposes) and to protect our business (for example, to prevent fraud or ensuring the Site security). 2.3.    <em>Legal Obligations</em>. In some cases, our use of your personal information will be necessary to comply with regulatory obligations or legal requirements (for example, addressing complaints from users or authorities); 2.4.    <em>Consent</em>. In certain cases. We will ask for your consent to process and use your Personal Information in order to provide additional services (for example, when you subscribe to our newsletters, create your user profile, provide us Personal Information to get tailored offers or add a review on a service or product). When we rely on consent as our legal basis of processing You may ask to withdraw your consent by sending us a request to [EMAIL ADDRESS]. Once we have verified your request, we will contact you.  2.    <strong>Our Legal Bases of Processing</strong>. We process and use Personal Information in accordance with data protection laws. Our legal ground for processing your Personal Information will typically be one or more of the following:  20720f67ae557c1390b189f2f1479213 28576cbe7a490aa8cdc862e413edb7ce 28a29504713bc9c3246a99a921f0263a 3.1.1.    <em>Third-Party Forms and Services</em>: The Site lists, rates and compares various third party businesses, brands and commercial entities (collectively, "Partners") and their respective products and/or services (collectively, "Products"). We may, from time to time, provide you with offers, services and Products of Partners via an online form to be filled by you, upon your intent choice and consent, and such information will be used to facilitate your connection and/or engagement with the relevant Partner (“Lead”). Additionally, if you want to receive more information about a Partner or a Product, or if you otherwise want to pursue a transaction with a Partner, and you click the relevant Partner icon or tab, you may be directed to a landing page or an online form that is owned and operated by the Partner (the "Landing Page"); similarly, you may also be directed to the Landing Page through other websites and online searches. In either case, however, you may be prompted to submit certain information on the Landing Page, including Personal Information such as your name, email address, and phone number (such information collectively, "Landing Page Info"). The Landing Page Info that you submit is collected directly by the Partner who provided it and will include its own privacy policy and data collection practices information. We advise you to refer to such privacy policy to better understand the purpose of the collection and use of the Personal Information you submit.  3.1.2.   <strong> </strong><em>Contact Us</em>. If you send us a ‘Contact Us’ (or similar) request, whether by submitting an online form that we make available, by sending an email to <a href="mailto:hello@bestwebsiteaccessability.com">hello@topwebaccessibilitychecker.com</a>, by using a feedback or reporting feature that is available on the Site, or by other means, you may be required to provide us with certain Personal Information, such as your name and email address. Such information, including the content of your approach, will be used for the purposes of processing and addressing your request and for our legitimate business purposes, as further described in sections 4 and 5 of this Privacy Policy. 3.1.3.    <em>Blogs, Newsletters, Surveys and Promotions</em>. The Site may contain a blog forum, reviews, and articles displaying information about our services, Partners, Products, and the relevant market vertical. The Site may also offer you the opportunity to subscribe to newsletters and participate in surveys and other promotional activities (some of which may be administered by us, and some of which may be administered by third party services with which we engage). In each of the foregoing cases, your interaction (such as responding on the blog forum, subscribing to a newsletter, or participating in a survey) may require you to provide certain Personal Information such as your name and email address. In which case, your submission of information will also be provided directly to such third party and will be used according to this privacy policy as well as such third party’s own privacy policy, and for the purpose for which you provided the information.  3.1.4.    <em>User Profile and User Reviews</em>. We may offer you to give a review to our Partners or to create a user profile in some of our Sites and provide us with additional information about you that will assist us in providing a customized experience for you.   3.1.   <em> Information You Provide to Us</em>. Some of the Services will require you to provide or submit certain Personal Information in order to be able to use them. Some of the features and/or services that require you to provide Personal Information are subject to your specific and active consent (e.g. creating user profile, adding your review to one of the services or products that appear our Sites, newsletters which may become available in certain Sites from time to time, promotional material and forms) and choice, and some will be collected upon your consent to this Privacy Policy, as described below. 3.2.1.    <em>Device Information</em>: For delivering and compatibility purposes, so as for language, geographic location and time zone, we collect device information such as: device type and version, operating system type, language and version, and information that can be extracted out of a user agent, such as browser type and version, screen resolution.  3.2.2.    <em>Interactions and Usage</em>: In order to provide the Services, operate the Site, and keep improving it, so as to be able to sync with our Partners and affiliates, we collect information about your interaction and use of the Services, such as: URLs from which you were directed to the Site and to which you are referred from the Site, internal URL’s of the Site in which you visited and your actions with respect to the Services (such as view or click), date/time stamp of Site use, timing for clicks and page-views. 3.2.3.   <em> Log Files</em>: We collect information through server log files, but also by deploying tracking technologies within the Site, such as pixel tags. We do this to analyze trends, track user’s engagement and interaction with the Site and respective Services. The type of information collected may include internet protocol (IP) addresses that your Internet Service Provider allocated to your device. We collect IP for a limited time and for specific purposes. One, is to better understand your geographic location (country, region and language) and therefore be able to provide the Services in your own language and relevant to your place of origin. Second, we use automated tools to analyze IP addresses and flag those that are fraudulent.  3.2.4.    <em>Cookies and Similar Technologies</em>. The Site may use "cookies" and other anonymous web tracking technologies (such as "web beacons" and "pixel tags") implemented by us or by third-party service providers. A cookie is a small file containing a string of characters that is sent to your computer when you visit a website. When you visit the website again, the cookie allows that website to recognize your browser. Cookies may store your online preferences and other information about the interaction you make in the Site. You can reset your browser to refuse all cookies or to indicate when a cookie is being sent to your browser. However, some features or services, included in the Site, may not function properly without cookies, and some features may become unavailable for you in such a case. A pixel tag is a type of technology placed on a website for the purpose of tracking activity on websites, or when emails are opened or accessed (if applicable), and is often used in combination with cookies.  3.2.    <em>Information We Collect from You</em>. We collect information about your use of the Services and your interaction with the Site or the Services, as follows, and in the following ways: 3.3.    <em>Information we receive from Third Parties</em>. Like many Internet-based services, the Site and the Services are operated and provided in collaboration, sync, and interaction with third parties, including Partners, affiliates and other third parties. In order to enable and facilitate such sync, we may receive information from third parties, such as: an internal user identifier that our system generates to each user (not a unique identifier), and the summary of action taken by you (click, purchase, view, form submission or a phone call). In some Sites, you will be able – upon your choice – to consume the Service on a click-by-call basis. In which case, a third-party vendor/s facilitate the call transmission, tracking, and feedback for success. In such cases, the phone number from which you called may be provided to us (with no other information about you attached), for a limited time and for purposes of resolving referrals and successful calls with our Partners. Except for this purpose, we do not make any other use of such information, unless you will give us your explicit consent. 3.    <strong>Information We Process</strong>. When you visit the Site or use the Services we may collect certain information about your visit and usage of the Site and the Services, in one or more of the following ways: 32980231793c90cebfc12f2da0975050 329a6655ee3a366ab21cd6421a6f06d9 4.    <strong>Consent, Choice, and Modifications</strong>. You are not legally obligated to provide us with Personal Information (and you acknowledge that providing us with Personal Information is done freely). Nor are you required to use the Site in the first place. By using the Site and engage or interact with the Services, you consent to this Privacy Policy and to our information practices described herein. If you do not agree to this Privacy Policy, please do not access or otherwise use the Site or the Services.  4038a0896d8d513c64456d37ff7a45c0 44c20cd33cb091423bee235a30fab890 48f8bfa527c257e6d7ae8ea44ad2f1e9 4c0892778c58c899414a1c44a6332a5e 4c1f626e0b74595b6bd4eef90a27dba9 4f48cd7263d988c80360528f1ca21560 5.    <strong>The Way We Use Personal Information</strong>. In addition to the uses of information we collect (including Personal Information, jointly “your Information”) described elsewhere in this Privacy Policy, we also use Personal Information in the following ways: 51af3fa49a2acb3283bfdeb35c2d1d28 565ccc62715f569c6fdc899c215448c5 568abf111e8a0b3900c8803902ee1112 5fe41d231dcd36b1d41176ee68a9efaf 6.1.     <em>Data Storage and Security Platforms</em> (“Data Platforms); We use third party cloud and storage services to store and retain your Information. It means that we send or otherwise share your Information to/with third party entities that are located anywhere in the world, for the purpose of storing such information on our behalf, or for other processing needs. Such entities include Amazon Web Services (which privacy policy available at: https://aws.amazon.com/privacy These Data Platforms may be located anywhere in the world. We use Data Platforms that maintain strict privacy protection and data security policies, and strict compliance with applicable data protection laws. However, their practices and activities are fully governed by their own privacy policies.   6.2.    <em>Social Media Platforms</em>. We may share not identified information with social media platforms (such as Facebook, LinkedIn etc.) for marketing and operation purposes. Such social media platforms may collect your Personal Information independently and may combine the not identified information provided by us, with your Personal Information which they collect to which you gave your separate and independent consent. Any such action and any personal Information that may derive from it is subject to the social media's own privacy policy and is not, governed, controlled or known by us. . 6.3.    <em>Other Third Parties</em>. We may send or otherwise share your Information to/with various third parties that help us with our business operations (for example, customer service operators and email or phone administration providers), that help us understand how our users use the Site and Services, operate them and to improve them. These third parties may have their own privacy policies that they adhere to, and we require them to make it available whenever their services are provided. We recommend you revisit such privacy policies when those third parties render their services directly to you. Also, these third parties may be based (and their servers may be located) anywhere in the world.  6.4.    <em>Enforcement</em>. We may share your Personal Information with any third party if we believe that disclosure of such information is helpful or reasonably necessary to: (a) comply with any applicable law, regulation, legal process, or governmental request; (b) enforce our Terms of Use, including investigations of potential violations thereof; (c) detect, prevent, or otherwise address fraud or security issues; and/or (d) protect against harm to the rights, property or safety of Top Web Accessibility, our affiliated entities, our users, yourself, and/or the public. 6.5.    <em>Partners and Products</em>. The Services may include submission of online forms, in case you will choose and be interested to receive additional information about one service or another (“Online Form”). In which case, we may send your Online Form information or otherwise share it to/with the Partner about whom (or about whose Product) you wanted to receive more information or with which you have expressed interest in pursuing a transaction (the "Corresponding Partner" and "Corresponding Product", respectively), for such Corresponding Partner to contact you back with the information you have requested. In either case, you acknowledge that: (a) the Partners may contact you using the Online Form information which you submitted; (b) we do not control the way in which Partner may contact you, or the subject matter of such contact; and (c) each Partner is required to provide its own privacy policy, and so it would not necessarily adhere to this Privacy Policy; and (d) Partners may be based (and their servers may be located) anywhere in the world.  6.6.   <em>Merger, Sale or Bankruptcy</em>. In the event that Top Web Accessibility or one of our affiliated entities is acquired by or merged with, a third party entity, or otherwise sells all or part of our/its assets, we may (and hereby reserve the right to) transfer or assign the Personal Information and other information we have collected or received. In such a case, we will require the acquiring entity to post its data practices and provide you with any of your rights as per the jurisdiction of your residency. 6.7.   <em> Third parties we engage or host or provide us with services and features we can offer to you collect data from the Site and visitors’ engagement with the content and features on our behalf</em>, or for the provisioning of their services. This happens in cases where we use or deploy third party technology, service or feature, or where we otherwise give these third parties access to our Site or technology for specific purposes, as described in the Privacy Policy or in the Site. We may use analytics tools, including Google Analytics and Tableau. These tools help us understand users’ behavior on our Site and Services, including by tracking page content, and click/touch, movements, scrolls, and keystroke activities. The privacy practices of these tools are subject to their own policies and they may use their own cookies to provide their services. For further information about cookies, please see our Cookie Policy. Click [here] for further information about Google cookies. Click [here] to opt-out of Google Analytics. 6.    <strong>Sharing Your Information with Third Parties</strong>.  604093119f19f01463f23996a407e4ba 6836e1bed503c422ff3ee3d457e5520a 6c38007673771001527056f35d5944e0 7.   <strong> Links to and Interaction with Third-Party Properties</strong>. The Site may link to third party websites (such as those of Partners), content, and other online properties (collectively, "Third Party Properties"). Data associated with your clicking through to such Third Party Properties, as well as any Personal Information you provide on such Third-Party Properties, is provided to the third party (although Top Web Accessibility and its affiliated entities may have arrangements with such third parties pursuant to which such data and Personal Information is sent or shared back to/with us). We are not responsible for the privacy practices of such third parties, or for such Third-Party Properties, Products, and respective data collection practices. We encourage you to read the terms and conditions and privacy policy of each Third-Party Property that you choose to use or interact with 7557d5b2b147325cfc2df936c59f08a0 75be32b21b94d39328cdbcea13be2695 7633864c5cc541fc4848a0546861f7fb 764584379f832708a40aaa0e5c9e7df9 78021cd0386937d20adf786c2bdf8a37 7b863e3c3dd35ad562e58d4f5ef78638 8.1.    <em>Cookies and Tracking Technologies</em>. you may set your browser to refuse and/or block any cookies from the Site or third party services (for more details please refer to Section ‎2.1.1 hereof).  8.2.    When applicable, <em>review and update or request to remove your Personal Information from our servers</em> (for more details please refer to Section ‎2). 8.    <strong>Choices and Opt-Out</strong>. It is your choice to provide us with your information for the uses described in this Privacy Policy. You can decide at any time to exercise your rights with respect to your Personal Information and the jurisdiction of your residency. For example (where applicable and relevant): 89ef7e61dca116dc61e68138b1634f29 8ad5a161de9160a6ad3414b9b849dd43 8d75f241f86b39be6382f02168c9b2a3 9.1.    <em>EU Data Subject Rights</em>. Any Personal Information we collect according to this Privacy Policy is handled and treated in compliance with the GDPR.  9.    <strong>Your Rights</strong> 91e676ddea9170a106550748ee524855 92cb68d916aecd7cc8ce16b54053053d 962d6751df5e045422f823370b1282e3 9c889f19e031c96be9dbe308488d83b0 9eee07121ef84f525d52e91e2b624562 <strong>Privacy Policy<br/>Last Updated: March, 2021</strong><br/> As an EU based user, you may: Children’s Privacy Choices and Opt-Out Commitment Consent, Choice, and Modifications Contact Us Details Privacy Policy For the purposes of this Privacy Policy, "Personal Information" means information that may be used, either alone or in combination with other information, to personally identify an individual, such as a first and last name, an email address, phone number, a home or other physical address, and other contact information. In some jurisdictions and given the specific use we make with the information, IP address that your Internet service provider allocates for your device may also be considered as Personal Information. In which case, we will be treating the information as personal in all handling and safeguarding aspects, however, we do not make any use of such information for user identification purposes. How we use your personal information  Information Security Information We Collect and How We Use It Links to and Interaction with Third Party Properties Non-US Users Our Legal Bases of Processing Please refer to our <a href="https://topwebaccessibilitychecker.com/cookie-policy/">Cookie Policy</a> for more information about the type of cookies used and the manner in which we use cookies/tracking technologies on our Site.  Purposes for the collection of information Sharing Your Information with Third Parties Some of the Services will require you to submit specific Personal Information. In such a case you can choose whether you want to use such Services or not. Be aware that even if you provided us with your Personal Information, you have the full right to withdraw such consent at any time (please refer to section ‎8 below to get further details about opting out and withdrawing consent).  Terms of Use The information is used to operate our websites, provide you with the services, improve them over time, monitor analytics and statistics and enable us to sync and resolve payment issued with our business partners (including clients, vendors and affiliates). This Privacy Policy includes:<br/> This privacy policy (“Privacy Policy”) aims to give you information on how [Top Web Accessibility] collects and processes your personal data through your use of this website, including any data you may provide through this website in connection with your use or engagement with any feature, content, offer, element or service we make available to you via the Site (the “Services”). The section and sub-section headings used below are for convenience of reading only (and are not to be used for interpretive purposes), and we hope they ease navigating this Privacy Policy. <br/><br/>We keep our privacy policy under regular review and reserve the right, in our sole and absolute discretion, to change this Privacy Policy at any time. In which case, we will post the modified Privacy Policy on the Site, and the “Last Updated” statement at the bottom shall reflect the effective date of the modified version. Any access or use you make of the Site following such date constitutes your consent and acceptance of the modified Privacy Policy. If we make significant changes, we will make sure to provide more prominent notice to you, such as posting a notification on the Site, seven (7) days before the changes become effective. In which case, the changes will be effective seven (7) days after such notification was published. To generally provide you with the Site, Services and constantly improve them. We will use your Information to:  Top Web Accessibility respects your privacy and is committed to protecting your personal data. This privacy policy will inform you as to how we look after your personal data when you visit our website (regardless of where you visit it from) and tell you about your privacy rights and how the law protects you.<br/><br/>Our privacy policy is designed to inform you about the information we collect, how we use it, and your options about certain uses of information. Key elements of our full privacy policy are summarized below for your convenience.<br/><br/>Our privacy policy applies to website visitors. This summary is designed to provide you with the highlights in an easy to understand language. However, the full version of our privacy policy is the legally binding one.<br/><br/> We do not share Personal Information about you with third parties, except for the following circumstances: We may also share some non-Personal Information with the following type of third parties and under the following circumstances: We will not share your information unless we are required to by law, or as required to provide you with our websites and services. We truly encourage you to read, understand and revisit our full privacy policy from time to time.<br/><br/> When you visit our websites, we collect certain information about your visit, interaction with content, the site from which you arrived and to which you are heading, and some additional technical information about the device and system from which you are consuming the services. Upon your choice to use a “click to call” service we may offer, the phone number from which you made the call may be collected for a limited time of up to 3 months. Your Rights a30240f9b9532a539831f840e0ccbdd4 a7b7a8e86eaa8b117004d82ab475b1fb b0fd9776ed11be6091d6bb3e1307e283 b1769daa6ac86e6abab37aac50225bc3 b82aea2e12f3f9ffab6d576eb8823d80 ba8302cbd44d6d0d0bbcb807c9531cba bbea22e4f4e2771a9ba81624213ce7c4 bc3439ecd2ea5478c98b1d38e6bc184e c15e648d9e6a62703d6f20e042fc45da c27ba3dc7d3539d2736917f3ea4ec091 c6757f555436140fdada54d0cc066741 c6a3c535c2e29d2eca2e7d8651d9fd84 c6ba0054a3463aab4e115b86e8018a1a cfbb7117582ea6c0dce962413d39bd82 d27cb20bea44d5aa6358fe27955d97ef d52b90d0beaeb06d9612705c14910b67 d6fe5932190e0a5c194f0783e1a9d00a d7f170b23d36879bd8f351142c4c3914 d8b301dddecca0936cf68d72d61ccd94 da43f2fa19f3cc2e5a0850dc5f580e78 def51847cd8158e9e2a52a7cede169fb e04bf3d9cf249f9915cc6a6cf532290e e2490641bced17bdacb1a903a1a48884 e27be844050276659addcd2bc9c3b3bb e4e89371a04c165361707cd084ac9d00 e4ed27894d17e5755e2c0781e108f35a e574ad56ba437620fb6da2e59a3b3dbc e6e9cab600b763d174e40e8e32ac1633 e8cb10b0aca021666ff853b70af7ea5e eb02d6b1a2323c673906646917c3d141 ee2f2fdfd1f867253763e9ea2059f603 f6127a6b02e71f5f53bc5a23c2206383 f62b894762c2d4d53d6934d058d7554e f7aa6038d159206c2b1a59e8b0ff5b2d •    Access the Personal Information that we keep about you. We will need to ask you to provide us certain credentials to make sure that you are who you claim you are. If you find that the Personal Information is not accurate, complete or updated, then please provide us the necessary information to correct it. •    Contact us if you want to withdraw your consent to the processing of your Personal Information. Exercising this right will not affect the lawfulness of processing based on consent before its withdrawal. •    Have a right to lodge a complaint with a data protection supervisory authority. •    Object to the processing of your Personal Information for direct marketing purposes, in case your Personal Information used for such purpose. •    Request to delete or restrict access to your Personal Information. If you exercise one (or more) of the above-mentioned rights, in accordance with the provisions under the law, you may request to be informed that third parties that hold your Personal Information, in accordance with this Privacy Policy, will act accordingly. •    Sending an email to <a href="mailto:hello@bestwebsiteaccessability.com">hello@topwebaccessibilitychecker.com</a>  <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(a) amministrare e mettere a tua disposizione il Sito e i Servizi; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(b) sviluppare ulteriormente e in generale migliorare il Sito ei Servizi; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(c) scopi di conteggio e monitoraggio di segnalazioni e annunci; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(d) convalida degli utenti come monitoraggio umano e antifrode </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(e) rispondere alle tue richieste e ad altre comunicazioni con te; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(f) identificare o autenticare l'accesso e l'utilizzo del Sito.</span></p> Non condividiamo informazioni personali su di te con terze parti, ad eccezione delle seguenti circostanze:
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.3. Blog, Newsletter, Sondaggi e Promozioni. Il Sito può contenere un forum di blog, recensioni e articoli che mostrano informazioni sui nostri servizi, Partner, Prodotti e il relativo mercato verticale. Il Sito può anche offrirti l'opportunità di iscriverti a newsletter e partecipare a sondaggi e altre attività promozionali (alcune delle quali possono essere amministrate da noi, e altre possono essere amministrate da servizi di terze parti con cui collaboriamo). In ciascuno dei casi precedenti, la tua interazione (come rispondere sul forum del blog, iscriverti a una newsletter o partecipare a un sondaggio) potrebbe richiederti di fornire determinate informazioni personali come il tuo nome e indirizzo e-mail. In tal caso, l'invio delle informazioni verrà fornito anche direttamente a tale terza parte e verrà utilizzato in base alla presente informativa sulla privacy, nonché alla propria informativa sulla privacy di tale terza parte, e per gli scopi per cui ha fornito le informazioni.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.1. Informazioni sul dispositivo: per scopi di consegna e compatibilità, così come per lingua, posizione geografica e fuso orario, raccogliamo informazioni sul dispositivo come: tipo e versione del dispositivo, tipo di sistema operativo, lingua e versione e informazioni che possono essere estratte da un utente agente, come il tipo e la versione del browser, la risoluzione dello schermo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.1. Piattaforme di archiviazione e sicurezza dei dati ("Piattaforme dati); Utilizziamo servizi cloud e di archiviazione di terze parti per archiviare e conservare le tue informazioni. Significa che inviamo o condividiamo in altro modo le tue informazioni a/con entità di terze parti che si trovano in qualsiasi parte del mondo, allo scopo di archiviare tali informazioni per nostro conto o per altre esigenze di elaborazione. Tali entità includono Amazon Web Services (la cui informativa sulla privacy è disponibile su: https://aws.amazon.com/privacy Queste piattaforme dati possono essere ubicate in qualsiasi parte del mondo. Utilizziamo piattaforme dati che mantengono rigide politiche di protezione della privacy e sicurezza dei dati e rigoroso rispetto delle leggi sulla protezione dei dati applicabili, tuttavia, le loro pratiche e attività sono completamente disciplinate dalle proprie politiche sulla privacy.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3. Informazioni che elaboriamo. Quando visiti il ​​Sito o utilizzi i Servizi, potremmo raccogliere determinate informazioni sulla tua visita e sull'utilizzo del Sito e dei Servizi, in uno o più dei seguenti modi:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">13.2. Cerchiamo di rendere questa Informativa sulla privacy semplice e di facile comprensione. Tuttavia, se hai domande, puoi contattarci all'indirizzo: hello@topwebaccessibilitychecker.com è per richieste di protezione dei dati in conformità con i tuoi diritti legali sopra elencati. Qualsiasi altra richiesta inviata a questa casella di posta elettronica verrà ignorata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Scelte e rinuncia <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">1. Termini di utilizzo. La presente Informativa sulla privacy costituisce parte integrante delle Condizioni d'uso del Sito (le "Condizioni"). Qualsiasi termine in maiuscolo nella presente Informativa sulla privacy che non definiamo, avrà il significato ad esso attribuito nei Termini.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">10. <strong>Sicurezza delle informazioni</strong>. Abbiamo messo in atto adeguate misure di sicurezza per impedire che i tuoi dati personali vengano accidentalmente persi, utilizzati o accessibili in modo non autorizzato, alterati o divulgati. Ad esempio, il Sito è crittografato utilizzando SSL e così come qualsiasi altra informazione al trasporto, esaminiamo periodicamente le nostre pratiche di raccolta di informazioni e dati per garantire un monitoraggio costante, utilizziamo servizi di elaborazione e servizio di terze parti che implementano vari standard e certificazioni di sicurezza delle informazioni (a riposo e trasmissione) e monitoriamo e filtriamo l'accesso ai dati quando si tratta di personale nella nostra organizzazione. Tali sistemi e procedure riducono il rischio di violazioni della sicurezza, ma non forniscono una sicurezza assoluta, poiché non esiste nulla di simile quando si tratta della rete mondiale. Pertanto, non possiamo garantire che il Sito e i nostri server siano immuni da accessi non autorizzati alle informazioni ivi archiviate e ad altri rischi per la sicurezza delle informazioni.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">11. <strong>Privacy dei bambini</strong>. Il Sito o i Servizi non sono progettati per attirare bambini di età inferiore ai 16 anni. Di conseguenza, non intendiamo raccogliere informazioni personali da chiunque sappiamo essere minore di 16 anni. Se apprendiamo di aver raccolto informazioni personali da un bambino di età inferiore a 16 anni , faremo sforzi commercialmente ragionevoli per eliminare tali informazioni il più rapidamente possibile. Se ritieni che potremmo avere tali informazioni, ti preghiamo di contattarci all'indirizzo: hello@topwebaccessibilitychecker.com.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">12. Impegno. La protezione della tua privacy online è una pratica in evoluzione e utilizziamo i nostri migliori sforzi per evolvere il nostro sito in modo da soddisfare questi requisiti e le migliori pratiche in base a tutte le giurisdizioni in cui il sito è disponibile.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">13.1. <em>Regolamento generale sulla protezione dei dati</em> (GDPR) - Rappresentante europeo. Ai sensi dell'articolo 27 del Regolamento generale sulla protezione dei dati (GDPR), Top Web Accessibility. ha nominato l'Ufficio europeo per la protezione dei dati (EDPO) come suo rappresentante GDPR nell'UE. È possibile contattare EDPO in merito a questioni relative al GDPR tramite:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">13.2. Cerchiamo di rendere questa Informativa sulla privacy semplice e di facile comprensione. Tuttavia, se hai domande, puoi contattarci all'indirizzo: hello@topwebaccessibilitychecker.com è per richieste di protezione dei dati in conformità con i tuoi diritti legali sopra elencati. Qualsiasi altra richiesta inviata a questa casella di posta elettronica verrà ignorata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> 13.    <strong>Contattaci</strong> Alcuni dei Servizi richiederanno l'invio di Dati Personali specifici. In tal caso è possibile scegliere se si desidera utilizzare o meno tali Servizi. Tieni presente che anche se ci hai fornito le tue informazioni personali, hai il pieno diritto di revocare tale consenso in qualsiasi momento (fare riferimento alla sezione 8 di seguito per ottenere ulteriori dettagli sulla disattivazione e la revoca del consenso).
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.4. Consenso. In certi casi. Chiederemo il tuo consenso per elaborare e utilizzare le tue informazioni personali al fine di fornire servizi aggiuntivi (ad esempio, quando ti iscrivi alle nostre newsletter, crei il tuo profilo utente, ci fornisci informazioni personali per ottenere offerte personalizzate o aggiungi una recensione su un servizio o prodotto). Quando ci affidiamo al consenso come base legale del trattamento, puoi chiedere di revocare il tuo consenso inviandoci una richiesta a [INDIRIZZO EMAIL]. Una volta verificata la tua richiesta, ti contatteremo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Non condivideremo le tue informazioni a meno che non ci venga richiesto dalla legge o come richiesto per fornirti i nostri siti Web e servizi. Ti invitiamo sinceramente a leggere, comprendere e rivedere la nostra politica sulla privacy completa di volta in volta.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Collegamenti e interazione con proprietà di terze parti <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.1. Esecuzione di un contratto. Accedendo o utilizzando il Sito, ci chiedi di fornirti i Servizi in conformità con le nostre Condizioni d'uso. Per fare ciò, abbiamo bisogno di elaborare alcune delle tue informazioni personali (come il tuo indirizzo IP).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.2. Interessi legittimi. Potremmo utilizzare alcune delle tue informazioni personali laddove abbiamo motivi commerciali legittimi per farlo. Utilizziamo queste Informazioni personali per gestire, migliorare e promuovere i Servizi (ad esempio, quando personalizziamo la tua esperienza dei Servizi o per scopi di analisi) e per proteggere la nostra attività (ad esempio, per prevenire frodi o garantire la sicurezza del Sito).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.3. Obblighi giuridici. In alcuni casi, il nostro utilizzo delle tue informazioni personali sarà necessario per ottemperare a obblighi normativi o requisiti legali (ad esempio, affrontare reclami da parte di utenti o autorità);</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.4. Consenso. In certi casi. Chiederemo il tuo consenso per elaborare e utilizzare le tue informazioni personali al fine di fornire servizi aggiuntivi (ad esempio, quando ti iscrivi alle nostre newsletter, crei il tuo profilo utente, ci fornisci informazioni personali per ottenere offerte personalizzate o aggiungi una recensione su un servizio o prodotto). Quando ci affidiamo al consenso come base legale del trattamento, puoi chiedere di revocare il tuo consenso inviandoci una richiesta a [INDIRIZZO EMAIL]. Una volta verificata la tua richiesta, ti contatteremo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><strong><span class="Y2IQFc" lang="it">2. Le nostre basi legali del trattamento.</span></strong></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it"> Elaboriamo e utilizziamo le informazioni personali in conformità con le leggi sulla protezione dei dati. La nostra base legale per il trattamento delle tue informazioni personali sarà in genere una o più delle seguenti:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.3. Informazioni che riceviamo da terze parti. Come molti servizi basati su Internet, il Sito e i Servizi sono gestiti e forniti in collaborazione, sincronizzazione e interazione con terze parti, inclusi Partner, affiliati e altre terze parti. Al fine di abilitare e facilitare tale sincronizzazione, potremmo ricevere informazioni da terze parti, quali: un identificatore utente interno che il nostro sistema genera per ciascun utente (non un identificatore univoco) e il riepilogo delle azioni intraprese dall'utente (clic, acquisto , visualizzare, inviare moduli o telefonare). In alcuni Siti potrai, a tua scelta, usufruire del Servizio click-by-call. In tal caso, un fornitore di terze parti facilita la trasmissione della chiamata, il monitoraggio e il feedback per il successo. In tali casi, il numero di telefono da cui hai chiamato potrebbe essere fornito a noi (senza altre informazioni su di te allegate), per un periodo di tempo limitato e allo scopo di risolvere i rinvii e le chiamate di successo con i nostri Partner. Ad eccezione di questo scopo, non facciamo altro uso di tali informazioni, a meno che tu non ci fornisca il tuo esplicito consenso.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.6. Fusione, vendita o fallimento. Nel caso in cui Top Web Accessibility o una delle nostre entità affiliate venga acquisita o fusa con un'entità terza, o venda in altro modo tutto o parte dei nostri/suoi beni, possiamo (e con la presente riserviamo il diritto di) trasferire o assegnare le Informazioni personali e altre informazioni che abbiamo raccolto o ricevuto. In tal caso, richiederemo all'entità acquirente di pubblicare le sue pratiche sui dati e di fornirti tutti i tuoi diritti secondo la giurisdizione della tua residenza.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.3. Altre terze parti. Potremmo inviare o condividere in altro modo le tue informazioni a/con varie terze parti che ci aiutano nelle nostre operazioni commerciali (ad esempio, operatori del servizio clienti e fornitori di servizi di posta elettronica o telefonia), che ci aiutano a capire come i nostri utenti utilizzano il sito e i servizi, operano loro e per migliorarli. Queste terze parti potrebbero disporre di proprie informative sulla privacy a cui aderiscono e che richiediamo loro di renderle disponibili ogni volta che i loro servizi vengono forniti. Ti consigliamo di rivedere tali politiche sulla privacy quando tali terze parti ti forniscono i loro servizi direttamente. Inoltre, queste terze parti possono avere sede (e i loro server possono trovarsi) in qualsiasi parte del mondo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.1. Moduli e servizi di terze parti: il Sito elenca, valuta e confronta varie aziende, marchi ed entità commerciali di terze parti (collettivamente, "Partner") e i rispettivi prodotti e/o servizi (collettivamente, "Prodotti"). Potremmo, di volta in volta, fornirti offerte, servizi e Prodotti di Partner tramite un modulo online che deve essere compilato da te, previa tua scelta e consenso intenzionale, e tali informazioni verranno utilizzate per facilitare la tua connessione e/o coinvolgimento con il relativo Partner (“Lead”). Inoltre, se desideri ricevere maggiori informazioni su un Partner o un Prodotto, o se desideri altrimenti portare a termine una transazione con un Partner e fai clic sull'icona o sulla scheda del Partner pertinente, potresti essere indirizzato a una pagina di destinazione o a un modulo che è di proprietà e gestito dal Partner (la "Pagina di destinazione"); allo stesso modo, potresti essere indirizzato alla pagina di destinazione anche tramite altri siti Web e ricerche online. In entrambi i casi, tuttavia, potrebbe esserti richiesto di inviare determinate informazioni sulla pagina di destinazione, comprese le informazioni personali come il tuo nome, indirizzo e-mail e numero di telefono (tali informazioni collettivamente, "Informazioni sulla pagina di destinazione"). Le informazioni sulla pagina di destinazione che invii vengono raccolte direttamente dal Partner che le ha fornite e includeranno la propria politica sulla privacy e le informazioni sulle pratiche di raccolta dei dati. Ti consigliamo di fare riferimento a tale politica sulla privacy per comprendere meglio lo scopo della raccolta e dell'uso delle informazioni personali che invii.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.2. Contattaci. Se ci invii una richiesta "Contattaci" (o simile), inviando un modulo online che rendiamo disponibile, inviando un'e-mail a hello@topwebaccessibilitychecker.com, utilizzando una funzione di feedback o segnalazione disponibile sul sito , o con altri mezzi, potrebbe esserti richiesto di fornirci determinate Informazioni personali, come il tuo nome e indirizzo email. Tali informazioni, compreso il contenuto del tuo approccio, verranno utilizzate ai fini dell'elaborazione e dell'inoltro della tua richiesta e per i nostri legittimi scopi commerciali, come ulteriormente descritto nelle sezioni 4 e 5 della presente Informativa sulla privacy.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.3. Blog, Newsletter, Sondaggi e Promozioni. Il Sito può contenere un forum di blog, recensioni e articoli che mostrano informazioni sui nostri servizi, Partner, Prodotti e il relativo mercato verticale. Il Sito può anche offrirti l'opportunità di iscriverti a newsletter e partecipare a sondaggi e altre attività promozionali (alcune delle quali possono essere amministrate da noi, e altre possono essere amministrate da servizi di terze parti con cui collaboriamo). In ciascuno dei casi precedenti, la tua interazione (come rispondere sul forum del blog, iscriverti a una newsletter o partecipare a un sondaggio) potrebbe richiederti di fornire determinate informazioni personali come il tuo nome e indirizzo e-mail. In tal caso, l'invio delle informazioni verrà fornito anche direttamente a tale terza parte e verrà utilizzato in base alla presente informativa sulla privacy, nonché alla propria informativa sulla privacy di tale terza parte, e per gli scopi per cui ha fornito le informazioni.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.4. Profilo utente e recensioni degli utenti. Potremmo offrirti di fornire una recensione ai nostri Partner o di creare un profilo utente in alcuni dei nostri Siti e fornirci ulteriori informazioni su di te che ci aiuteranno a fornirti un'esperienza personalizzata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1. Informazioni che ci fornisci. Alcuni dei Servizi richiederanno all'utente di fornire o inviare determinate Informazioni personali per poterli utilizzare. Alcune delle funzionalità e/o dei servizi che richiedono la fornitura di Dati Personali sono soggetti al tuo specifico e attivo consenso (es. creazione del profilo utente, aggiunta della tua recensione a uno dei servizi o prodotti che compaiono sui nostri Siti, newsletter che potrebbero essere disponibili in determinati Siti di volta in volta, materiale e moduli promozionali) e scelta, e alcuni saranno raccolti previo tuo consenso alla presente Informativa sulla privacy, come descritto di seguito.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.1. Informazioni sul dispositivo: per scopi di consegna e compatibilità, così come per lingua, posizione geografica e fuso orario, raccogliamo informazioni sul dispositivo come: tipo e versione del dispositivo, tipo di sistema operativo, lingua e versione e informazioni che possono essere estratte da un utente agente, come il tipo e la versione del browser, la risoluzione dello schermo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.2. Interazioni e utilizzo: Al fine di fornire i Servizi, gestire il Sito e continuare a migliorarlo, in modo da essere in grado di sincronizzarsi con i nostri Partner e affiliati, raccogliamo informazioni sulla tua interazione e utilizzo dei Servizi, come: URL da a cui sei stato indirizzato al Sito e a cui sei stato indirizzato dal Sito, URL interni del Sito in cui hai visitato e le tue azioni in relazione ai Servizi (come visualizzazione o clic), data/ora di utilizzo del Sito, tempi per clic e visualizzazioni di pagina.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.3. File di registro: raccogliamo informazioni tramite file di registro del server, ma anche implementando tecnologie di tracciamento all'interno del Sito, come i pixel tag. Lo facciamo per analizzare le tendenze, tenere traccia del coinvolgimento e dell'interazione dell'utente con il Sito e i rispettivi Servizi. Il tipo di informazioni raccolte può includere indirizzi di protocollo Internet (IP) che il tuo provider di servizi Internet ha assegnato al tuo dispositivo. Raccogliamo IP per un periodo di tempo limitato e per scopi specifici. Uno, è comprendere meglio la tua posizione geografica (paese, regione e lingua) e quindi essere in grado di fornire i Servizi nella tua lingua e pertinenti al tuo luogo di origine. In secondo luogo, utilizziamo strumenti automatizzati per analizzare gli indirizzi IP e segnalare quelli fraudolenti.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.4. Cookie e tecnologie simili. Il Sito può utilizzare "cookie" e altre tecnologie di tracciamento web anonime (come "web beacon" e "pixel tag") implementate da noi o da fornitori di servizi di terze parti. Un cookie è un piccolo file contenente una stringa di caratteri che viene inviato al tuo computer quando visiti un sito web. Quando visiti nuovamente il sito Web, il cookie consente a quel sito Web di riconoscere il tuo browser. I cookie possono memorizzare le tue preferenze online e altre informazioni sull'interazione che fai nel sito. Puoi reimpostare il tuo browser per rifiutare tutti i cookie o per indicare quando un cookie viene inviato al tuo browser. Tuttavia, alcune funzionalità o servizi, inclusi nel Sito, potrebbero non funzionare correttamente senza i cookie e alcune funzionalità potrebbero non essere disponibili in tal caso. Un pixel tag è un tipo di tecnologia posizionata su un sito Web allo scopo di tracciare l'attività sui siti Web o quando si aprono o si accede alle e-mail (se applicabile) e viene spesso utilizzato in combinazione con i cookie.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2. Informazioni che raccogliamo da te. Raccogliamo informazioni sul tuo utilizzo dei Servizi e sulla tua interazione con il Sito o i Servizi, come segue e nei seguenti modi:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.3. Informazioni che riceviamo da terze parti. Come molti servizi basati su Internet, il Sito e i Servizi sono gestiti e forniti in collaborazione, sincronizzazione e interazione con terze parti, inclusi Partner, affiliati e altre terze parti. Al fine di abilitare e facilitare tale sincronizzazione, potremmo ricevere informazioni da terze parti, quali: un identificatore utente interno che il nostro sistema genera per ciascun utente (non un identificatore univoco) e il riepilogo delle azioni intraprese dall'utente (clic, acquisto , visualizzare, inviare moduli o telefonare). In alcuni Siti potrai, a tua scelta, usufruire del Servizio click-by-call. In tal caso, un fornitore di terze parti facilita la trasmissione della chiamata, il monitoraggio e il feedback per il successo. In tali casi, il numero di telefono da cui hai chiamato potrebbe essere fornito a noi (senza altre informazioni su di te allegate), per un periodo di tempo limitato e allo scopo di risolvere i rinvii e le chiamate di successo con i nostri Partner. Ad eccezione di questo scopo, non facciamo altro uso di tali informazioni, a meno che tu non ci fornisca il tuo esplicito consenso.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3. Informazioni che elaboriamo. Quando visiti il ​​Sito o utilizzi i Servizi, potremmo raccogliere determinate informazioni sulla tua visita e sull'utilizzo del Sito e dei Servizi, in uno o più dei seguenti modi:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">1. Termini di utilizzo. La presente Informativa sulla privacy costituisce parte integrante delle Condizioni d'uso del Sito (le "Condizioni"). Qualsiasi termine in maiuscolo nella presente Informativa sulla privacy che non definiamo, avrà il significato ad esso attribuito nei Termini.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Ai fini della presente Informativa sulla privacy, per "Informazioni personali" si intendono le informazioni che possono essere utilizzate, da sole o in combinazione con altre informazioni, per identificare personalmente un individuo, quali nome e cognome, indirizzo e-mail, numero di telefono, domicilio o altro indirizzo fisico e altre informazioni di contatto. In alcune giurisdizioni e dato l'uso specifico che facciamo con le informazioni, anche l'indirizzo IP che il tuo provider di servizi Internet assegna al tuo dispositivo può essere considerato come un'informazione personale. In tal caso, tratteremo le informazioni come personali in tutti gli aspetti di gestione e salvaguardia, tuttavia, non utilizziamo tali informazioni per scopi di identificazione dell'utente.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">4. Consenso, scelta e modifiche. Non sei legalmente obbligato a fornirci Informazioni Personali (e riconosci che fornirci Informazioni Personali è fatto liberamente). Né sei obbligato a utilizzare il Sito in primo luogo. Utilizzando il Sito e impegnandosi o interagendo con i Servizi, acconsenti alla presente Informativa sulla privacy e alle nostre pratiche informative qui descritte. Se non accetti la presente Informativa sulla privacy, ti preghiamo di non accedere o utilizzare in altro modo il Sito o i Servizi.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Consenso, scelta e modifiche <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">8.2. Se applicabile, rivedi e aggiorna o richiedi di rimuovere le tue informazioni personali dai nostri server (per maggiori dettagli fai riferimento alla Sezione ‎2).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> •    Avere il diritto di presentare un reclamo a un'autorità di controllo della protezione dei dati.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">9.1. <em>Diritti dell'interessato dell'UE</em>. Qualsiasi informazione personale che raccogliamo in base alla presente Informativa sulla privacy viene gestita e trattata in conformità con il GDPR.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> •    Contattaci se desideri revocare il tuo consenso al trattamento delle tue informazioni personali. L'esercizio di tale diritto non pregiudicherà la liceità del trattamento basata sul consenso prima della revoca.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.2. Contattaci. Se ci invii una richiesta "Contattaci" (o simile), inviando un modulo online che rendiamo disponibile, inviando un'e-mail a hello@topwebaccessibilitychecker.com, utilizzando una funzione di feedback o segnalazione disponibile sul sito , o con altri mezzi, potrebbe esserti richiesto di fornirci determinate Informazioni personali, come il tuo nome e indirizzo email. Tali informazioni, compreso il contenuto del tuo approccio, verranno utilizzate ai fini dell'elaborazione e dell'inoltro della tua richiesta e per i nostri legittimi scopi commerciali, come ulteriormente descritto nelle sezioni 4 e 5 della presente Informativa sulla privacy.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">5. Il modo in cui utilizziamo le informazioni personali. Oltre agli usi delle informazioni che raccogliamo (comprese le informazioni personali, congiuntamente "le tue informazioni") descritte altrove nella presente Informativa sulla privacy, utilizziamo anche le informazioni personali nei seguenti modi:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> •    Inviando una email a <a href="mailto:hello@bestwebsiteaccessability.com">hello@topwebaccessibilitychecker.com</a> Informazioni di sicurezza Contattaci <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.1. Esecuzione di un contratto. Accedendo o utilizzando il Sito, ci chiedi di fornirti i Servizi in conformità con le nostre Condizioni d'uso. Per fare ciò, abbiamo bisogno di elaborare alcune delle tue informazioni personali (come il tuo indirizzo IP).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.1. Piattaforme di archiviazione e sicurezza dei dati ("Piattaforme dati); Utilizziamo servizi cloud e di archiviazione di terze parti per archiviare e conservare le tue informazioni. Significa che inviamo o condividiamo in altro modo le tue informazioni a/con entità di terze parti che si trovano in qualsiasi parte del mondo, allo scopo di archiviare tali informazioni per nostro conto o per altre esigenze di elaborazione. Tali entità includono Amazon Web Services (la cui informativa sulla privacy è disponibile su: https://aws.amazon.com/privacy Queste piattaforme dati possono essere ubicate in qualsiasi parte del mondo. Utilizziamo piattaforme dati che mantengono rigide politiche di protezione della privacy e sicurezza dei dati e rigoroso rispetto delle leggi sulla protezione dei dati applicabili, tuttavia, le loro pratiche e attività sono completamente disciplinate dalle proprie politiche sulla privacy.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.2. Piattaforme di social media. Potremmo condividere informazioni non identificate con piattaforme di social media (come Facebook, LinkedIn ecc.) per scopi di marketing e operativi. Tali piattaforme di social media possono raccogliere le tue informazioni personali in modo indipendente e possono combinare le informazioni non identificate fornite da noi, con le tue informazioni personali che raccolgono a cui hai dato il tuo consenso separato e indipendente. Qualsiasi azione di questo tipo e qualsiasi informazione personale che possa derivare da essa è soggetta alla politica sulla privacy dei social media e non è, governata, controllata o conosciuta da noi. .</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.3. Altre terze parti. Potremmo inviare o condividere in altro modo le tue informazioni a/con varie terze parti che ci aiutano nelle nostre operazioni commerciali (ad esempio, operatori del servizio clienti e fornitori di servizi di posta elettronica o telefonia), che ci aiutano a capire come i nostri utenti utilizzano il sito e i servizi, operano loro e per migliorarli. Queste terze parti potrebbero disporre di proprie informative sulla privacy a cui aderiscono e che richiediamo loro di renderle disponibili ogni volta che i loro servizi vengono forniti. Ti consigliamo di rivedere tali politiche sulla privacy quando tali terze parti ti forniscono i loro servizi direttamente. Inoltre, queste terze parti possono avere sede (e i loro server possono trovarsi) in qualsiasi parte del mondo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.4. Rinforzo. Potremmo condividere le tue informazioni personali con terze parti se riteniamo che la divulgazione di tali informazioni sia utile o ragionevolmente necessaria per: (a) rispettare qualsiasi legge, regolamento, procedura legale o richiesta governativa applicabile; (b) applicare le nostre Condizioni d'uso, comprese le indagini su potenziali violazioni delle stesse; (c) rilevare, prevenire o altrimenti affrontare frodi o problemi di sicurezza; e/o (d) proteggere da danni ai diritti, alla proprietà o alla sicurezza di Top Web Accessibility, delle nostre entità affiliate, dei nostri utenti, di te stesso e/o del pubblico.</span></p> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.5. Partner e prodotti. I Servizi possono includere l'invio di moduli online, nel caso in cui l'utente scelga e sia interessato a ricevere informazioni aggiuntive su un servizio o un altro ("Modulo online"). In tal caso, potremmo inviare le informazioni del modulo online o condividerle in altro modo con/con il Partner sul quale (o sul cui Prodotto) desideri ricevere maggiori informazioni o con il quale hai espresso interesse a perseguire una transazione (il "Partner corrispondente " e "Prodotto corrispondente", rispettivamente), affinché tale Partner corrispondente possa ricontattarti con le informazioni che hai richiesto. In entrambi i casi, riconosci che: (a) i Partner possono contattarti utilizzando le informazioni del modulo online che hai inviato; (b) non controlliamo il modo in cui il Partner può contattarti o l'oggetto di tale contatto; e (c) ogni Partner è tenuto a fornire la propria informativa sulla privacy, e quindi non aderirà necessariamente alla presente Informativa sulla privacy; e (d) i partner possono avere sede (ei loro server possono essere ubicati) in qualsiasi parte del mondo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.6. Fusione, vendita o fallimento. Nel caso in cui Top Web Accessibility o una delle nostre entità affiliate venga acquisita o fusa con un'entità terza, o venda in altro modo tutto o parte dei nostri/suoi beni, possiamo (e con la presente riserviamo il diritto di) trasferire o assegnare le Informazioni personali e altre informazioni che abbiamo raccolto o ricevuto. In tal caso, richiederemo all'entità acquirente di pubblicare le sue pratiche sui dati e di fornirti tutti i tuoi diritti secondo la giurisdizione della tua residenza.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.7. Terze parti che coinvolgiamo o ospitiamo o ci forniscono servizi e funzionalità che possiamo offrirti raccolgono dati dal Sito e il coinvolgimento dei visitatori con i contenuti e le funzionalità per nostro conto o per la fornitura dei loro servizi. Ciò accade nei casi in cui utilizziamo o distribuiamo tecnologia, servizio o funzionalità di terze parti, o laddove diamo in altro modo a tali terze parti l'accesso al nostro Sito o alla nostra tecnologia per scopi specifici, come descritto nell'Informativa sulla privacy o nel Sito. Potremmo utilizzare strumenti di analisi, inclusi Google Analytics e Tableau. Questi strumenti ci aiutano a comprendere il comportamento degli utenti sul nostro sito e servizi, anche monitorando il contenuto della pagina e le attività di clic/tocco, movimenti, scorrimento e battitura. Le pratiche sulla privacy di questi strumenti sono soggette alle proprie politiche e possono utilizzare i propri cookie per fornire i propri servizi. Per ulteriori informazioni sui cookie, consulta la nostra Cookie Policy. Fare clic su [qui] per ulteriori informazioni sui cookie di Google. Fare clic su [qui] per disattivare Google Analytics.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6. Condivisione delle tue informazioni con terze parti.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Come utilizziamo le tue informazioni personali Per fornirti in generale il Sito, i Servizi e migliorarli costantemente. Utilizzeremo le tue informazioni per:
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.4. Profilo utente e recensioni degli utenti. Potremmo offrirti di fornire una recensione ai nostri Partner o di creare un profilo utente in alcuni dei nostri Siti e fornirci ulteriori informazioni su di te che ci aiuteranno a fornirti un'esperienza personalizzata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">7. Collegamenti e interazione con proprietà di terze parti. Il Sito può collegarsi a siti Web di terze parti (come quelli dei Partner), contenuti e altre proprietà online (collettivamente, "Proprietà di terze parti"). I dati associati al tuo clic su tali Proprietà di terze parti, così come qualsiasi informazione personale che fornisci su tali Proprietà di terze parti, vengono forniti alla terza parte (sebbene Top Web Accessibility e le sue entità affiliate possano avere accordi con tali terze parti ai sensi a cui tali dati e Informazioni personali vengono inviati o condivisi di nuovo a/con noi). Non siamo responsabili per le pratiche sulla privacy di tali terze parti, o per tali proprietà di terze parti, prodotti e rispettive pratiche di raccolta dei dati. Ti invitiamo a leggere i termini e le condizioni e l'informativa sulla privacy di ogni Proprietà di terze parti che scegli di utilizzare o con cui interagire</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Potremmo anche condividere alcune Informazioni non personali con il seguente tipo di terze parti e nelle seguenti circostanze:
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.2. Piattaforme di social media. Potremmo condividere informazioni non identificate con piattaforme di social media (come Facebook, LinkedIn ecc.) per scopi di marketing e operativi. Tali piattaforme di social media possono raccogliere le tue informazioni personali in modo indipendente e possono combinare le informazioni non identificate fornite da noi, con le tue informazioni personali che raccolgono a cui hai dato il tuo consenso separato e indipendente. Qualsiasi azione di questo tipo e qualsiasi informazione personale che possa derivare da essa è soggetta alla politica sulla privacy dei social media e non è, governata, controllata o conosciuta da noi. .</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Finalità della raccolta di informazioni La politica sulla riservatezza include: <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1.1. Moduli e servizi di terze parti: il Sito elenca, valuta e confronta varie aziende, marchi ed entità commerciali di terze parti (collettivamente, "Partner") e i rispettivi prodotti e/o servizi (collettivamente, "Prodotti"). Potremmo, di volta in volta, fornirti offerte, servizi e Prodotti di Partner tramite un modulo online che deve essere compilato da te, previa tua scelta e consenso intenzionale, e tali informazioni verranno utilizzate per facilitare la tua connessione e/o coinvolgimento con il relativo Partner (“Lead”). Inoltre, se desideri ricevere maggiori informazioni su un Partner o un Prodotto, o se desideri altrimenti portare a termine una transazione con un Partner e fai clic sull'icona o sulla scheda del Partner pertinente, potresti essere indirizzato a una pagina di destinazione o a un modulo che è di proprietà e gestito dal Partner (la "Pagina di destinazione"); allo stesso modo, potresti essere indirizzato alla pagina di destinazione anche tramite altri siti Web e ricerche online. In entrambi i casi, tuttavia, potrebbe esserti richiesto di inviare determinate informazioni sulla pagina di destinazione, comprese le informazioni personali come il tuo nome, indirizzo e-mail e numero di telefono (tali informazioni collettivamente, "Informazioni sulla pagina di destinazione"). Le informazioni sulla pagina di destinazione che invii vengono raccolte direttamente dal Partner che le ha fornite e includeranno la propria politica sulla privacy e le informazioni sulle pratiche di raccolta dei dati. Ti consigliamo di fare riferimento a tale politica sulla privacy per comprendere meglio lo scopo della raccolta e dell'uso delle informazioni personali che invii.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> In qualità di utente con sede nell'UE, puoi: <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">8.1. <em>Cookie e tecnologie di tracciamento</em>. è possibile impostare il browser in modo da rifiutare e/o bloccare i cookie del Sito o di servizi di terze parti (per maggiori dettagli si rimanda alla Sezione ‎2.1.1 della presente).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">8.2. Se applicabile, rivedi e aggiorna o richiedi di rimuovere le tue informazioni personali dai nostri server (per maggiori dettagli fai riferimento alla Sezione ‎2).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">8. Scelte e rinuncia. È tua scelta fornirci le tue informazioni per gli usi descritti nella presente Informativa sulla privacy. Puoi decidere in qualsiasi momento di esercitare i tuoi diritti in relazione alle tue Informazioni Personali e alla giurisdizione della tua residenza. Ad esempio (dove applicabile e pertinente):</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.3. File di registro: raccogliamo informazioni tramite file di registro del server, ma anche implementando tecnologie di tracciamento all'interno del Sito, come i pixel tag. Lo facciamo per analizzare le tendenze, tenere traccia del coinvolgimento e dell'interazione dell'utente con il Sito e i rispettivi Servizi. Il tipo di informazioni raccolte può includere indirizzi di protocollo Internet (IP) che il tuo provider di servizi Internet ha assegnato al tuo dispositivo. Raccogliamo IP per un periodo di tempo limitato e per scopi specifici. Uno, è comprendere meglio la tua posizione geografica (paese, regione e lingua) e quindi essere in grado di fornire i Servizi nella tua lingua e pertinenti al tuo luogo di origine. In secondo luogo, utilizziamo strumenti automatizzati per analizzare gli indirizzi IP e segnalare quelli fraudolenti.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> •    Accedi alle informazioni personali che conserviamo su di te. Dovremo chiederti di fornirci determinate credenziali per assicurarci di essere chi dichiari di essere. Se ritieni che le Informazioni personali non siano accurate, complete o aggiornate, ti preghiamo di fornirci le informazioni necessarie per correggerle.
 <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(a) amministrare e mettere a tua disposizione il Sito e i Servizi; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(b) sviluppare ulteriormente e in generale migliorare il Sito ei Servizi; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(c) scopi di conteggio e monitoraggio di segnalazioni e annunci; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(d) convalida degli utenti come monitoraggio umano e antifrode </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(e) rispondere alle tue richieste e ad altre comunicazioni con te; </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">(f) identificare o autenticare l'accesso e l'utilizzo del Sito.</span></p> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">9.1. <em>Diritti dell'interessato dell'UE</em>. Qualsiasi informazione personale che raccogliamo in base alla presente Informativa sulla privacy viene gestita e trattata in conformità con il GDPR.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> 9.    <strong>I tuoi diritti</strong> <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Questa politica sulla privacy ("Informativa sulla privacy") mira a fornirti informazioni su come [Top Web Accessibility] raccoglie ed elabora i tuoi dati personali attraverso l'utilizzo di questo sito Web, inclusi tutti i dati che potresti fornire attraverso questo sito Web in relazione al tuo utilizzo o impegno con qualsiasi caratteristica, contenuto, offerta, elemento o servizio che mettiamo a tua disposizione tramite il Sito (i "Servizi"). Le intestazioni delle sezioni e delle sottosezioni utilizzate di seguito sono solo per comodità di lettura (e non devono essere utilizzate per scopi interpretativi) e ci auguriamo che facilitino la navigazione nella presente Informativa sulla privacy. </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Manteniamo la nostra politica sulla privacy sotto controllo regolare e ci riserviamo il diritto, a nostra esclusiva e assoluta discrezione, di modificare questa politica sulla privacy in qualsiasi momento. In tal caso, pubblicheremo l'Informativa sulla privacy modificata sul Sito e la dichiarazione "Ultimo aggiornamento" in fondo rifletterà la data di entrata in vigore della versione modificata. Qualsiasi accesso o utilizzo che fai del Sito dopo tale data costituisce il tuo consenso e accettazione della Politica sulla riservatezza modificata. Se apportiamo modifiche significative, ci assicureremo di fornirti un avviso più evidente, come la pubblicazione di una notifica sul Sito, sette (7) giorni prima che le modifiche diventino effettive. In tal caso, le modifiche entreranno in vigore sette (7) giorni dopo la pubblicazione di tale notifica.</span></p> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><strong><span class="Y2IQFc" lang="it">2. Le nostre basi legali del trattamento.</span></strong></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it"> Elaboriamo e utilizziamo le informazioni personali in conformità con le leggi sulla protezione dei dati. La nostra base legale per il trattamento delle tue informazioni personali sarà in genere una o più delle seguenti:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.7. Terze parti che coinvolgiamo o ospitiamo o ci forniscono servizi e funzionalità che possiamo offrirti raccolgono dati dal Sito e il coinvolgimento dei visitatori con i contenuti e le funzionalità per nostro conto o per la fornitura dei loro servizi. Ciò accade nei casi in cui utilizziamo o distribuiamo tecnologia, servizio o funzionalità di terze parti, o laddove diamo in altro modo a tali terze parti l'accesso al nostro Sito o alla nostra tecnologia per scopi specifici, come descritto nell'Informativa sulla privacy o nel Sito. Potremmo utilizzare strumenti di analisi, inclusi Google Analytics e Tableau. Questi strumenti ci aiutano a comprendere il comportamento degli utenti sul nostro sito e servizi, anche monitorando il contenuto della pagina e le attività di clic/tocco, movimenti, scorrimento e battitura. Le pratiche sulla privacy di questi strumenti sono soggette alle proprie politiche e possono utilizzare i propri cookie per fornire i propri servizi. Per ulteriori informazioni sui cookie, consulta la nostra Cookie Policy. Fare clic su [qui] per ulteriori informazioni sui cookie di Google. Fare clic su [qui] per disattivare Google Analytics.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> I tuoi diritti Termini d'uso Politica sulla riservatezza

L'ultimo aggiornamento: Marzo, 2021 In qualità di utente con sede nell'UE, puoi: Privacy dei bambini Scelte e rinuncia Impegno Consenso, scelta e modifiche Contattaci Dettagli Informativa sulla privacy Ai fini della presente Informativa sulla privacy, per "Informazioni personali" si intendono le informazioni che possono essere utilizzate, da sole o in combinazione con altre informazioni, per identificare personalmente un individuo, quali nome e cognome, indirizzo e-mail, numero di telefono, domicilio o altro indirizzo fisico e altre informazioni di contatto. In alcune giurisdizioni e dato l'uso specifico che facciamo con le informazioni, anche l'indirizzo IP che il tuo provider di servizi Internet assegna al tuo dispositivo può essere considerato come un'informazione personale. In tal caso, tratteremo le informazioni come personali in tutti gli aspetti di gestione e salvaguardia, tuttavia, non utilizziamo tali informazioni per scopi di identificazione dell'utente.
 Come utilizziamo le tue informazioni personali Informazioni di sicurezza Informazioni che raccogliamo e come le utilizziamo Collegamenti e interazione con proprietà di terze parti Utenti non statunitensi Le nostre basi legali del trattamento <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Si prega di fare riferimento alla nostra Cookie Policy per ulteriori informazioni sul tipo di cookie utilizzati e sul modo in cui utilizziamo i cookie/tecnologie di tracciamento sul nostro sito.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Finalità della raccolta di informazioni Condivisione delle tue informazioni con terze parti Alcuni dei Servizi richiederanno l'invio di Dati Personali specifici. In tal caso è possibile scegliere se si desidera utilizzare o meno tali Servizi. Tieni presente che anche se ci hai fornito le tue informazioni personali, hai il pieno diritto di revocare tale consenso in qualsiasi momento (fare riferimento alla sezione 8 di seguito per ottenere ulteriori dettagli sulla disattivazione e la revoca del consenso).
 Termini d'uso Le informazioni vengono utilizzate per gestire i nostri siti Web, fornire i servizi, migliorarli nel tempo, monitorare analisi e statistiche e consentirci di sincronizzare e risolvere i pagamenti emessi con i nostri partner commerciali (inclusi clienti, fornitori e affiliati).
 La politica sulla riservatezza include: <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Questa politica sulla privacy ("Informativa sulla privacy") mira a fornirti informazioni su come [Top Web Accessibility] raccoglie ed elabora i tuoi dati personali attraverso l'utilizzo di questo sito Web, inclusi tutti i dati che potresti fornire attraverso questo sito Web in relazione al tuo utilizzo o impegno con qualsiasi caratteristica, contenuto, offerta, elemento o servizio che mettiamo a tua disposizione tramite il Sito (i "Servizi"). Le intestazioni delle sezioni e delle sottosezioni utilizzate di seguito sono solo per comodità di lettura (e non devono essere utilizzate per scopi interpretativi) e ci auguriamo che facilitino la navigazione nella presente Informativa sulla privacy. </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Manteniamo la nostra politica sulla privacy sotto controllo regolare e ci riserviamo il diritto, a nostra esclusiva e assoluta discrezione, di modificare questa politica sulla privacy in qualsiasi momento. In tal caso, pubblicheremo l'Informativa sulla privacy modificata sul Sito e la dichiarazione "Ultimo aggiornamento" in fondo rifletterà la data di entrata in vigore della versione modificata. Qualsiasi accesso o utilizzo che fai del Sito dopo tale data costituisce il tuo consenso e accettazione della Politica sulla riservatezza modificata. Se apportiamo modifiche significative, ci assicureremo di fornirti un avviso più evidente, come la pubblicazione di una notifica sul Sito, sette (7) giorni prima che le modifiche diventino effettive. In tal caso, le modifiche entreranno in vigore sette (7) giorni dopo la pubblicazione di tale notifica.</span></p> Per fornirti in generale il Sito, i Servizi e migliorarli costantemente. Utilizzeremo le tue informazioni per:
 <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Top Web Accessibility rispetta la tua privacy e si impegna a proteggere i tuoi dati personali. Questa politica sulla privacy ti informerà su come trattiamo i tuoi dati personali quando visiti il ​​nostro sito Web (indipendentemente da dove lo visiti) e ti informerà sui tuoi diritti alla privacy e su come la legge ti protegge. </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">La nostra politica sulla privacy è progettata per informarti sulle informazioni che raccogliamo, su come le utilizziamo e sulle tue opzioni su determinati usi delle informazioni. Gli elementi chiave della nostra informativa sulla privacy completa sono riepilogati di seguito per comodità dell'utente. </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">La nostra politica sulla privacy si applica ai visitatori del sito web. Questo riepilogo è progettato per fornirti i punti salienti in un linguaggio di facile comprensione. Tuttavia, la versione completa della nostra politica sulla privacy è quella legalmente vincolante.</span></p> Non condividiamo informazioni personali su di te con terze parti, ad eccezione delle seguenti circostanze:
 Potremmo anche condividere alcune Informazioni non personali con il seguente tipo di terze parti e nelle seguenti circostanze:
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Non condivideremo le tue informazioni a meno che non ci venga richiesto dalla legge o come richiesto per fornirti i nostri siti Web e servizi. Ti invitiamo sinceramente a leggere, comprendere e rivedere la nostra politica sulla privacy completa di volta in volta.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Quando visiti i nostri siti Web, raccogliamo alcune informazioni sulla tua visita, sull'interazione con i contenuti, sul sito da cui sei arrivato e al quale ti stai dirigendo e alcune informazioni tecniche aggiuntive sul dispositivo e sul sistema da cui stai consumando i servizi. Se scegli di utilizzare un servizio "fai clic per chiamare" che possiamo offrire, il numero di telefono da cui hai effettuato la chiamata può essere raccolto per un periodo di tempo limitato fino a 3 mesi.
 I tuoi diritti <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.4. Rinforzo. Potremmo condividere le tue informazioni personali con terze parti se riteniamo che la divulgazione di tali informazioni sia utile o ragionevolmente necessaria per: (a) rispettare qualsiasi legge, regolamento, procedura legale o richiesta governativa applicabile; (b) applicare le nostre Condizioni d'uso, comprese le indagini su potenziali violazioni delle stesse; (c) rilevare, prevenire o altrimenti affrontare frodi o problemi di sicurezza; e/o (d) proteggere da danni ai diritti, alla proprietà o alla sicurezza di Top Web Accessibility, delle nostre entità affiliate, dei nostri utenti, di te stesso e/o del pubblico.</span></p> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">7. Collegamenti e interazione con proprietà di terze parti. Il Sito può collegarsi a siti Web di terze parti (come quelli dei Partner), contenuti e altre proprietà online (collettivamente, "Proprietà di terze parti"). I dati associati al tuo clic su tali Proprietà di terze parti, così come qualsiasi informazione personale che fornisci su tali Proprietà di terze parti, vengono forniti alla terza parte (sebbene Top Web Accessibility e le sue entità affiliate possano avere accordi con tali terze parti ai sensi a cui tali dati e Informazioni personali vengono inviati o condivisi di nuovo a/con noi). Non siamo responsabili per le pratiche sulla privacy di tali terze parti, o per tali proprietà di terze parti, prodotti e rispettive pratiche di raccolta dei dati. Ti invitiamo a leggere i termini e le condizioni e l'informativa sulla privacy di ogni Proprietà di terze parti che scegli di utilizzare o con cui interagire</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">11. <strong>Privacy dei bambini</strong>. Il Sito o i Servizi non sono progettati per attirare bambini di età inferiore ai 16 anni. Di conseguenza, non intendiamo raccogliere informazioni personali da chiunque sappiamo essere minore di 16 anni. Se apprendiamo di aver raccolto informazioni personali da un bambino di età inferiore a 16 anni , faremo sforzi commercialmente ragionevoli per eliminare tali informazioni il più rapidamente possibile. Se ritieni che potremmo avere tali informazioni, ti preghiamo di contattarci all'indirizzo: hello@topwebaccessibilitychecker.com.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Le informazioni vengono utilizzate per gestire i nostri siti Web, fornire i servizi, migliorarli nel tempo, monitorare analisi e statistiche e consentirci di sincronizzare e risolvere i pagamenti emessi con i nostri partner commerciali (inclusi clienti, fornitori e affiliati).
 Quando visiti i nostri siti Web, raccogliamo alcune informazioni sulla tua visita, sull'interazione con i contenuti, sul sito da cui sei arrivato e al quale ti stai dirigendo e alcune informazioni tecniche aggiuntive sul dispositivo e sul sistema da cui stai consumando i servizi. Se scegli di utilizzare un servizio "fai clic per chiamare" che possiamo offrire, il numero di telefono da cui hai effettuato la chiamata può essere raccolto per un periodo di tempo limitato fino a 3 mesi.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Si prega di fare riferimento alla nostra Cookie Policy per ulteriori informazioni sul tipo di cookie utilizzati e sul modo in cui utilizziamo i cookie/tecnologie di tracciamento sul nostro sito.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> •    Opporsi al trattamento delle tue Informazioni personali per scopi di marketing diretto, nel caso in cui le tue Informazioni personali siano utilizzate per tale scopo.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.2. Interessi legittimi. Potremmo utilizzare alcune delle tue informazioni personali laddove abbiamo motivi commerciali legittimi per farlo. Utilizziamo queste Informazioni personali per gestire, migliorare e promuovere i Servizi (ad esempio, quando personalizziamo la tua esperienza dei Servizi o per scopi di analisi) e per proteggere la nostra attività (ad esempio, per prevenire frodi o garantire la sicurezza del Sito).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Top Web Accessibility rispetta la tua privacy e si impegna a proteggere i tuoi dati personali. Questa politica sulla privacy ti informerà su come trattiamo i tuoi dati personali quando visiti il ​​nostro sito Web (indipendentemente da dove lo visiti) e ti informerà sui tuoi diritti alla privacy e su come la legge ti protegge. </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">La nostra politica sulla privacy è progettata per informarti sulle informazioni che raccogliamo, su come le utilizziamo e sulle tue opzioni su determinati usi delle informazioni. Gli elementi chiave della nostra informativa sulla privacy completa sono riepilogati di seguito per comodità dell'utente. </span></p>
<p class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">La nostra politica sulla privacy si applica ai visitatori del sito web. Questo riepilogo è progettato per fornirti i punti salienti in un linguaggio di facile comprensione. Tuttavia, la versione completa della nostra politica sulla privacy è quella legalmente vincolante.</span></p> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.2. Interazioni e utilizzo: Al fine di fornire i Servizi, gestire il Sito e continuare a migliorarlo, in modo da essere in grado di sincronizzarsi con i nostri Partner e affiliati, raccogliamo informazioni sulla tua interazione e utilizzo dei Servizi, come: URL da a cui sei stato indirizzato al Sito e a cui sei stato indirizzato dal Sito, URL interni del Sito in cui hai visitato e le tue azioni in relazione ai Servizi (come visualizzazione o clic), data/ora di utilizzo del Sito, tempi per clic e visualizzazioni di pagina.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> 9.    <strong>I tuoi diritti</strong> Informazioni che raccogliamo e come le utilizziamo Utenti non statunitensi Privacy dei bambini 13.    <strong>Contattaci</strong> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.1. Informazioni che ci fornisci. Alcuni dei Servizi richiederanno all'utente di fornire o inviare determinate Informazioni personali per poterli utilizzare. Alcune delle funzionalità e/o dei servizi che richiedono la fornitura di Dati Personali sono soggetti al tuo specifico e attivo consenso (es. creazione del profilo utente, aggiunta della tua recensione a uno dei servizi o prodotti che compaiono sui nostri Siti, newsletter che potrebbero essere disponibili in determinati Siti di volta in volta, materiale e moduli promozionali) e scelta, e alcuni saranno raccolti previo tuo consenso alla presente Informativa sulla privacy, come descritto di seguito.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Impegno •    Richiedere di eliminare o limitare l'accesso alle Informazioni personali. Qualora eserciti uno (o più) dei suddetti diritti, in conformità alle disposizioni di legge, potrà richiedere di essere informato che i terzi titolari dei suoi Dati Personali, ai sensi della presente Privacy Policy, agiranno di conseguenza.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2.4. Cookie e tecnologie simili. Il Sito può utilizzare "cookie" e altre tecnologie di tracciamento web anonime (come "web beacon" e "pixel tag") implementate da noi o da fornitori di servizi di terze parti. Un cookie è un piccolo file contenente una stringa di caratteri che viene inviato al tuo computer quando visiti un sito web. Quando visiti nuovamente il sito Web, il cookie consente a quel sito Web di riconoscere il tuo browser. I cookie possono memorizzare le tue preferenze online e altre informazioni sull'interazione che fai nel sito. Puoi reimpostare il tuo browser per rifiutare tutti i cookie o per indicare quando un cookie viene inviato al tuo browser. Tuttavia, alcune funzionalità o servizi, inclusi nel Sito, potrebbero non funzionare correttamente senza i cookie e alcune funzionalità potrebbero non essere disponibili in tal caso. Un pixel tag è un tipo di tecnologia posizionata su un sito Web allo scopo di tracciare l'attività sui siti Web o quando si aprono o si accede alle e-mail (se applicabile) e viene spesso utilizzato in combinazione con i cookie.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">8. Scelte e rinuncia. È tua scelta fornirci le tue informazioni per gli usi descritti nella presente Informativa sulla privacy. Puoi decidere in qualsiasi momento di esercitare i tuoi diritti in relazione alle tue Informazioni Personali e alla giurisdizione della tua residenza. Ad esempio (dove applicabile e pertinente):</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">4. Consenso, scelta e modifiche. Non sei legalmente obbligato a fornirci Informazioni Personali (e riconosci che fornirci Informazioni Personali è fatto liberamente). Né sei obbligato a utilizzare il Sito in primo luogo. Utilizzando il Sito e impegnandosi o interagendo con i Servizi, acconsenti alla presente Informativa sulla privacy e alle nostre pratiche informative qui descritte. Se non accetti la presente Informativa sulla privacy, ti preghiamo di non accedere o utilizzare in altro modo il Sito o i Servizi.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">12. Impegno. La protezione della tua privacy online è una pratica in evoluzione e utilizziamo i nostri migliori sforzi per evolvere il nostro sito in modo da soddisfare questi requisiti e le migliori pratiche in base a tutte le giurisdizioni in cui il sito è disponibile.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6.5. Partner e prodotti. I Servizi possono includere l'invio di moduli online, nel caso in cui l'utente scelga e sia interessato a ricevere informazioni aggiuntive su un servizio o un altro ("Modulo online"). In tal caso, potremmo inviare le informazioni del modulo online o condividerle in altro modo con/con il Partner sul quale (o sul cui Prodotto) desideri ricevere maggiori informazioni o con il quale hai espresso interesse a perseguire una transazione (il "Partner corrispondente " e "Prodotto corrispondente", rispettivamente), affinché tale Partner corrispondente possa ricontattarti con le informazioni che hai richiesto. In entrambi i casi, riconosci che: (a) i Partner possono contattarti utilizzando le informazioni del modulo online che hai inviato; (b) non controlliamo il modo in cui il Partner può contattarti o l'oggetto di tale contatto; e (c) ogni Partner è tenuto a fornire la propria informativa sulla privacy, e quindi non aderirà necessariamente alla presente Informativa sulla privacy; e (d) i partner possono avere sede (ei loro server possono essere ubicati) in qualsiasi parte del mondo.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">8.1. <em>Cookie e tecnologie di tracciamento</em>. è possibile impostare il browser in modo da rifiutare e/o bloccare i cookie del Sito o di servizi di terze parti (per maggiori dettagli si rimanda alla Sezione ‎2.1.1 della presente).</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Politica sulla riservatezza

L'ultimo aggiornamento: Marzo, 2021 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">6. Condivisione delle tue informazioni con terze parti.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Le nostre basi legali del trattamento <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">3.2. Informazioni che raccogliamo da te. Raccogliamo informazioni sul tuo utilizzo dei Servizi e sulla tua interazione con il Sito o i Servizi, come segue e nei seguenti modi:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Condivisione delle tue informazioni con terze parti <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">5. Il modo in cui utilizziamo le informazioni personali. Oltre agli usi delle informazioni che raccogliamo (comprese le informazioni personali, congiuntamente "le tue informazioni") descritte altrove nella presente Informativa sulla privacy, utilizziamo anche le informazioni personali nei seguenti modi:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">10. <strong>Sicurezza delle informazioni</strong>. Abbiamo messo in atto adeguate misure di sicurezza per impedire che i tuoi dati personali vengano accidentalmente persi, utilizzati o accessibili in modo non autorizzato, alterati o divulgati. Ad esempio, il Sito è crittografato utilizzando SSL e così come qualsiasi altra informazione al trasporto, esaminiamo periodicamente le nostre pratiche di raccolta di informazioni e dati per garantire un monitoraggio costante, utilizziamo servizi di elaborazione e servizio di terze parti che implementano vari standard e certificazioni di sicurezza delle informazioni (a riposo e trasmissione) e monitoriamo e filtriamo l'accesso ai dati quando si tratta di personale nella nostra organizzazione. Tali sistemi e procedure riducono il rischio di violazioni della sicurezza, ma non forniscono una sicurezza assoluta, poiché non esiste nulla di simile quando si tratta della rete mondiale. Pertanto, non possiamo garantire che il Sito e i nostri server siano immuni da accessi non autorizzati alle informazioni ivi archiviate e ad altri rischi per la sicurezza delle informazioni.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Dettagli Informativa sulla privacy <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">2.3. Obblighi giuridici. In alcuni casi, il nostro utilizzo delle tue informazioni personali sarà necessario per ottemperare a obblighi normativi o requisiti legali (ad esempio, affrontare reclami da parte di utenti o autorità);</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target" class="">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container F0azHf tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">13.1. <em>Regolamento generale sulla protezione dei dati</em> (GDPR) - Rappresentante europeo. Ai sensi dell'articolo 27 del Regolamento generale sulla protezione dei dati (GDPR), Top Web Accessibility. ha nominato l'Ufficio europeo per la protezione dei dati (EDPO) come suo rappresentante GDPR nell'UE. È possibile contattare EDPO in merito a questioni relative al GDPR tramite:</span></p>

</div>
</div>
</div>
</div>
</div>
</div> •    Accedi alle informazioni personali che conserviamo su di te. Dovremo chiederti di fornirci determinate credenziali per assicurarci di essere chi dichiari di essere. Se ritieni che le Informazioni personali non siano accurate, complete o aggiornate, ti preghiamo di fornirci le informazioni necessarie per correggerle.
 •    Contattaci se desideri revocare il tuo consenso al trattamento delle tue informazioni personali. L'esercizio di tale diritto non pregiudicherà la liceità del trattamento basata sul consenso prima della revoca.
 •    Avere il diritto di presentare un reclamo a un'autorità di controllo della protezione dei dati.
 •    Opporsi al trattamento delle tue Informazioni personali per scopi di marketing diretto, nel caso in cui le tue Informazioni personali siano utilizzate per tale scopo.
 •    Richiedere di eliminare o limitare l'accesso alle Informazioni personali. Qualora eserciti uno (o più) dei suddetti diritti, in conformità alle disposizioni di legge, potrà richiedere di essere informato che i terzi titolari dei suoi Dati Personali, ai sensi della presente Privacy Policy, agiranno di conseguenza.
 •    Inviando una email a <a href="mailto:hello@bestwebsiteaccessability.com">hello@topwebaccessibilitychecker.com</a> 