��    Y      �      �      �      �      �      �            1      R      s      �      �      �      �            9      Z      {      �      �      �      �         �  A  
  �	     �
  �  w  V  &  �  }    j    |  _   ~  �  �  �  �  4   ,    a  g   w  D   �  L   $  ?   q  H   �  
   �      r   �  2   �  G   +  �  s  8   j!  [  �!  �  �$  A   �&  8   �&  �  '  �   �(  h   �)  �  �)  '   �,  (   �,  /   �,     (-     >-     U-  �  n-  �   l/     X0  �   u0     �0      1      91      Z1      {1      �1      �1      �1      �1       2      A2      b2      �2      �2      �2      �2      3      (3      I3      j3      �3      �3      �3      �3      4      04  @   14  �   r4     �4  �  5  �  �6  B   �:  W   �:  a  U;  �  �?  �  wA  i  `E  �  �H    �J  z   �K  G   1L  *  yL     �M  G  �M  =  R  �  IX  =  �]  �  2b  �  �e  G  �h  �  m  a  �p  @  Hu  '  �y  v   �z  �  ({  �  }  F   �  �  2�  �   ݆  Q   i�  W   ��  G   �  i   [�     ň  �  ш  �   ��  @   �  V   T�  i  ��  @   �  =  V�  �  ��  B   W�  U   ��  �  �  *  �  z   �  �  ��  ,   Q�  9   ~�  4   ��     ��     �     *�  [  I�    ��  "   ��  �   ڦ     g�  v   ��  �  ��  =  �  �   0�  �  ��  '  ��  "   ��  ,   ϴ  F   ��  @   C�  �  ��     0�  Q   P�  @  ��  i   �     M�  U   Y�  9   ��  V   �     @�  4   _�  �   ��  �   �  [  ��   026bb7eb2000f30d128bbec175d2604d 07080d00d8285005d1c2ecbc81ef1ffd 086b112b20b6c4dc9ecf356ef5c974f9 0bd562a604410eb175fe7f01c4d8037b 29a52b954d28388a72de07dcfa1c2140 2c02943bd1746680e209aaead6e348d6 2f40f333128dc1d02bdcf95dae3c8c88 416fe7915397ed24f150e2008380b82c 42d02e20b9125e549275b8440ece474f 44cfd2d4697c89a8b2a170212a79f2eb 47e668bb9de39db4f5168581b7c4e3df 589860b6002d3c67d7321b2821a4c079 6035e910390b63c5305295cf7b28e87f 61846985fe5ca18753188afbc74176b9 709602a8991eab0978e0818fc0a9cfb7 72a0fcae38d420fdf1990933fd5ec671 8def0b52a514131b90b256dcbfab6176 902354d81b1b84116d7c9920df78c6fe 92a1f614f8bd4334f0cd96e25b913091 9dca3920ccdeee13e1a02c0f6d0a966b <strong>Ensure that the published transcript matches the final audio presentation</strong>: In a situation where audio content was created with a script, do not use the initial script as accessible alternative content. Instead, be sure that every update that happened during editing and production gets effected in the final transcript. At all costs, make sure that dialogue matches what is in the edited audio presentation.  <strong>If audio plays automatically beyond 3 seconds, you should provide a mechanism to control (pause or stop) it:</strong> However, if sound plays for 3 seconds or less (e.g., a page opens with "Welcome to our Help Desk" and then goes off) there is no violation. <strong>Note</strong>: If audio content is an alternative for text content, there is no further need to provide a transcript.  <strong>Place the link to alternative audio content before or immediately after the pre-recorded audio content</strong>: There is no issue if the audio content is on the same page or a different URL. However, you should ensure that a valid link is present to deliver users to the document. Besides, you should check that a back function is available to return users to the page, leading them to the alternative to media content.  <strong>Provide a control at the beginning of the page for users to quickly turn off automatic sounds:</strong> However, in a situation where there is an automatic sound on the web page, users should stop the sound upon landing on your page. A WCAG 2.1 Success Criterion requires that you provide an audio control that is keyboard navigable. <strong>Transcripts must comprehend important information in the audio-only conten</strong>t: For this alternative to be productive, it should contain an equivalent of the information in the audio-only content. In other words, the document must have the same story as the pre-recorded content. This transcript document serves as a long description and should include all the critical points in the dialogue. It should also have a description of background sounds that are part of the story.  A basic requirement is that multimedia content plays on the user's request. As a form of best practice, the <a href="https://www.w3.org/WAI/WCAG21/Understanding/audio-control.html" target="_blank" rel="noreferrer noopener">WCAG 2.1 discourages automatically starting sounds</a>. Due to their reliance on screen readers, the visually-impaired audience may not hear the speech output if another sound plays simultaneously. Therefore, turning off an irritating sound should not be the first experience users encounter on your site. A live caption service should be handled by a trained human operator who listens to the audio and provides a nearly instantaneous transcription. To make this process effective, you should give a viewport with the caption texts on the same page as the audio. Also, ensure that the connection is correctly labeled as leading to alternative media content  Although it could be a bit daunting to put all these checks in place, it is a worthwhile undertaking for you and your business. By maintaining an accessible website, you are providing inclusion for 466 million people across the globe. Remember, there is a massive gain in this for you. You can retain 60% of people in the workforce who possess considerable buying power. At the same time, you are preventing your business from enervating legal issues.  Another common oversight on the part of website owners is <strong>restricting </strong>web applications or services to <strong>voice interaction</strong>. This is an outright denial to the hearing impaired, especially those who are deaf or deaf-blind. Lastly, not providing <strong>text adjustment options or color caption</strong> could present a serious challenge to these visitors.  Audio Accessibility checklist: What to check and how Before we dive into the checklist of audio accessibility, we first need to explain what audio accessibility is. Audio accessibility is a way of making audio content accessible to people with auditory disabilities. Auditory disability is said to be present when a particular individual encounters difficulty accessing content with sound. Hearing impairment (another term for auditory disabilities) has three broad categories. Auditory impairment could be <em>mild</em>, <em>severe</em>, or <em>profound</em>. More often than not, <strong>persons with hearing impairment rely on assistive technologies, hearing aids, and captioning</strong> to make sense of audio content. Each of these categories has its peculiar needs that website owners must consider when creating online content.  Check for the availability of a link to allow users to get to alternative content if on a separate page Check that all automatically playing sounds do not exceed 3 seconds. Check that dialogue presented in pre-recorded audio matches the transcript  Check that transcript identifies each speaker in the dialogue  Clearly label the control button so that users can identify it with ease Conclusion Ensure that live audio is accessible to users who are hard of hearing or deaf by providing a live text caption. A transcript may be a worthwhile substitute if live audio follows a script. Even so, a real-time caption is preferable because it can play out at the same pace as the audio. Besides, it is possible to adjust to any deviation from the script if circumstances warrant it.  Ensure that there is no more than 30 seconds delay before the text of life audio content appears on the viewport  Ensure that users can reduce sound to level zero  Finally, check for a way that users can manually start music or sound  First and foremost, an audio-accessible site levels the ground for users with auditory impairment. It is one of the main ways of ensuring equal access to the most neglected disability group. A website visitor who is hard of hearing could rely on transcript text or audio description without losing any bit of useful information presented in audio content. Consequently, this improves the user experience and leaves a positive impression. Moreover, audio transcripts and captions are proven ways to boost your site's performance on search engines. No doubt, SEO is an integral aspect of every content strategy if you want more users to discover your business. Interestingly, audio transcripts are a way of enhancing whatever SEO framework you have as basic.  Four major obstacles for persons with hearing impairment Having checked popular aspects of your website accessibility, there is one thing you should not forget —audio accessibility. This is exceedingly important if you want to keep people's auditory impairments on your website. If not, most of these potential clients will leave your website as soon as they discover that you have no concern for their needs. Generally, audio accessibility checking involves three things. One, providing control for users to pause or stop irritating sounds. Two, providing media alternatives in the form of a transcript or audio descriptions for pre-recorded content. Finally, providing a live caption for live audio so that individuals with auditory challenges are not left behind. If you want to check or scan your whole website's accessibility then take a look at our <a href="/reviews/">web accessibility checker reviews</a>.  In addition to enhancing website accessibility, an audio transcript is a way of doubling your content. The direct implication of these transcripts is that search engine algorithms have more content to crawl from your site. Since SEO is both a quality and quantity game, you could improve your odds with well-transcribed audio content. You are killing two birds with one stone, just like that.  Listen to audio-only content while referring to the alternative  Load web page to check that no sounds play automatically Making your site audio accessible is a must. So, what is in it for you to keep your website compliant with the WCAG 2.1<strong> </strong>audio accessibility requirements? Well, there are <strong>tons of benefits in ticking off all items on the audio accessibility checklist</strong>. For many reasons, a blog or website available to the deaf and hard of hearing should be the standard. Now that you have convincing reasons to bring your site to full audio accessibility compliance, what are the steps to take? The following are the items that the World Content Accessibility Guidelines 2.1 requires you to tick to attain your goal.  On a page with music, check for control at the beginning that allows users to turn off automatic music  One of the disabilities that are included in web accessibility compliance is an audio disability. Before​​​​ you can say that your website is fully accessible, you must check the status of the audio accessibility. To make sure you cover all the parts, we have created this audio accessibility checklist. Our checklist tells you what parts of your website you should make accessible for users with auditory disabilities. Often, website owners turn less focus on this aspect of accessibility compliance. With this checklist, we guide you through the process of improving your website on audio accessibility today. Checklist Audio Accessibility – 3 Tips To Improve Your Website Accessibility Provide a mechanism for audio control  Provide live text caption for live audio Provide transcripts for audio-only presentation Quick Check Procedure Quick Check Procedure: Quick Check Procedure:  There are four major obstacles that persons with hearing impairment face as they navigate the web. <strong>Multimedia content</strong> (including audio and video with sound) <strong>without transcripts</strong> is the most prominent barrier to hearing or suffering profound deafness. Similarly, media players who <strong>do not</strong> <strong>provide audio control mechanisms</strong> may strain this group of people. Thus, an irritating sound might put off an end-user with an auditory disability.  Transcripts are the mainstay of multimedia content accessibility. The World Content Accessibility Guidelines (WCAG) stipulates that for every audio-only content available on your website, an "alternative content" should accompany it.  What is audio accessibility? When streaming an audio life, check that the web page is functional, featuring a viewport that is connected with the live audio content  Why Audio Accessibility? a2a9c01c89b8982a63b25d7fff76e161 a52041ba38edb7ac86ce5825fd33bd98 b37bd578f629e6841d39452edb586b55 b70db71a5562502c37c09c024199ab1f b9dca91628e44772f494417194faf8ce bb3023936a2349fd2dd0b776d69110dc c07a1476ae32abca28bd0b854e5ccb7a c2207a79e8022108981804394539995c c41c0786f4c5abaa0bdfc34930b6a7ac c82444c2d6d65be2e75617738f8dacd0 cb20848a2b5a35aeec91a8b0ab7b52dd ce1c79004fc398777991b1d0f75641e4 ce6f5279ee86cd63bcc6f6d82c16e615 d7b042f74be2a1f9df734ae467fcb650 dce928b92ff8b1337160f37edf759a85 dfb61c997313920f00904aa2cdfd21ba e214e2194db5e31a3d318ef3aec4e6b9 eefca2f7a7fbd140a41601a2dee18d8c f0a920583bfb4007b8228126791f9122 f1316192f56339828e6d87b4e16eec01 f24bd8bf92b6ccb4c0bd60703f92f0cc f44b8744347a88b9cf833e019440d61e f75013a6b5c2704358023ce6631e680d f767f2babd639213edc0920e0a9bb1da  Quattro ostacoli principali per le persone con problemi di udito Assicurati che non ci siano più di 30 secondi di ritardo prima che il testo del contenuto audio della vita appaia sul viewport
 Procedura di controllo rapido Oltre a migliorare l'accessibilità del sito Web, una trascrizione audio è un modo per raddoppiare i tuoi contenuti. L'implicazione diretta di queste trascrizioni è che gli algoritmi dei motori di ricerca hanno più contenuti da scansionare dal tuo sito. Poiché la SEO è sia un gioco di qualità che di quantità, potresti migliorare le tue probabilità con contenuti audio ben trascritti. Stai prendendo due piccioni con una fava, proprio così. <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Fornisci un controllo all'inizio della pagina per consentire agli utenti di disattivare rapidamente i suoni automatici: tuttavia, in una situazione in cui è presente un suono automatico sulla pagina web, gli utenti dovrebbero interrompere il suono quando arrivano sulla tua pagina. Un criterio di successo WCAG 2.1 richiede che tu fornisca un controllo audio navigabile da tastiera.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Ascolta i contenuti solo audio facendo riferimento all'alternativa Verifica che il dialogo presentato in audio preregistrato corrisponda alla trascrizione <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Le trascrizioni devono comprendere informazioni importanti nel contenuto solo audio: affinché questa alternativa sia produttiva, dovrebbe contenere un equivalente delle informazioni nel contenuto solo audio. In altre parole, il documento deve avere la stessa storia del contenuto preregistrato. Questo documento di trascrizione serve come una lunga descrizione e dovrebbe includere tutti i punti critici del dialogo. Dovrebbe anche avere una descrizione dei suoni di sottofondo che fanno parte della storia.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Assicurati che l'audio dal vivo sia accessibile agli utenti con problemi di udito o non udenti fornendo una didascalia di testo dal vivo. Una trascrizione può essere un valido sostituto se l'audio dal vivo segue un copione. Anche così, è preferibile una didascalia in tempo reale perché può essere riprodotta allo stesso ritmo dell'audio. Inoltre, è possibile adeguarsi a qualsiasi deviazione dal copione se le circostanze lo giustificano.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Un'altra svista comune da parte dei proprietari di siti Web è limitare le applicazioni Web oi servizi all'interazione vocale. Questa è una negazione assoluta per le persone con problemi di udito, in particolare coloro che sono sordi o sordo-ciechi. Infine, non fornire opzioni di regolazione del testo o didascalie a colori potrebbe rappresentare una seria sfida per questi visitatori.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Innanzitutto, un sito audio-accessibile livella il terreno per gli utenti con disabilità uditive. È uno dei modi principali per garantire la parità di accesso al gruppo di disabili più trascurato. Un visitatore del sito Web con problemi di udito può fare affidamento sul testo della trascrizione o sulla descrizione audio senza perdere alcuna informazione utile presentata nel contenuto audio. Di conseguenza, ciò migliora l'esperienza dell'utente e lascia un'impressione positiva. Inoltre, le trascrizioni audio e le didascalie sono modi comprovati per aumentare le prestazioni del tuo sito sui motori di ricerca. Senza dubbio, la SEO è un aspetto integrante di ogni strategia di contenuto se vuoi che più utenti scoprano la tua attività. È interessante notare che le trascrizioni audio sono un modo per migliorare qualsiasi framework SEO di cui disponi di base. Anche se potrebbe essere un po' scoraggiante mettere in atto tutti questi controlli, è un'impresa proficua per te e la tua attività. Mantenendo un sito Web accessibile, fornisci l'inclusione a 466 milioni di persone in tutto il mondo. Ricorda, c'è un enorme guadagno in questo per te. Puoi trattenere il 60% delle persone nella forza lavoro che possiedono un notevole potere d'acquisto. Allo stesso tempo, stai impedendo alla tua azienda di snervare le questioni legali. Le trascrizioni sono il cardine dell'accessibilità dei contenuti multimediali. Le Linee guida per l'accessibilità dei contenuti mondiali (WCAG) stabiliscono che per ogni contenuto solo audio disponibile sul tuo sito web, un "contenuto alternativo" dovrebbe accompagnarlo. In una pagina con musica, controlla il controllo all'inizio che consente agli utenti di disattivare la musica automatica
 Verifica che la trascrizione identifichi ogni interlocutore nel dialogo Ora che hai motivi convincenti per portare il tuo sito alla piena conformità all'accessibilità audio, quali sono i passi da compiere? Di seguito sono riportati gli elementi che le Linee guida per l'accessibilità dei contenuti mondiali 2.1 richiedono di spuntare per raggiungere il tuo obiettivo. Procedura di controllo rapido: <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Posiziona il collegamento ai contenuti audio alternativi prima o immediatamente dopo il contenuto audio preregistrato: non c'è problema se il contenuto audio si trova sulla stessa pagina o su un URL diverso. Tuttavia, dovresti assicurarti che sia presente un collegamento valido per fornire agli utenti il ​​documento. Inoltre, dovresti verificare che sia disponibile una funzione back per riportare gli utenti alla pagina, portandoli all'alternativa ai contenuti multimediali.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Dopo aver verificato gli aspetti popolari dell'accessibilità del tuo sito web, c'è una cosa che non dovresti dimenticare: l'accessibilità audio. Questo è estremamente importante se vuoi mantenere i disturbi uditivi delle persone sul tuo sito web. In caso contrario, la maggior parte di questi potenziali clienti lascerà il tuo sito Web non appena scoprirà che non ti preoccupi delle loro esigenze. In genere, il controllo dell'accessibilità audio implica tre cose. Uno, che fornisce agli utenti il ​​controllo per mettere in pausa o interrompere i suoni irritanti. Due, fornire alternative multimediali sotto forma di trascrizione o descrizioni audio per contenuti preregistrati. Infine, fornire una didascalia dal vivo per l'audio dal vivo in modo che gli individui con problemi uditivi non vengano lasciati indietro. Se desideri controllare o scansionare l'accessibilità dell'intero sito web, dai un'occhiata alle nostre recensioni sul controllo dell'accessibilità web.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Prima di immergerci nell'elenco di controllo dell'accessibilità audio, dobbiamo prima spiegare cos'è l'accessibilità audio. L'accessibilità audio è un modo per rendere i contenuti audio accessibili alle persone con disabilità uditive. Si dice che la disabilità uditiva sia presente quando un particolare individuo incontra difficoltà ad accedere ai contenuti con il suono. La disabilità uditiva (un altro termine per le disabilità uditive) ha tre grandi categorie. La compromissione uditiva può essere lieve, grave o profonda. Il più delle volte, le persone con problemi di udito si affidano a tecnologie assistive, apparecchi acustici e sottotitoli per dare un senso ai contenuti audio. Ognuna di queste categorie ha le sue esigenze peculiari che i proprietari dei siti web devono considerare quando creano contenuti online.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Assicurati che la trascrizione pubblicata corrisponda alla presentazione audio finale: in una situazione in cui il contenuto audio è stato creato con uno script, non utilizzare lo script iniziale come contenuto alternativo accessibile. Invece, assicurati che ogni aggiornamento avvenuto durante l'editing e la produzione venga effettuato nella trascrizione finale. A tutti i costi, assicurati che il dialogo corrisponda a ciò che è nella presentazione audio modificata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Se l'audio viene riprodotto automaticamente oltre i 3 secondi, è necessario fornire un meccanismo per controllarlo (metterlo in pausa o interromperlo): Tuttavia, se l'audio viene riprodotto per 3 secondi o meno (ad esempio, si apre una pagina con "Benvenuti al nostro Help Desk" e poi si spegne ) non vi è alcuna violazione.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Nota: se il contenuto audio è un'alternativa al contenuto di testo, non è più necessario fornire una trascrizione.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Posiziona il collegamento ai contenuti audio alternativi prima o immediatamente dopo il contenuto audio preregistrato: non c'è problema se il contenuto audio si trova sulla stessa pagina o su un URL diverso. Tuttavia, dovresti assicurarti che sia presente un collegamento valido per fornire agli utenti il ​​documento. Inoltre, dovresti verificare che sia disponibile una funzione back per riportare gli utenti alla pagina, portandoli all'alternativa ai contenuti multimediali.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Fornisci un controllo all'inizio della pagina per consentire agli utenti di disattivare rapidamente i suoni automatici: tuttavia, in una situazione in cui è presente un suono automatico sulla pagina web, gli utenti dovrebbero interrompere il suono quando arrivano sulla tua pagina. Un criterio di successo WCAG 2.1 richiede che tu fornisca un controllo audio navigabile da tastiera.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Le trascrizioni devono comprendere informazioni importanti nel contenuto solo audio: affinché questa alternativa sia produttiva, dovrebbe contenere un equivalente delle informazioni nel contenuto solo audio. In altre parole, il documento deve avere la stessa storia del contenuto preregistrato. Questo documento di trascrizione serve come una lunga descrizione e dovrebbe includere tutti i punti critici del dialogo. Dovrebbe anche avere una descrizione dei suoni di sottofondo che fanno parte della storia.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Un requisito fondamentale è che i contenuti multimediali vengano riprodotti su richiesta dell'utente. Come forma di best practice, le WCAG 2.1 scoraggiano l'avvio automatico dei suoni. A causa della loro dipendenza dai lettori di schermo, il pubblico ipovedente potrebbe non sentire l'output vocale se viene riprodotto un altro suono contemporaneamente. Pertanto, disattivare un suono irritante non dovrebbe essere la prima esperienza che gli utenti incontrano sul tuo sito.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Un servizio di sottotitoli in tempo reale dovrebbe essere gestito da un operatore umano addestrato che ascolta l'audio e fornisce una trascrizione quasi istantanea. Per rendere efficace questo processo, dovresti fornire una finestra con i testi delle didascalie sulla stessa pagina dell'audio.
 Inoltre, assicurati che la connessione sia correttamente etichettata come porta a contenuti multimediali alternativi
 Anche se potrebbe essere un po' scoraggiante mettere in atto tutti questi controlli, è un'impresa proficua per te e la tua attività. Mantenendo un sito Web accessibile, fornisci l'inclusione a 466 milioni di persone in tutto il mondo. Ricorda, c'è un enorme guadagno in questo per te. Puoi trattenere il 60% delle persone nella forza lavoro che possiedono un notevole potere d'acquisto. Allo stesso tempo, stai impedendo alla tua azienda di snervare le questioni legali. <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Un'altra svista comune da parte dei proprietari di siti Web è limitare le applicazioni Web oi servizi all'interazione vocale. Questa è una negazione assoluta per le persone con problemi di udito, in particolare coloro che sono sordi o sordo-ciechi. Infine, non fornire opzioni di regolazione del testo o didascalie a colori potrebbe rappresentare una seria sfida per questi visitatori.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Lista di controllo per l'accessibilità audio: cosa controllare e come <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Prima di immergerci nell'elenco di controllo dell'accessibilità audio, dobbiamo prima spiegare cos'è l'accessibilità audio. L'accessibilità audio è un modo per rendere i contenuti audio accessibili alle persone con disabilità uditive. Si dice che la disabilità uditiva sia presente quando un particolare individuo incontra difficoltà ad accedere ai contenuti con il suono. La disabilità uditiva (un altro termine per le disabilità uditive) ha tre grandi categorie. La compromissione uditiva può essere lieve, grave o profonda. Il più delle volte, le persone con problemi di udito si affidano a tecnologie assistive, apparecchi acustici e sottotitoli per dare un senso ai contenuti audio. Ognuna di queste categorie ha le sue esigenze peculiari che i proprietari dei siti web devono considerare quando creano contenuti online.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Verificare la disponibilità di un collegamento per consentire agli utenti di accedere ai contenuti alternativi se su una pagina separata
 Verificare che tutti i suoni in riproduzione automatica non superino i 3 secondi. Verifica che il dialogo presentato in audio preregistrato corrisponda alla trascrizione Verifica che la trascrizione identifichi ogni interlocutore nel dialogo Etichetta chiaramente il pulsante di controllo in modo che gli utenti possano identificarlo con facilità Conclusione Assicurati che l'audio dal vivo sia accessibile agli utenti con problemi di udito o non udenti fornendo una didascalia di testo dal vivo. Una trascrizione può essere un valido sostituto se l'audio dal vivo segue un copione. Anche così, è preferibile una didascalia in tempo reale perché può essere riprodotta allo stesso ritmo dell'audio. Inoltre, è possibile adeguarsi a qualsiasi deviazione dal copione se le circostanze lo giustificano.
 Assicurati che non ci siano più di 30 secondi di ritardo prima che il testo del contenuto audio della vita appaia sul viewport
 Garantire che gli utenti possano ridurre il suono a livello zero Infine, controlla un modo in cui gli utenti possono avviare manualmente musica o suoni Innanzitutto, un sito audio-accessibile livella il terreno per gli utenti con disabilità uditive. È uno dei modi principali per garantire la parità di accesso al gruppo di disabili più trascurato. Un visitatore del sito Web con problemi di udito può fare affidamento sul testo della trascrizione o sulla descrizione audio senza perdere alcuna informazione utile presentata nel contenuto audio. Di conseguenza, ciò migliora l'esperienza dell'utente e lascia un'impressione positiva. Inoltre, le trascrizioni audio e le didascalie sono modi comprovati per aumentare le prestazioni del tuo sito sui motori di ricerca. Senza dubbio, la SEO è un aspetto integrante di ogni strategia di contenuto se vuoi che più utenti scoprano la tua attività. È interessante notare che le trascrizioni audio sono un modo per migliorare qualsiasi framework SEO di cui disponi di base. Quattro ostacoli principali per le persone con problemi di udito <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Dopo aver verificato gli aspetti popolari dell'accessibilità del tuo sito web, c'è una cosa che non dovresti dimenticare: l'accessibilità audio. Questo è estremamente importante se vuoi mantenere i disturbi uditivi delle persone sul tuo sito web. In caso contrario, la maggior parte di questi potenziali clienti lascerà il tuo sito Web non appena scoprirà che non ti preoccupi delle loro esigenze. In genere, il controllo dell'accessibilità audio implica tre cose. Uno, che fornisce agli utenti il ​​controllo per mettere in pausa o interrompere i suoni irritanti. Due, fornire alternative multimediali sotto forma di trascrizione o descrizioni audio per contenuti preregistrati. Infine, fornire una didascalia dal vivo per l'audio dal vivo in modo che gli individui con problemi uditivi non vengano lasciati indietro. Se desideri controllare o scansionare l'accessibilità dell'intero sito web, dai un'occhiata alle nostre recensioni sul controllo dell'accessibilità web.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Oltre a migliorare l'accessibilità del sito Web, una trascrizione audio è un modo per raddoppiare i tuoi contenuti. L'implicazione diretta di queste trascrizioni è che gli algoritmi dei motori di ricerca hanno più contenuti da scansionare dal tuo sito. Poiché la SEO è sia un gioco di qualità che di quantità, potresti migliorare le tue probabilità con contenuti audio ben trascritti. Stai prendendo due piccioni con una fava, proprio così. Ascolta i contenuti solo audio facendo riferimento all'alternativa Carica la pagina web per verificare che nessun suono venga riprodotto automaticamente <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Rendere accessibile l'audio del tuo sito è un must. Quindi, cosa significa per te mantenere il tuo sito web conforme ai requisiti di accessibilità audio WCAG 2.1? Bene, ci sono un sacco di vantaggi nel spuntare tutti gli elementi nell'elenco di controllo dell'accessibilità audio. Per molte ragioni, un blog o un sito web disponibile per i non udenti e gli ipoudenti dovrebbe essere lo standard.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Ora che hai motivi convincenti per portare il tuo sito alla piena conformità all'accessibilità audio, quali sono i passi da compiere? Di seguito sono riportati gli elementi che le Linee guida per l'accessibilità dei contenuti mondiali 2.1 richiedono di spuntare per raggiungere il tuo obiettivo. In una pagina con musica, controlla il controllo all'inizio che consente agli utenti di disattivare la musica automatica
 Una delle disabilità incluse nella conformità all'accessibilità web è una disabilità audio. Prima di poter dire che il tuo sito web è completamente accessibile, devi controllare lo stato dell'accessibilità audio. Per assicurarci di coprire tutte le parti, abbiamo creato questa lista di controllo per l'accessibilità audio. La nostra lista di controllo ti dice quali parti del tuo sito web dovresti rendere accessibili agli utenti con disabilità uditive. Spesso, i proprietari di siti Web dedicano meno attenzione a questo aspetto della conformità all'accessibilità. Con questo elenco di controllo, ti guidiamo attraverso il processo di miglioramento dell'accessibilità audio del tuo sito web oggi. Fornire un meccanismo per il controllo audio Fornire didascalie di testo dal vivo per l'audio dal vivo Fornire trascrizioni per la presentazione solo audio Procedura di controllo rapido Procedura di controllo rapido: Procedura di controllo rapido: <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Ci sono quattro ostacoli principali che le persone con problemi di udito devono affrontare mentre navigano sul web. I contenuti multimediali (inclusi audio e video con audio) senza trascrizioni sono la barriera più importante all'udito o alla sordità profonda. Allo stesso modo, i lettori multimediali che non forniscono meccanismi di controllo audio possono mettere a dura prova questo gruppo di persone. Pertanto, un suono irritante potrebbe scoraggiare un utente finale con la disabilità uditiva.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Le trascrizioni sono il cardine dell'accessibilità dei contenuti multimediali. Le Linee guida per l'accessibilità dei contenuti mondiali (WCAG) stabiliscono che per ogni contenuto solo audio disponibile sul tuo sito web, un "contenuto alternativo" dovrebbe accompagnarlo. Che cos'è l'accessibilità audio? Durante lo streaming di una vita audio, controlla che la pagina web sia funzionante, con un viewport collegato al contenuto audio dal vivo
 Perché l'accessibilità audio? Inoltre, assicurati che la connessione sia correttamente etichettata come porta a contenuti multimediali alternativi
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Rendere accessibile l'audio del tuo sito è un must. Quindi, cosa significa per te mantenere il tuo sito web conforme ai requisiti di accessibilità audio WCAG 2.1? Bene, ci sono un sacco di vantaggi nel spuntare tutti gli elementi nell'elenco di controllo dell'accessibilità audio. Per molte ragioni, un blog o un sito web disponibile per i non udenti e gli ipoudenti dovrebbe essere lo standard.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Assicurati che la trascrizione pubblicata corrisponda alla presentazione audio finale: in una situazione in cui il contenuto audio è stato creato con uno script, non utilizzare lo script iniziale come contenuto alternativo accessibile. Invece, assicurati che ogni aggiornamento avvenuto durante l'editing e la produzione venga effettuato nella trascrizione finale. A tutti i costi, assicurati che il dialogo corrisponda a ciò che è nella presentazione audio modificata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Durante lo streaming di una vita audio, controlla che la pagina web sia funzionante, con un viewport collegato al contenuto audio dal vivo
 Una delle disabilità incluse nella conformità all'accessibilità web è una disabilità audio. Prima di poter dire che il tuo sito web è completamente accessibile, devi controllare lo stato dell'accessibilità audio. Per assicurarci di coprire tutte le parti, abbiamo creato questa lista di controllo per l'accessibilità audio. La nostra lista di controllo ti dice quali parti del tuo sito web dovresti rendere accessibili agli utenti con disabilità uditive. Spesso, i proprietari di siti Web dedicano meno attenzione a questo aspetto della conformità all'accessibilità. Con questo elenco di controllo, ti guidiamo attraverso il processo di miglioramento dell'accessibilità audio del tuo sito web oggi. Un servizio di sottotitoli in tempo reale dovrebbe essere gestito da un operatore umano addestrato che ascolta l'audio e fornisce una trascrizione quasi istantanea. Per rendere efficace questo processo, dovresti fornire una finestra con i testi delle didascalie sulla stessa pagina dell'audio.
 Che cos'è l'accessibilità audio? Fornire un meccanismo per il controllo audio Lista di controllo per l'accessibilità audio: cosa controllare e come Garantire che gli utenti possano ridurre il suono a livello zero <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Se l'audio viene riprodotto automaticamente oltre i 3 secondi, è necessario fornire un meccanismo per controllarlo (metterlo in pausa o interromperlo): Tuttavia, se l'audio viene riprodotto per 3 secondi o meno (ad esempio, si apre una pagina con "Benvenuti al nostro Help Desk" e poi si spegne ) non vi è alcuna violazione.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Perché l'accessibilità audio? Verificare che tutti i suoni in riproduzione automatica non superino i 3 secondi. <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Un requisito fondamentale è che i contenuti multimediali vengano riprodotti su richiesta dell'utente. Come forma di best practice, le WCAG 2.1 scoraggiano l'avvio automatico dei suoni. A causa della loro dipendenza dai lettori di schermo, il pubblico ipovedente potrebbe non sentire l'output vocale se viene riprodotto un altro suono contemporaneamente. Pertanto, disattivare un suono irritante non dovrebbe essere la prima esperienza che gli utenti incontrano sul tuo sito.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Etichetta chiaramente il pulsante di controllo in modo che gli utenti possano identificarlo con facilità Conclusione Carica la pagina web per verificare che nessun suono venga riprodotto automaticamente Fornire didascalie di testo dal vivo per l'audio dal vivo Infine, controlla un modo in cui gli utenti possono avviare manualmente musica o suoni Procedura di controllo rapido: Fornire trascrizioni per la presentazione solo audio Verificare la disponibilità di un collegamento per consentire agli utenti di accedere ai contenuti alternativi se su una pagina separata
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Nota: se il contenuto audio è un'alternativa al contenuto di testo, non è più necessario fornire una trascrizione.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Ci sono quattro ostacoli principali che le persone con problemi di udito devono affrontare mentre navigano sul web. I contenuti multimediali (inclusi audio e video con audio) senza trascrizioni sono la barriera più importante all'udito o alla sordità profonda. Allo stesso modo, i lettori multimediali che non forniscono meccanismi di controllo audio possono mettere a dura prova questo gruppo di persone. Pertanto, un suono irritante potrebbe scoraggiare un utente finale con la disabilità uditiva.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> 