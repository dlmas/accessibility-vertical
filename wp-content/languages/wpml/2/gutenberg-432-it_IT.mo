��    u      �      l      l      m      �      �      �  $   �            7      X     y      �  !   �      �      �      	     >	      P	      q	     �	      �	      �	      �	      
      )
      J
  :   k
      �
      �
      �
  "   	      ,      M      n      �      �      �      �            4  p  U  :  �  (     f   *  i   �  Y  �  1  U  �  �  #   j  Q   �  $   �  +     �   1  F        _         �    �  �  �  �    	   /!  O   9!  <  �!  �   �"  ;   �#  _  �#  d  8%  �   �&  M   ~'  {   �'    H(  /   �)  �   �)  `   �*  �   �*  J   �+  G  �+    --  �   1.  +   �.     �.  0   �.    &/  ?   50  i  u0  R   �1  \   22  n   �2     �2  u   3      �3      �3      �3      �3      
4      +4      L4      m4      �4      �4      �4      �4      5      35      T5      u5      �5      �5      �5      �5      6      ;6      \6      }6      �6      �6      �6  $   7      &7  �   '7  �   �7  �  �8     z<  5   �<  L   �<  L   =  4   c=  ,   �=  7   �=  "   �=     >  t   )?     �?     �?  �  �?  �   �A     #B  "   :B  7  ]B  9   �F  �   �F  "   \G    G  L   �H  K   �H  �  I  �   �J  &   �K  5   �K  r   'L  ,  �L  q   �M  \   9N    �N  ,   �Q  1  �Q     �R  �  S    �X     �[  �   \  �   �\  =  #]  M  a`  �  �a  $   �e  S   �e  6   �e  A   2f    tf  K   }g  "   �g     �g  7  h  1  =l  /  om  Q  �q     �v  T   w  �  Yw  �   �x  G   �y    <z  �  �{    p}  \   v~  �   �~  �    7   ^�  �   ��  r   .�  �   ��  P   ��  c  ڃ  V  >�  �   ��  E   �     d�  9   {�  ,  ��  L   �  �  /�  t   �  q   W�  �   ɋ     h�  �   |�  �  �  �   ��  &   @�     g�  M  ~�     ̐  $   �  A   �  S   O�  �  ��  =  z�  Q  ��  T   
�     _�  G   r�  V  ��     �  6   )�  c  `�  P   ģ    �  �   ��  �  $�  E   �  �   I�  /  ��  �   %�  4   ��   0020aaa4123a1b6e20f71a261240107c 019c73319a8805b7022ed0879bd1b87a 041018e6ab31b3793ba22d8ed1def41a 0505f87237d16c4e743b5dd216c07653 1. Avoid Seizure Triggering Features 109842861ab10389ff0be3866b953f34 143654e513df320b8c8ffd5350f2c7af 1797138fb2ceb8c7b7cf3cb68f3a3951 2. Control Access and Exposure 2a88b1753a07bdd8a8ff2f7bbfe14513 3. Determine if a GIF is Animated 305eff3f8746bf661d9cfbb32008b0e7 3813d0755a2629f6f089a8b4fce4e1a3 3d1c56dcbace12f93a6a02d6adb2a5d1 4. Limit Flashing 469ba3b032290837ad2e66c9f63b9d1f 4a073fc97f23dedd90ceb75bcbc7b9d0 5. Reduce Contrast 50fc1a8ad235ccd524d77c43226684d3 5204e786828a6889d05ea457cbd0adcb 568ff331da88bf71ef3296cdf10b199e 56b88a14b4e58280d9d5f990da8c2aa5 5e84bfb5c01478eaf5fe6a6576c49698 5f704d909c1385f44c7ff6cdeba9a8e6 6. Saturated Reds should not be used for Flashing Content. 62592abb842aaee11eae7b1d40f70571 69268856fcb80e444f6bc29766499b45 6eb99f5d6fccb57d0832303c5831e2ec 7. Take Advantage of Media Queries 713bdbb1fb3aaaf174ce4284e354d2b0 734180bd166b6c0886e82c861046d27d 743da2d6e232392ab0fbb961693dc4b9 76336615a07a6514205ad34a45e21761 786f7bbe2a1fce142884872a3f22b33f 7cfd5628c3a245be7e3c149b0929dafb 7d9527bc55583d4371689bc633b2adad 8771b4a5528c9f4db85973a4656beff0 87e9d288e4e7ac534b5e42d92fc34633 <a target="_blank" href="https://theweco.com/epilepsy-and-digital-accessibility/" rel="noreferrer noopener">Photosensitive epilepsy</a> (PSE) is a form of epilepsy in which certain things trigger seizures. It's not common, though, but it can be diagnosed when an EEG test is done. <br/><strong><br/>Triggers of photosensitive epilepsy include the following:</strong> <em>If you want to know if your website is in full compliance with accessibility guidelines, you can use <a target="_blank" href="https://topwebaccessibilitychecker.com/reviews/a11y-accessibility-checker-testing-platform/?lang=en" rel="noreferrer noopener">A</a><a href="/reviews/a11y-accessibility-checker-testing-platform/" target="_blank" rel="noreferrer noopener">1</a><a target="_blank" href="https://topwebaccessibilitychecker.com/reviews/a11y-accessibility-checker-testing-platform/?lang=en" rel="noreferrer noopener">1y</a>, one of the top checkers around.</em> <strong>Photosensitive Epilepsy</strong> A control measure or mechanism should be deployed to limit any flashing content even before they occur A customized flash rate limit should be included in web contents; this should be made adjustable by users According to the <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/Accessibility/Seizure_disorders" rel="noreferrer noopener">Epilepsy Foundation of America</a>, a flash poses a danger to those with photosensitive seizures if the luminance is at least 20 cd/m3 with a frequency of 3Hz and a visual angle of 0.006 steradians. Also, avoid problem-causing patterns. Furthermore, while .png or .gif with flashes is best avoided, they can be recorded and converted to a video format with control options available for users. Users should be given the option of turning it off, avoiding it completely, or making it less harmful for use. Animated GIFs are distractions and can <a target="_blank" href="https://www.epilepsy.com/article/2014/3/shedding-light-photosensitivity-one-epilepsys-most-complex-conditions-0" rel="noreferrer noopener">trigger seizures in those with photoelectric sensitivity</a>. Hence, if you must include animated GIFs, ensure they are dormant until the user chooses to activate them. It can be done so that users will have to click a box or push a button before the animation can be activated. Certain colors such as blue and red Certain types of visual patterns, particularly those with high contrasting colors Checklist for Epilepsy Accessibility Conclusion Checklist Epilepsy Accessibility Contents should not be designed so that it causes harm or triggers seizures in its users. There are several ways of ensuring this, one of which is ensuring that animation that users cannot control is not included in the content.  Contrast for flashing contents should be reduced to the barest minimum Creating alternative CSS Styles Distinct Style Switching Epilepsy (also known as a seizure disorder) is a <a target="_blank" href="https://www.who.int/news-room/fact-sheets/detail/epilepsy" rel="noreferrer noopener">chronic non-communicable disease that affects the nervous system</a>. This disability is characterized by recurring seizures involving part of the body or the entire body. It is often accompanied by loss of bowel control, bladder functions, and unconsciousness. Other symptoms of epilepsy include disturbance of movement, mood, sensation, and loss of other cognitive functions. Epilepsy can affect people of all ages. It is particularly common in babies between the ages of 0 to 2 years but rare in toddlers between three and five. It is also common in children aged six to thirteen, teenagers, young adults, adults, and elderly people. Epilepsy is the 4th most common neurological disease in the world, affecting about 50 million people globally. According to studies, this neurological disorder is more common than Parkinson's disease, Cerebral palsy, autism, and multiple sclerosis combined. Generally, web contents do not harm individuals with epilepsy (specifically photosensitive epilepsy). However, some web owners use design and dramatic effects such as flickering or flashing lights and strobe-like effects, which may cause harm to some users. Even if they do not trigger seizures, as is the case for photosensitive epilepsy, it can be distracting and cause dizziness or nausea. Indeed, you wouldn't want your web content associated with things like that. Therefore, to prevent this from happening, we have created a checklist for epilepsy accessibility. Here, we will tell you what part of your website should be accessible for those with seizures. There's a lot to learn, so let's get started. Exposure to damaging web contents should be controlled to prevent those susceptible to seizures from being accidentally exposed. If your web content contains <strong>animations or images</strong> that can trigger seizures, then you should develop a control measure around it that limits or prevents access to such damaging contents. Such control measure could include displaying a <strong>warning</strong> about the danger of the content, after which the content must be placed in a location that requires users to select the option if they are to access it. This can be done either by including a button or having a specific and clear warning posted before the page can be accessed. Fireworks Flashing should be restricted to small areas of approximately 341 by 256 pixels For instance, by clicking a button, users can select whether they want their device to vibrate when accessing the web content or not. Also, 'prefers color scheme' can be used when ambient light API is not available. This allows users to determine whether they want their device to be displayed in dark or light mode. Furthermore, media update features can be introduced; this reduces the number of flickers produced by the screen, thereby ensuring stability. However, it is expensive and cannot be afforded by every web developer. High intensity of strobe lights such as visual fire alarms. However, when the web content is animated, reducing the contrast will reduce the possibility of the content triggering any seizure form. Thus, web developers are advised to reduce the contrast ratio if three flashes are observed within a second. A more proactive approach will be to adjust the contrast before publishing it or uploading it to the web. Ideally, most web contents are safe for use even by those with a photosensitive seizure disorder. However, since epilepsy is a severe disorder, web owners need to be aware of the risk involved if their websites contain flickering graphics, flashing, or animations. Asides from triggering seizures, they can also cause nausea or dizziness in regular people. Ideally, web contents with high contrasts are easier to access. The higher the contrast of text color to its background, the better the readability of the web content; users with low vision particularly more appreciate this. If moments occur more than three times in a second, reduce the contrast ratio If you are a web owner or hope to develop one, then the following checklist of epilepsy accessibility should be considered: If you must have to flash on your web content, try to keep it small. Generally, the size of the flash area should be limited to 341 x 256 pixels or less. At this distance, the user is safer. However, this site may be considered too big if the screen's image is to be viewed at a closer range. To prevent this, a WebVR can be used. WebVR can be used on headsets, computers, or phones. Moving escalators and faulty fluorescent lights Natural light, including sunlight. It is more severe when the light shimmers off the water or flickers through the slats of blinds or trees. No component of any content should contain flashes that occur more than three times in a second. Regardless of the level of luminance, though, transitioning to and from saturated red can still pose a risk to users. However, there are several ways to tackle this. They include: Rolling or flickering images from computer, laptop, or television screens. Seizure episodes occur when there is excessive electrical discharge from one part of the brain, this can range from brief muscle jerk to severe convulsions and vary in frequency of occurrence. Asides from electrical activity, epilepsy can also be caused by brain damage or family history. However, the cause is yet to be known. Setting up media queries gives users control over the web features. Generally, these controls are either available in the OS or browser. With these controls, users can set things like 'prefers reduced motion,' which helps them develop an ideal motion speed.  Some types of Tv broadcast or video games, especially those containing alternating patterns of different colors or rapid flashes. Summary of Checklist Epilepsy Accessibility Text only alternative Theater lights, night club lights, strobe lights This is why web developers must take care to create web contents that are appealing and accessible to all, including those with epilepsy. This way, the over 50 million persons with this disorder are not left out of all the fun and opportunities websites have to offer.  To avoid rapid flashes from happening, slow down live materials To ensure people with photosensitive seizures are protected, the World Wide Web Consortium (W3C) has included guidelines for web owners to follow in its guidelines related to Web Content Accessibility Guidelines (WCAG). This W3C provides a success criterion that enables photosensitive epilepsy disorder to access any web content without triggering a seizure.  Tools should be used to ensure web contents do not violate the set flash threshold We have summarized all the important accessibility checklist epilepsy in these points below: Web owners can also explore other accessibility checklists such as accessibility checklist color blind well.  What is Epilepsy? When flashes occur more than three times per second, the image should be frozen momentarily until it stops or reduces a2a2d0a20a2523fb354d7d51196f5a7c a3c083f66477bf332888df46e0d9763d a6c0e87cc08e3cc75d381c52cb613b83 adabc9d86d3cf054c827d18b6efa71b1 b053a896cdf9724ef42dbc212f8bf859 b0ea028aed7d242ce76673cfdb13132e b3fbd8068afda5509ade08beac07e813 b5861b00e783d296ee73541d20b7c077 b5f7fa37708c3c84410fe5826b0e9064 b60a3bca80af399a3cbbbf3268e0de88 b6fbe547c79389a523cd71c65644e289 ba2119b0d1b616bbe855bc01af46d4ba ba3a05d5bd51f0f8232f7aa523becbc7 bfc5ce4c2c9e3f57ed7d970815f92dba c2405e3963299f17984394c3b71c2ba9 c682925354116665173f63e922adf702 d011dd853befd651eaf87b976c3c8ee5 d48d86e8f6ba4dc51d81c314283b3df2 d4ed8cce08ce99527d6c91f53f1128a1 d776e5aac9e3b3f3b26bcdec746a9a2e d887f509bdd8fa9b908b470fdeca3829 d8edca1a37d8244914b271f569ff5bff ecee3218ebc94a2f3bbba4d9854b8310 f2603b6cc52ccbc3e118ac664bee814b f768fd7f66151f038c2a2cbdae1ffb3a fb617ac453271931e3beb771c5fc3850 fb66c75cdc17eed2435316bad54c5f65 light viewed through fast-moving fan  Luce naturale, compresa la luce del sole. È più grave quando la luce brilla sull'acqua o passa attraverso le lamelle delle persiane o degli alberi.
 Indipendentemente dal livello di luminanza, tuttavia, la transizione da e verso il rosso saturo può comunque rappresentare un rischio per gli utenti. Tuttavia, ci sono diversi modi per affrontare questo problema. Loro includono:
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Le GIF animate sono distrazioni e possono innescare convulsioni in quelli con sensibilità fotoelettrica. Quindi, se devi includere GIF animate, assicurati che siano inattive fino a quando l'utente non sceglie di attivarle. Può essere fatto in modo che gli utenti debbano fare clic su una casella o premere un pulsante prima che l'animazione possa essere attivata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Cambio di stile distinto 1. Evita le funzioni di attivazione delle convulsioni 6. I rossi saturi non devono essere utilizzati per i contenuti lampeggianti. Per evitare che si verifichino lampi rapidi, rallenta i materiali in diretta luce vista attraverso la ventola in rapido movimento 2. Controllo dell'accesso e dell'esposizione Scale mobili in movimento e luci fluorescenti difettose 3. Determina se una GIF è animata I contenuti non devono essere progettati in modo da causare danni o innescare convulsioni nei suoi utenti. Esistono diversi modi per garantire ciò, uno dei quali è garantire che l'animazione che gli utenti non possono controllare non sia inclusa nel contenuto.
 Gli strumenti dovrebbero essere utilizzati per garantire che i contenuti web non violino la soglia flash impostata
 5. Riduci il contrasto 4. Limita il lampeggiamento Per garantire che le persone con sequestri fotosensibili siano protette, il World Wide Web Consortium (W3C) ha incluso linee guida che i proprietari di siti Web devono seguire nelle sue linee guida relative alle Linee guida per l'accessibilità dei contenuti Web (WCAG). Questo W3C fornisce un criterio di successo che consente al disturbo da epilessia fotosensibile di accedere a qualsiasi contenuto web senza innescare un attacco.
 I proprietari di siti Web possono anche esplorare altre liste di controllo dell'accessibilità come la lista di controllo dell'accessibilità per daltonici.
 5. Riduci il contrasto Creazione di stili CSS alternativi <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">L'epilessia (nota anche come disturbo convulsivo) è una malattia cronica non trasmissibile che colpisce il sistema nervoso. Questa disabilità è caratterizzata da crisi ricorrenti che coinvolgono parte del corpo o l'intero corpo. È spesso accompagnato da perdita di controllo intestinale, funzioni della vescica e incoscienza. Altri sintomi dell'epilessia includono disturbi del movimento, dell'umore, delle sensazioni e della perdita di altre funzioni cognitive.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Luci del teatro, luci del night club, luci stroboscopiche Nei contenuti web dovrebbe essere incluso un limite di velocità flash personalizzato; questo dovrebbe essere reso regolabile dagli utenti
 3. Determina se una GIF è animata Idealmente, i contenuti web con un contrasto elevato sono più facilmente accessibili. Maggiore è il contrasto del colore del testo con il suo sfondo, migliore è la leggibilità del contenuto web; gli utenti ipovedenti lo apprezzano particolarmente di più.
 6. I rossi saturi non devono essere utilizzati per i contenuti lampeggianti. Il contrasto per i contenuti lampeggianti dovrebbe essere ridotto al minimo Idealmente, la maggior parte dei contenuti Web è sicura per l'uso anche da parte di chi soffre di un disturbo convulsivo fotosensibile. Tuttavia, poiché l'epilessia è una malattia grave, i proprietari di siti Web devono essere consapevoli del rischio che comportano se i loro siti Web contengono grafica sfarfallio, flash o animazioni. Oltre a scatenare convulsioni, possono anche causare nausea o vertigini nelle persone normali.
 Inoltre, possono essere introdotte funzionalità di aggiornamento dei media; questo riduce il numero di sfarfallii prodotti dallo schermo, garantendo così stabilità. Tuttavia, è costoso e non può essere offerto da tutti gli sviluppatori web.
 7. Approfitta delle query multimediali 1. Evita le funzioni di attivazione delle convulsioni Nessun componente di qualsiasi contenuto deve contenere flash che si verificano più di tre volte in un secondo.
 Ecco perché gli sviluppatori web devono aver cura di creare contenuti web accattivanti e accessibili a tutti, compresi quelli con epilessia. In questo modo, oltre 50 milioni di persone con questo disturbo non sono esclusi da tutto il divertimento e le opportunità che i siti web hanno da offrire.
 Abbiamo riassunto tutte le importanti liste di controllo dell'accessibilità dell'epilessia nei seguenti punti:
 Se i momenti si verificano più di tre volte in un secondo, ridurre il rapporto di contrasto <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Se vuoi sapere se il tuo sito web è pienamente conforme alle linee guida sull'accessibilità, puoi utilizzare A11y, uno dei migliori controlli in circolazione.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> 2. Controllo dell'accesso e dell'esposizione L'epilessia può colpire persone di tutte le età. È particolarmente comune nei bambini di età compresa tra 0 e 2 anni, ma raro nei bambini di età compresa tra tre e cinque anni. È anche comune nei bambini dai sei ai tredici anni, negli adolescenti, nei giovani adulti, negli adulti e negli anziani.
 Cos'è l'epilessia? <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">L'epilessia fotosensibile (PSE) è una forma di epilessia in cui determinate cose scatenano convulsioni. Non è comune, tuttavia, ma può essere diagnosticato quando viene eseguito un test EEG.</span></p>

<div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">I fattori scatenanti dell'epilessia fotosensibile includono quanto segue:</span></p>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Se vuoi sapere se il tuo sito web è pienamente conforme alle linee guida sull'accessibilità, puoi utilizzare A11y, uno dei migliori controlli in circolazione.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Epilessia fotosensibile Dovrebbe essere implementato un meccanismo o una misura di controllo per limitare qualsiasi contenuto lampeggiante anche prima che si verifichi
 Nei contenuti web dovrebbe essere incluso un limite di velocità flash personalizzato; questo dovrebbe essere reso regolabile dagli utenti
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Secondo l'Epilepsy Foundation of America, un flash rappresenta un pericolo per chi soffre di crisi fotosensibili se la luminanza è di almeno 20 cd/m3 con una frequenza di 3Hz e un angolo visivo di 0,006 steradianti.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Inoltre, evita schemi che causano problemi. Inoltre, mentre è meglio evitare .png o .gif con flash, possono essere registrati e convertiti in un formato video con opzioni di controllo disponibili per gli utenti. Gli utenti dovrebbero avere la possibilità di disattivarlo, evitarlo completamente o renderlo meno dannoso per l'uso.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Le GIF animate sono distrazioni e possono innescare convulsioni in quelli con sensibilità fotoelettrica. Quindi, se devi includere GIF animate, assicurati che siano inattive fino a quando l'utente non sceglie di attivarle. Può essere fatto in modo che gli utenti debbano fare clic su una casella o premere un pulsante prima che l'animazione possa essere attivata.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Alcuni colori come il blu e il rosso Alcuni tipi di pattern visivi, in particolare quelli con colori ad alto contrasto
 Lista di controllo per l'accessibilità dell'epilessia Conclusione Lista di controllo dell'accessibilità dell'epilessia I contenuti non devono essere progettati in modo da causare danni o innescare convulsioni nei suoi utenti. Esistono diversi modi per garantire ciò, uno dei quali è garantire che l'animazione che gli utenti non possono controllare non sia inclusa nel contenuto.
 Il contrasto per i contenuti lampeggianti dovrebbe essere ridotto al minimo Creazione di stili CSS alternativi Cambio di stile distinto <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">L'epilessia (nota anche come disturbo convulsivo) è una malattia cronica non trasmissibile che colpisce il sistema nervoso. Questa disabilità è caratterizzata da crisi ricorrenti che coinvolgono parte del corpo o l'intero corpo. È spesso accompagnato da perdita di controllo intestinale, funzioni della vescica e incoscienza. Altri sintomi dell'epilessia includono disturbi del movimento, dell'umore, delle sensazioni e della perdita di altre funzioni cognitive.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> L'epilessia può colpire persone di tutte le età. È particolarmente comune nei bambini di età compresa tra 0 e 2 anni, ma raro nei bambini di età compresa tra tre e cinque anni. È anche comune nei bambini dai sei ai tredici anni, negli adolescenti, nei giovani adulti, negli adulti e negli anziani.
 L'epilessia è la quarta malattia neurologica più comune al mondo, che colpisce circa 50 milioni di persone in tutto il mondo. Secondo gli studi, questo disturbo neurologico è più comune del morbo di Parkinson, della paralisi cerebrale, dell'autismo e della sclerosi multipla messi insieme. Generalmente, i contenuti web non danneggiano le persone con epilessia (in particolare l'epilessia fotosensibile). Tuttavia, alcuni proprietari di siti Web utilizzano effetti di design ed effetti drammatici come luci tremolanti o lampeggianti ed effetti simili a strobo, che possono causare danni ad alcuni utenti. Anche se non provocano convulsioni, come nel caso dell'epilessia fotosensibile, possono distrarre e causare vertigini o nausea. In effetti, non vorresti che i tuoi contenuti web fossero associati a cose del genere. Pertanto, per evitare che ciò accada, abbiamo creato una lista di controllo per l'accessibilità dell'epilessia. Qui ti diremo quale parte del tuo sito web dovrebbe essere accessibile per chi ha crisi. C'è molto da imparare, quindi cominciamo.
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">L'esposizione a contenuti web dannosi dovrebbe essere controllata per evitare che le persone soggette a convulsioni vengano esposte accidentalmente. Se il tuo contenuto web contiene animazioni o immagini che possono scatenare convulsioni, allora dovresti sviluppare una misura di controllo attorno ad esso che limiti o impedisca l'accesso a tali contenuti dannosi. Tale misura di controllo potrebbe includere la visualizzazione di un avviso sulla pericolosità del contenuto, dopodiché il contenuto deve essere collocato in una posizione che richieda agli utenti di selezionare l'opzione se desiderano accedervi. Questo può essere fatto includendo un pulsante o facendo pubblicare un avviso specifico e chiaro prima di poter accedere alla pagina.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Fuochi d'artificio Il lampeggiamento dovrebbe essere limitato a piccole aree di circa 341 per 256 pixel Ad esempio, facendo clic su un pulsante, gli utenti possono selezionare se desiderano che il proprio dispositivo vibri quando accedono al contenuto Web o meno. Inoltre, è possibile utilizzare "preferisce la combinazione di colori" quando l'API della luce ambientale non è disponibile. Ciò consente agli utenti di determinare se desiderano che il proprio dispositivo venga visualizzato in modalità scura o chiara.
 Inoltre, possono essere introdotte funzionalità di aggiornamento dei media; questo riduce il numero di sfarfallii prodotti dallo schermo, garantendo così stabilità. Tuttavia, è costoso e non può essere offerto da tutti gli sviluppatori web.
 Luci stroboscopiche ad alta intensità come allarmi antincendio visivi. Tuttavia, quando il contenuto web è animato, la riduzione del contrasto ridurrà la possibilità che il contenuto attivi qualsiasi forma di sequestro. Pertanto, si consiglia agli sviluppatori web di ridurre il rapporto di contrasto se si osservano tre flash in un secondo. Un approccio più proattivo sarà quello di regolare il contrasto prima di pubblicarlo o caricarlo sul web.
 Idealmente, la maggior parte dei contenuti Web è sicura per l'uso anche da parte di chi soffre di un disturbo convulsivo fotosensibile. Tuttavia, poiché l'epilessia è una malattia grave, i proprietari di siti Web devono essere consapevoli del rischio che comportano se i loro siti Web contengono grafica sfarfallio, flash o animazioni. Oltre a scatenare convulsioni, possono anche causare nausea o vertigini nelle persone normali.
 Idealmente, i contenuti web con un contrasto elevato sono più facilmente accessibili. Maggiore è il contrasto del colore del testo con il suo sfondo, migliore è la leggibilità del contenuto web; gli utenti ipovedenti lo apprezzano particolarmente di più.
 Se i momenti si verificano più di tre volte in un secondo, ridurre il rapporto di contrasto Se sei un proprietario di un sito web o speri di svilupparne uno, dovresti prendere in considerazione il seguente elenco di controllo dell'accessibilità dell'epilessia:
 Se devi eseguire il flash sul tuo contenuto web, cerca di mantenerlo piccolo. In genere, la dimensione dell'area del flash dovrebbe essere limitata a 341 x 256 pixel o meno. A questa distanza, l'utente è più sicuro. Tuttavia, questo sito può essere considerato troppo grande se l'immagine dello schermo deve essere visualizzata a una distanza più ravvicinata. Per evitare ciò, è possibile utilizzare un WebVR. WebVR può essere utilizzato su cuffie, computer o telefoni.
 Scale mobili in movimento e luci fluorescenti difettose Luce naturale, compresa la luce del sole. È più grave quando la luce brilla sull'acqua o passa attraverso le lamelle delle persiane o degli alberi.
 Nessun componente di qualsiasi contenuto deve contenere flash che si verificano più di tre volte in un secondo.
 Indipendentemente dal livello di luminanza, tuttavia, la transizione da e verso il rosso saturo può comunque rappresentare un rischio per gli utenti. Tuttavia, ci sono diversi modi per affrontare questo problema. Loro includono:
 Immagini che scorrono o sfarfallano da schermi di computer, laptop o televisori. Episodi di convulsioni si verificano quando c'è un'eccessiva scarica elettrica da una parte del cervello, che può variare da un breve scatto muscolare a gravi convulsioni e variare in frequenza. Oltre all'attività elettrica, l'epilessia può anche essere causata da danni cerebrali o dalla storia familiare. Tuttavia, la causa è ancora da conoscere.
 L'impostazione delle query multimediali offre agli utenti il ​​controllo sulle funzionalità web. In genere, questi controlli sono disponibili nel sistema operativo o nel browser. Con questi controlli, gli utenti possono impostare cose come "preferisce il movimento ridotto", che li aiuta a sviluppare una velocità di movimento ideale.
 Alcuni tipi di trasmissioni TV o videogiochi, in particolare quelli che contengono modelli alternati di colori diversi o lampi rapidi.
 Riepilogo della lista di controllo dell'accessibilità dell'epilessia Alternativa solo testo Luci del teatro, luci del night club, luci stroboscopiche Ecco perché gli sviluppatori web devono aver cura di creare contenuti web accattivanti e accessibili a tutti, compresi quelli con epilessia. In questo modo, oltre 50 milioni di persone con questo disturbo non sono esclusi da tutto il divertimento e le opportunità che i siti web hanno da offrire.
 Per evitare che si verifichino lampi rapidi, rallenta i materiali in diretta Per garantire che le persone con sequestri fotosensibili siano protette, il World Wide Web Consortium (W3C) ha incluso linee guida che i proprietari di siti Web devono seguire nelle sue linee guida relative alle Linee guida per l'accessibilità dei contenuti Web (WCAG). Questo W3C fornisce un criterio di successo che consente al disturbo da epilessia fotosensibile di accedere a qualsiasi contenuto web senza innescare un attacco.
 Gli strumenti dovrebbero essere utilizzati per garantire che i contenuti web non violino la soglia flash impostata
 Abbiamo riassunto tutte le importanti liste di controllo dell'accessibilità dell'epilessia nei seguenti punti:
 I proprietari di siti Web possono anche esplorare altre liste di controllo dell'accessibilità come la lista di controllo dell'accessibilità per daltonici.
 Cos'è l'epilessia? Quando i flash si verificano più di tre volte al secondo, l'immagine deve essere congelata momentaneamente finché non si ferma o si riduce
 Ad esempio, facendo clic su un pulsante, gli utenti possono selezionare se desiderano che il proprio dispositivo vibri quando accedono al contenuto Web o meno. Inoltre, è possibile utilizzare "preferisce la combinazione di colori" quando l'API della luce ambientale non è disponibile. Ciò consente agli utenti di determinare se desiderano che il proprio dispositivo venga visualizzato in modalità scura o chiara.
 Dovrebbe essere implementato un meccanismo o una misura di controllo per limitare qualsiasi contenuto lampeggiante anche prima che si verifichi
 7. Approfitta delle query multimediali Alternativa solo testo Inoltre, evita schemi che causano problemi. Inoltre, mentre è meglio evitare .png o .gif con flash, possono essere registrati e convertiti in un formato video con opzioni di controllo disponibili per gli utenti. Gli utenti dovrebbero avere la possibilità di disattivarlo, evitarlo completamente o renderlo meno dannoso per l'uso.
 4. Limita il lampeggiamento Alcuni colori come il blu e il rosso Conclusione Lista di controllo dell'accessibilità dell'epilessia Alcuni tipi di pattern visivi, in particolare quelli con colori ad alto contrasto
 <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">L'epilessia fotosensibile (PSE) è una forma di epilessia in cui determinate cose scatenano convulsioni. Non è comune, tuttavia, ma può essere diagnosticato quando viene eseguito un test EEG.</span></p>

<div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">I fattori scatenanti dell'epilessia fotosensibile includono quanto segue:</span></p>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">Secondo l'Epilepsy Foundation of America, un flash rappresenta un pericolo per chi soffre di crisi fotosensibili se la luminanza è di almeno 20 cd/m3 con una frequenza di 3Hz e un angolo visivo di 0,006 steradianti.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> <div id="tw-container" class="YQaNob" data-cp="1" data-is-ver="false" data-nnttsvi="1" data-sm="1" data-sugg-time="500" data-sugg-url="https://clients1.google.com/complete/search" data-uilc="en">
<div id="tw-ob" class="tw-src-ltr">
<div class="oSioSc">
<div id="tw-target">
<div id="kAz1tf" class="g9WsWb">
<div id="tw-target-text-container" class="tw-ta-container hide-focus-ring tw-nfl" tabindex="0">
<p id="tw-target-text" class="tw-data-text tw-text-large XcVN5d tw-ta" dir="ltr" data-placeholder="Translation"><span class="Y2IQFc" lang="it">L'esposizione a contenuti web dannosi dovrebbe essere controllata per evitare che le persone soggette a convulsioni vengano esposte accidentalmente. Se il tuo contenuto web contiene animazioni o immagini che possono scatenare convulsioni, allora dovresti sviluppare una misura di controllo attorno ad esso che limiti o impedisca l'accesso a tali contenuti dannosi. Tale misura di controllo potrebbe includere la visualizzazione di un avviso sulla pericolosità del contenuto, dopodiché il contenuto deve essere collocato in una posizione che richieda agli utenti di selezionare l'opzione se desiderano accedervi. Questo può essere fatto includendo un pulsante o facendo pubblicare un avviso specifico e chiaro prima di poter accedere alla pagina.</span></p>

</div>
</div>
</div>
</div>
</div>
</div> Il lampeggiamento dovrebbe essere limitato a piccole aree di circa 341 per 256 pixel Fuochi d'artificio Luci stroboscopiche ad alta intensità come allarmi antincendio visivi. L'impostazione delle query multimediali offre agli utenti il ​​controllo sulle funzionalità web. In genere, questi controlli sono disponibili nel sistema operativo o nel browser. Con questi controlli, gli utenti possono impostare cose come "preferisce il movimento ridotto", che li aiuta a sviluppare una velocità di movimento ideale.
 Epilessia fotosensibile Lista di controllo per l'accessibilità dell'epilessia Episodi di convulsioni si verificano quando c'è un'eccessiva scarica elettrica da una parte del cervello, che può variare da un breve scatto muscolare a gravi convulsioni e variare in frequenza. Oltre all'attività elettrica, l'epilessia può anche essere causata da danni cerebrali o dalla storia familiare. Tuttavia, la causa è ancora da conoscere.
 Immagini che scorrono o sfarfallano da schermi di computer, laptop o televisori. Tuttavia, quando il contenuto web è animato, la riduzione del contrasto ridurrà la possibilità che il contenuto attivi qualsiasi forma di sequestro. Pertanto, si consiglia agli sviluppatori web di ridurre il rapporto di contrasto se si osservano tre flash in un secondo. Un approccio più proattivo sarà quello di regolare il contrasto prima di pubblicarlo o caricarlo sul web.
 Quando i flash si verificano più di tre volte al secondo, l'immagine deve essere congelata momentaneamente finché non si ferma o si riduce
 Se devi eseguire il flash sul tuo contenuto web, cerca di mantenerlo piccolo. In genere, la dimensione dell'area del flash dovrebbe essere limitata a 341 x 256 pixel o meno. A questa distanza, l'utente è più sicuro. Tuttavia, questo sito può essere considerato troppo grande se l'immagine dello schermo deve essere visualizzata a una distanza più ravvicinata. Per evitare ciò, è possibile utilizzare un WebVR. WebVR può essere utilizzato su cuffie, computer o telefoni.
 Riepilogo della lista di controllo dell'accessibilità dell'epilessia Se sei un proprietario di un sito web o speri di svilupparne uno, dovresti prendere in considerazione il seguente elenco di controllo dell'accessibilità dell'epilessia:
 L'epilessia è la quarta malattia neurologica più comune al mondo, che colpisce circa 50 milioni di persone in tutto il mondo. Secondo gli studi, questo disturbo neurologico è più comune del morbo di Parkinson, della paralisi cerebrale, dell'autismo e della sclerosi multipla messi insieme. Generalmente, i contenuti web non danneggiano le persone con epilessia (in particolare l'epilessia fotosensibile). Tuttavia, alcuni proprietari di siti Web utilizzano effetti di design ed effetti drammatici come luci tremolanti o lampeggianti ed effetti simili a strobo, che possono causare danni ad alcuni utenti. Anche se non provocano convulsioni, come nel caso dell'epilessia fotosensibile, possono distrarre e causare vertigini o nausea. In effetti, non vorresti che i tuoi contenuti web fossero associati a cose del genere. Pertanto, per evitare che ciò accada, abbiamo creato una lista di controllo per l'accessibilità dell'epilessia. Qui ti diremo quale parte del tuo sito web dovrebbe essere accessibile per chi ha crisi. C'è molto da imparare, quindi cominciamo.
 Alcuni tipi di trasmissioni TV o videogiochi, in particolare quelli che contengono modelli alternati di colori diversi o lampi rapidi.
 luce vista attraverso la ventola in rapido movimento 