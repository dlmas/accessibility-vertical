<?php
/**
 * Plugin name: Enqueue wp-api script
 */
add_action( 'admin_init', 'ewa_enqueue_scripts' );

function ewa_enqueue_scripts() {
	wp_enqueue_script( 'wp-api' );
}
