<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sobix
 */

get_header();
?>
<div class="full-page-bg">
	<div class="container">
		<main class="error-404">
			<section class="main__error-404 not-found">
				<img src="/wp-content/themes/sobix2/assets/img/404.png" alt="">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'sobix' ); ?></h1>
				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'sobix' ); ?></p>
				</div><!-- .page-content -->
				<a href="/" class="btn_404">Take Me Home</a>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div>
</div>
<?php
get_footer();
