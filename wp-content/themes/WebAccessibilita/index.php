<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

get_header();
gettype(get_sites());
// echo widget_lineup_builder('lineup_3_top', 'reviews', 3);
?>
<div class="header-block">
    <div class="container">
        <div class="header_home">
            <div class="header_home__block">
                <h1 class="header_home__title"><?php the_field('header_content_title', 'options') ?></h1>
                <!-- <div class="header_home__text">
                    <?php the_field('header_content', 'options') ?>
                </div> -->
                <?php if( have_rows('header_bullet_points', 'options') ): ?>
                    <div class="header_home__check-row">
                    <?php while( have_rows('header_bullet_points', 'options') ): the_row(); ?>
                        <div class="header_home__check-item"><?php the_sub_field('header_bullet_points_text'); ?></div>
                    <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <?php if( $button_compare_now = get_field('button_compare_now', 'options') ){ ?>
                    <a href="<?php echo apply_filters( 'wpml_permalink', home_url(get_field('url_compare_btn', 'options'), 'https') ); ?><?php  if( wp_is_mobile()) { echo '?step=two'; } ?>" class="header_btn-compare"><?php echo $button_compare_now; ?></a>
                <?php } ?>
            </div>
            <?php if(!wp_is_mobile()) { ?>
            <div class="header_home__animate-icon">
            <script
                src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script><lottie-player src="https://assets2.lottiefiles.com/packages/lf20_dqgdwbzi.json" background="transparent" speed="1" style="width: 100%; height: 100%;" loop autoplay></lottie-player>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="hh-block">
    <div class="container">
        <div class="header_home__global-disclosure">
            <div class="header_home__last-updated-sep">
                <?php the_field('last_updated', 'options');?>
                <?php $timestamp = strtotime(date('F Y')); ?>
                <span><?php echo date_i18n( 'F Y', $timestamp); ?></span>
        </div>
            <div class="header_home__referal-disclosure">
                <div class="header_home__referal"><?php the_field('we_receive_referral_fees_from_partners', 'options');?></div>
                <div class="header_home__disclosure"><?php the_field('advertising_disclosure', 'options'); ?></div>
            </div>
        </div>
    </div>
</div>
<div class="popap-bg">
    <div class="container">
        <div class="advertiser-disclosure-text">
            <div class="advertiser-close"></div>
            <?php the_field('advertising_disclosure_text', 'options'); ?>
        </div>
    </div>
</div>

<div class="container">

    <div class="witget-top3">
        <h2><?php the_field('home_title_top_3', 'options'); ?></h2>
        <div style="font-size:10px;">
        </div>
        <div class="witget-top3__row">

            <?php
			// переключаемся на блог 1
			//switch_to_blog( 1 );
			
			// Выводим данные блога на который переключились
			// Получаем посты с блога 1
			

            do_action( 'lineup_3_top', 'lineup_3_top', 'reviews', 3);
			if ( $query->have_posts() ) : 
            $i = 1;
			while ( $query->have_posts() ) : $query->the_post(); ?>
            <a href="<?php the_field('visit_site_url'); ?>" target="_blank" class="witget-top3__item" id="witget-top3__item" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_3_top_##_' . $i; ?>">
                <div class="witget-top3__img"><?php the_post_thumbnail(); ?></div>
                <div class="witget-top3__text"><?php the_field('top_3_witget_text');?></div>
                <div class="witget-top3__raiting"><span><?php the_field('raiting'); ?></span></div>
                <div class="witget-top3__link"><?php the_field('homepage_top3_text', 'options')?></div>
            </a>
            <?php $i++;
				endwhile; 
			endif; 
			wp_reset_query(); 

			// возвращемся к текущему блогу
			//restore_current_blog();

			?>
        </div>
    </div>
    <div style="clear:both;"></div>





</div>
<div class="container">
    
	<div class="clearfix" style="    position: relative;">
		<div id="sidebar">
			<div class="sidebar__inner">
            <h2 class="witget-all__h2"><?php the_field('home_title_top_10', 'options'); ?></h2>
            <div class="witget-all__row">
                <?php
			// переключаемся на блог 1
			//switch_to_blog( 1 );
			
			// Выводим данные блога на который переключились
			// Получаем посты с блога 1

			
			do_action( 'lineup_20_homepage', 'lineup_20_homepage', 'reviews', 10);

			if ( $query->have_posts() ) : 
            
                $i = 1;
				while ( $query->have_posts() ) : $query->the_post(); 
					include (locate_template( 'template-parts/content-home-page.php' ));
                    $i++;
				endwhile; 
			endif; 
			wp_reset_query(); 
			// возвращемся к текущему блогу
			//restore_current_blog();
			?>
            </div>
            <?php // AJAX loading posts ?>
            <?php if (  $query->max_num_pages > 1 ) : ?>
            <script>
                var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                var true_posts = '<?php echo serialize($query->query_vars); ?>';
                var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                var max_pages = '<?php echo $query->max_num_pages; ?>';

            </script>
            <div id="true_loadmore"><?php the_field('show_more_options', 'options'); ?></div>
            <?php endif; ?>
            <a href="<?php echo apply_filters( 'wpml_permalink', home_url('/comparison', 'https') ); ?><?php if( wp_is_mobile()) { echo '?step=two'; } ?>" class="comparison-mobile-pc"><?php echo $button_compare_now; ?></a>
			</div>
		</div>
		<div id="content">
        <?php $query = new WP_Query( array(
				'post_type' => 'article',
				'posts_per_page'	=> 5,
			)); 
			if ( $query->have_posts() ) : ?>
                    <div class="sidebar-witget-article">
                        <div class="sidebar-witget-article__title"><?php the_field('must_reads_title', 'options'); ?></div>
                        <div class="sidebar-witget-article__row">
                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <div class="sidebar-witget-article__item">
                                <a href="<?php the_permalink(); ?>" class="witget-article__title"><?php the_title(); ?></a>
                                    <?php if(get_the_post_thumbnail()): ?>
									    <a href="<?php the_permalink(); ?>" class="top-5-witget-article__img"><?php the_post_thumbnail('large'); ?></a>
									<?php endif; ?>
                                <div class="witget-article__content"><?php the_field('short_content_article'); ?></div>
                                <a href="<?php the_permalink(); ?>" class="witget-article__linck"><?php the_field('button_title_witget_top_5_atticle', 'options'); ?></a>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <?php endif; 
			wp_reset_query(); ?>
		</div>		
	</div>
</div>
<div style="clear: both;"></div>
<div class="content content_home container">
    <main id="primary" class="site-main">

        <?php if ( function_exists('dynamic_sidebar') )
			//dynamic_sidebar('homepage-sidebar');
		?>
        <div class="component">
            <?php the_field('home_content', 'options'); ?>
        </div>
        <div class="component__read-more"><?php the_field('bottom_read_more', 'options'); ?> ></div>
    </main><!-- #main -->


</div>

<script>
    jQuery(document).ready(function($) {
        $('#true_loadmore').click(function() {
            $(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
            var data = {
                'action': 'loadmore',
                'query': true_posts,
                'page': current_page,
                'template_part': 'home-page',
            };
            $.ajax({
                url: ajaxurl, // обработчик
                data: data, // данные
                type: 'POST', // тип запроса
                success: function(data) {
                    if (data) {
                        $('#true_loadmore').text('Load more').before(data); // вставляем новые посты
                        current_page++; // увеличиваем номер страницы на единицу
                        if (current_page == max_pages) $("#true_loadmore").remove(); // если последняя страница, удаляем кнопку
                    } else {
                        $('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
                    }
                }
            });
        });
    });

</script>
    <script type="text/javascript" src="/wp-content/themes/sobix2/assets/js/sticky-sidebar.js"></script>
	<script type="text/javascript">
jQuery(document).ready(function($) {
		var stickySidebar = new StickySidebar('#sidebar', {
			topSpacing: 20,
			bottomSpacing: 20,
			containerSelector: '.container',
			innerWrapperSelector: '.sidebar__inner'
		});
    });
	</script>
<?php get_footer(); ?>
<?php 
    if( domain_user() == 'bestwebaccessibility.com' ) {
        the_field('сode_end_body_homepage_copy_bestwebaccessibility', 'options'); 
    }else{
        the_field('сode_end_body_homepage', 'options'); 
    }
?>
</body>

</html>