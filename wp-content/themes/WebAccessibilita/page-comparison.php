<?php
/*
Template Name: comparison page
Template Post Type: post, page, product
*/
get_header(); ?>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/sobix2/assets/css/slick.css" />
<style>
    body {
        overflow-x: hidden;
    }

</style>

<div class="full-page-bg">
    <div class="container">
        <div class="breadcrumbs-disclosure">
            <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
            <div class="header_home__disclosure disclosure-all-page"><?php the_field('advertising_disclosure', 'options'); ?></div>
        </div>

        <h1 class="archive-title"><?php the_title(); ?></h1>

    </div>

    <div class="container single-page-bg" style="display:flex">
        <div class="filter-sidebar">
            <div class="popup-filter-title"><span>Filter</span>
                <div class="advertiser-close"></div>
            </div>
            <div class="sort-block">
                <div class="sort-block__title"><?php the_field('sort_by_title'); ?></div>
                <ul class="sort-list">
                    <li class="sort-list-item" data-meta-key="functionality" data-order="ASC"><?php the_field('easy_to_use_title'); ?></li>
                    <!--                    <li class="sort-list-item" data-meta-key="functionality" data-order="DSC">Easy to use DOWN</li>-->

                    <li class="sort-list-item" data-meta-key="user-friendliness" data-order="ASC"><?php the_field('value_for_money_title'); ?></li>
                    <!--                    <li class="sort-list-item" data-meta-key="user-friendliness" data-order="DSC">Value for money DOWN</li>-->

                    <li class="sort-list-item" data-meta-key="customer_support" data-order="ASC"><?php the_field('best_features_title'); ?></li>
                    <!--                    <li class="sort-list-item" data-meta-key="customer_support" data-order="DSC">Best features DOWN</li>-->

                    <li class="sort-list-item" data-meta-key="affordability" data-order="ASC"><?php the_field('best_customer_service_title'); ?></li>
                    <!--                    <li class="sort-list-item" data-meta-key="affordability" data-order="DSC">Best Customer Service DOWN</li>-->

                    <div class="clear-sort"><span><?php the_field('reset_sort_title'); ?></span></div>
                </ul>
            </div>

            <div class="filter-list__block"><?php the_field('filter_by_title'); ?></div>
            <ul class="filter-list">
                <?php 
                if(ICL_LANGUAGE_CODE=='fr'){
                    $id_fields_filder = 1582;
                }elseif(ICL_LANGUAGE_CODE=='de'){
                    $id_fields_filder = 1595;
                }elseif(ICL_LANGUAGE_CODE=='it'){
                    $id_fields_filder = 4036;
                }else{
                    $id_fields_filder = 897;
                }
                
                $acf_fields = acf_get_fields( $id_fields_filder ); //acf_get_fields_by_id( 897 )
            
            for( $i = 0; $i < count($acf_fields); $i++ ){
                echo '<div class="filter-list__item">';
                echo '<div class="filter-list__title">' . $acf_fields[$i]['label'] . '</div>';
                echo '<ul class="filter-list__check '. $acf_fields[$i]['type'] .'">';
                
                foreach($acf_fields[$i]['choices'] as $key) {
                    echo '<li><label><input class="cat-list_item '. $acf_fields[$i]['type'] .'" type="checkbox" value="' . $key . '" name="' . $acf_fields[$i]['name'] . '" />' . $key . '</label></li>';
                }
                
                echo '</ul>';
                echo '</div>';
            } ?>

            </ul>
        </div>

        <div class="comparison-main">
            <div class="table-col">
                <div class="table-col__top"></div>

                <div class="table-col__item">
                    <div class="table-col__title"><?php the_field('compare_rating', 'options'); ?></div>
                    <div class="table-col__name-row">
                        <div class="table-col__name"><?php the_field('ease_of_use', 'options'); ?></div>
                        <div class="table-col__name"><?php the_field('value_for_money', 'options'); ?></div>
                        <div class="table-col__name"><?php the_field('compare_features', 'options'); ?></div>
                        <div class="table-col__name"><?php the_field('customer_service', 'options'); ?></div>
                    </div>
                </div>

                <?php //$acf_fields = acf_get_fields_by_id( 897 );
                for( $i = 0; $i < count($acf_fields); $i++ ){ 
                    echo '<div class="table-col__item">';
                    echo '<div class="table-col__title">' . $acf_fields[$i]['label'] . '</div>';
                    echo '<div class="table-col__name-row '. $acf_fields[$i]['type'] .'">';

                    foreach($acf_fields[$i]['choices'] as $key) {
                        echo '<div class="table-col__name">' . $key . '</div>';
                    }

                    echo '</div>';
                    echo '</div>';
            } ?>

            </div>

            <?php 
  $projects = new WP_Query([
      'post_type' => 'reviews',
      'post_status' => 'publish',
      'posts_per_page' => 5,
            'meta_key'       => 'raiting',
            'orderby'        => 'meta_value',
            'order'          => 'DESC'
  ]);
?>

            <?php if($projects->have_posts()): ?>
            <ul class="project-tiles">
                <?php while($projects->have_posts()) : $projects->the_post();
                    get_template_part( 'template-parts/content', 'comparison' );
                endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
            <?php endif; ?>
        </div>
    </div><!-- #main -->
</div>
<div class="btn-mobile-compare"><?php the_field('tap_to_compare', 'options');?></div>
<div class="popup-compare">

    <div class="popup-compare__step-one">
        <div class="comparadise__header">
            <div class="container">
                <div class="comparadise__header-navigation-back"><?php the_field('back', 'options');?></div>
                <div class="comparadise__header-navigation-title"><?php the_field('compare_your_options', 'options');?></div>
                <div class="comparadise__header-navigation--cta"></div>
            </div>
        </div>
        <div class="comparadise__review-row container">
            <?php 
                      $query = new WP_Query([
                          'post_type' => 'reviews',
                          'post_status' => 'publish',
                          'posts_per_page' => -1,
                        //   'meta_query' => [
                        //     [
                        //         'key'     => 'adoric_popup_active',
                        //         'value'   => '0',
                        //         'compare' => '==',
                        //     ],
                        //     // 'orderby' => [
                        //     //     'adoric_popup_active'  => 'ASC',
                        //     //     // 'state_clause' => 'DESC',
                        //     // ],
                        // ],
                        'meta_key' => 'adoric_popup_active',
                        'orderby'  => 'meta_value_num',
                        'order'    => 'DESC'
                      ]);
                if($query->have_posts()):
                while($query->have_posts()) : $query->the_post(); ?>
                    <div class="comparadise__review-item" data-post-id="<?php the_ID(); ?>"><?php the_post_thumbnail(); ?></div>
                <?php endwhile; endif; ?>
        </div>
        <div class="compare-button"><?php the_field('lets_compare', 'options');?> (<span class="comparadise__footer__btn">0</span>/3)</div>
    </div>
    <div class="popup-compare__step-two" id="popup-compare__step-two">

    <div class="popup-compare__scroll-img visible">
            <svg id="swipe" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 121 48"><g fill="#fff" fill-rule="evenodd"><image width="40" height="48" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMkAAADwCAYAAACqs0BgAAAABGdBTUEAALGOfPtRkwAAGApJREFUeAHtXQm0HVWV/TGQEAIhoCECIQmTEQIhKHRDmiAh4ACKjNK2LKOAq2l1gY3a3SsKTYMiAi4RRZeo4FJkEmXGJgoYUImMAYEwLEhCIGGeQoggpHvv/F/fyuPVq7r33Jr3WWv/V6/qnnPP3feeV3eq+kP6JGkMjEeCPYBtgUnA1sBoYH1gPYCyfAAv4PMhYAFwL/B74DlAIgYax8BUlOhM4EHg/wx4E7q3AV8BNgUkYqDWDAyB94cBdwCWwEjSfQN2LwMYgBIxUDsG9oLH9wBJDTzk+VXI5wJgLCARA5VngGOKHwEhgyCrrWeR7yGVZ0gOtpoBDsLvA7I26rzSfR0+sKsnEQOVYmBXePM8kFfDd7XLu5kCpVJNpN3OvBfFfwVwbch5pz+r3dWi0leFAQ6WnwDybvC+9o+tClHyo70M/KbCAcLAeh34h/ZWj0peNgOz4IDvL3yRegvg57CyyVL+f2dg6N8PG33Eqd6rgZE1KOU74OOrwB9q4KtcbBADs1GWIu8G1rw487ZBg/ivdVHacCcZjhq6CIg2I9ahwkbAyReBP9bB2ab72Ia5+X9GJV4YqCK5/4obFrmFZQnwMsC7xobAGGDnAayNT6sshoEtANqXiIFcGbgK1q3dn2Ww8XkgSxeIW+iPARYB1nz3hA2JGMiVgXVgfQVgaaxzob+Rh5fs3ln3hWmB0YN4qbgx8AEktwTIX6DPO4NFuD3e14eHLRlLVwxkYeBkJPJtoNzSzi0sVtkcBl4DfP3YxOqA9G0MvM2mXnntnQwezoEuH8CyCgf4FxuMWMpgyFaqEQNND5IpUUE9PkPNiDFri63JHr5LRQxkYoDTsHzG3LebMyFTLtkScaWf+7J8fDk3WxZKlRcDTb6TbAzSfMu3HLpcpwglnGF7wNMYxzSSEhnwbUQlupw569GZU741IbfTh5aHPA1ykVJSIgNNDhKukfjKSl/FHnpLe1zrdWlUr4u6lj8DTQ4SS9k4/RtauIXFR4b7KEknHAOWhhTOi3ZY4r4vH1Ed+bAWUKfJFcCZJIkYMDPQ5CAxkyMDYoAMKEjUDsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMKEjUBsRACgMKkhSCdFkMrCUKKs8A/2npecBjAF/i/ShwH/AMICmAAQVJASQbs+BrTj/VxcbTOMd/VzcP4L+y5ucLgCQwAwqSwIQWaI7/WmLmAJgt3198K3ANcBVwNyAJwIDGJAFIrIgJ1uWuAP9P5HyAXbL/AjYDJAYGFCQG8iquuh38+wawEDgfCPFPUmGmfaIgaX6d89/ifQK4HWA3bHtA4sCAgsSBrAYk/TDKwLHKjwDOmkkyMKAgyUBSw5Kwzo8C7gcObljZcimOgiQXWmth9J3w8lLge8CwWnhckpMKkpKIr1C2n4MvNwKcUpZ0YUBB0oWUFp6ahjL/GZjUwrKnFllBkkpRaxJMRElvBnZsTYkzFlRBkpGoliTj/4y/DpgCrAcMAVovTd6Wogr2a95joRZtaeE/Z30VeAV4EVgG8P/RR58P43gBwAXLN4FGSpODhBUssTHAH5qRA2DwJI1ZXsM1BswdADdbEgyeRtRBk4MEddQYmY6S8Jd8a4BdoanAbgCncasgw+EEV/KJWQMOcUfyHOBy4FrgZUBSMQZ2hj/8JfPB7TmU5URPX+j/Tgn+vAvnjwGuB9jd8SlrETq803B38keBoYCkIgzsAj98G0BdgiRO9ab4Mht43FBuX75c9JbAv+OB0UAtpMmzW6y4NgkH1KcAWwBHA08CVZRxcOokgE9Y/iewLlBpaXKQtHV2629ocT8E2BX7foVbHzdYngpwwP/hCvvZ1+QgqTLvRfi2HJlwy8kBAKdwqyrsJnIL/0+AUVV0sslB0rbuVlL7ugIXZgAvJSWoyPkj4AfHgu+uiD+DbjQ5SNra3Rqs3NgBG99BAGfAqizbwLl5wB5VcrLJQaI7yZot7QZ8vXjNU5X8tgG84rrK9Kp41+QgqQrHVfLjZ1VypocvXOXnIiTvLKWLgqT0KijUgecLzc2W2UZQ/zWwjs2MXVvbUuwcykJ+DGwP01cC3MLPRVKCi5GPAJzqLkSaHCQauIdtQtwNzD1aRW8r2Qd5EnFhgHAD5T3A3QCDiJMTuUxMNDlIwJkkIAPfgC2C6xpbAZMA/tJPBd4DFLlyvjbymzKAw/FJ4RT3XOA64FLgaSCIKEiC0NgaI/ylZneH+D0QCRvtTsDewH4AdygXfSfnrNj+A/gOPucA5wMc13CDpbdo4O5NnRRjDLD7cyvAvWP/BHB/FvdlcexQhvDHf1/gAoB7xL4IrAd4iYLEizYppTCwFNdPA94FHALcD5Ql7B6eASwGjgOGAk7S5CDRYqJTU8gl8SpY/RXA8cOXAVO3B/oW2QjK3wL49CRfLJ5ZmhwkRfeJM5PewoQcy/DXfHfgqZLLvyPy/xNwMpCp/WdKVHKhfLPXncSXufz0bofpDwGFrXEkFIU/oF8FuPs49eGvJgdJAj86XTIDdyH/F0v2Icqeg/tbgJ7vClCQRHTps60McGv+9cDGSQQoSJKY0fk2MbAdCvs7oGvXS0HSpqagsvZiYAdcvBR4yxSxgqQXbbrWNgZmosCc9VpDFCRr0KEvYmD1TgFOVQ9KGUEyHLkXka/WSQaruTEHC1GSrwGXA4/nVCq2zXMA7kdbLdzjkodMhFHu4ZkKTAYmAJsB6wNRgKzE8RMA99bcCtwE3Ai8AUjEQDcGuBB5fOzCFjh+P3AQwK7SW8YTOOcj20LpKOAHPspJOnRuH+BsYBHAhTwfPAu904FNAKu07Q2OWfiycMLFt1DyNAz5tA+uaSQJf4Q5nuAajI/tTh3eqdjrGfxV57GPjIfSqcASgFuTPwtMAHzl7VD8EsDdo/y0/DKw0JL2MMBeCe8yE4GzAO4bswiD7uM0EHV9XI3xdnQRwK4St0SH+OWHmUEZgSPeUW4Exg2edTvQmMSNr6ak5p3kWGAGsMxYqM9Q3zVIuCp5LnAvcBhg+aWHeqpMR4o7AY5vJGLAhQGOcbnb92EXpY600/B9nEuQfBoKDwH8dNFDcpOMgfb1wOpbn8mSlNvGwGMo8N4Ax0C+sm+Wxj4K1vkIJO8gfESyDOEA6hfAMWVkrjxrzQAD5ZOGEkxLC5KtYPw24EBDJqFUOcbgs8v/Hcqg7LSGgetQ0is9Szu5V5BMgdE/AnwEs0pyIpzhjJpEDLgwcKZL4ljaIUlBsg0S/RYYG0tcpUPOqPFRTIkYyMrAXCRcnjVxLN1J3VbcOe64GkjcXx8zUObhcQOZ800YdRBOR97p6ehKT71OtRU4we6zjyz1UaqQzir48irAXR9muQQWuBBXF3A9pZvsjJO+ZeBjppL8GMhjxT2Lt08ikWub2L+zu3UojBB1Eq7Mf71ODsvXejEQD5J14PoZ9XJ/0NvZODph8JsOxEBABuJjkiNhd3xA25Ep3lr5Cpf5APdk8Zb3OrA5cBqwKRBC/gdG2OeOBvScMpaIgaAMcDXdtb+WlJ6DVE7Tvgfo1Vi5iewvQJIdn/NHwx5lF8BHnzoak5DB/KRWY5KIht1w4Nug4nrcXnwUEL9DRXkkfY7GBd5p4nYsx5zF+CSgIAEJFZVaBUk0JvlIADLPgw0uPP4YcHlwirs29wFuAEII71znAoeEMCYbYiBiwPpL/oXIkOGT2+P/F7DcRULpqrtlqMgMqrW7k/CXl1tQfOUcKJ7pqxzT44LZR4HfxM7pUAyUzgC7W+8ARnp6wlmq2Z663dRew0luplSgdGNH50phgAPsMYac74Hucwb9bqpRoFyBix/olkDnas/Av6AEwzxKwfFr4cIgWduQKxcg8xAGygHA1cDMPDKQzVIZ+F2puTtmzu4WN335ynZQHO2rnKL3V1zfH7gpJZ0ui4FcGWCQPGPIgfqHG/TTVBnA+wHz0hLquhjIiwE2cvbzLOOKaIU7Lx9fgeEPAb7bzPPyS3ZbwgCDhHJX/4fX38nQ2stLM7sSA5mD+PuzqyilGAjDQBQkNxvNhZwGTnLlWVzYG+AmSYkYKIyBKEh8H5KPHOUMFN9xlLdw4yQD5fG8M5J9MRAxEAXJfJx4MDrp+VnUg0+L4B8DhVsbJGIgdwaiIGFG3Jhokb2g/EGLAQddBjTHKC856CipGDAzwJV3LuJZNglyBX6o2ZPsBnZH0hWAxeduutrgmL0O6pTySY+2ssYz7lwvuchY4h2g/1mjDRf1PyDxwcDfXJSUVgxYGOBu4G6/rC7nOF071uKEh+7HoPMm4OJnr7S6k3hUQg1UzHcSlpHdpTnGwvK9Xd822nBVvwQKRd7BXP1T+oYxMA3l6fUrm/VaiKcdXan9aiDfdSdxZb4e6b3uJElFC/GEINcyRidlkOP5M2E7ayAnpVOQ5FhBJZoOGiSWlyjEG975JRDCJy1/DsT9cD1WkJRQcQVkGTRI6O+FgGvj6pb+0AIK35kFn5G51uC/gqST0WZ8Dx4kE8ELn+no1vBdzj0PG+OBomUkMpwHuPgapVWQFF1bxeTnFSTxFfdONxfhxLc6T3p83xA6FwBFLjLSTS4y8lkU63Yb2pKIgUQG1sWVxUD0C2v5PCUxl3wvTID5pY5l0J0k3zopy7rXnSSLswc4NrCkQFoFO/xlL0N2RKbc55XkW+d5BUkZtZR/nrkFCV2/DOhsSD7fOT7ZkgZLkBnIM+veNAVJCRVUQJa5BsmmKAC3m/gERqfO3bDDblwZchgy5R2t06fO7wqSMmon/zy9gqTXwD3uMvv0of7tGveH8V29ZcjFyPS4MjJWnu1hgO/B6vzV9f3OLSRlyenIuJffupOUVTP55ut1J3F1aRMo8M0qvRpY1mvs9nCbexnCVXnuBkjyVUFSRq3kn2chQcJiHAQkNS7X83yvFrfAlCFcleeO524+K0jKqJH88ywsSFiUc4BujcvnHB2fQKMlyPrI806g028FSQmVUUCWhQYJZ6fu69K4Ohtb1u98nxZX5ssQPiD2KBD3VUFSRk3kn6dXkGSd3ep0n90kblzkZwjZFkYuB4aHMOZo4ymk50slnnXUU/KWMOAbJKSHv/7/FpCnPWDrvID2XEw9jMTcDcD9XhIxEJyBH8BivKtiPT48uIfZDe6LpHyphLpb2TmrU0qv7laIAg6DkVsAa3BE+pxiHhPCMU8bn4bebZ66Uqs2A6UFCWnhthXXnbZRUHT7DLFF31JdZW3EtPgs3XQGSg0Susd3AYd4SItBsxLYDJCIgZAMeAWJZeDe6fw8nDiq86Tn93Wgd6KnrtTEQOUZ+Bo87NaFcj33BuxwalgiBkIw8K8wwn8I5doO9w+ReacN7ou60MOZbs5f2Wlc38WAJwP3Qq9bG0s7l9v4dDgcmuvpVKfTe8KORAxYGeA/f+psW1m+72PNuJf+aFz0jd6483fBTsixUy+fda25DPjuXv/HvCkZhwxCTA0fmbejst9oBtZC6XxfqD6pCGaOQCbxO4PPMfdXjSrCWeXRSAa4nODT7qhTyMZbvm8rxI7hshcYG9l6WlIoruH5BAlfHFKYfAQ5+TgZ13kdNjQlXFiVNSqjj3u2Pz5CUdiA+Crkxdkui/BJwrMtBqTbWga28Sz5EuoVOWv0JeTHO4NFZkCZvwoSMeDCwGSXxLG0i2LHhR3+HDnFu1A+x9x/w+lliRjIyoDvmPj4rBmETMcpYT7N6BMccR0+Yy8RA1kYYDedzwjF20/W49J6LSd6OhwvGF9HNB2QiIE0BvgyxHjbcTmemmY8r+sjYHixwfGokHx8mA98ScRALwY+g4tRm3H55OIjd6OXJh9Dzi4OJ6U9obQSKOO6MHCeZ1t7sAoFvMHT+XjAvAYbvJ1KxEASAw/gQrzNZD2+KMlgkee3Q2a+A6p4QefDDgdnEjHQycDbcSLeVlyO/6PTWFnfzzAUIl7gk8sqgPKtNAOWV/LOrErJ1oMjjwHxBu9zzKcYd6lKoeRHZRj4PjzxbU/rV6YUcORAz4J0Fn4B7JQ6G1ElUuXLagYW4m9nO8nynV34yskV8CiL82lpzqpcyeRQWQzsYGhT3yvL6V75ciX+ZUOh4sHDHccSMcDlgXi7cDnmWKaS8jl45VKQpLR88TWDTtJuBrjYnNRGep3nImIhD1r5VA93JN/sWbDOQv8JdjQt7FMLzdB5r6Ed/bnqFGwDB181FDAeLBqfVL228/PP8hL3Unb+ulLx74GChAEzyzVzpa89A3yMYjkQ/8F0OS5tU6ML8+x23WQoZJwQbluZ5pK50taegdkoQbwNuBw/WqfST4SzoWa7+KaVretUePnqzQAH3C8ALoERT3uKd84lKX7KUNh4wXn8CPBOQNJsBn6I4nXWvct3rq3UTn4Jj10K2SvtPbA1pnYMyOGsDByIhL3qP+3aHVkzqlo6DsIWGgsfJ4evXR1btULKHzMDu8HCCiBe167HfDirtsIXi4XYUh+RthD2fN+eUVsSG+z49ijb80BUvz6fL0F/ZN05+qKRhE7iSMp+dSdF/vfxeRHOSHXWr+v3bzaFy0sCkBEnj9sPPt8UclpYjiEo8xwgXqc+xythozFdcD574rsfpxd5fGmepH4MfBku96rXrNe+U7+i9/aY21aeC0ROnMQje2erqxVjgOOQ14F4HfoccyzDLlvjZE+UKARBcVJp732NY6qZBeKODG5CjNef7/GxzaSov1RHBCIpTu4y2GxM37TBlX90oLrnTvGhDeZpddFmByIrHijXNJ20mpdvFPx/OkC9cwPkVjXnIrP7ZwQgLB4kPJ6VOXclLJqBU5FhZ325fues5sFFO15mfpwGPBtwJapXev5SbVhmoZR3VwYm4uxfgV51l+XaF7pab8HJ0wOQFye4cdOCDWgDPwlQx62v16+AxFUBiGSw8DmULQFJNRgYDzesM5pzYaPxA/Us1cW3XLwCxO8Kvsc/zZKh0hTCAF/x41uP1GObmAhIBhjg45eczrWQSl1uqtTdBCSULHwOiFtHLPWp7UddKpFPIoaYKvxuF9s6VSwDJyE7S4DcBf23FetyfXJ7H1zlO4ItBPM2rZmu8up8OLK2/tjNKM/9euR8Gty0BAl1WztlWIEq/oSx/rQ4nKESRyDNQiPRCzLkoyT5MHCTse52zset5lk93Eg07ybTmkdL5UvEHd+WXsDVlS9hhRzkoO1eI+F8C4ekWAZORnaWINm9WHfrn9ssI+F87mDt+tNQqxJYHsu9pVYlrYizbOBPAJZfpn0rUpY2uLGLsa4ODUlSW+aPuTD4YyNxQYk3+tJ09QMNBVwK3csM+q1W3Ryl5zZp37vJM9DV3p9imtB8Qz2dUIyLzc3ltwbyGVx7NJeaypRsjKGOuHg8LnRJ2tLdinj7RXTg+alxiSdxDmrDHNJ2Jr0OJx7vPKnvbgzw8U/Lgzt3u2Wn1B4MbAYd3y7xIR75SaULA9cYKoGVx12pkvwYsAQJ93oFl7Z1t0igdeZjr+C1IIOhGODDcsGljUFyrZHFmUZ9qdeMgTYGCefR7zPU054GXanWkIE2BgmriVPBvrIlFNlvlrSEAQWJX0VP91OTVh0ZaGuQ8M0ZfPuGr2jrvC9zNdRra5CsQF3xpcu+ood5fJmroV5bg4RVxRcm+8qOUNQ+Ll/2aqbX5iCx3EnWRT1Pqlldy11PBtocJPM8OYvUpkQH+mw2A20OkmWo2scM1buDQVeqNWKgzUHCarJ0uRQkNWroFlfbHiS3G8jj/+2TtICBtVpQxl5FtGx9nwjDuwKW9ZZevrX12sZVK/iQqjlUsD+bID/u5ZI0g4Fc2nMuRmvGN18XpPf91qzSEtzNpT23fUxCrvUa04QWp9P9DChI+voeUGMQA70YUJD09T3SiyBdEwMKEgWJoiCFAQVJX9/iFI50ueUMKEj6+pa0vA2o+CkM5DJllpJn1S5zyzsXBPWDUbWacfcnl/ashtH/fmC+51ciBroyoCDpp+WpruzopBgAAwqS/magIFE4JDKgIFGQJDYOXehnQEHSz8OTahBiIIkBBUk/M+puJbUQne/LZcqshryOgM8b1NBvubwmA7n0CP4fN8Nmg6Vup4EAAAAASUVORK5CYII=" transform="translate(40)"></image><path fill="#000" d="M11.429 13l2 2-8 8H30v2.857H5.429l8 8-2 2L0 24.43zm98.142 0l-2 2 8 8H91v2.857h24.571l-8 8 2 2L121 24.43z"></path></g></svg>
            <span><?php the_field('swipe', 'options');?></span>
    </div>
    <a href="<?php echo home_url(); ?>" class="comparadise__home-btn"><?php the_field('go_to_home', 'options'); ?></a>
        <div class="comparadise__header">
            <div class="container">
                <div class="comparadise__header-navigation-modify"><?php the_field('comparison_modify', 'options');?></div>
                <div class="comparadise__header-navigation-title"><?php the_field('compare_your_options', 'options');?></div>
                <div class="comparadise__header-navigation-back"><?php the_field('close', 'options');?></div>
            </div>
        </div>
        <div class="comparadise__review-result-row">
            <div class="comparadise__review-result-item">
                
            </div>
        </div>
    </div>

</div>


<div class="btn-mobile-filter"><?php the_field('compare_filter', 'options')?></div>
<div class="btn-mobile-filter-bg"></div>
<?php get_footer(); ?>
<script type="text/javascript" src="/wp-content/themes/sobix2/assets/js/slick.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        // Считывает GET переменные из URL страницы и возвращает их как ассоциативный массив.
        function getUrlVars()
        {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        if(getUrlVars()['step'] == 'two'){
            $('.popup-compare').addClass('active');
        }

        //        $('.filter-list__check.radio input').on('click', function() {
        //            let parental = $(this).parent().parent().parent();
        //            $(parental).find('input').removeAttr('checked');
        //            $(this).prop('checked', true);
        //        });


        function ajaxDispatch(metaKey, order) {
            let allValue = [];
            $('.cat-list_item:checked').each(function() {
                var atrName = $(this).attr('name') + ':' + $(this).val();
                allValue.push(atrName);
            });

            allValueJson = JSON.stringify(allValue);
            $('.project-tiles').slick('unslick');
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'filter_projects',
                    acfValue: allValueJson,
                    sortingMetaKey: metaKey,
                    sortingMetaKeyOrder: order,
                },
                success: function(res) {

                    $('.project-tiles').html(res);
                    $('.project-tiles').slick($opts);
                }
            });
            console.log(allValue);
        }

        $('.sort-list-item').on('click', function() {
            $('.sort-list-item').removeClass('active');
            $(this).addClass('active');
            let metaKey = $(this).data('meta-key');
            let order = $(this).data('order');

            ajaxDispatch(metaKey, order);
        });

        $('.cat-list_item').on('click', function() {
            let metaKey = $('.sort-list-item.active').data('meta-key');
            let order = $('.sort-list-item.active').data('order');

            ajaxDispatch(metaKey, order);
        });

        $('.clear-sort').on('click', function() {
            $('.sort-list-item').removeClass('active');
            ajaxDispatch();
        });

        $('.filter-list__title').on('click', function() {
            $(this).toggleClass('active');
            $(this).siblings('.filter-list__check').toggleClass('active');
        });

        $('.sort-block__title').on('click', function() {
            $(this).toggleClass('active');
            $(this).siblings('.sort-list').toggleClass('active');
        });

        $('.btn-mobile-filter').on('click', function() {
            $('.filter-sidebar').addClass('active');
            $('.btn-mobile-filter-bg').addClass('active');
            $('.btn-mobile-filter-bg').addClass('btn-mobile-filter-bg');
        });

        $(document).mouseup(function(e) { // событие клика по веб-документу
            var div = $(".filter-sidebar"); // тут указываем ID элемента
            if (!div.is(e.target) // если клик был не по нашему блоку
                &&
                div.has(e.target).length === 0) { // и не по его дочерним элементам
                div.removeClass('active'); // скрываем его
                $('.btn-mobile-filter-bg').removeClass('active');
            }
        });
        $('.advertiser-close').on('click', function() {
            $('.filter-sidebar').removeClass('active');
            $('.btn-mobile-filter-bg').removeClass('active');
        });


        var $opts = {
            infinite: false,
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 577,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        }

        $('.project-tiles').slick($opts);
        //scroll add class
        var postion = $('.project-tiles').offset().top,
            height = $('.project-tiles').height();
        $(document).on('scroll', function() {
            var scroll = $(document).scrollTop();
            if (scroll > postion && scroll < (postion + height)) {
                $('.project-tiles__item').addClass('active');
            } else {
                $('.project-tiles__item').removeClass('active');
            }
        });


        //compare
        $('.comparadise__header-navigation-back').on('click', function() {
            $('.popup-compare').removeClass('active');
            $('.popup-compare__step-two').removeClass('visible');
            $('.popup-compare__step-one').removeClass('hidden');
            $('.comparadise__review-item').removeClass('active');
            $('.comparadise__review-row').removeClass('disabled');
            
            $('.popup-compare__scroll-img').removeClass('hidden');
            $('.popup-compare__scroll-img').addClass('visible');
            
        });
        
        $('.comparadise__header-navigation-modify').on('click', function() {
            $('.popup-compare__step-two').removeClass('visible');
            $('.popup-compare__step-one').removeClass('hidden');

            $('.popup-compare__scroll-img').removeClass('hidden');
            $('.popup-compare__scroll-img').addClass('visible');
        });
        
        $('.btn-mobile-compare').on('click', function() {
            $('.popup-compare').addClass('active');
        });


        $('.comparadise__review-item').on('click', function() {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('.comparadise__footer__btn').html($('.comparadise__review-item.active').length);
            } else {
                if ($('.comparadise__review-item.active').length <= 2) {
                    $(this).addClass('active');
                    $('.comparadise__footer__btn').html($('.comparadise__review-item.active').length);

                }
            }
            
            if ($('.comparadise__review-item.active').length == 3 ) {
                $('.comparadise__review-row').addClass('disabled');
            }else{
                $('.comparadise__review-row').removeClass('disabled');
            }
            
            if ($('.comparadise__review-item.active').length > 1) {
                $('.compare-button').addClass('active');
            } else {
                $('.compare-button').removeClass('active');
            }
            
            
            
        });
        
        let allcompare = [];
        $('.compare-button').on('click', function() {
            if ($(this).hasClass('active')) {
                $('.popup-compare__step-one').addClass('hidden');
                $('.popup-compare__step-two').addClass('visible');
                $('.comparadise__review-item.active').each(function() {
                    var compare_atrName = $(this).attr('data-post-id');
                    allcompare.push(compare_atrName);
                });
            }
                $.ajax({
                        type: 'POST',
                        url: '/wp-admin/admin-ajax.php',
                        dataType: 'html',
                        data: {
                            action: 'compare_reviews',
                            allcomparePostId: allcompare,
                        },
                        success: function(res) {
                            $('.comparadise__review-result-row').html(res);
                            //width blocks all heigth
                            let compareNameFilter = $('.compare-block').eq(0).children('.compare-name__filter');
                            for(let x = 0; x < compareNameFilter.length; x++){
                                let maxWidth = 0;
                                for(let i = 0; i < $('.compare-block').length; i++){
                                    let compareHeigth = $('.compare-block').eq(i).children('.compare-name__filter').eq(x).height();
                                    if(maxWidth < compareHeigth) {
                                        maxWidth = compareHeigth;
                                    }
                                }
                                // console.log( x + ':' + maxWidth);
                                for(let i = 0; i < $('.compare-block').length; i++){
                                    $('.compare-block').eq(i).children('.compare-name__filter').eq(x).height(maxWidth);
                                }
                            }
                            // console.log($('.compare-block').eq(0).children('.compare-name__filter').length);
                        }
                    });
            // console.log(allcompare);
            allcompare.splice(0,allcompare.length);
            // console.log(allcompare);
            
        });

        
            $('.popup-compare__step-two').on('click', function() {
                if ( $('.popup-compare__step-two').hasClass('visible') && $('.popup-compare__scroll-img').hasClass('visible') ) {
                    $('.popup-compare__scroll-img').removeClass('visible');
                    $('.popup-compare__scroll-img').addClass('hidden');
                }
                console.log('yes');
            });
            $( '.popup-compare__step-two' ).scroll(function(){
                if ( $('.popup-compare__step-two').hasClass('visible') && $('.popup-compare__scroll-img').hasClass('visible') ) {
                    $('.popup-compare__scroll-img').removeClass('visible');
                    $('.popup-compare__scroll-img').addClass('hidden');
                }
                console.log('scroll');
            });
        

        // $(document).scroll(function(){
		// 	// $('#compare-name').css({
        //     //     left: $(document).scrollLeft()
        //     // });
        //     // $('#compare-name').addClass('aaaaaaaa');
        //     alert('yes');
		// });
        // $( '.popup-compare__step-two' ).scroll(function(){
        //     $('#compare-name').css({
        //         left: $(document).scrollLeft()
        //     });
        // }); 
        // var lastScrollTop = 0;
        // $(window).scroll(function(event){
        // var st = $(this).scrollTop();
        // if (st > lastScrollTop){
        //     console.log($(document).scrollTop());
        //     alert('1231231');
        // } else {
        //     console.log($(document).scrollBottom());
        // }
        // lastScrollTop = st;
        // });

        $('.popup-compare').scroll(function() {
            height = $(this).scrollTop();
            width = $(this).scrollLeft();
            if(height > 1){
                $('.project-tiles__info').addClass('hidden');
                $('.popup-compare__scroll-img').addClass('hidden');
            }else{
                $('.project-tiles__info').removeClass('hidden');
            }

            if(width > 1){
                $('.popup-compare__scroll-img').addClass('hidden');
            }
        });

    });

</script>

</body>

</html>
