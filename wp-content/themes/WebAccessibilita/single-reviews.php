<?php get_header(); ?>

<div class="full-page-bg">
    <div class="container">
        <div class="breadcrumbs-disclosure">
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<div class="breadcrumbs">','</div>' );
                }
            ?>
        </div>

    </div>
    <div class="container single-page-bg">
        <main id="article" class="site-main reviews">
            <div class="full-page__top-title">
                <?php the_title( '<h1 class="full-page__title">', '</h1>' ); ?>
                <div class="full-page__autor-info">
                    <?php if($name_autor = get_field('name_autor')) :?>
                    <div class="full-page__autor"><?php the_field('by_autor', 'options'); ?> <?php echo $name_autor; ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="single-reviews-info">
                <?php if(is_single('accessibe')) { ?>
                    <a href="https://accessibe.go2cloud.org/aff_c?offer_id=9&aff_id=18" class="single-reviews-info__img"><?php the_post_thumbnail(); ?></a>
                <?php }else{ ?>
                    <div class="single-reviews-info__img"><?php the_post_thumbnail(); ?></div>
                <?php } ?>
                <div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('stars'); ?>"><span style=""></span></span></div>
                <div class="witget-all__raiting">
                    <span><?php the_field('raiting'); ?></span>
                    <div class="witget-all__raiting__desc"><?php the_field('our_score', 'options'); ?></div>
                </div>
                <div class="witget-all__btn">
                    <div id="spb-rts-461" class="bb-bubble-rts">Over 220 people choose this site today</div>
                    
                    <?php
                    if(!get_field('adoric_popup_active')){ ?>
                        <div href="<?php the_field('visit_site_url');?>" target="_blank" class="witget-all__link adoric_popup" id="home-visit-site"><?php the_field('top_3_learn_more', 'options'); ?></div>
                    <?php }else{ ?>
                        <a href="<?php the_field('visit_site_url');?>" target="_blank" class="witget-all__link" id="home-visit-site"><?php the_field('btn_visit', 'options')?> <?php echo str_replace(['Review', 'Rezension', 'Revue'], '', get_the_title()); ?></a>
                    <?php } ?>
                </div>
            </div>
            <div class="nav-content">
                <ul>
                    <?php 
                        if(get_field('overview_add_to_sub_menu') == true) echo'<li class="menuItem"><a href="#overview">'. get_field('title', 'options') .'</a></li>'; 
                        if( have_rows('full_page_content') ):
						  // перебираем данные
                        
						  while ( have_rows('full_page_content') ) : the_row(); 
                          //   if(get_sub_field('full_page_content__title') == 'Features' || get_sub_field('full_page_content__title') == 'Compliance' || get_sub_field('full_page_content__title') == 'Pricing' || get_sub_field('full_page_content__title') == 'Customer Support') :
                                if(get_sub_field('add_to_sub_menu') == true)  : ?>
                            <li class="menuItem"><a href="#<?php 
                                    $string = str_replace(' ', '', get_sub_field('full_page_content__title'));
                                    echo $string; 
                                ?>"><?php the_sub_field('full_page_content__title'); ?></a>
                            </li>
                    <?php
                                endif; 
                            endwhile;
					   endif; 
                       if( get_field('pros_and_cons_add_to_menu') == true ) echo'<li class="menuItem"><a href="#prosandcons">'. get_field('pros_and_cons', 'options') .'</a></li>'; 
                        ?>
                </ul>
            </div>
            <div class="rewiews-links-cta">
                <?php if($verdict = get_field('top_content_verdict')) {
                    echo '<div class="single-reviews-castom-row single-reviews-castom-row_verdict">';
					echo '<h2 class="full-page-content-item__title">'. get_field('verdict_title', 'options') .'</h2>';
				    echo '<div class="full-page-content-item__content">' . $verdict . '</div>' ;
                    echo '</div>';
			} ?>

                <div class="single-reviews-scoring__row">
                    <div class="single-reviews-scoring__item">
                        <div class="single-reviews-scoring__raiting"><?php the_field('functionality') ?></div>
                        <div class="single-reviews-scoring__title"><?php the_field('easy_to_use', 'options'); ?></div>
                    </div>
                    <div class="single-reviews-scoring__item">
                        <div class="single-reviews-scoring__raiting"><?php the_field('customer_support') ?></div>
                        <div class="single-reviews-scoring__title"><?php the_field('features', 'options'); ?></div>
                    </div>
                    <div class="single-reviews-scoring__item">
                        <div class="single-reviews-scoring__raiting"><?php the_field('affordability') ?></div>
                        <div class="single-reviews-scoring__title"><?php the_field('customer_service', 'options'); ?></div>
                    </div>
                    <div class="single-reviews-scoring__item">
                        <div class="single-reviews-scoring__raiting"><?php the_field('user-friendliness') ?></div>
                        <div class="single-reviews-scoring__title"><?php the_field('value_for_money', 'options'); ?></div>
                    </div>
                </div>

                <div class="single-reviews-castom-row">
                    <?php  
                    
                    if($overview = get_field('overview')) {
                        echo '<div id="overview" class="block-content-nav full-page-content-item">';
						echo '<h2 class="full-page-content-item__title">' . get_field('title', 'options') . '</h2>';
						echo '<div class="full-page-content-item__content">' . $overview . '</div>' ;
                        echo '</div>';
						} 
                    ?>
                    <?php
					// проверяем есть ли в повторителе данные
					if( have_rows('full_page_content') ):
						// перебираем данные
						while ( have_rows('full_page_content') ) : the_row(); ?>
                    <div id="<?php 
                                    $string = str_replace(' ', '', get_sub_field('full_page_content__title'));
                                    echo $string; 
                                ?>" class="full-page-content-item block-content-nav">
                        <h2 class="full-page-content-item__title"><?php the_sub_field('full_page_content__title'); ?></h2>
                        <div class="full-page-content-item__content"><?php the_sub_field('full_page_content__content'); ?></div>
                    </div>
                    <?php endwhile;
					else :
						// вложенных полей не найдено
					endif; 
if( have_rows('pros') || have_rows('cons')) { ?>
                    <div id="prosandcons" class="block-content-nav">
                        <?php
					// проверяем есть ли в повторителе данные
					if( have_rows('pros') ):
						echo '<div class="full-page-content-item">';
						echo '<h2 class="full-page-content-item__title">' .  get_field('pros', 'options') . '</h2>';
						echo '<ul class="full-page-pros__row">';
						// перебираем данные
						while ( have_rows('pros') ) : the_row(); ?>
                        <li class="full-page-pros__item"><?php the_sub_field('pros_item'); ?></li>
                        <?php endwhile;
						echo '</ul></div>';
					else :
						// вложенных полей не найдено
					endif; 
				?>

                        <?php
					// проверяем есть ли в повторителе данные
					if( have_rows('cons') ):
						echo '<div class="full-page-content-item">';
						echo '<h2 class="full-page-content-item__title">' .  get_field('cons', 'options') . '</h2>';
						echo '<ul class="full-page-cons__row">';
						// перебираем данные
						while ( have_rows('cons') ) : the_row(); ?>
                        <li class="full-page-cons__item"><?php the_sub_field('cons_item'); ?></li>
                        <?php endwhile;
						echo '</ul></div>';
					else :
						// вложенных полей не найдено
					endif; 
				?>
                        <a href="<?php the_field('visit_site_url');?>" target="_blank" class="single-reviews-castom-row__btn-visit">
                        <?php $visit_review_button = get_field('visit_review_button'); 
                            if($visit_review_button) {
                                echo $visit_review_button;
                            }else{
                                echo 'Visit ' . get_the_title();
                            } ?> 
                            <i class="baseline_trending_withe"></i>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php if($reviews_final = get_field('reviews_final')) : ?>
                <div class="single-reviews-final">
                    <?php echo $reviews_final; ?>
                </div>
            <?php endif; ?>
            <?php if( wp_is_mobile()) {?>
                <a href="<?php echo apply_filters( 'wpml_permalink', home_url('/comparison', 'https') ); ?>?step=two" class="single-reviews-castom-row__btn-visit single-reviews-castom-row__btn-visit_mobile"><?php the_field('compare_this_solution', 'options'); ?> <i class="baseline_trending_withe"></i></a>
                <?php }else{ ?>
                <a href="<?php echo apply_filters( 'wpml_permalink', home_url('/comparison', 'https') ); ?>" class="single-reviews-castom-row__btn-visit single-reviews-castom-row__btn-visit_pc"><?php the_field('compare_this_solution', 'options'); ?> <i class="baseline_trending_withe"></i></a>
            <?php }?>
            <div class="single-reviews-images_full_rewiews">
                <?php 
				$image = get_field('images_full_rewiews');
				$size = 'full'; // (thumbnail, medium, large, full или ваш размер)

				if( $image ) {
					echo wp_get_attachment_image( $image, $size );
				}
                
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
				?>
            </div>
        </main><!-- #main -->
        <?php if(!wp_is_mobile()) { ?>
        <div id="aside1" class="sidebar-full-page">
            <div class="sidebar_block">
                <div class="sidebar__inner">
                    <?php get_template_part( 'template-parts/witget', 'reviews-top5' ); ?>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div style="clear:both;"></div>
    </div>
</div>

<script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Review",
        "itemReviewed": {
            "@type": "CreativeWorkSeason",
            "name": "<?php the_title()?>",
            "image": {
                "@type": "ImageObject",
                "url": "/wp-content/uploads/2020/10/header-logo.svg"
            }
        },
        "reviewRating": {
            "@type": "Rating",
            "ratingValue": "5"
        },
        "author": {
            "@type": "Person",
            "name": "<?php the_field('name_autor') ?>"
        }
    }

</script>
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(function($) {

        const section = $('.block-content-nav'),
            nav = $('.nav-content'),
            navHeight = nav.outerHeight(); // получаем высоту навигации 

        // поворот экрана 
        window.addEventListener('orientationchange', function() {
            navHeight = nav.outerHeight();
        }, false);

        $(window).on('scroll', function() {
            const position = $(this).scrollTop();

            section.each(function() {
                const top = $(this).offset().top - navHeight - 5,
                    bottom = top + $(this).outerHeight();

                if (position >= top && position <= bottom) {
                    nav.find('a').removeClass('active');
                    section.removeClass('active');

                    $(this).addClass('active');
                    nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');
                }
            });
        });

        nav.find('a').on('click', function() {
            const id = $(this).attr('href');

            $('html, body').animate({
                scrollTop: $(id).offset().top - navHeight
            }, 487);

            return false;
        });

        $('.shortcode_url_brand_content').attr('data-gtm-url', function() {
            var url = $(location).attr('href');
            var sectionTitle = $(this).parent().parent().prev().text();
            return url + '##' + sectionTitle;
        });

    });

</script>
<?php 
    if( domain_user() == 'bestwebaccessibility.com' ) { 
        the_field('сode_end_body_bestwebaccessibility', 'options'); 
    }else{
        the_field('сode_end_body', 'options'); 
    }
?>
</body>

</html>
