<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

?>

<article id="post-<?php the_ID(); ?>" class="article-content__item <?php if(!get_the_post_thumbnail()) echo 'article-content__item_no-img' ?>">
	<div class="article-content__img">
        <?php if(get_the_post_thumbnail()):?>
	        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
	    <?php endif; ?>
	    <div class="article-content__date"><?php the_date(); ?></div>
	</div>
	<div class="article-content__text">
		<a href="<?php the_permalink(); ?>" class="article-content__title"><?php the_title( '<h2 class="article-content__title">', '</h2>' ); ?></a>
		<div class="article-content__content"><?php $content = the_field('descriptions_text'); ?></div>
		<a href="<?php the_permalink(); ?>" class="article-content__link"><?php the_field('bottom_read_more', 'options')?> ></a>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
