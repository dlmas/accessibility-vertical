<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

?>

<div class="container">
      <div class="breadcrumbs-disclosure">
					<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
					<!-- <div class="header_home__disclosure disclosure-all-page">Advertising Disclosure</div> -->
</div>
<div class="single-page-bg"><article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="full-page__top-title">
                <?php the_title( '<h1 class="full-page__title">', '</h1>' ); ?>
            </div>
                            
                <?php the_post_thumbnail(); ?>
            
                <div class="entry-content">
                    <?php
                    the_content();
            
                    wp_link_pages(
                        array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sobix' ),
                            'after'  => '</div>',
                        )
                    );
                    ?>
                </div><!-- .entry-content -->
            
            
            </article><!-- #post-<?php the_ID(); ?> --></div>
    
    </div>