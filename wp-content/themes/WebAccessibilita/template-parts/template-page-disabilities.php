<?php
/*
Template Name: Template page Disabilities
*/

get_header();
?>
<div class="full-page-bg">
    <main id="primary" class="site-main reviews article">

        <?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

   <div class="container">
    <?php 
                            $query = new WP_Query( array(
                                'post_type' => 'article',
                                'posts_per_page' => -1,
                                'post_status'=> 'publish', 
//                                'order'=>'DESC',
                                'article_category' => 'disabilities',

	                           ) ); 
                        ?>


    <?php if( $query->have_posts() ) : ?>
            <?php while ( $query->have_posts() ) : $query->the_post();
                get_template_part( 'template-parts/content', get_post_type() );
            endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</div>
    </main><!-- #main -->

</div>
<?php get_footer(); ?>
</body>

</html>
