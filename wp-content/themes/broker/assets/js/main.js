jQuery(document).ready(function($) {

    // $('.block-polls__row').on('click', '.block-polls__item:not(.active)', function() {
    //     $(this).addClass('active').siblings().removeClass('active')
    //         .closest('div.witget-broker-stages__block').find('div.witget-broker-stages__content').removeClass('active').eq($(this).index()).addClass('active');
    // });
    $('.block-polls__row .block-polls__item').on('click', function() {
        $(this).toggleClass('active');
    });
    // $('.block-polls__title').on('click', function() {
    //     $('.block-polls__item').css('transform', 'translate(-980px,0)');
    // });
    var leftUIEl = $('.witget-broker-priew');
	var rightUIEl = $('.witget-broker-priew-next');

    var elementsList = $('.block-polls__row');
    var pixelsOffset = 192;
	var currentLeftValue = 0;

    var elementsCount = elementsList.find('.block-polls__item').length;
	var minimumOffset = - ((elementsCount - 5) * pixelsOffset);
 
	var maximumOffset = 0;

	leftUIEl.click(function() {
        if (currentLeftValue != maximumOffset) {
            currentLeftValue += 192 * 5;
            elementsList.animate({ left : currentLeftValue + "px"}, 500);
            var elementsTabsItem = $('.witget-broker__tabs-row .witget-broker__tabs-item');
            var elementsTabs = $('.witget-broker__tabs-row .witget-broker__tabs-item.active').index();
            elementsTabs--;
            elementsTabsItem.removeClass('active');
            elementsTabsItem.eq(elementsTabs).addClass('active');

            var elementsTabsWidth = -190 * (- elementsTabs);
            $('.witget-broker__bg').css('left', + elementsTabsWidth);

            var elementsTabsTitleItem = $('.block-polls__title-row .block-polls__title-item');
            var elementsTitile = $('.block-polls__title-row .block-polls__title-item.active').index();
            elementsTitile--;
            elementsTabsTitleItem.removeClass('active');
            elementsTabsTitleItem.eq(elementsTitile).addClass('active');
        }
	});

	rightUIEl.click(function() {
        if (currentLeftValue != minimumOffset) {
            currentLeftValue -= 192 * 5;
            elementsList.animate({ left : currentLeftValue + "px"}, 500);
            var elementsTabsItem = $('.witget-broker__tabs-row .witget-broker__tabs-item');
            var elementsTabs = $('.witget-broker__tabs-row .witget-broker__tabs-item.active').index();
            elementsTabs++;
            elementsTabsItem.removeClass('active');
            elementsTabsItem.eq(elementsTabs).addClass('active');
            
            var elementsTabsWidth = 190 * (elementsTabs);
            $('.witget-broker__bg').css('left', + elementsTabsWidth);

            var elementsTabsTitleItem = $('.block-polls__title-row .block-polls__title-item');
            var elementsTitile = $('.block-polls__title-row .block-polls__title-item.active').index();
            elementsTitile++;
            elementsTabsTitleItem.removeClass('active');
            elementsTabsTitleItem.eq(elementsTitile).addClass('active');
        }
	});
                            //width blocks all heigth
                            let compareNameFilter = $('.sort-table__item').eq(0).children('.sort-table__sub-item');
                            
                            
                            for(let x = 0; x < compareNameFilter.length; x++){
                                let maxWidth = 0;
                                for(let i = 0; i < $('.sort-table__item').length; i++){
                                    let compareHeigth = $('.sort-table__item').eq(i).children('.sort-table__sub-item').eq(x).outerHeight();
                                    if(maxWidth < compareHeigth) {
                                        maxWidth = compareHeigth;
                                    }
                                }
                                //  console.log( x + ':' + maxWidth);
                                
                                for(let i = 0; i < $('.sort-table__item').length; i++){
                                    $('.sort-table__item').eq(i).children('.sort-table__sub-item').eq(x).outerHeight(maxWidth);
                                }
                            }
                            //  console.log($('.compare-block').eq(0).children('.sort-table__sub-item').length);
        //comparison carusel
        var sortBrokerRowWidth = $('.sort-broker__row').width();
        var sortBrokerNum = Math.trunc(sortBrokerRowWidth / 1136);
        var sortBrokerHtml;
        for(let i=0; i<sortBrokerNum; i++){
            if(i==0){
                $('.sort-broker__markew-row').append('<div class="sort-broker__markew-item active"></div>');
            }else{
                $('.sort-broker__markew-row').append('<div class="sort-broker__markew-item"></div>');
            }
        }
        
        

        var leftUIElCom = $('.sort-broker__nav-priew');
        var rightUIElCom = $('.sort-broker__nav-next');
    
        var elementsListCom = $('.sort-broker__row');
        var pixelsOffsetCom = 232;
        var currentLeftValueCom = 0;
    
        var elementsCountCom = elementsListCom.find('.sort-broker__item').length / 8;
        var minimumOffsetCom = - ((elementsCountCom - 5) * pixelsOffsetCom);
        var maximumOffsetCom = 0;
        leftUIElCom.click(function() {
            if (currentLeftValueCom != maximumOffsetCom) {
                currentLeftValueCom += 232 * 5;
                elementsListCom.animate({ left : currentLeftValueCom + "px"}, 500);
                var elementsTabsItemCom = $('.sort-broker__markew-row .sort-broker__markew-item');
                var elementsTabsCom = $('.sort-broker__markew-row .sort-broker__markew-item.active').index();
                elementsTabsCom--;
                elementsTabsItemCom.removeClass('active');
                elementsTabsItemCom.eq(elementsTabsCom).addClass('active');
            }
        });
    
        rightUIElCom.click(function() {
            if (currentLeftValueCom != minimumOffsetCom) {
                currentLeftValueCom -= 232 * 5;
                elementsListCom.animate({ left : currentLeftValueCom + "px"}, 500);

                var elementsTabsItemCom = $('.sort-broker__markew-row .sort-broker__markew-item');
                var elementsTabsCom = $('.sort-broker__markew-row .sort-broker__markew-item.active').index();
                elementsTabsCom++;
                elementsTabsItemCom.removeClass('active');
                elementsTabsItemCom.eq(elementsTabsCom).addClass('active');
            }
        });
});