<?php
//удаляем лишнее
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
function remove_comment_reply_script () {
	wp_deregister_script ('comment-reply');
}
add_action ('init', 'remove_comment_reply_script');
add_action( 'after_setup_theme', 'prefix_remove_unnecessary_tags' );
function prefix_remove_unnecessary_tags(){

    // REMOVE WP EMOJI
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );


    // remove all tags from header


    // Удаляет ссылки RSS-лент записи и комментариев
    remove_action( 'wp_head', 'feed_links', 2 ); 
    // Удаляет ссылки RSS-лент категорий и архивов
    remove_action( 'wp_head', 'feed_links_extra', 3 ); 
    // Удаляет RSD ссылку для удаленной публикации
    remove_action( 'wp_head', 'rsd_link' ); 
    // Удаляет ссылку Windows для Live Writer
    remove_action( 'wp_head', 'wlwmanifest_link' ); 
    // Удаляет короткую ссылку
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0); 
    // Удаляет информацию о версии WordPress
    remove_action( 'wp_head', 'wp_generator' ); 
    // Удаляет ссылки на предыдущую и следующую статьи
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
    remove_action( 'wp_head',      'rest_output_link_wp_head'              );
    remove_action( 'wp_head',      'wp_oembed_add_discovery_links'         );
    remove_action( 'template_redirect', 'rest_output_link_header', 11 );
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
    // language
    add_filter('multilingualpress.hreflang_type', '__return_false');
    add_filter('multilingualpress.render_hreflang', function() { return false; });
    
}
add_filter( 'wpseo_opengraph_url' , '__return_false' );

// Регистрирую стили
function sobix_scripts() {
	wp_register_style( 'my_style', get_template_directory_uri() . '/assets/css/main.css');
	wp_register_style( 'my_media', get_template_directory_uri() . '/assets/css/media.css');
	// wp_register_style( 'my_montserrat', get_template_directory_uri() . '/assets/fonts/Montserrat/stylesheet.css');
	// wp_register_style( 'my_poppins', get_template_directory_uri() . '/assets/fonts/poppins/stylesheet.css');

	// Подключаю стили
	wp_enqueue_style( 'my_style');
	wp_enqueue_style( 'my_media');
	// wp_enqueue_style( 'my_montserrat');
	// wp_enqueue_style( 'my_poppins');
	wp_enqueue_script( 'main-script', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'sobix_scripts' );

//подключение шрифтов Open Sans и Roboto от Google start
function wph_add_google_fonts() {
    if ( !is_admin() ) {
        // wp_register_style('google-poppins', 'https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;600;700&display=swap', array(), null, 'all');
        // wp_register_style('google-sarabun', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;600&display=swap', array(), null, 'all');
        // wp_enqueue_style('google-poppins');
		// wp_enqueue_style('google-sarabun');
    }
}
add_action('wp_enqueue_scripts', 'wph_add_google_fonts');

add_theme_support( 'post-thumbnails' ); // для всех типов постов подключаем миниатюры


/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method() {
	wp_enqueue_script( 'jquery' );
}

/**
 * Use ACF image field as avatar
 * @author Mike Hemberger
 * @link http://thestizmedia.com/acf-pro-simple-local-avatars/
 * @uses ACF Pro image field (tested return value set as Array )
 */


//options page admin
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme homepage Settings',
		'menu_title'	=> 'Homepage',
		'parent_slug'	=> 'theme-general-settings',
	));
    
}

 /* Новый тип записи */
 add_action('init', 'register_postType_reviews');
 function register_postType_reviews() {
	 $labels = array(
		 'name' => 'Reviews',
		 'singular_name' => 'Reviews',
		 'add_new' => 'Add Reviews',
		 'add_new_item' => 'Add new Reviews',
		 'edit_item' => 'Edit Reviews',
		 'new_item' => 'New Reviews',
		 'all_items' => 'All Reviews',
		 'view_item' => 'View Reviews',
		 'search_items' => 'Search Reviews',
		 'not_found' => 'Reviews not found',
		 'not_found_in_trash' => 'No in basket Reviews',
		 'menu_name' => 'Reviews'
	 );
	 $args   = array(
		 'labels' => $labels,
		 'rewrite' => array( 'slug' => 'reviews'),
		 'public' => true,
		 'show_ui' => true,
		 'has_archive' => false,
		 'publicly_queryable' => true,
		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		 'menu_position' => 5,
		 'menu_icon' => 'dashicons-feedback',
		 'show_in_nav_menus' => true, //выводить в меню
		 'show_in_rest'       => true, //вкл. гутенберг
		 'supports' => array(
			 'title',
			 'editor',
			 'thumbnail',
			 'excerpt',
             'comments'
		 )
	 );
	 register_post_type('reviews', $args);
 }
 
 function register_taxonomies_reviews() {
	 $labels = array(
		 'name'              => _x( 'Categories Reviews', 'taxonomy general name' ),
		 'singular_name'     => _x( 'Categories Reviews', 'taxonomy singular name' ),
		 'search_items'      => __( 'Search ' ),
		 'all_items'         => __( 'All categories' ),
		 'parent_item'       => __( 'Parental categories' ),
		 'parent_item_colon' => __( 'Parental categories:' ),
		 'edit_item'         => __( 'Edit categories' ),
		 'update_item'       => __( 'Refresh categories' ),
		 'add_new_item'      => __( 'Add new categories' ),
		 'new_item_name'     => __( 'New name categories' ),
		 'menu_name'         => __( 'Categories Reviews' ),
	 );
 
	 $args =  array(
		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		 'labels'            => $labels,
		 'show_ui'           => true,
		 'show_admin_column' => true,
		 'query_var'         => true,
         'show_in_rest' => true,
		 'supports' => array( 'title', 'editor', 'comments', 'post-templates'),
		 //'rewrite' => array( 'slug' => 'housing', // ярлык
							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	 );
  
	 register_taxonomy( 'reviews_category', 'reviews', $args );
 }
 add_action( 'init', 'register_taxonomies_reviews', 0 );

/* Новый тип записи */
add_action('init', 'register_postType_news');
function register_postType_news() {
	$labels = array(
		'name' => 'News',
		'singular_name' => 'News',
		'add_new' => 'Add News',
		'add_new_item' => 'Add new News',
		'edit_item' => 'Edit News',
		'new_item' => 'New News',
		'all_items' => 'All News',
		'view_item' => 'View News',
		'search_items' => 'Search News',
		'not_found' => 'News not found',
		'not_found_in_trash' => 'No in basket News',
		'menu_name' => 'News'
	);
	$args   = array(
		'labels' => $labels,
		'rewrite' => array( 'slug' => 'news'),
		'public' => true,
		'show_ui' => true,
		'has_archive' => false,
		'publicly_queryable' => true,
		//'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		'menu_position' => 6,
		'menu_icon' => 'dashicons-format-aside',
		'show_in_rest'       => true, //вкл. гутенберг
		'supports' => array(
			'title',
			'editor',
			'thumbnail'
		)
	);
	register_post_type('news', $args);
}

function register_taxonomies_news() {
	$labels = array(
		'name'              => _x( 'Categories News', 'taxonomy general name' ),
		'singular_name'     => _x( 'Categories News', 'taxonomy singular name' ),
		'search_items'      => __( 'Search ' ),
		'all_items'         => __( 'All categories' ),
		'parent_item'       => __( 'Parental categories' ),
		'parent_item_colon' => __( 'Parental categories:' ),
		'edit_item'         => __( 'Edit categories' ),
		'update_item'       => __( 'Refresh categories' ),
		'add_new_item'      => __( 'Add new categories' ),
		'new_item_name'     => __( 'New name categories' ),
		'menu_name'         => __( 'Categories News' ),
	);

	$args =  array(
		'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_in_rest' => true,
		//'rewrite' => array( 'slug' => 'housing', // ярлык
						   //'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	);
 
	register_taxonomy( 'news_category', 'news', $args );
}
add_action( 'init', 'register_taxonomies_news', 0 );

 


add_action('after_setup_theme', function(){
	register_nav_menus( array(
	  'main_menu' => __( 'Primary menu', 'crea' ),
	  'foot_menu' => __( 'Footer menu', 'crea' ), 
	) );
  });

// ************* Remove default Posts type since no blog *************
// Remove side menu
add_action( 'admin_menu', 'remove_default_post_type' );

function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}

// Remove +New post in top Admin Menu Bar
add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );

function remove_default_post_type_menu_bar( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'new-post' );
}

// Remove Quick Draft Dashboard Widget
add_action( 'wp_dashboard_setup', 'remove_draft_widget', 999 );

function remove_draft_widget(){
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}

// End remove post type

function filter_projects() {
    $services_value = json_decode(stripslashes($_POST['acfValue']));

  $services_array = [
      'relation' => 'AND',
  ];

    foreach($services_value as $service_value) {
        $key = explode(':', $service_value)[0];
        $value = explode(':', $service_value)[1];
        
        $services_value_arrey[$key][] = $value;
    }
    

  $i = 0;
  foreach($services_value_arrey as $key => $value) {
        array_push($services_array, ['relation' => 'OR']);
        
        foreach($value as $value_sub){ //прокрутиль по кол-ву value
            
                    array_push($services_array[$i], [
                            'key'       => $key,
                            'value'     => $value_sub,
                            'compare'   => 'LIKE',
                    ]);
      }
      $i++;
  }
    


    // echo '<pre>'; print_r($services_array); echo '</pre>';

    //$args['meta_query'] = $services_array;
    $args = [
      'post_type' => 'reviews',
      'post_status' => 'publish',
      'posts_per_page' => 5,
      'meta_query' => $services_array,
            // 'meta_key'       => 'raiting',
            'orderby'        => 'meta_value',
            'order'          => 'DESC',
			//'suppress_filters' => true //all lanuage
      
    ];
    if(isset( $sortingMetaKey) && isset( $sortingMetaKey)){
        $args['meta_key'] = $sortingMetaKey;
        $args['orderby'] = 'meta_value';
        $args['order'] = $sortingMetaKeyOrder;
    }
  $ajaxposts = new WP_Query( $args );
 
    //echo '<pre>'; print_r($ajaxposts); echo '</pre>';
  if($ajaxposts->have_posts()) {
    while($ajaxposts->have_posts()) : $ajaxposts->the_post();
	include (locate_template( 'template-parts/hp-witget-broker.php' ));
    endwhile;
  } else {
    echo '<div class="filter-no-match">Sorry, but there are no such results.</div>';
  }

  exit;
}
add_action('wp_ajax_filter_projects', 'filter_projects');
add_action('wp_ajax_nopriv_filter_projects', 'filter_projects');