<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="" type="image/png">
    <!-- <link rel="stylesheet" href="/wp-content/themes/broker/assets/css/main.css">
    <link rel="stylesheet" href="/wp-content/themes/broker/assets/css/media.css">
    <script src="/wp-content/themes/broker/assets/js/main.js"></script> -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100;300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Krona+One&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>
<header>
        <div class="header__top-block container" >
            <div class="header__logo"><img src="/wp-content/themes/broker/assets/img/logo.png" alt="logo" width="170" height="26"></div>
            <nav class="main-menu">
                <ul>
                    <li class="menu-item menu-item-has-children">
                        <a href="">Market News</a>
                        <ul class="sub-menu has-sub-menu-caption">
                        <li class="menu-item-has-children">
                            <a href="">Top Stock Brokers</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="">Dividend stocks</a>
                                </li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="">Top Stock Brokers</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="">Dividend stocks</a>
                                </li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="">Top Stock Brokers</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="">Dividend stocks</a>
                                </li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="">Top Stock Brokers</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="">Dividend stocks</a>
                                </li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                                <li><a href="">Dividend stocks</a></li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                    <li class="menu-item">
                        <a href="">Broker Reviews</a>
                    </li>
                    <li class="menu-item">
                        <a href="">Forex</a>
                    </li>
                    <li class="menu-item">
                        <a href="">Stocks</a>
                    </li>
                    <li class="menu-item">
                        <a href="">Cryptocurrency</a>
                    </li>
                    <li class="menu-item">
                        <a href="">CFD</a>
                    </li>
                    <li class="menu-item">
                        <a href="">Tools</a>
                    </li>
                    <li class="menu-item">
                        <a href="">Education</a>
                    </li>
                </ul>
            </nav>
            <a href="" class="header__btn">Start trading</a>
        </div>
        <div class="header__bottom-block">
            <ul class="sub-menu-bottom">
                <li>
                    <a href="">Business News</a>
                </li>
                <li><a href="">Top Broker Reviews</a></li>
                <li><a href="">Stock Markets</a></li>
                <li><a href="">Popular Cryptocurrency</a></li>
                <li><a href="">Guides</a></li>
            </ul>
        </div>
    </header>
