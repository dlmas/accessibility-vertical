<?php get_header(); ?>
<div class="container">
    <div class="witget-broker-stages">
        <div class="witget-broker-stages__title">Find the right broker for you</div>
            <div class="witget-broker__tabs-row">
                <div class="witget-broker__tabs-item active"><div class="witget-broker__bg" data-number="1"></div></div>
                <div class="witget-broker__tabs-item"></div>
                <div class="witget-broker__tabs-item"></div>
                <div class="witget-broker__tabs-item"></div>
                <div class="witget-broker__tabs-item"></div>
            </div>
            <div class="block-polls">
                <div class="block-polls__title">
                    <div class="witget-broker-priew"></div>
                    <div class="block-polls__title-row">
                        <div class="block-polls__title-item active">What do you want to trade?</div>
                        <div class="block-polls__title-item">Where do you want to trade?</div>
                        <div class="block-polls__title-item">what technology do you want to use?</div>
                        <div class="block-polls__title-item">What financial regulatory authority to follow?</div>
                        <div class="block-polls__title-item">What amount do you want to invest?</div>
                    </div>
                    <div class="witget-broker-priew-next"></div>
                </div>
                
                <div class="sidebar-block">
                    <div class="block-polls__row">
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/homepage-step-option-01.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_one:Forex">Forex</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/homepage-step-option-02.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_one:CFD">CFD</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/homepage-step-option-03.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_one:Stocks">Stocks</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/homepage-step-option-04.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_one:cryptocurrency">cryptocurrency</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/homepage-step-option-05.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_one:S&P 500">S&P 500</div>
                        </div>
                        <!-- setap 2-->
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step2-1.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_two:United States of America">United States of America</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step2-2.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_two:Europe">Europe</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step2-3.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_two:Australia">Australia</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step2-4.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_two:Canada">Canada</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step2-5.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_two:Asia">Asia</div>
                        </div>
                        <!-- setap 3 -->
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step3-1.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_three:Desktop">Desktop</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step3-2.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_three:Application">Application</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step3-3.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_three:Social Trading">Social Trading</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step3-4.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_three:Personal Platform">Personal Platform</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step3-5.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_three:Mobile">Mobile</div>
                        </div>
                        <!-- setap 4 -->
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-1.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_four:ASIC">ASIC</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-2.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_four:CySEC">CySEC</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-3.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_four:IIROC">IIROC</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-4.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_four:AFM">AFM</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-5.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_four:FINRA">FINRA</div>
                        </div>
                        <!-- setap 4 -->
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-1.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_five:200 >"> 200 ></div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-2.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_five:200-1,000">200-1,000</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-3.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_five:1,000-5,000">1,000-5,000</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-4.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_five:5,000-10,000">5,000-10,000</div>
                        </div>
                        <div class="block-polls__item">
                            <div class="block-polls__item-img"><img src="/wp-content/themes/broker/assets/img/step4-5.webp" alt=""></div>
                            <div class="block-polls__item-title" data-choices="step_five:+10,000">+10,000</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="witget-broker-stages__hr"></div>
            <div class="best-results">
                <div class="best-results__title">best results for you</div>
                <div class="best-results__row">
                    <?php
                    
                    $args = array(
                        'post_type' => 'reviews',
                        'posts_per_page' => 5,
                        'order' => 'DESC',
                    );
                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) : 
                        while ( $query->have_posts() ) : $query->the_post(); 
                            include (locate_template( 'template-parts/hp-witget-broker.php' ));
                        endwhile;
                    endif;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
    </div>
    <div class="news-one">
        <div class="news-one__row">
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <div class="business-news">
        <div class="business-news__title title__h2">Business news</div>
        <div class="news-one__row">
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
        </div>
    </div>

    <div class="trending-broker">
        <div class="trending-broker__title title__h2">Trending broker reviews</div>
        <div class="trending-broker__row">
          <div class="trending-broker__item">
              <div class="trending-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
              <a href="" class="trending-broker__linck">EuropeFX</a>
          </div>
          <div class="trending-broker__item">
              <div class="trending-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
              <a href="" class="trending-broker__linck">EuropeFX</a>
          </div>
          <div class="trending-broker__item">
              <div class="trending-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
              <a href="" class="trending-broker__linck">EuropeFX</a>
          </div>
          <div class="trending-broker__item">
              <div class="trending-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
              <a href="" class="trending-broker__linck">EuropeFX</a>
          </div>
          <div class="trending-broker__item">
              <div class="trending-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
              <a href="" class="trending-broker__linck">EuropeFX</a>
          </div>
        </div>

    </div>

    <div class="business-news">
        <div class="business-news__title title__h2">Cryptocurrency news</div>
        <div class="business-news__cat-row">
            <a href="" class="business-news__cat-item">Crypto tags</a><a href="" class="business-news__cat-item">Crypto tags</a><a href="" class="business-news__cat-item">Crypto tags</a><a href="" class="business-news__cat-item">Crypto tags</a><a href="" class="business-news__cat-item">Crypto tags</a><a href="" class="business-news__cat-item">Crypto tags</a>
        </div>
        <div class="news-one__row">
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <div class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></div>
                <div class="news-one__cat-name">Category</div>
                <div class="news-one__title">Jim Cramer bets stocks will gain from the collapse in crypto, speculative assets</div>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
        </div>
    </div>
    <div class="live-stock">
        <div class="live-stock__left">
            <div class="title__h2">Live stock prices</div>
            <div class="live-stock__witget">
                <img src="/wp-content/themes/broker/assets/img/witget.PNG" alt="">
            </div>
        </div>
        <div class="live-stock__right">
            <a href="" class="live-stock__btn-brocer">BEST STOCK BROKERS</a>
            <div class="live-stock__row">
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($) {
        function ajaxDispatch(metaKey) {
            let allValue = [];
            $('.block-polls__item.active').each(function() {
                var atrName = $(this).children('.block-polls__item-title').data('choices');
                allValue.push(atrName);
            });

            allValueJson = JSON.stringify(allValue);
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'filter_projects',
                    acfValue: allValueJson,
                    // sortingMetaKey: metaKey,
                    // sortingMetaKeyOrder: order,
                },
                success: function(res) {
                    $('.best-results__row').html(res);
                }
            });
            console.log(allValue);
        }
        $('.block-polls__item').on('click', function() {
            let metaKey =  $(this).children('.block-polls__item-title').data('choices');
            ajaxDispatch(metaKey);
        });
    });
</script>
<?php get_footer(); ?>