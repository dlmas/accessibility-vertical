<?php get_header(); ?>
<div class="container">
    <div class="live-stock">
        <div class="live-stock__left">
            <div class="title__h2">Live stock prices</div>
            <div class="live-stock__witget">
                <img src="/wp-content/themes/broker/assets/img/witget.PNG" alt="">
            </div>
        </div>
        <div class="live-stock__right">
            <a href="" class="live-stock__btn-brocer">BEST STOCK BROKERS</a>
            <div class="live-stock__row">
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
                <div class="live-stock__item">
                    <div class="">
                        <div class="live-stock__num"></div>
                        <div class="live-stock__logo-title">
                            <div class="live-stock__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                            <div class="live-stock__title">EuropeFX</div>
                        </div>
                    </div>
                    <a href="" class="live-stock__link">Visit broker</a>
                </div>
            </div>
        </div>
    </div>

    <div class="news-one">
        <div class="news-one__row">
            <div class="news-one__item">
                <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                <div class="news-one__cat-name">Category</div>
                <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                <div class="news-one__cat-name">Category</div>
                <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                <div class="news-one__cat-name">Category</div>
                <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                <div class="news-one__cat-name">Category</div>
                <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
            <div class="news-one__item">
                <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                <div class="news-one__cat-name">Category</div>
                <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                <div class="news-one__date">03 Apr 2021</div>
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <div class="news-marcets">
        <div class="business-news__title title__h2">American Markets</div>
        <div class="news-marcets__row">
            <div class="news-marcets__item">
                <div class="news-marcets__news-row">
                    <div class="news-marcets__news-item">
                        <a href="" class="news-marcets__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                        <div class="news-marcets__cat-name">Category</div>
                        <a href="" class="news-marcets__title">Walmart Q1 FY 2022 Earnings Report </a>
                        <div class="news-marcets__date">03 Apr 2021</div>
                    </div>
                    <div class="news-marcets__news-item">
                        <div class="news-marcets__cat-name">Category</div>
                        <a href="" class="news-marcets__title">Jim Cramer bets stocks will gain from</a>
                        <div class="news-marcets__date">03 Apr 2021</div>
                    </div>
                </div>
            </div>
            <div class="news-marcets__item">
                <div class="best-broker__title">Best brokers American Market</div>
                <div class="best-broker__row">
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                </div>
            </div>
            <div class="news-marcets__item">
                <img src="/wp-content/themes/broker/assets/img/witget-2.PNG" alt="">
            </div>
        </div>
    </div>
        <div class="business-news business-news__page">
            <div class="news-one__row">
                <div class="news-one__item">
                    <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>
                <div class="news-one__item">
                    <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>
                <div class="news-one__item">
                    <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>
                <div class="news-one__item">
                    <a href="" class="news-one__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>

                <div class="news-one__item">
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>
                <div class="news-one__item">
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>
                <div class="news-one__item">
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>
                <div class="news-one__item">
                    <div class="news-one__cat-name">Category</div>
                    <a href="" class="news-one__title">Walmart Q1 FY 2022 Earnings Report </a>
                    <div class="news-one__date">03 Apr 2021</div>
                </div>
            </div>
        </div>

        <div class="news-marcets">
        <div class="business-news__title title__h2">American Markets</div>
        <div class="news-marcets__row">
            <div class="news-marcets__item">
                <div class="news-marcets__news-row">
                    <div class="news-marcets__news-item">
                        <a href="" class="news-marcets__img"><img src="/wp-content/themes/broker/assets/img/news-big.PNG" alt=""></a>
                        <div class="news-marcets__cat-name">Category</div>
                        <a href="" class="news-marcets__title">Walmart Q1 FY 2022 Earnings Report </a>
                        <div class="news-marcets__date">03 Apr 2021</div>
                    </div>
                    <div class="news-marcets__news-item">
                        <div class="news-marcets__cat-name">Category</div>
                        <a href="" class="news-marcets__title">Jim Cramer bets stocks will gain from</a>
                        <div class="news-marcets__date">03 Apr 2021</div>
                    </div>
                </div>
            </div>
            <div class="news-marcets__item">
                <div class="best-broker__title">Best brokers American Market</div>
                <div class="best-broker__row">
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                <div class="best-broker__item">
                    <div class="best-broker__bloks">
                        <div class="best-broker__img"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.webp" alt=""></div>
                        <div class="best-broker__item-title">EuropeFX</div>
                    </div>
                    <a class="best-broker__link">Visit broker</a>
                </div>
                </div>
            </div>
            <div class="news-marcets__item">
                <img src="/wp-content/themes/broker/assets/img/witget-2.PNG" alt="">
            </div>
        </div>
    </div>

        
</div>
<?php get_footer(); ?>