<?php get_header(); ?>
<main class="container sgingle-rewiews">
        <div class="main__single-reviews">
            <div class="single-reviews__header-one">
                <div class="single-reviews__logo">
                    <?php the_post_thumbnail(); ?>
                </div>
                <a href="<?php the_field('visit_site'); ?>" class="single-reviews__btn">Start trading <?php the_title(); ?></a>
            </div>
            <div class="single-reviews__header-two">
                <h1 class="single-reviews__title"><?php the_title(); ?> broker review</h1>
                <div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="4.9"><span style="width: 140px;"></span></span></div>
            </div>
            <div class="overall-title">Overall review</div>
            <div class="overall__row">
                <div class="overall__item">
                    <div class="overall__number<?php $overall_rating = get_field('overall_rating'); if($overall_rating < 5.5) echo ' overall__number_orange' ?>"><?php echo $overall_rating; ?></div>
                    <div class="overall__text">Overall Rating</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $fees = get_field('fees'); if($fees < 5.5) echo ' overall__number_orange' ?>"><?php echo $fees; ?></div>
                    <div class="overall__text">Fees</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $markets_and_products = get_field('markets_and_products'); if($markets_and_products < 5.5) echo ' overall__number_orange' ?>"><?php echo $markets_and_products; ?></div>
                    <div class="overall__text">Markets and Products</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $account_opening = get_field('account_opening'); if($account_opening < 5.5) echo ' overall__number_orange' ?>"><?php echo $account_opening; ?></div>
                    <div class="overall__text">Account Opening</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $research = get_field('research'); if($research < 5.5) echo ' overall__number_orange' ?>"><?php echo $research; ?></div>
                    <div class="overall__text">Research</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $deposit_and_withdrawal = get_field('deposit_and_withdrawal'); if($deposit_and_withdrawal < 5.5) echo ' overall__number_orange' ?>"><?php echo $deposit_and_withdrawal; ?></div>
                    <div class="overall__text">Deposit and Withdrawal</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $customer_service = get_field('customer_service'); if($customer_service < 5.5) echo ' overall__number_orange' ?>"><?php echo $customer_service; ?></div>
                    <div class="overall__text">Customer Service</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $trading_platform = get_field('trading_platform'); if($trading_platform < 5.5) echo ' overall__number_orange' ?>"><?php echo $trading_platform; ?></div>
                    <div class="overall__text">Trading Platform</div>
                </div>
                <div class="overall__item">
                    <div class="overall__number<?php $education = get_field('education'); if($education < 5.5) echo ' overall__number_orange' ?>"><?php echo $education; ?></div>
                    <div class="overall__text">Education</div>
                </div>
            </div>
            
            <div class="single-repeater-content__row">
                <div class="single-repeater-content__item">
                    <!-- <div class="single-repeater__title">Financial Stocks</div>
                    <div class="single-repeater__text">
                        <p>
                            The financial services sector of the economy has seen massive changes in the past few years as technology-led disruption acts as both a creative and destructive force within the industry. The global finance market is expected to grow from $20 trillion in 2020 to $22 trillion in 2021 at a compound annual growth rate of close to 10%. Some of the catalysts for this growth are fintech firms, blockchain-based currencies, and the rise of the sharing economy. However, these change-makers are also fueling the demise of traditional finance institutions.
                        </p>
                        <p>
                            According to research by London-based professional services firm PwC, 63% of insurance executives believe that the Internet of Things universe (IoT) will soon become strategically important to their business. There is also a growing unease within the banking industry that risks losing close to a quarter of business dealings to fintech firms within the next five years. The dramatic rise of blockchain over the past few years has also left industry experts baffled and most financial services firms are still not fully prepared to integrate it into the economy.
                        </p>
                    </div>
                    <a href="" class="single-repeater__btn">
                        Compare to other broker reviews
                        <svg width="14" height="16px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-arrow-right fa-w-14 fa-3x"><path fill="currentColor" d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z" class=""></path></svg>
                    </a>
                    <div class="single-repeater__title">Financial Stocks</div>
                    <div class="single-repeater__text">
                        <p>
                            The financial services sector of the economy has seen massive changes in the past few years as technology-led disruption acts as both a creative and destructive force within the industry. The global finance market is expected to grow from $20 trillion in 2020 to $22 trillion in 2021 at a compound annual growth rate of close to 10%. Some of the catalysts for this growth are fintech firms, blockchain-based currencies, and the rise of the sharing economy. However, these change-makers are also fueling the demise of traditional finance institutions.
                        </p>
                        <p>
                            According to research by London-based professional services firm PwC, 63% of insurance executives believe that the Internet of Things universe (IoT) will soon become strategically important to their business. There is also a growing unease within the banking industry that risks losing close to a quarter of business dealings to fintech firms within the next five years. The dramatic rise of blockchain over the past few years has also left industry experts baffled and most financial services firms are still not fully prepared to integrate it into the economy.
                        </p>
                    </div>
                    <a href="" class="single-repeater__btn">
                        Compare to other broker reviews
                        <svg width="14" height="16px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-arrow-right fa-w-14 fa-3x"><path fill="currentColor" d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z" class=""></path></svg>
                    </a> -->
                    <?php the_content();?>
                </div>
            </div>
        </div>
        <div class="main__single-sidebar">
            <div class="main__single-sidebar-logo"><?php the_post_thumbnail(); ?></div>
            <div class="main__single-sidebar-row">
                <div class="main__single-sidebar-item">
                    <div class="main__single-sidebar-title">Minimum deposit</div>
                    <div class="main__single-sidebar-price">$<?php echo number_format(get_field('minimum_deposit')); ?></div>
                </div>
                <div class="main__single-sidebar-item">
                    <div class="main__single-sidebar-title">Minimum deposit</div>
                    <div class="main__single-sidebar-price">$<?php echo number_format(get_field('regulated')); ?></div>
                </div>
                <div class="main__single-sidebar-item">
                    <div class="main__single-sidebar-title">Minimum deposit</div>
                    <div class="main__single-sidebar-price">$<?php echo number_format(get_field('trading_instruments')); ?></div>
                </div>
                <div class="main__single-sidebar-item">
                    <div class="main__single-sidebar-title">Minimum deposit</div>
                    <div class="main__single-sidebar-price">$<?php echo number_format(get_field('monthly_users_')); ?></div>
                </div>
            </div>
            <a href="<?php the_field('visit_site'); ?>" class="main__single-sidebar-btn">Start trading</a>
        </div>


    </main>
    <div class="container">
        <div class="explore-other">
            <div class="explore-other__title-block">Explore to other broker reviews</div>
            <div class="explore-other__row">
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
                <div class="explore-other__item">
                    <div class="explore-other__logo"><img src="/wp-content/themes/broker/assets/img/homepage-resault-a-01-desktop.png" alt=""></div>
                    <div class="explore-other__title">EuropeFX</div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>