<div class="best-results__item">
    <div>
        <a href="<?php the_permalink(); ?>" class="best-results__img">
            <?php the_post_thumbnail(); ?>
        </a>
        <a  href="<?php the_permalink(); ?>" class="best-results__item-title"><?php the_title(); ?></a>
    </div>
    <a href="<?php the_field('visit_site'); ?>" class="best-results__btn">Visit broker</a>
</div>