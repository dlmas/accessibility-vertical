<?php get_header(); ?>
<main id="main" class="no-url">
 
       <img src="/wp-content/themes/realestatesinvestment/assets/img/404.png" alt="">
        <h1 class="elementor-heading-title elementor-size-default">Sorry we couldn't find that page. </h1>
        <a href="/">Take me to Realestates investment home page</a>

</main>
<?php get_footer(); ?>