(function($) {
    var defaultImg = $('#profile-user-img').attr('data-src');
    //������������ ���� �� ������ Select Image
    $('.user_image_button').on('click', function(event) {

       var image = wp.media({
           title: 'Upload Image',
           // �������� mutiple: true ���������, ���� ����� ��������� ��������� ������ �� ���
           // � ����� ������ ����� ������������ �����������, �������: 
           multiple: false
        }).open()
        .on('select', function(e) {
            // ���������� ��������� � �����-���������� ����������� ��� ������
            var uploaded_image = image.state().get('selection').first();
            // ������������ ����������� ����������� � ������ JSON ��� ����� �������� ������� � ����
            // ��������� � ������� ������� ������������ �����������
               console.log(uploaded_image);
            // �������� url ������������ �����������
               var image_url = uploaded_image.toJSON().url;
            // ������� url ����������� � ��������� ����, � ���� �������� ������ ���������� �����������
               $('#userimg').val(image_url);
               $('#profile-user-img').attr('src', image_url);
           });
        return false;

    });
    //������� ������ ����� ����� �� ������ "x"
    $('.remove-user-image').on('click', function(event) {
        $('#userimg').val('');
        $('#profile-user-img').attr('src', defaultImg);
    });

})(jQuery);