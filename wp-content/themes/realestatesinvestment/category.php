<?php get_header(); ?>

<main id="main">
    <div class="container">
    <div style="clear:both;"></div>
        <div id="article" class="archive-content main-content">
        <h1 class="title-page"><?php single_cat_title(); ?></h1>
        <?php global $wp;
            $current_slug = $wp->query_vars['category_name']; 
        ?>
            <div class="archive-page-desc"><?php echo category_description(); ?></div>
            <div class="archive-page-row">
            <?php 
                $query = new WP_Query( array(
                    'post_type' => 'post',
                    'posts_per_page'	=> '20',
                    'category_name' => $current_slug
                ));
                if ( $query->have_posts() ) : 
                    while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="archive-page-item">
                            <a href="<?php the_permalink() ?>" class="archive-page-item__img"><?php the_post_thumbnail(array(359, 160)) ?></a>
                            <div class="home-two-post__cat-row">
                            <?php
                                                foreach ( get_the_category() as $category ) {
                                                    printf(
                                                        '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                                                        esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                                                        esc_html( $category->name ) // Название рубрики
                                                    );
                                                }
                                            ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="home-two-post__title"><?php the_title(); ?></a>
                            <div class="home-one-post__autor-data">
                                <div class="home-one-post__autor-ava"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '33'); ?></div>
                                <div class="home-one-post__autor"><?php the_author() ?></div>
                                <div class="home-one-post__data"><?php echo get_the_date('F d, Y'); ?></div>
                            </div>
                        </div>
                <?php endwhile;
                    wp_reset_postdata(); // reset the query
                endif;
            ?>
                
                
            </div>
        </div>
        <div id="aside1" class="sidebar">
            <?php get_sidebar() ?>
        </div>
        <div style="clear:both;"></div>
    </div>
</main>
                
<?php get_footer(); ?>