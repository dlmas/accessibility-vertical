<div class="container">
            <div class="home-form">
                <h2 class="home-form__title"><?php the_field('form_title', 'options'); ?></h2>
                <!-- <form action="">
                    <label>
                        <span class="wpcf7-form-control-wrap email-332">
                            <input type="email" name="email-332" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Name">
                        </span>
                    </label>
                    <label>
                        <span class="wpcf7-form-control-wrap email-332">
                            <input type="email" name="email-332" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email">
                        </span>
                    </label>
                    <label>
                        <span class="wpcf7-form-control-wrap email-332">
                            <input type="email" name="email-332" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Phone">
                        </span>
                    </label>
                    <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit">
                </form> -->
                <?php echo do_shortcode('[contact-form-7 id="8172" title="Footer form"]');?>
            </div>
        </div>
    </main>
 <footer>
        <div class="container">
            <div class="footer__row">
                <div class="footer__item">
                    <a class="footer__logo"><img src="/wp-content/themes/realestatesinvestment/assets/img/footer-logo.svg" alt="" width="140" height="48"></a>
                    <div class="footer__social-row">
                        <?php while( have_rows('footer_social_menu', 'options') ) : the_row(); ?>
                            <a href="<?php the_sub_field('footer_social_url'); ?>" target="_blank"><?php the_sub_field('footer_social_img'); ?></a>
                        <?php endwhile; ?>
                    </div>
                </div>
                <div class="footer__item footer__item_pages">
                    <h5 class="footer__item-title"><?php the_field('footer_title_pages', 'options'); ?></h5>
                    <?php wp_nav_menu( array( 
                        'container_class' => 'menu-header', 
                        'theme_location' => 'foot_menu',
                        'container'       => 'div',
                        'menu_class'      => 'footer__menu-row',
                    ) ); ?>
                </div>
                <div class="footer__item">
                    <h5 class="footer__item-title"><?php the_field('footer_title_categories', 'options'); ?></h5>
                    <div class="footer__cat-row">
                        <?php wp_list_categories('title_li='); ?>
                    </div>
                </div>
                <div class="footer__item footer__item_post">
                    <h5 class="footer__item-title"><?php the_field('footer_title_advertising_disclosure', 'options'); ?></h5>
                        <div class="footer-text"><?php the_field('footer_text', 'options'); ?></div>
                </div>
            </div>
        </div>
        <!-- <div class="bottom-menu-footer__row"> -->
            <!-- <a href="#" class="bottom-menu-footer__item">Advertising Disclosure</a>
            <a href="/privacy-policy/" class="bottom-menu-footer__item">Privacy Policy</a>
            <a href="/cookie-policy/" class="bottom-menu-footer__item">Cookie Policy</a>
            <a href="/terms-conditions/" class="bottom-menu-footer__item">Terms and Conditions</a> -->
            <?php wp_nav_menu( array( 
                // 'container_class' => 'menu-header', 
                        'theme_location' => 'foot_bottom_menu',
                        'container'       => 'div',
                        'menu_class'      => 'bottom-menu-footer__row',
                    ) ); ?>
        <!-- </div> -->
    </footer>
    <div class="cookies-popup" style="display: none;">
        <div class="cookies-popup__text"><?php the_field('cookies_popup_text', 'options'); ?> <a href="<?php the_field('cookies_popup_page', 'options'); ?>"><?php the_field('learn_more_btm', 'options'); ?></a></div>
        <div class="cookies-popup__btn"><?php the_field('cookies_popup_button_text', 'options'); ?></div>
    </div>
    <div id="scroller" class="b-top" style="display: none;"><span class="b-top-but"><svg width="40" height="40" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-chevron-circle-up fa-w-16 fa-2x"><path fill="rgb(138 253 196)" d="M8 256C8 119 119 8 256 8s248 111 248 248-111 248-248 248S8 393 8 256zm231-113.9L103.5 277.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L256 226.9l101.6 101.6c9.4 9.4 24.6 9.4 33.9 0l17-17c9.4-9.4 9.4-24.6 0-33.9L273 142.1c-9.4-9.4-24.6-9.4-34 0z" class=""></path></svg></span></div>
    <script>
    jQuery(function($) {
        $('.tabs-wrapper').each(function() {
            let ths = $(this);
            ths.find('.tab-item').not(':first').hide();
            ths.find('.tab').click(function() {
                ths.find('.tab').removeClass('active').eq($(this).index()).addClass('active');
                ths.find('.tab-item').hide().eq($(this).index()).fadeIn()
            }).eq(0).addClass('active');
        });
        //Burger menu
        $('.menu-toggle').click(function () {
            $(this).toggleClass('active');
            $('nav').toggleClass('active');
            $('body').toggleClass('active');
            //$('.menu-item-has-children a').removeAttr('href');
        });
        $('.menu-item-has-children').click(function () {
            $('.sub-menu').toggleClass('active');
        });
        $(window).scroll(function() {
            var height = $(window).scrollTop();
            if(height > 40){
                $('.single-review-title').addClass('header-fixed');

                $('.single-review-header__btn').addClass('header-fixed__btn');
                $('.single-review-header__btn').css('margin-left', $('.container').width() - $('.single-review-header__btn').width());

                $('.single-review-header__star').addClass('header-fixed__star');
                $('.single-review-header__star').css('margin-left', $('.container').width() - $('.single-review-header__star').width() - $('.single-review-header__btn').width() - 50);

                if( $('.single-review-title').children().length === 0 ){
                    $('.single-review-title').wrapInner("<div class='container'></div>");
                }
            } else{
                $('.single-review-title').removeClass('header-fixed');
                $('.single-review-title .container').contents().unwrap();
                $('.single-review-header__btn').removeClass('header-fixed__btn');
                $('.single-review-header__btn').css('margin-left', 0);
                $('.single-review-header__star').removeClass('header-fixed__star');
                $('.single-review-header__star').css('margin-left', 0);

            }
        });
    });
    </script>
    <?php wp_footer(); ?>
</body>
</html>