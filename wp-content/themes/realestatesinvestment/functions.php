<?php

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

//удаляем лишнее
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
function remove_comment_reply_script () {
	wp_deregister_script ('comment-reply');
}
add_action ('init', 'remove_comment_reply_script');
add_action( 'after_setup_theme', 'prefix_remove_unnecessary_tags' );
function prefix_remove_unnecessary_tags(){

    // REMOVE WP EMOJI
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );


    // remove all tags from header


    // Удаляет ссылки RSS-лент записи и комментариев
    remove_action( 'wp_head', 'feed_links', 2 );
    // Удаляет ссылки RSS-лент категорий и архивов
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    // Удаляет RSD ссылку для удаленной публикации
    remove_action( 'wp_head', 'rsd_link' );
    // Удаляет ссылку Windows для Live Writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // Удаляет короткую ссылку
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0);
    // Удаляет информацию о версии WordPress
    remove_action( 'wp_head', 'wp_generator' );
    // Удаляет ссылки на предыдущую и следующую статьи
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
    remove_action( 'wp_head',      'rest_output_link_wp_head'              );
    remove_action( 'wp_head',      'wp_oembed_add_discovery_links'         );
    remove_action( 'template_redirect', 'rest_output_link_header', 11 );
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
    // language
    add_filter('multilingualpress.hreflang_type', '__return_false');
    add_filter('multilingualpress.render_hreflang', function() { return false; });

}
add_filter( 'wpseo_opengraph_url' , '__return_false' );

// Регистрирую стили
function sobix_scripts() {
	wp_register_style( 'my_style', get_template_directory_uri() . '/assets/css/style.css');
	wp_register_style( 'my_media', get_template_directory_uri() . '/assets/css/media.css');
	// wp_register_style( 'my_montserrat', get_template_directory_uri() . '/assets/fonts/Montserrat/stylesheet.css');
	// wp_register_style( 'my_poppins', get_template_directory_uri() . '/assets/fonts/poppins/stylesheet.css');

	// Подключаю стили
	wp_enqueue_style( 'my_style');
	wp_enqueue_style( 'my_media');
	// wp_enqueue_style( 'my_montserrat');
	// wp_enqueue_style( 'my_poppins');

	wp_register_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
	wp_enqueue_style('slick');

	wp_register_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );
	wp_enqueue_script('slick');

	wp_register_script( 'main', get_template_directory_uri() . '/assets/js/main.js' );
	wp_enqueue_script('main');
}
add_action( 'wp_enqueue_scripts', 'sobix_scripts' );

//подключение шрифтов Open Sans и Roboto от Google start
function wph_add_google_fonts() {
    if ( !is_admin() ) {
        wp_register_style('google-poppins', 'https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;600;700&display=swap', array(), null, 'all');
        wp_register_style('google-sarabun', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;600&display=swap', array(), null, 'all');
        wp_enqueue_style('google-poppins');
		wp_enqueue_style('google-sarabun');
    }
}
add_action('wp_enqueue_scripts', 'wph_add_google_fonts');

add_theme_support( 'post-thumbnails' ); // для всех типов постов подключаем миниатюры

/**
 * Use ACF image field as avatar
 * @author Mike Hemberger
 * @link http://thestizmedia.com/acf-pro-simple-local-avatars/
 * @uses ACF Pro image field (tested return value set as Array )
 */
add_filter('get_avatar', 'tsm_acf_profile_avatar', 10, 5);
function tsm_acf_profile_avatar( $avatar, $id_or_email, $size, $default, $alt ) {

    $user = '';

    // Get user by id or email
    if ( is_numeric( $id_or_email ) ) {

        $id   = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );

    } elseif ( is_object( $id_or_email ) ) {

        if ( ! empty( $id_or_email->user_id ) ) {
            $id   = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }

    } else {
        $user = get_user_by( 'email', $id_or_email );
    }

    if ( ! $user ) {
        return $avatar;
    }

    // Get the user id
    $user_id = $user->ID;

    // Get the file id
    $image_id = get_user_meta($user_id, 'profile_image', true); // CHANGE TO YOUR FIELD NAME

    // Bail if we don't have a local avatar
    if ( ! $image_id ) {
        return $avatar;
    }

    // Get the file size
    $image_url  = wp_get_attachment_image_src( $image_id, 'thumbnail' ); // Set image size by name
    // Get the file url
    $avatar_url = $image_url[0];
    // Get the img markup
    $avatar = '<img alt="' . $alt . '" src="' . $avatar_url . '" class="avatar avatar-' . $size . '" height="' . $size . '" width="' . $size . '"/>';

    // Return our new avatar
    return $avatar;
}

//options page admin
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme homepage Settings',
		'menu_title'	=> 'Homepage',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Cookies Settings',
		'menu_title'	=> 'Cookies',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Reviews Settings',
		'menu_title'	=> 'Reviews',
		'parent_slug'	=> 'theme-general-settings',
	));
}

 /* Новый тип записи */
 add_action('init', 'register_postType_reviews');
 function register_postType_reviews() {
	 $labels = array(
		 'name' => 'Reviews',
		 'singular_name' => 'Reviews',
		 'add_new' => 'Add Reviews',
		 'add_new_item' => 'Add new Reviews',
		 'edit_item' => 'Edit Reviews',
		 'new_item' => 'New Reviews',
		 'all_items' => 'All Reviews',
		 'view_item' => 'View Reviews',
		 'search_items' => 'Search Reviews',
		 'not_found' => 'Reviews not found',
		 'not_found_in_trash' => 'No in basket Reviews',
		 'menu_name' => 'Reviews'
	 );
	 $args   = array(
		 'labels' => $labels,
		 'rewrite' => array( 'slug' => 'real-estate-reviews'),
		 'public' => true,
		 'show_ui' => true,
		 'has_archive' => 'reviews',
		 'publicly_queryable' => true,
		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		 'menu_position' => 5,
		 'menu_icon' => 'dashicons-feedback',
		 'show_in_nav_menus' => true, //выводить в меню
		 'show_in_rest'       => true, //вкл. гутенберг
		 'supports' => array(
			 'title',
			 'editor',
			 'thumbnail',
			 'excerpt',
             'comments'
		 )
	 );
	 register_post_type('reviews', $args);
 }

 function register_taxonomies_reviews() {
	 $labels = array(
		 'name'              => _x( 'Categories Reviews', 'taxonomy general name' ),
		 'singular_name'     => _x( 'Categories Reviews', 'taxonomy singular name' ),
		 'search_items'      => __( 'Search ' ),
		 'all_items'         => __( 'All categories' ),
		 'parent_item'       => __( 'Parental categories' ),
		 'parent_item_colon' => __( 'Parental categories:' ),
		 'edit_item'         => __( 'Edit categories' ),
		 'update_item'       => __( 'Refresh categories' ),
		 'add_new_item'      => __( 'Add new categories' ),
		 'new_item_name'     => __( 'New name categories' ),
		 'menu_name'         => __( 'Categories Reviews' ),
	 );

	 $args =  array(
		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		 'labels'            => $labels,
		 'show_ui'           => true,
		 'show_admin_column' => true,
		 'query_var'         => true,
         'show_in_rest' => true,
		 'supports' => array( 'title', 'editor', 'comments', 'post-templates'),
		 //'rewrite' => array( 'slug' => 'housing', // ярлык
							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	 );

	 register_taxonomy( 'reviews_category', 'reviews', $args );
 }
 add_action( 'init', 'register_taxonomies_reviews', 0 );




 add_action( 'widgets_init', 'register_my_widgets' );
 function register_my_widgets(){

	 register_sidebar( array(
		 'name'          => sprintf(__('Sidebar %d'), 1 ),
		 'id'            => "sidebar-1",
		 'description'   => '',
		 'class'         => '',
		 'before_widget' => '<li id="%1$s" class="widget %2$s">',
		 'after_widget'  => "</li>\n",
		 'before_title'  => '<h2 class="widgettitle">',
		 'after_title'   => "</h2>\n",
		 'before_sidebar' => '', // WP 5.6
		 'after_sidebar'  => '', // WP 5.6
	 ) );
 }

 function rex_my_action ($review, $post_id) {
    // Build your action after the review is saved.

}
add_action('rwp_after_saving_review', 'rex_my_action', 11, 2);

// $meta_values = get_post_meta( 3894, 'rwp_reviews' );
// echo '<pre>'; print_r($meta_values[0]['review_custom_tabs']); echo '</pre>';

//
function get_post_rating(){
	$postId = get_the_ID();
    $meta_values = get_post_meta( $postId, 'rwp_user_score' );
    $meta_values = round($meta_values[0], 1);
    return $meta_values;
}

function get_post_number_votes(){
	$postId = get_the_ID();
	$arrComm = get_post_meta( $postId, 'rwp_rating_0' );
    return count($arrComm) . ' votes';
}

function get_stars() {
	$meta_values = get_post_rating();
	$output .= '<div class="rwp-rating-stars" style="background-image: url(/wp-content/themes/realestatesinvestment/assets/img/stars44.png); width: 120px; ">';
	$output .= '<div style="width: '. $meta_values * 24 .'px; "></div>';
	$output .= '</div>';
	return $output;
}

// Remove category from URL
add_filter('user_trailingslashit', 'wpse311858_remove_category_from_url');
function wpse311858_remove_category_from_url($link) {
  return str_replace('category/', '/', $link);
}

// Add new category URLs
add_filter('generate_rewrite_rules', 'wpse311858_category_rewrite');
function wpse311858_category_rewrite($wp_rewrite) {
  $new_rules = [];

  // Get all the categories including empty categories
  $cats = get_categories(['exclude' => 1, 'hide_empty' => false ]); //exclude uncategorized catagory if there were

  // Loop through each category to add new rules to core rewrite pattern
  foreach( $cats as $cat ) {
    $cat_nicename = $cat->slug;

    // Check if it is parent or child category
    if( $cat->parent !== 0 ) {
      $cat_nicename = get_category_parents( $cat->parent, false, '/', true ) . $cat_nicename;
    }

    // For each pattern
    $new_rules['(' . $cat_nicename . ')/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?category_name=$matches[1]&feed=$matches[2]'; // for feed pattern
    $new_rules['(' . $cat_nicename . ')/(.+?)/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?category_name=$matches[1]&feed=$matches[2]'; // for another feed pattern
    $new_rules['(' . $cat_nicename . ')/(.+?)/embed/?$'] = 'index.php?category_name=$matches[1]&embed=true'; // for embed page
    $new_rules['(' . $cat_nicename . ')/page/?([0-9]{1,})/?$'] = 'index.php?category_name=$matches[1]&paged=$matches[2]'; // for paged page
    $new_rules['(' . $cat_nicename . ')/?$'] = 'index.php?category_name=$matches[1]'; // for category archive page

  } //endforeach

  // Finally add new rules
  $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}

add_action('after_setup_theme', function(){
	register_nav_menus( array(
	  'main_menu' => __( 'Primary menu', 'crea' ),
	  'foot_menu' => __( 'Footer menu', 'crea' ),
	  'foot_bottom_menu' => __( 'Footer bottom menu', 'crea' )
	) );
  });

//rest api
## Получает посты указанного автора
function my_awesome_func( WP_REST_Request $request ){

	// $posts = get_posts( array(
	// 	'author' => (int) $request['id'],
	// ) );

	// if ( empty( $posts ) )
	// 	return new WP_Error( 'no_author_posts', 'Записей не найдено', [ 'status' => 404 ] );

	// return $posts;
	$query = new WP_Query( array(
		'post_type' => 'reviews',
		'posts_per_page'	=> '-1',

		));
		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) : $query->the_post();
				$allPostId[] = get_the_ID();
			endwhile;
			wp_reset_postdata(); // reset the query
		endif;
		foreach ($allPostId as $value) {
			$arrComm = get_post_meta( $value, 'rwp_rating_0' );
		}
	return $arrComm;
}

add_action( 'rest_api_init', function(){

	register_rest_route( 'myplugin/v1', '/comments', [ // /(?P<id>\d+)
		'methods'  => 'GET',
		'callback' => 'my_awesome_func',
	] );

} );


/* Регистрация виджета */

function wpb_load_widget_100_homepage() {
	register_widget( 'lineup_100_homepage' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_100_homepage' );

  class lineup_100_homepage extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_100_homepage', 'lineup_100_homepage', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

  /* Регистрация виджета */

function wpb_load_widget_100_reviewpage() {
	register_widget( 'lineup_100_reviewpage' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_100_reviewpage' );

  class lineup_100_reviewpage extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_100_reviewpage', 'lineup_100_reviewpage', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

/* Регистрация виджета */
  function wpb_load_widget_5_top() {
	register_widget( 'lineup_5_top' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_5_top' );

  class lineup_5_top extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_5_top', 'lineup_5_top', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }


/* Регистрация виджета */
function wpb_load_widget_5_low() {
	register_widget( 'lineup_5_low' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_5_low' );

  class lineup_5_low extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_5_low', 'lineup_5_low', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

/* Регистрация виджета */
function wpb_load_widget_5_recent() {
	register_widget( 'lineup_5_recent' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_5_recent' );

  class lineup_5_recent extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_5_recent', 'lineup_5_recent', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

  /* Регистрация виджета */
function wpb_load_widget_5_popular() {
	register_widget( 'lineup_5_popular' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_5_popular' );

  class lineup_5_popular extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_5_popular', 'lineup_5_popular', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }


// Выводим IP пользователя в WordPress
function getIp() {
	$keys = [
	  'HTTP_CLIENT_IP',
	  'HTTP_X_FORWARDED_FOR',
	  'REMOTE_ADDR'
	];
	foreach ($keys as $key) {
	  if (!empty($_SERVER[$key])) {
		$tmp = explode(',', $_SERVER[$key]);
        $file_extension = end($tmp);
        $ip = trim($file_extension);
		if (filter_var($ip, FILTER_VALIDATE_IP)) {
		  return $ip;
		}
	  }
	}
  }

// url user
function url_user() {
	$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	return $url;
}

//domain user
function domain_user() {
	$current_domain = $_SERVER['HTTP_HOST'];
	return $current_domain;
}

//Screen resolution
//function screen_resolutionn() {
//	$width = $_COOKIE['sw'];
//	$height = $_COOKIE['sh'];
//
//	return 'width: '.$width.', height: '.$height;
//}

//reset api no function
function isRestUrl() {
    $bIsRest = false;
    if ( function_exists( 'rest_url' ) && !empty( $_SERVER[ 'REQUEST_URI' ] ) ) {
        $sRestUrlBase = get_rest_url( get_current_blog_id(), '/' );
        $sRestPath = trim( parse_url( $sRestUrlBase, PHP_URL_PATH ), '/' );
        $sRequestPath = trim( $_SERVER[ 'REQUEST_URI' ], '/' );
        $bIsRest = ( strpos( $sRequestPath, $sRestPath ) === 0 );
    }
    return $bIsRest;
}

//setcookie( 'lineup_3_top', '', time() - 1, COOKIEPATH, COOKIE_DOMAIN );
//setcookie( 'lineup_5_top', '', time() - 1, COOKIEPATH, COOKIE_DOMAIN );
//setcookie( 'lineup_20_homepage', '', time() - 1, COOKIEPATH, COOKIE_DOMAIN );

$array_result = [];
$user_agent = $_SERVER['HTTP_USER_AGENT'];

/**
 * @param $widget_name
 */
function widget_lineup_builder_v2($widget_name) {

    global $array_result, $user_agent;

    if (!isset($_COOKIE[$widget_name])) {

        // массив для переменных, которые будут переданы с запросом
//        if(domain_user() == 'bestwebsiteaccessibility.com'){
//            $paramsArray = [
//                'ip'         => getIp(),
//                'host'       => 'bestwebsiteaccessibility.com',
//                'widget'     => $widget_name,
//                'user_agent' => $user_agent
//            ];
//        }else if(domain_user() == 'bestwebaccessibility.com'){
//            $paramsArray = [
//                'ip'         => getIp(),
//                'host'       => 'bestwebaccessibility.com',
//                'widget'     => $widget_name,
//                'user_agent' => $user_agent
//            ];
//        }else{
            $paramsArray = [
                'ip'         => getIp(),
                'host'       => domain_user(),
                'widget'     => $widget_name,
                'user_agent' => $user_agent
            ];

//        }
        // преобразуем массив в URL-кодированную строку
        $vars = json_encode($paramsArray, JSON_UNESCAPED_UNICODE);

        // создаем параметры контекста
        $options = [
            'http' => [
                'method'  => 'POST', // метод передачи данных
                'header'  => 'Content-Type: application/json', // заголовок
                'content' => $vars // переменные
            ]
        ];

        $context = stream_context_create($options); // создаём контекст потока


                $result = file_get_contents(
                'https://lb.bestwebsiteaccessibility.com/lines',
                false,
                $context
            ); //отправляем запрос




        $result = json_decode($result);
        // echo '<pre>'; print_r($result); echo '</pre>';
        if($result){
            $array_result[$widget_name] = $result;
        }
    }
}

if(!isRestUrl()){
    widget_lineup_builder_v2('lineup_100_homepage');
	widget_lineup_builder_v2('lineup_100_reviewpage');
    widget_lineup_builder_v2('lineup_5_top');
    widget_lineup_builder_v2('lineup_5_recent');
    widget_lineup_builder_v2('lineup_5_popular');
    widget_lineup_builder_v2('lineup_5_low');
}
/**
 * @param $widget_name
 * @param $post_type
 * @param $posts_per_page
 *
 * @return \WP_Query
 */
function widget_lineup_builder_v3($widget_name, $post_type, $posts_per_page)
{
    global $query, $array_result;

    //mobile posts_per_page for homepage lineup_20_homepage witget
//    if( $widget_name == 'lineup_20_homepage' && wp_is_mobile() || $widget_name == 'lineup_100_reviewspage' && wp_is_mobile()) {
//        $posts_per_page = 5;
//    }

    if (array_key_exists($widget_name, $array_result)) {
        return $query = new WP_Query(
            [
                'post_type'      => $post_type,
                'posts_per_page' => $posts_per_page,
                'post__in'       => $array_result[$widget_name],
                'orderby'        => 'post__in'
            ]
        );
    }

    if (isset($_COOKIE[$widget_name])) {
        return $query = new WP_Query(
            [
                'post_type'      => $post_type,
                'posts_per_page' => $posts_per_page,
                'post__in'       => unserialize($_COOKIE[$widget_name]),
                'orderby'        => 'post__in'
            ]
        );
    }

//    return $query = new WP_Query(
//        [
//            'post_type'      => $post_type,
//            'posts_per_page' => $posts_per_page,
//            'meta_key'       => 'raiting',
//            'orderby'        => 'meta_value',
//            'order'          => 'DESC'
//        ]
//    );
}

add_action('lineup_100_homepage', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_100_reviewpage', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_5_top', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_5_low', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_5_recent', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_5_popular', 'widget_lineup_builder_v3', 10, 3);

//cookie lineups
 add_action( 'init', 'setcookie_lineups' );
 function setcookie_lineups() {
     global $array_result;
     foreach($array_result as $key => $val) {
	       setcookie( $key, serialize($val), time() + (86400 * 30), COOKIEPATH, COOKIE_DOMAIN ); //1 день time() + (86400 * 30)
	 }
 }



//load more ajax
function true_load_posts(){
    $posts_per_page = 21;
//    if( wp_is_mobile()) {
//        $posts_per_page = 5;
//    }

    $postIn = unserialize( stripslashes( $_POST['query'] ) );
	$args = $postIn;
	$args['paged'] = $_POST['page'] + 1; // следующая страница
	$args['post_status'] = 'publish';
	$args['posts_per_page'] = $posts_per_page; // по сколько записей подгружать
    $args['post_type'] = 'post';



	// обычно лучше использовать WP_Query, но не здесь
	query_posts( $args );
	// если посты есть
	if( have_posts() ) :
		// запускаем цикл
		while( have_posts() ): the_post();
                get_template_part( 'template-parts/content', 'blog' );
		endwhile;

	endif;
	die();
}


add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');



//load more ajax home
function true_load_postsHome(){
    $posts_per_page = 12;
//    if( wp_is_mobile()) {
//        $posts_per_page = 5;
//    }

    $postIn = unserialize( stripslashes( $_POST['query'] ) );
	$args = $postIn;
	$args['paged'] = $_POST['page'] + 1; // следующая страница
	$args['post_status'] = 'publish';
	$args['posts_per_page'] = $posts_per_page; // по сколько записей подгружать
    $args['post_type'] = 'reviews';



	// обычно лучше использовать WP_Query, но не здесь
	query_posts( $args );
	// если посты есть
	if( have_posts() ) :
		// запускаем цикл
		while( have_posts() ): the_post();
                get_template_part( 'template-parts/content', 'home' );
		endwhile;

	endif;
	die();
}
add_action('wp_ajax_loadmoreHome', 'true_load_postsHome');
add_action('wp_ajax_nopriv_loadmoreHome', 'true_load_postsHome');

//load more ajax home readlsee
function true_load_loadmoreHomeReadLess(){
    $posts_per_page = 12;
//    if( wp_is_mobile()) {
//        $posts_per_page = 5;
//    }

    $postIn = unserialize( stripslashes( $_POST['query'] ) );
	$args = $postIn;
	$args['paged'] = 1; // следующая страница
	$args['post_status'] = 'publish';
	$args['posts_per_page'] = $posts_per_page; // по сколько записей подгружать
    $args['post_type'] = 'reviews';



	// обычно лучше использовать WP_Query, но не здесь
	query_posts( $args );
	// если посты есть
	if( have_posts() ) :
		// запускаем цикл
		while( have_posts() ): the_post();
                get_template_part( 'template-parts/content', 'home' );
		endwhile;

	endif;
	die();
}
add_action('wp_ajax_loadmoreHomeReadLess', 'true_load_loadmoreHomeReadLess');
add_action('wp_ajax_nopriv_loadmoreHomeReadLess', 'true_load_loadmoreHomeReadLess');

//load more ajax reviews
function true_load_reviews(){
    $posts_per_page = 20;
//    if( wp_is_mobile()) {
//        $posts_per_page = 5;
//    }

    $postIn = unserialize( stripslashes( $_POST['query'] ) );
	$args = $postIn;
	$args['paged'] = $_POST['page'] + 1; // следующая страница
	$args['post_status'] = 'publish';
	$args['posts_per_page'] = 10; // по сколько записей подгружать
    $args['post_type'] = 'reviews';
	$args['orderby'] = 'meta_value';
	$args['meta_key'] = 'rwp_user_score';



	// обычно лучше использовать WP_Query, но не здесь
	query_posts( $args );
	// если посты есть
	if( have_posts() ) :
		// запускаем цикл
		while( have_posts() ): the_post();
                get_template_part( 'template-parts/content', 'reviews' );
		endwhile;

	endif;
	die();
}


add_action('wp_ajax_reviews', 'true_load_reviews');
add_action('wp_ajax_nopriv_reviews', 'true_load_reviews');


// add_rewrite_rule('xxxx/page/([0-9]{1,})/?$','index.php?post_type=xxxx&page=$matches[1]','top');

// remove pagination from url
// $pattern = '/page(\/)*([0-9\/])*/i';
// $url = preg_replace($pattern, '', $GLOBALS['wp']->request);

/**
 * Numeric pagination via WP core function paginate_links().
 * @link http://codex.wordpress.org/Function_Reference/paginate_links
 *
 * @param array $srgs
 *
 * @return string HTML for numneric pagination
 */
function wpse_pagination( $args = array() ) {
    global $wp_query;

    if ( $wp_query->max_num_pages <= 1 ) {
        return;
    }

    $pagination_args = array(
        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
        'total'        => $wp_query->max_num_pages,
        'current'      => max( 1, get_query_var( 'paged' ) ),
        'format'       => '?paged=%#%',
        'show_all'     => false,
        'type'         => 'array',
        'end_size'     => 2,
        'mid_size'     => 3,
        'prev_next'    => true,
        'prev_text'    => __( '&laquo; Prev', 'wpse' ),
        'next_text'    => __( 'Next &raquo;', 'wpse' ),
        'add_args'     => false,
        'add_fragment' => '',

        // Custom arguments not part of WP core:
        'show_page_position' => false, // Optionally allows the "Page X of XX" HTML to be displayed.
    );

    $pagination = paginate_links( array_merge( $pagination_args, $args ) );

    // Remove /page/1/ from links.
    $pagination = array_map(
        function( $string ) {
            return str_replace( '/page/1/', '/', $string );
        },
        $pagination
    );

    return implode( '', $pagination );
}

echo wpse_pagination();

str_replace( '/page/2/', '/', $string );
