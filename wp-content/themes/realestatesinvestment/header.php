<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/wp-content/themes/realestatesinvestment/assets/img/cropped-guides.png" type="image/png">
    <?php wp_head(); ?>
    <style>
        /* review box -company page */
        .review_form_list__title {
            margin-top: 50px;
            font-size: 24px;
            font-weight: 600 !important;
            letter-spacing: 0.21px;
            line-height: 1.25 !important;
            margin-bottom: 10px;
        }
.review_form_list .rwp-ratings-form-label,
.review_form_list .rwp-title,
.review_form_list .rwp-has-image,
.review_form_list .rwp-people-label,
.review_form_list .rwp-form-upload-images,
.review_form_list  .rwp-ur
{

display: none !important;
}

.review_form_list .rwp-numeric-rating{

    font-family: Montserrat;
    font-size: 24px;
    font-weight: 500;
    font-style: normal;
}
.review_form_list .rwp-review-wrap .rwp-review .rwp-ratings-form-wrap {
    margin-top: 0 !important;
}


.review_form_list  .rwp-review{
    display: flex;
    flex-direction: column-reverse;
}
.review_form_list .rwp-rating-form-content input:focus{

    outline: solid 1px #0eba7c !important;
}
.review_form_list .rwp-rating-form-content textarea:focus{

    outline: solid 1px #0eba7c !important;
}


.review_form_list .rwp-rating-form-content input{
    border: solid 1px #0eba7c !important;
    padding: 10px 0 10px 15px !important;
}
.review_form_list .rwp-review-wrap{
    margin: 0;
}
.review_form_list .rwp-rating-form-content textarea{
    border: solid 1px #0eba7c !important;
    padding: 10px 0 0 15px !important;
  height: 96px !important;
}
.review_form_list  .rwp-stars:before {
    content: "Add rate";
    font-family: Poppins !important;
    font-size: 16px;
    font-weight: 400;
    display: inline;
    margin-right: 10px;

    line-height: 0.1;

}

.review_form_list .rwp-box__sorting i{
	    flex-basis: unset;
	margin-right: 10px;
}
.review_form_list{
    max-width: 552px;
}
.review_form_list label{
    background-image: url(/wp-content/themes/realestatesinvestment/assets/img/starsempty.png) !important;
}

.review_form_list  .rwp-rating-form-content{
    border-bottom: 1px solid black;

}

.review_form_list label:last-child  {
display:none !important;
}



.review_form_list .rwp-box__sorting i{
    font-family: Montserrat !important;
    font-size: 14px !important;
    font-weight: 500 !important;
    color: #242424 !important;
}
.review_form_list .rwp-select select{
    font-family: Montserrat !important;
    font-size: 13px !important;
    font-weight: 600 !important;
    width:89px !important;
    color: #0eba7c !important;
    border: 0px;

}
 .review_form_list .rwp-select select option{
    font-family: Montserrat !important;
    font-size: 12px !important;
    font-weight: 400 !important;
    padding: 10px;
    width:89px !important;
    color: #0eba7c !important;
    border: 0px;

}




.review_form_list .rwp-box__sorting{

	margin-top: 0 !important;
}

.review_form_list .rwp-select:after {
 position: inherit !important;
top:0 !important;
 width: unset !important;
left: 0 !important;

	 }


.review_form_list .rwp-review-wrap .rwp-review .rwp-users-reviews-wrap{


	margin-top: 7px !important;
}
.rwp-box__sorting {
    margin-bottom: 2px  !important;


}
.review_form_list  .rwp-u-review__content{
    margin-left:  0 !important;
}

.review_form_list .rwp-submit-wrap input:hover{
    transform: scale(1.1);
  transition: all 0.2s;
}
.review_form_list .rwp-submit-wrap input{

border: 1px solid #0eba7c!important;
    font-family: Poppins  !important;
    font-size: 14px  !important;
    letter-spacing: -0.2px  !important;
    font-weight: 400  !important;
    transition: all 0.2s;
    background-color: #0eba7c
    !important;

    color: white  !important;
    border-radius: 22px  !important;
    padding: 11px 42px  !important;

}
.review_form_list .rwp-submit-wrap{
    display: inline-block;
    float: right;

}
.review_form_list  .rwp-u-review__content {
    display: flex;
    flex-direction: column;
    border-bottom: 1px solid #242424;


 }

 .review_form_list  .rwp-u-review__username{
 font-family: Poppins;
  font-size: 12px;
  font-weight: 300;

 }

 .review_form_list .rwp-numeric-rating i{

    display: none;
 }
 .review_form_list .rwp-numeric-rating span{
    letter-spacing: 0.21px;
    font-family: Montserrat;
  font-size: 24px;
  font-weight: 600;
  color:#242424;
 }





 .review_form_list .rwp-numeric-rating{
    letter-spacing: 0.21px;
    font-family: Montserrat;
    color: #242424;
    font-size: 24px;
    font-weight: 500;
 }
 .review_form_list  .rwp-u-review__comment{
    order:-1;
}
.review_form_list input::placeholder, .review_form_list textarea::placeholder{
color:#242424;
    font-family: Poppins;
    font-size: 16px;
    font-weight: 300;
}
.review_form_list  .rwp-u-review__actions{

    order:6;
}
.review_form_list .rwp-u-review__date{
    order:5

}
.rwp-u-review__userinfo{

order:4;
}
.review_form_list  .rwp-scores-sliders{

    margin: 6px 0 20px  !important;
    display: inline-block;
}
.user_reviews_list_latest .rwp-widget-ratings{
    max-width: 100%;
}
.user_reviews_list .rwp-widget-ratings {

    max-width: 100%;
}




.rwp-u-review__positive-icon {
    display: inline-block;
    vertical-align: middle;
    width: 20px;
    height: 20px;
    background-image: url(/wp-content/themes/realestatesinvestment/assets/img/like.svg);
    background-repeat: no-repeat;
    background-size: 19px;
    background-position: 0px 3px;
    margin: 0 3px;
}
.rwp-u-review__negative-icon {
    display: inline-block;
    vertical-align: middle;
    width: 20px;
    height: 20px;
    background-image: url(/wp-content/themes/realestatesinvestment/assets/img/unlike.svg);
    background-repeat: no-repeat;
    background-size: 19px;
    background-position: 0px 5px;
    margin: 0px 2px 0 2px;
}
.rwp-u-review__positive-count{
    font-family: Poppins;
    font-size: 12px;
    font-weight: 300;
color:#242424;

}
.rwp-u-review__negative{
    direction: rtl;
    margin-left: 4px;
}
.rwp-u-review__sharing a{
    color: #242424;
}
.rwp-u-review__negative-count{

 font-family: Poppins;
 font-size: 12px;
 font-weight: 300;
 color: #242424;
}
.single-review-header__score .rwp-criterion-label, .single-review-header__score .rwp-criterion-bar-base, .single-review-header__star .rwp-rating-stars-count, .single-review-header__votes .rwp-rating-stars{
    display: none;
}
.single-review-header__votes .rwp-rating-stars-count {
    font-size: 20px;
    margin-top: 0;
}
.single-review-header__score .rwp-criterion-score {
 font-size: 20px;
}

    </style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-194132870-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-194132870-1');
</script>

<script>
(function (a, d, o, r, i, c, u, p, w, m) {
m = d.getElementsByTagName(o)[0], a[c] = a[c]
|| {}, a[c].trigger = a[c].trigger || function () {
(a[c].trigger.arg = a[c].trigger.arg || []).push(arguments)},
a[c].on = a[c].on || function () {(a[c].on.arg = a[c].on.arg || []).push(arguments)},
a[c].off = a[c].off || function () {(a[c].off.arg = a[c].off.arg || []).push(arguments)
}, w = d.createElement(o), w.id = i, w.src = r, w.async = 1, w.setAttribute(p, u),
m.parentNode.insertBefore(w, m), w = null}
)(window, document, "script", "https://21778888.adoric-om.com/adoric.js", "Adoric_Script", "adoric","fe6aba937070ef0dd72ea8dae5062a7c", "data-key");
</script>
</head>
<body>
    <header>
        <div class="header-block container">
            <a href="/" class="header__img"><img src="/wp-content/themes/realestatesinvestment/assets/img/header-logo.svg" alt="logo" width="142" height="48"></a>
            <nav>
                <?php wp_nav_menu( array(
                    'container_class' => 'menu-header',
                    'theme_location' => 'main_menu',
                    'container'       => 'ul',
                    'menu_class'      => 'menu-main-menu',
                    ) ); ?>
            </nav>

            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
            	<div class="menu-icon"></div>
            </button>
        </div>
    </header>
