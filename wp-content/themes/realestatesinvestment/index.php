<?php get_header(); ?>
    <main id="main" class="home-page">
        <div class="container">
                <div class="main-content">

                <?php $home_main_top_post = get_field('home_main_top_post', 'options');
                    if( $home_main_top_post ):
                        foreach( $home_main_top_post as $post ): setup_postdata($post); ?>
                            <article class="home-one-post" style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);">
                                <a href="<?php the_permalink() ?>">
                                    <h2 class="home-one-post__h2">Exclusive</h2>
                                    <h1 class="home-one-post__h1"><?php the_title() ?></h1>
                                    <div class="home-one-post__autor-data">
                                        <div class="home-one-post__autor-ava"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '33'); ?></div>
                                        <div class="home-one-post__autor"><?php the_author() ?></div>
                                        <div class="home-one-post__data"><?php the_date('F d, Y'); ?></div>
                                    </div>
                                </a>
                            </article>
                    <?php
                    endforeach;
                    endif;
                    ?>
                <?php $home_main_buttom_post = get_field('home_main_buttom_post', 'options');
                    if( $home_main_buttom_post ): ?>
                    <div class="home-two-post">
                    <?php foreach( $home_main_buttom_post as $post ): setup_postdata($post);?>

                            <article class="home-two-post__item">
                                <a href="<?php the_permalink() ?>" class="home-two-post__img"><?php the_post_thumbnail(array(366, 200)) ?></a>

                                <div class="home-two-post__cat-row">
                                    <?php
                                    foreach ( get_the_category() as $category ) {
                                        printf(
                                            '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                                            esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                                            esc_html( $category->name ) // Название рубрики
                                        );
                                    }
                                    ?>
                                </div>

                                <a href="<?php the_permalink() ?>" class="home-two-post__title"><?php the_title() ?></a>
                                <div class="home-one-post__autor-data">
                                    <div class="home-one-post__autor-ava"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '33'); ?></div>
                                    <div class="home-one-post__autor"><?php the_author() ?></div>
                                    <div class="home-one-post__data"><?php the_date('F d, Y'); ?></div>
                                </div>
                            </article>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                    <?php $home_popular_post = get_field('home_popular_post', 'options');
                       if( $home_popular_post ):
                    ?>
                    <div class="home-bottom-block-post">
                        <div class="home-bottom-block-post__titile"><span><?php the_field('popular_title', 'options'); ?></span></div>
                        <div class="home-bottom-block-post__row">
                        <?php foreach( $home_popular_post as $post ): setup_postdata($post);?>
                                    <div class="home-bottom-block-post__item">
                                        <a href="<?php the_permalink() ?>" class="home-bottom-block-post__img">
                                        <?php the_post_thumbnail(array(262, 200)) ?>
                                        </a>
                                        <div class="home-bottom-block-post__text">
                                            <div class="home-two-post__cat-row">
                                            <?php
                                                foreach ( get_the_category() as $category ) {
                                                    printf(
                                                        '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                                                        esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                                                        esc_html( $category->name ) // Название рубрики
                                                    );
                                                }
                                            ?>
                                            </div>
                                            <h2 class="home-bottom-block-post__h2">
                                                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </h2>
                                            <?php if($description_homepage = get_field('short_description_homepage')) {?>
                                                <div class="home-bottom-block-post__desc"><?php echo $description_homepage; ?></div>
                                            <?php }?>
                                            <div class="home-one-post__autor-data">
                                                <div class="home-one-post__autor-ava"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '33'); ?></div>
                                                <div class="home-one-post__autor"><?php the_author() ?></div>
                                                <div class="home-one-post__data"><?php the_date('F d, Y'); ?></div>
                                            </div>
                                        </div>
                                    </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php $home_trending_post = get_field('home_trending_post', 'options');
                            if( $home_trending_post ):
                    ?>
                    <div class="home-bottom-block-post">
                        <div class="home-bottom-block-post__titile"><span><?php the_field('trending_title', 'options'); ?></span></div>
                        <div class="home-bottom-block-post__row">
                        <?php
                                foreach( $home_trending_post as $post ): setup_postdata($post);?>
                                    <div class="home-bottom-block-post__item">
                                        <a href="<?php the_permalink() ?>" class="home-bottom-block-post__img">
                                        <?php the_post_thumbnail(array(262, 200)) ?>
                                        </a>
                                        <div class="home-bottom-block-post__text">
                                            <div class="home-two-post__cat-row">
                                            <?php
                                                foreach ( get_the_category() as $category ) {
                                                    printf(
                                                        '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                                                        esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                                                        esc_html( $category->name ) // Название рубрики
                                                    );
                                                }
                                            ?>
                                            </div>
                                            <h2 class="home-bottom-block-post__h2">
                                                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                            </h2>
                                            <?php if($description_homepage = get_field('short_description_homepage')) {?>
                                                <div class="home-bottom-block-post__desc"><?php echo $description_homepage; ?></div>
                                            <?php }?>
                                            <div class="home-one-post__autor-data">
                                                <div class="home-one-post__autor-ava"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '33'); ?></div>
                                                <div class="home-one-post__autor"><?php the_author() ?></div>
                                                <div class="home-one-post__data"><?php the_date('F d, Y'); ?></div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>

                <div class="sidebar">
                    <?php get_sidebar() ?>
                </div>


        </div>

        <div class="container">
                <div class="home-companies">
                        <div class="home-companies__title"><?php the_field('real_estate_investments_companies_title', 'options'); ?></div>
                        <ul class="home-companies__row">
                        <?php
                            $query = new WP_Query([
                                'post_type' => 'reviews',
                                'post_status' => 'publish',
                                'posts_per_page' => 12,
                            ]);
                            do_action( 'lineup_100_homepage', 'lineup_100_homepage', 'reviews', -1);
                            ?>
                            <?php if($query->have_posts()):
                            while($query->have_posts()) : $query->the_post();
                                include (locate_template( 'template-parts/content-home.php' ));
                            endwhile; ?>

                            <?php endif; ?>
                        </ul>
                        <?php // AJAX загрузка постов ?>
                        <?php if (  $query->max_num_pages > 1 ) : ?>
                            <script>
                                var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                                var true_posts = '<?php echo serialize($query->query_vars); ?>';
                                var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                                var max_pages = '<?php echo $query->max_num_pages; ?>';
                            </script>
                            <div id="true_loadmore"><?php the_field('read_more_btn', 'options'); ?></div>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                        <!-- <div class="home-companies__read-more">Read more</div> -->
                    </div>
            </div>
<div class="lastReviewsSlider">
            <div class="container">
                            <div class="sidebar-witget__title">What do users think about the investment companies?</div>
                            <!-- <div class="sidebar-witget__sub-title"><span>Top Rated</span></div> -->
                            <div class="sidebar-latest-reviews__row">
<?php
                                if ( $query->have_posts() ) :
                                    while ( $query->have_posts() ) : $query->the_post();
                                        $allPostId[] = get_the_ID();
                                    endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif;
foreach ($allPostId as $value) {
    $arrComm = get_post_meta( $value, 'rwp_rating_0' );
    foreach ( $arrComm as $arrComms ) {
        $ratingScoring = $arrComms['rating_score'][0];
        if($ratingScoring == 5) {

            $commntObject[$arrComms['rating_date']][0] = $arrComms['rating_id'];
            $commntObject[$arrComms['rating_date']][1] = $arrComms['rating_date'];
            $commntObject[$arrComms['rating_date']][2] = $arrComms['rating_comment'];
            $commntObject[$arrComms['rating_date']][3] = $arrComms['rating_post_id'];
            $commntObject[$arrComms['rating_date']][4] = $arrComms['rating_user_name'];
            $commntObject[$arrComms['rating_date']][5] = $arrComms['rating_score'][0];
        }
        // $comObj[$value] = $commntObject;
    }
}
krsort($commntObject);
$i=1;
foreach ($commntObject as $key) { ?>
                                <div class="sidebar-latest-reviews__item">
                                    <div class="sidebar-latest-reviews__title">
                                        <?php $post_object = get_post( $key[3] );
                                             echo $post_object->post_title;
                                        ?>

                                    </div>
                                    <div class="home-one-post__autor-data">
                                        <div class="home-one-post__autor"><?php echo $key[4]; ?></div>
                                        <div class="home-one-post__data"><?php echo date('F d, Y, H:i', $key[1]); ?></div>
                                    </div>
                                    <div class="home-one-post__text"><?php echo $key[2]; ?></div>
                                        <div class="home-one-post__star">
                                        <div style="width: <?php echo $key[5] * 16;?>px;"></div>
                                        </div>
                                </div>
<?php

    $i++;
    if($i==4)break;
}
?>

                            <!-- </div> -->
                            <!-- <div class="sidebar-witget__sub-title"><span>Recent</span></div> -->

                            <!-- <div class="sidebar-latest-reviews__row"> -->


                            <?php
foreach ($allPostId as $value) {
    $arrComm = get_post_meta( $value, 'rwp_rating_0' );
    foreach ( $arrComm as $arrComms ) {
        $ratingScoring = $arrComms['rating_score'][0];


            $commntObject[$arrComms['rating_date']][0] = $arrComms['rating_id'];
            $commntObject[$arrComms['rating_date']][1] = $arrComms['rating_date'];
            $commntObject[$arrComms['rating_date']][2] = $arrComms['rating_comment'];
            $commntObject[$arrComms['rating_date']][3] = $arrComms['rating_post_id'];
            $commntObject[$arrComms['rating_date']][4] = $arrComms['rating_user_name'];
            $commntObject[$arrComms['rating_date']][5] = $arrComms['rating_score'][0];

        // $comObj[$value] = $commntObject;
    }
}
krsort($commntObject);
$i=1;
foreach ($commntObject as $key) { ?>
                                <div class="sidebar-latest-reviews__item">
                                    <div class="sidebar-latest-reviews__title">
                                        <?php $post_object = get_post( $key[3] );
                                             echo $post_object->post_title;
                                        ?>

                                    </div>
                                    <div class="home-one-post__autor-data">
                                        <div class="home-one-post__autor"><?php echo $key[4]; ?></div>
                                        <div class="home-one-post__data"><?php echo date('F d, Y, H:i', $key[1]); ?></div>
                                    </div>
                                    <div class="home-one-post__text"><?php echo $key[2]; ?></div>
                                        <div class="home-one-post__star">
                                            <div style="width: <?php echo $key[5] * 16;?>px;"></div>
                                        </div>
                                </div>
<?php

    $i++;
    if($i==4)break;
}
?>
    </div>
</div>

<script>
jQuery(function($){
    $(document).on('click','#true_loadmore',function(){
		$(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
		var data = {
			'action': 'loadmoreHome',
			'query': true_posts,
			'page' : current_page,
            'template_part' : 'reviews',
		};
		$.ajax({
			url:ajaxurl, // обработчик
			data:data, // данные
			type:'POST', // тип запроса
			success:function(data){
				if( data ) {
					// $('#true_loadmore').text('Load more').before(data); // вставляем новые посты
                    $('#true_loadmore').text('Load more');
                    $('.home-companies__row').append(data);
					current_page++; // увеличиваем номер страницы на единицу
					if (current_page == max_pages) {
                        $('#true_loadmore').before('<div id="read_less"><?php the_field('read_less_btn', 'options'); ?></div>');
                        $('#true_loadmore').remove(); // если последняя страница, удаляем кнопку
                    }
				} else {
					$('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
				}
			}
		});
	});

    $(document).on('click','#read_less',function(){
		$(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
		var data = {
			'action': 'loadmoreHomeReadLess',
		};
		$.ajax({
			url:ajaxurl, // обработчик
			data:data, // данные
			type:'POST', // тип запроса
			success:function(data){
				if( data ) {
					// $('#true_loadmore').text('Load more').before(data); // вставляем новые посты
                    $('#true_loadmore').text('Load more');
                    $('.home-companies__row').html(data);
					current_page = 1; // увеличиваем номер страницы на единицу
					if (current_page == 1) {
                        $('#read_less').before('<div id="true_loadmore"><?php the_field('read_more_btn', 'options'); ?></div>');
                        $('#read_less').remove();

                    }
				} else {
					$('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
				}
			}
		});
	});

});
</script>
<?php get_footer(); ?>
