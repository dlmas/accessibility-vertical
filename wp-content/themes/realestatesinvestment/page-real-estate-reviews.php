<?php get_header(); ?>
<main id="main">
    <div class="container">
	<div style="clear:both;"></div>
        <div id="article" class="main-content">
			<div class="desc-show">
            	<?php the_content(); ?>
			</div>
<br /><br /><br />
<div class="witget-top3__title"><?php the_field('title_top_3_witget', 'options'); ?></div>
<br /><br />
			<div class="witget-top3__row">

				<?php
				// переключаемся на блог 1
				//switch_to_blog( 1 );

				// Выводим данные блога на который переключились
				// Получаем посты с блога 1


				$query = new WP_Query( array(
					'post_type' => 'reviews',
					'posts_per_page'	=> '3',
					'order' => 'DESC',
					'orderby' => 'meta_value',
                    'meta_key' => 'rwp_user_score',
				));
				if ( $query->have_posts() ) : 
				$i = 1;
				while ( $query->have_posts() ) : $query->the_post(); ?>
				<a href="<?php the_field('visit_site_url'); ?>" target="_blank" class="witget-top3__item" id="witget-top3__item" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_3_top_##_' . $i; ?>">
					<div class="witget-top3__img"><?php the_post_thumbnail(); ?></div>
					<div class="witget-top3__block">
						<div class="witget-top3__text"><?php the_field('top_3_text'); ?></div>
						<div class="witget-top3__raiting"><span>
						<?php 
										$meta_values = get_post_meta( get_the_ID(), 'rwp_user_score' );
										$meta_values = round($meta_values[0], 1);
										echo $meta_values;
										?>
						</span></div>
						<div class="witget-top3__link"><?php the_field('learn_more_btm', 'options')?></div>
					</div>
				</a>
				<?php $i++;
					endwhile; 
				endif; 
				wp_reset_query(); 

				// возвращемся к текущему блогу
				//restore_current_blog();

				?>
				</div>
				<br />
				<div class="witget-top3__title"><?php the_field('title_top_all_witget', 'options'); ?></div>
				
            <div class="reviews-archive-row">
                <?php $query = new WP_Query( array(
									'post_type' => 'reviews',
									'posts_per_page'	=> '10',
									'order' => 'DESC',
									'orderby' => 'meta_value',
                                    'meta_key' => 'rwp_user_score',
								));
                        do_action( 'lineup_100_reviewpage', 'lineup_100_reviewpage', 'reviews', 10);
                        if ( $query->have_posts() ) : 
							while ( $query->have_posts() ) : $query->the_post(); 
                                include (locate_template( 'template-parts/content-reviews.php' ));
                            ?>

                <?php endwhile; endif;?>
                <?php // AJAX загрузка постов ?> 
			<?php if (  $query->max_num_pages > 1 ) : ?>
                <script>
                    var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                    var true_posts = '<?php echo serialize($query->query_vars); ?>';
                    var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                    var max_pages = '<?php echo $query->max_num_pages; ?>';
                </script>
                <div id="true_loadmore"><?php the_field('show_more_options', 'options'); ?></div>
            <?php endif; ?>
            </div>
        </div>
        <div id="aside1" class="sidebar">
            <?php get_sidebar() ?>
        </div>
		<div style="clear:both;"></div>
    </div>
</main>
<script>
jQuery(function($){
	$('#true_loadmore').click(function(){
		$(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
		var data = {
			'action': 'reviews',
			'query': true_posts,
			'page' : current_page,
            'template_part' : 'reviews',
		};
		$.ajax({
			url:ajaxurl, // обработчик
			data:data, // данные
			type:'POST', // тип запроса
			success:function(data){
				if( data ) { 
					$('#true_loadmore').text('Load more').before(data); // вставляем новые посты
					current_page++; // увеличиваем номер страницы на единицу
					if (current_page == max_pages) $("#true_loadmore").remove(); // если последняя страница, удаляем кнопку
				} else {
					$('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
				}
			}
		});
	});
	
	$('.witget-all__item .adoric_popup_ppc').on('click', function() {
		let nameReviews = $(this).attr('data-reviews') ;
		function sayHi() {
			$('.element-input-text[aria-label="Brand review"]').attr('readonly', true);
			$('.element-input-text[aria-label="Brand review"]').val( nameReviews );
		}
		setTimeout(sayHi, 100);
	});
});
</script>
<?php get_footer(); ?>