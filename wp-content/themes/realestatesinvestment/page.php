<?php get_header(); ?>
<main id="main">
    <div class="page-title">
        <div class="container">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="container">
        <div class="archive-content">
            <?php the_content(); ?>
        </div>
    </div>
</main>
                
<?php get_footer(); ?>