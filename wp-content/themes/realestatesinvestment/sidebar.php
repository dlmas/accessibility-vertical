<?php global $query; ?>
                    <div class="sidebar-witget">
                        

                        <div class="sidebar-witget__title">Reviews</div>
                        <div class="tabs-wrapper">
                            <div class="sidebar-witget-nav__row">
                                <div class="sidebar-witget-nav__item tab">TOP RATED</div>
                                <div class="sidebar-witget-nav__item tab">LOW RATED</div>
                                <div class="sidebar-witget-nav__item tab">RECENT</div>
                                <div class="sidebar-witget-nav__item tab">POPULAR</div>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php 
                            $query = new WP_Query( array(
									'post_type' => 'reviews',
									'posts_per_page'	=> '5',
									'order' => 'DESC',
                                    'orderby' => 'meta_value',
                                    'meta_key' => 'rwp_user_score',
								));
                                
                                do_action( 'lineup_5_top', 'lineup_5_top', 'reviews', 5);
						        if ( $query->have_posts() ) : 
							        while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(255 203 64)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                <?php endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif;
                                ?>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php $query = new WP_Query( array(
									'post_type' => 'reviews',
									'posts_per_page'	=> '5',
									'order' => 'ASC',
                                    'orderby' => 'meta_value',
                                    'meta_key' => 'rwp_user_score',
								));
                                do_action( 'lineup_5_low', 'lineup_5_low', 'reviews', 5);
						        if ( $query->have_posts() ) : 
							        while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(255 203 64)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                <?php endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif;
                                ?>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php $query = new WP_Query( array(
									'post_type' => 'reviews',
									'posts_per_page'	=> '5',
									// 'order' => 'ASC',
                                    'orderby' => 'date',
                                    // 'meta_key' => 'rwp_user_score',
								));
                                do_action( 'lineup_5_recent', 'lineup_5_recent', 'reviews', 5);
						        if ( $query->have_posts() ) : 
							        while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(255 203 64)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                <?php endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif;
                                ?>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php 
                                //Сортировка по комменариям
                                $query = new WP_Query( array(
                                'post_type' => 'reviews',
                                'posts_per_page'	=> '-1',

                                ));
                                if ( $query->have_posts() ) : 
                                    while ( $query->have_posts() ) : $query->the_post();
                                        $allPostId[] = get_the_ID();
                                    endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif; 
                                
                                $numberComments = [];
                                foreach ($allPostId as $value) {
                                    $allComment = get_post_meta( $value, 'rwp_rating_0' ); //массив с кол-вом комментариев
                                    $numberComments[count($allComment)] = $value;
                                }
                                krsort($numberComments);

                                // $i=1;
                                foreach ($numberComments as $key => $val) {
                                    $sortTopCom[] = $val;
                                    // $i++;
                                    // if($i==5)break;
                                }
                                $query = new WP_Query( array(
                                    'post_type' => 'reviews',
                                    'post__in' => $sortTopCom,
                                    'orderby'        => 'post__in',
                                    'posts_per_page'	=> '5',
                                ));
                                do_action( 'lineup_5_popular', 'lineup_5_popular', 'reviews', 5);
                                if ( $query->have_posts() ) : 
                                    while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(255 203 64)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                    <?php
                                    endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif; 

                                ?>
                            </div>
                        </div>
                    </div>
                        
                    <div class="sidebar-witget">
                        <div class="sidebar-witget__title">Basics of Real Estate</div>
                        <div class="sidebar-latest-reviews__row">
                                <?php
                                $lastedBlog = get_field('basics_of_real_estate', 'options');
                                foreach( $lastedBlog as $post ): setup_postdata($post);
                                ?>
                                    <div class="sidebar-latest-reviews__item">
                                    <a href="<?php the_permalink(); ?>" class="basicsRealEstate-image"><?php the_post_thumbnail('medium'); ?></a>
                                    <a href="<?php the_permalink(); ?>" class="basicsRealEstate-title"><?php the_title(); ?></a>
                                    </div>
                                <?php 
                                endforeach;
                                wp_reset_postdata(); // reset the query
                                ?>
                        </div>
                    </div>

                    <div class="sidebar-witget">
                        <div class="sidebar-witget__title">Latest Blog</div>
                        <div class="sidebar-latest-reviews__row">
                                <?php  
                                $lastedBlog = get_field('latest_blog', 'options');
                                foreach( $lastedBlog as $post ): setup_postdata($post); ?>
                                    <div class="sidebar-latest-reviews__item">
                                    <a href="<?php the_permalink(); ?>" class="basicsRealEstate-image"><?php the_post_thumbnail('medium'); ?></a>
                                    <a href="<?php the_permalink(); ?>" class="basicsRealEstate-title"><?php the_title(); ?></a>
                                    </div>
                                <?php 
                                endforeach;
                                wp_reset_postdata(); // reset the query
                                ?>
                        </div>
                    </div>


