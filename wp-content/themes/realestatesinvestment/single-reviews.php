<?php get_header(); ?>
<main id="main">
    <div class="container">
        <div id="article" class="main-content">
            <h1 class="single-review-title"><?php the_title(); ?></h1>
            <div class="single-review-header">
                <div class="single-review-header__logo"><?php the_post_thumbnail(array(200, 60)) ?></div>
                <div class="single-review-header__score">
                    <?php echo get_post_rating(); ?>
                </div>
                <div class="single-review-header__votes">
                    <?php echo get_post_number_votes(); ?>
                </div>
                <div class="single-review-header__star">
                    <?php echo get_stars(); ?>
                </div>
                <a href="<?php the_field('website_url'); ?>" class="single-review-header__btn" target="_blank"><?php the_field('visit_website_btn', 'options'); ?></a>
            </div>
            <!-- <div class="single-rewiew-sidebar single-rewiew-sidebar_main">
                <div class="single-rewiew-sidebar__logo"><?php the_post_thumbnail(array(200, 60)) ?></div>
                <ul class="single-rewiew-sidebar__row">
                    <li class="single-rewiew-sidebar__item">Objective Agent Performance Data</li>
                    <li class="single-rewiew-sidebar__item">Your Inputs and Individual Needs</li>
                    <li class="single-rewiew-sidebar__item">Local Real Estate Market Conditions</li>
                </ul>
                <a href="#form" class="single-rewiew-sidebar__btn">Register Now</a>
            </div> -->
            <div class="main-content-single">
                <?php the_content(); ?>
                <?php the_field('Full_content'); ?>
            </div>

            <?php //comments_template(); ?>
            <div class="review_form_list">
                <div class="review_form_list__title"><?php the_field('reviews_title', 'options'); ?></div>
                <?php //echo do_shortcode('[rwp_box id = "0" template="template1"]'); ?>
                <?php echo do_shortcode('[rwp_box_form id="0"]'); ?>
            </div>


            <div id="form">
                <div class="single-review-form">
                    <h3 class="single-review-form__title"><?php the_field('title_form_reviews', 'options'); ?> <?php echo str_replace(['Review', 'Rezension', 'Revue'], '', get_the_title()); ?></h3>
                    <?php echo do_shortcode('[contact-form-7 id="8086" title="Register with Review"]'); ?>
                </div>
            </div>



            <div class="claim-ownership">
                <div class="claim-ownership__item">
                    <h4 class="claim-ownership__title"><?php the_field('claim_ownership_title', 'options'); ?></h4>
                    <div class="claim-ownership__text"><?php the_field('claim_ownership_text', 'options'); ?></div>
                </div>
                <div class="claim-ownership__item claim-ownership__item_btn">
                    <div class="adoric-claim-ownership claim-ownership__btn"><?php the_field('claim_ownership_title', 'options'); ?></div>
                </div>
            </div>
        </div>
        <div id="aside1" class="sidebar">
            <div class="single-rewiew-sidebar">
                <div class="single-rewiew-sidebar__logo">
                <?php the_post_thumbnail(array(200, 60)) ?>
                </div>
                <ul class="single-rewiew-sidebar__row">
                    <?php  while( have_rows('register_block_repeater', 'options') ) : the_row(); ?>
                    <li class="single-rewiew-sidebar__item"><?php the_sub_field('register_block_title'); ?></li>
                    <?php endwhile; ?>
                </ul>
                <a href="#form" class="single-rewiew-sidebar__btn"><?php the_field('register_now_btn', 'options'); ?></a>
            </div>
            <?php get_sidebar() ?>
        </div>
        <div style="clear:both;"></div>
    </div>
    <div class="reviewsFullPage">
        <div class="container">
            <div class="sidebar-witget__title"><?php the_field('reviews_title', 'options'); ?></div>
            <ul class="sidebar-latest-reviews__row ">
            <?php
            $comment = get_post_meta( get_the_ID(), 'rwp_rating_0' );
            foreach($comment as $val) { ?>
                <div class="sidebar-latest-reviews__item">
                    <div class="sidebar-latest-reviews__title"><?php echo $val['rating_user_name']; ?></div>
                    <div class="home-one-post__autor-data">
                        <div class="home-one-post__data"><?php echo date('F d, Y, H:i', $val['rating_date']); ?></div>
                    </div>
                    <div class="home-one-post__text"><?php echo $val['rating_comment']; ?></div>
                    <div class="home-one-post__star"><div style="width: <?php echo $val['rating_score'][0] * 16;?>px;"></div></div>
                </div>
            <?php } ?>
            </ul>
        </div>
    </div>
</main>
<?php get_footer(); ?>
