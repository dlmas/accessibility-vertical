<?php get_header(); ?>
<main id="main">
    <div class="container">
    <div style="clear:both;"></div>
        <div id="article" class="main-content single">
            <h1 class="title-page"><?php the_title(); ?></h1>
            <div class="home-one-post__autor-data">
                <div class="home-one-post__autor-ava"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '33'); ?></div>
                <div class="home-one-post__autor"><?php the_author() ?></div>
                <div class="home-one-post__data"><?php the_date('F d, Y'); ?></div>
            </div>
            <div class="single-content-img"><?php the_post_thumbnail(); ?></div>
            <?php the_content(); ?>
            <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                }
            ?>
            <div class="home-two-post__cat-row">
                <?php
                    $posttags = get_the_tags();
                    if( $posttags ){
                        foreach( $posttags as $tag ){
                            echo '<a href="/' . $tag->slug . '" class="home-two-post__cat-item">' . $tag->name . '</a>';
                        }
                    }
                ?>
            </div>
            <br>
            <h2>Related articles</h2>
            <?php 
                                            $query = new WP_Query( array(
                                                'post_type' => 'post',
                                                'orderby'        => 'rand',
                                                'posts_per_page'	=> '5',
                                            ));
            ?>
            <div class="related-articles__row">
                <?php  while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="related-articles__item">
                        <a href="<?php the_permalink(); ?>" class="related-articles__item-img">
                            <?php the_post_thumbnail(); ?>
                        </a>
                        <a href="" class="related-articles__item-title"><?php the_title(); ?></a>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div id="aside1" class="sidebar">
            <?php get_sidebar() ?>
        </div>
        <div style="clear:both;"></div>
    </div>
</main>
                
<?php get_footer(); ?>