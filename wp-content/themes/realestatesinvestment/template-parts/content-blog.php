                        <div class="archive-page-item">
                            <a href="<?php the_permalink() ?>" class="archive-page-item__img"><?php the_post_thumbnail(array(359, 160)) ?></a>
                            <div class="home-two-post__cat-row">
                            <?php
                                                foreach ( get_the_category() as $category ) {
                                                    printf(
                                                        '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                                                        esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                                                        esc_html( $category->name ) // Название рубрики
                                                    );
                                                }
                                            ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="home-two-post__title"><?php the_title(); ?></a>
                            <div class="home-one-post__autor-data">
                                <div class="home-one-post__autor-ava"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '33'); ?></div>
                                <div class="home-one-post__autor"><?php the_author() ?></div>
                                <div class="home-one-post__data"><?php echo get_the_date('F d, Y'); ?></div>
                            </div>
                        </div>