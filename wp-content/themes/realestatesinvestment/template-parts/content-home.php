<li class="home-companies__item">
                                    <a href="<?php the_permalink() ?>">
                                        <div class="home-companies__item-img"><?php the_post_thumbnail(array(200, 200)); ?></div>
                                        <div class="home-companies__item-title"><?php the_title() ?></div>
                                    </a>
</li>