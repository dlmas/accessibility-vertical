<div class="witget-all__item reviews-archive-item">
               <?php if($ribbon_top_text = get_field('ribbon_top_text')) { ?>
                    <div class="ribbon-top" style="background-color: <?php the_field('ribbon_top_background_color'); ?>"><?php echo $ribbon_top_text; ?></div>
                <?php } ?>
					<div class="witget-all__block">
						<div>
							<div class="witget-all__img-block">
								<a href="<?php the_permalink(); ?>" class="reviews-archive-item_img witget-all__img">
									<?php the_post_thumbnail(); ?>
								</a>
								<div class="witget-all__star_row"><?php echo get_stars(); ?></div>
							</div>
							<div class="witget-all__info">
								<div class="witget-all__title"><?php the_title(); ?></div>
								
									<div class="witget-all__list-features-row">
									<?php if( get_field('advantages') ) : ?>
										<?php while( have_rows('advantages') ): the_row(); ?>
												<div class="list-features-row__item" ><?php echo get_sub_field('advantages_text'); ?></div>
										<?php endwhile; ?>
										<?php endif; ?>
                                        
									</div>
								
								</div>
						</div>
						<div>
							<div class="witget-all__raiting">
								<span>
                                   <?php 
                                    $meta_values = get_post_meta( get_the_ID(), 'rwp_user_score' );
                                    $meta_values = round($meta_values[0], 1);
                                    echo $meta_values;
                                    ?>
                                </span>
								<?php if( $tool_tip_score = get_field('tool_tip_score', 'option') ): ?><i class="tlt-icon">?</i><?php endif;?>
								
								<div class="witget-all__raiting__desc">
                                    <?php 
                                            $raiting = $meta_values;
                                            if($raiting >= 4.6) {
												echo 'Exceptional';
                                            }else if($raiting >= 4.4 && $raiting <= 4.5) {
												echo 'Great';
                                            }else if($raiting >= 4.1 && $raiting <= 4.3) {
												echo 'Good';
                                            }else if($raiting >= 3.8 && $raiting <= 4.0) {
                                                echo 'Normal';
                                            }else if($raiting >= 3.4 && $raiting <= 3.7) {
												echo 'Average';
                                            }else if($raiting >= 3.0 && $raiting <= 3.3) {
                                                echo 'Basic';
                                            }else{
												echo '';
											}
                                    
                                    ?>
                                </div>
								<?php if( $tool_tip_score ): ?>
                                    <div class="tlt-wrap" data-open="false">
                                        <div class="tlt-text">
                                            <?php echo $tool_tip_score; ?>
                                        </div>
                                        <div class="tlt-close"><?php the_field('close_btn', 'options'); ?></div>
                                        <div class="tlt-arrow"></div>
                                    </div>
								<?php endif;?>
							</div>
							<div class="witget-all__btn">
								<div id="spb-rts-461" class="bb-bubble-rts" style="<?php if(get_field('click_views_on_off')) { echo 'display: block;'; } ?>"><?php the_field('one_over_people', 'options');?> <?php the_field('click_views_quantity'); ?> <?php the_field('tow_over_people', 'options');?></div>
								
      
                                    <div class="witget-all__link adoric_popup_ppc" data-reviews="<?php the_title(); ?>"><?php the_field('start_investing_title', 'options'); ?></div>
                                    <a href="<?php the_permalink(); ?>" class="witget-all__read-more"><span><?php the_field('read_more_btn', 'options'); ?></span></a>
               
							</div>
						</div>
					</div>
					<?php if( get_field('highlights_title') && get_field('highlights') ) :?>
					<!-- <div class="expandable">
						<div class="expandable-row">
							<div class="expandable-row__text">
								<div class="">
									<div class="expandable-row__title">Highlights</div>
									<?php if(get_field('highlights_title')) :?><div class="expandable-row__title"><?php the_field('highlights_title')?></div><?php endif;?>
									<?php if( get_field('highlights') ) : ?>
										<ul class="expandable-row__list-features">
										<?php while( have_rows('highlights') ): the_row(); ?>
											<li><?php echo get_sub_field('highlights_text'); ?></li>
										<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
								<a href="<?php the_permalink(); ?>" class="expandable__link">Read review >></a>
							</div>
							<div class="expandable-row__img">
									<?php 
									$image = get_field('image_home');
									$size = 'full'; // (thumbnail, medium, large, full или ваш размер)

									if( $image ) {
										echo wp_get_attachment_image( $image, $size );
									}
									?>
								<a href="" target="_blank" class="expandable__link">Visit <?php the_title(); ?> >></a>
							</div>
						</div>
					</div>
					<div class="witget-all__more-info">More info</div> -->
					<?php endif;?>
				</div>