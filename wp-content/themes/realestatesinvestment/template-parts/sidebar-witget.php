
                    <div class="sidebar-witget">
                        

                        <div class="sidebar-witget__title"><?php the_field('sidebar_title', 'options'); ?></div>
                        <div class="tabs-wrapper">
                            <div class="sidebar-witget-nav__row">
                                <div class="sidebar-witget-nav__item tab"><?php the_field('sidebar_title_top_rated', 'options'); ?></div>
                                <div class="sidebar-witget-nav__item tab"><?php the_field('sidebar_title_low_rated', 'options'); ?></div>
                                <div class="sidebar-witget-nav__item tab"><?php the_field('sidebar_title_recent', 'options'); ?></div>
                                <div class="sidebar-witget-nav__item tab"><?php the_field('sidebar_title_popular', 'options'); ?></div>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php 
                            $query = new WP_Query( array(
									'post_type' => 'reviews',
									'posts_per_page'	=> '5',
									'order' => 'DESC',
                                    'orderby' => 'meta_value',
                                    'meta_key' => 'rwp_user_score',
								));
                                do_action( 'lineup_5_top', 'lineup_5_top', 'reviews', 5);
                                echo '<pre>'; print_r($query); echo '</pre>';
						        if ( $query->have_posts() ) : 
							        while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(95 250 173)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                <?php endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif;
                                ?>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php $query = new WP_Query( array(
									'post_type' => 'reviews',
									'posts_per_page'	=> '5',
									'order' => 'ASC',
                                    'orderby' => 'meta_value',
                                    'meta_key' => 'rwp_user_score',
								));
                                do_action( 'lineup_5_low', 'lineup_5_low', 'reviews', 5);
						        if ( $query->have_posts() ) : 
							        while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(95 250 173)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                <?php endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif;
                                ?>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php $query = new WP_Query( array(
									'post_type' => 'reviews',
									'posts_per_page'	=> '5',
									// 'order' => 'ASC',
                                    'orderby' => 'date',
                                    // 'meta_key' => 'rwp_user_score',
								));
                                do_action( 'lineup_5_recent', 'lineup_5_recent', 'reviews', 5);
						        if ( $query->have_posts() ) : 
							        while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(95 250 173)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                <?php endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif;
                                ?>
                            </div>
                            <div class="sidebar-witget__row tab-item">
                            <?php 
                                //Сортировка по комменариям
                                $query = new WP_Query( array(
                                'post_type' => 'reviews',
                                'posts_per_page'	=> '-1',

                                ));
                                if ( $query->have_posts() ) : 
                                    while ( $query->have_posts() ) : $query->the_post();
                                        $allPostId[] = get_the_ID();
                                    endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif; 
                                
                                $numberComments = [];
                                foreach ($allPostId as $value) {
                                    $allComment = get_post_meta( $value, 'rwp_rating_0' ); //массив с кол-вом комментариев
                                    $numberComments[count($allComment)] = $value;
                                }
                                krsort($numberComments);

                                // $i=1;
                                foreach ($numberComments as $key => $val) {
                                    $sortTopCom[] = $val;
                                    // $i++;
                                    // if($i==5)break;
                                }
                                $query = new WP_Query( array(
                                    'post_type' => 'reviews',
                                    'post__in' => $sortTopCom,
                                    'orderby'        => 'post__in',
                                    'posts_per_page'	=> '5',
                                ));
                                do_action( 'lineup_5_popular', 'lineup_5_popular', 'reviews', 5);
                                if ( $query->have_posts() ) : 
                                    while ( $query->have_posts() ) : $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="sidebar-witget__item">
                                            <div class="witget-rating">
                                                <div class="witget-rating__number">
                                                <?php 
                                                $postId = get_the_ID();
                                                $meta_values = get_post_meta( $postId, 'rwp_user_score' );
                                                $meta_values = round($meta_values[0], 1);
                                                echo $meta_values;
                                                ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 43 43"><path fill="rgb(95 250 173)" fill-rule="nonzero" d="M21.5 34.52L8.166 43l3.566-15.928L0 16.362l15.453-1.413L21.5 0l6.047 14.95L43 16.362l-11.68 10.71L34.834 43z"/></svg></div>
                                                
                                            </div>
                                            <div class="witget-rating__title"><?php the_title(); ?></div>
                                        </a>
                                    <?php
                                    endwhile;
                                    wp_reset_postdata(); // reset the query
                                endif; 

                                ?>
                            </div>
                        </div>
                    </div>

                        <div class="sidebar-witget">
                            <div class="sidebar-witget__title"><?php the_field('sidebar_title_latest_reviews', 'options'); ?></div>
                            <div class="sidebar-witget__sub-title"><span><?php the_field('sidebar_title_top_rated', 'options'); ?></span></div>
                            <div class="sidebar-latest-reviews__row">
<?php 
foreach ($allPostId as $value) {
    $arrComm = get_post_meta( $value, 'rwp_rating_0' );
    foreach ( $arrComm as $arrComms ) {
        $ratingScoring = $arrComms['rating_score'][0];
        if($ratingScoring == 5) {

            $commntObject[$arrComms['rating_date']][0] = $arrComms['rating_id'];
            $commntObject[$arrComms['rating_date']][1] = $arrComms['rating_date'];
            $commntObject[$arrComms['rating_date']][2] = $arrComms['rating_comment'];
            $commntObject[$arrComms['rating_date']][3] = $arrComms['rating_post_id'];
            $commntObject[$arrComms['rating_date']][4] = $arrComms['rating_user_name'];
            $commntObject[$arrComms['rating_date']][5] = $arrComms['rating_score'][0];
        }
        // $comObj[$value] = $commntObject;
    }
}
krsort($commntObject);
$i=1;
foreach ($commntObject as $key) { ?>
                                <div class="sidebar-latest-reviews__item">
                                    <div class="sidebar-latest-reviews__title">
                                        <?php $post_object = get_post( $key[3] ); 
                                             echo $post_object->post_title;
                                        ?>

                                    </div>
                                    <div class="home-one-post__autor-data">
                                        <div class="home-one-post__autor"><?php echo $key[4]; ?></div>
                                        <div class="home-one-post__data"><?php echo date('F d, Y, H:i', $key[1]); ?></div>
                                    </div>
                                    <div class="home-one-post__text"><?php echo $key[2]; ?></div>
                                        <div class="home-one-post__star">
                                        <div style="width: <?php echo $key[5] * 16;?>px;"></div>
                                        </div>
                                </div>
<?php
    
    $i++;
    if($i==4)break;
}
?>

                            </div>
                            <div class="sidebar-witget__sub-title"><span><?php the_field('sidebar_title_recent', 'options'); ?></span></div>

                            <div class="sidebar-latest-reviews__row">


                            <?php 
foreach ($allPostId as $value) {
    $arrComm = get_post_meta( $value, 'rwp_rating_0' );
    foreach ( $arrComm as $arrComms ) {
        $ratingScoring = $arrComms['rating_score'][0];


            $commntObject[$arrComms['rating_date']][0] = $arrComms['rating_id'];
            $commntObject[$arrComms['rating_date']][1] = $arrComms['rating_date'];
            $commntObject[$arrComms['rating_date']][2] = $arrComms['rating_comment'];
            $commntObject[$arrComms['rating_date']][3] = $arrComms['rating_post_id'];
            $commntObject[$arrComms['rating_date']][4] = $arrComms['rating_user_name'];
            $commntObject[$arrComms['rating_date']][5] = $arrComms['rating_score'][0];

        // $comObj[$value] = $commntObject;
    }
}
krsort($commntObject);
$i=1;
foreach ($commntObject as $key) { ?>
                                <div class="sidebar-latest-reviews__item">
                                    <div class="sidebar-latest-reviews__title">
                                        <?php $post_object = get_post( $key[3] ); 
                                             echo $post_object->post_title;
                                        ?>

                                    </div>
                                    <div class="home-one-post__autor-data">
                                        <div class="home-one-post__autor"><?php echo $key[4]; ?></div>
                                        <div class="home-one-post__data"><?php echo date('F d, Y, H:i', $key[1]); ?></div>
                                    </div>
                                    <div class="home-one-post__text"><?php echo $key[2]; ?></div>
                                        <div class="home-one-post__star">
                                            <div style="width: <?php echo $key[5] * 16;?>px;"></div>
                                        </div>
                                </div>
<?php
    
    $i++;
    if($i==4)break;
}
?>
                            </div>
                        </div>
