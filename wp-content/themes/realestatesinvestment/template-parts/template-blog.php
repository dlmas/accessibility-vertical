<?php
/*
Template Name: Template blog
*/
?>
<?php get_header(); ?>
<main id="main">
    <div class="container">
        <div style="clear:both;"></div>
        <div id="article" class="archive-content main-content">
            <?php the_content(); ?>
            <div class="archive-page-row">
            <?php 
                $query = new WP_Query( array(
                    'post_type' => 'post',
                    'posts_per_page'	=> '20',
                ));
                if ( $query->have_posts() ) : 
                    while ( $query->have_posts() ) : $query->the_post();
                        get_template_part( 'template-parts/content', 'blog' );
                    endwhile;
                    wp_reset_postdata(); // reset the query
                endif;
            ?>
            <?php // AJAX loading posts ?>
            <?php if (  $query->max_num_pages > 1 ) : ?>
            <script>
                var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                var true_posts = '<?php echo serialize($query->query_vars); ?>';
                var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                var max_pages = '<?php echo $query->max_num_pages; ?>';
            </script>
            <div id="true_loadmore"><?php the_field('show_more_options', 'options'); ?></div>
            <?php endif; ?>
                
            </div>
        </div>
        <div id="aside1" class="sidebar">
            <?php get_sidebar() ?>
        </div>
        <div style="clear:both;"></div>
    </div>
</main>
<script>
    jQuery(document).ready(function($) {
        $('#true_loadmore').click(function() {
            $(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
            var data = {
                'action': 'loadmore',
                'query': true_posts,
                'page': current_page,
                'template_part': 'home-page',
            };
            $.ajax({
                url: ajaxurl, // обработчик
                data: data, // данные
                type: 'POST', // тип запроса
                success: function(data) {
                    if (data) {
                        $('#true_loadmore').text('Load more').before(data); // вставляем новые посты
                        current_page++; // увеличиваем номер страницы на единицу
                        if (current_page == max_pages) $("#true_loadmore").remove(); // если последняя страница, удаляем кнопку
                    } else {
                        $('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
                    }
                }
            });
        });
    });

</script>
<?php get_footer(); ?>