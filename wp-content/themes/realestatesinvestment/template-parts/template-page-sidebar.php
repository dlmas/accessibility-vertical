<?php
/*
Template Name: Template page sidebar
*/
?>
<?php get_header(); ?>
<main id="main">
    <div class="container">
    <div style="clear:both;"></div>
        <div id="article" class="main-content">
            <h1 class="title-page"><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>
        <div id="aside1" class="sidebar">
            <?php get_sidebar() ?>
        </div>
        <div style="clear:both;"></div>
    </div>
</main>
                
<?php get_footer(); ?>