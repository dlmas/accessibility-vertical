jQuery(document).ready(function($) {
    $('.sub-menu')
        .parent().parent()
        .addClass('has-sub-menu-caption');

    $('.witget-all__row .tlt-icon, .arсhive-rewiews .tlt-icon, .top-5-witget__row .tlt-icon').on('click', function() {
        $('.witget-all__raiting, .top-5-witget__rating').removeClass('open');
        $(this).next().parent().addClass('open');
    });

    $(document).mouseup(function(e) {
        var div = $(".witget-all__raiting, .top-5-witget__rating");
        if (!div.is(e.target) &&
            div.has(e.target).length === 0) {
            div.removeClass('open');
        }
    });

    $('.tlt-close').on('click', function() {
        $('.witget-all__raiting, .top-5-witget__rating').removeClass('open');
    });

    $('.menu-toggle').click(function() {
        $(this).toggleClass('active');
        $('nav').toggleClass('active');
        $('body').toggleClass('active');
    });

    jQuery(".wpcr_averageStars").each(function() {
        var val1 = jQuery(this).attr("data-wpcravg");
        var size1 = Math.max(0, (Math.min(10, val1)));
        size1 = rating_star(8, 15, 32, 39, 56, 64, 86, 88, 104, 112, size1);
        var $span1 = jQuery('<span />').width(size1);
        jQuery(this).html($span1);
    });

    jQuery(".single-reviews-info .wpcr_averageStars").each(function() {
        var val1 = jQuery(this).attr("data-wpcravg");
        var size1 = Math.max(0, (Math.min(5, val1)));
        size1 = rating_star(10, 20, 40, 50, 70, 80, 100, 110, 130, 140, size1);
        var $span1 = jQuery('<span />').width(size1);
        jQuery(this).html($span1);
    });

    jQuery(".top-5-witget .wpcr_averageStars").each(function() {
        var val1 = jQuery(this).attr("data-wpcravg");
        var size1 = Math.max(0, (Math.min(10, val1)));
        size1 = rating_star(6, 12, 24, 30, 42, 48, 60, 66, 78, 90, size1);
        var $span1 = jQuery('<span />').width(size1);
        jQuery(this).html($span1);
    });
    jQuery(".check__item .wpcr_averageStars").each(function() {
        var val1 = jQuery(this).attr("data-wpcravg");
        var size1 = Math.max(0, (Math.min(10, val1)));
        size1 = rating_star(6, 12, 24, 30, 42, 48, 60, 66, 78, 90, size1);
        var $span1 = jQuery('<span />').width(size1);
        jQuery(this).html($span1);
    });

    function rating_star(sizeWidth1, sizeWidth2, sizeWidth3, sizeWidth4, sizeWidth5, sizeWidth6, sizeWidth7, sizeWidth8, sizeWidth9, sizeWidth10, size1) {


        if (size1 >= 0.1 && size1 <= 0.6) {
            return sizeWidth1;
        } else if (size1 >= 0.7 && size1 <= 1) {
            return sizeWidth2;
        } else if (size1 >= 1.1 && size1 <= 1.6) {
            return sizeWidth3;
        } else if (size1 >= 1.7 && size1 <= 2) {
            return sizeWidth4;
        } else if (size1 >= 2.1 && size1 <= 2.6) {
            return sizeWidth5;
        } else if (size1 >= 2.7 && size1 <= 3) {
            return sizeWidth6;
        } else if (size1 >= 3.1 && size1 <= 3.6) {
            return sizeWidth7;
        } else if (size1 >= 3.7 && size1 <= 4) {
            return sizeWidth8;
        } else if (size1 >= 4.1 && size1 <= 4.6) {
            return sizeWidth9;
        } else if (size1 >= 4.7) {
            return sizeWidth10;
        } else {
            return 0;
        }
    }

    $('.witget-all__row .witget-all__more-info').on('click', function() {
        $(this).toggleClass('active');
        $(this).siblings('.expandable').toggleClass('active');
    });

    $('.menu-item-has-children').append('<div class="mobile-menu-children"></div>');

    $('.nav__row .mobile-menu-children').on('click', function() {
        $(this).toggleClass('active');
        $(this).siblings('.sub-menu').toggleClass('active');
    });


    $('.header_home__disclosure').on('click', function() {
        $('.popap-bg').toggleClass('active');
    });
    $(document).mouseup(function(e) {
        var div = $(".advertiser-disclosure-text");
        if (!div.is(e.target) &&
            div.has(e.target).length === 0) {
            $('.popap-bg').removeClass('active');
        }
    });
    $('.advertiser-close').on('click', function() {
        $('.popap-bg').removeClass('active');
    });
    if ($('#aside1').length && !$('#yourID').is(':visible')) {
        (function() {
            var a = document.querySelector('#aside1'),
                b = null,
                K = null,
                Z = 0,
                P = 0,
                N = 0;
            window.addEventListener('scroll', Ascroll, false);
            document.body.addEventListener('scroll', Ascroll, false);

            function Ascroll() {
                var Ra = a.getBoundingClientRect(),
                    R1bottom = document.querySelector('#article').getBoundingClientRect().bottom;
                if (Ra.bottom < R1bottom) {
                    if (b == null) {
                        var Sa = getComputedStyle(a, ''),
                            s = '';
                        for (var i = 0; i < Sa.length; i++) {
                            if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                                s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                            }
                        }
                        b = document.createElement('div');
                        b.className = "stop";
                        b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                        a.insertBefore(b, a.firstChild);
                        var l = a.childNodes.length;
                        for (var i = 1; i < l; i++) {
                            b.appendChild(a.childNodes[1]);
                        }
                        a.style.height = b.getBoundingClientRect().height + 'px';
                        a.style.padding = '0';
                        a.style.border = '0';
                    }
                    var Rb = b.getBoundingClientRect(),
                        Rh = Ra.top + Rb.height,
                        W = document.documentElement.clientHeight,
                        R1 = Math.round(Rh - R1bottom),
                        R2 = Math.round(Rh - W);
                    if (Rb.height > W) {
                        if (Ra.top < K) {
                            if (R2 + N > R1) {
                                if (Rb.bottom - W + N <= 0) {
                                    b.className = 'sticky';
                                    b.style.top = W - Rb.height - N + 'px';
                                    Z = N + Ra.top + Rb.height - W;
                                } else {
                                    b.className = 'stop';
                                    b.style.top = -Z + 'px';
                                }
                            } else {
                                b.className = 'stop';
                                b.style.top = -R1 + 'px';
                                Z = R1;
                            }
                        } else {
                            if (Ra.top - P < 0) {
                                if (Rb.top - P >= 0) {
                                    b.className = 'sticky';
                                    b.style.top = P + 'px';
                                    Z = Ra.top - P;
                                } else {
                                    b.className = 'stop';
                                    b.style.top = -Z + 'px';
                                }
                            } else {
                                b.className = '';
                                b.style.top = '';
                                Z = 0;
                            }
                        }
                        K = Ra.top;
                    } else {
                        if ((Ra.top - P) <= 0) {
                            if ((Ra.top - P) <= R1) {
                                b.className = 'stop';
                                b.style.top = -R1 + 'px';
                            } else {
                                b.className = 'sticky';
                                b.style.top = P + 'px';
                            }
                        } else {
                            b.className = '';
                            b.style.top = '';
                        }
                    }
                    window.addEventListener('resize', function() {
                        a.children[0].style.width = getComputedStyle(a, '').width
                    }, false);
                }
            }
        })();
    }
    $('.nav-content').on('click', function() {
        $(this).toggleClass('active');
    });

    $('.nav-content a').on('click', function() {
        $('.nav-content').removeClass('active');
    });

    $(window).scroll(function() {
        var the_top = $(document).scrollTop();
        if (the_top > 480) {
            $('.nav-content').addClass('fixed');
            $('.single-reviews-info').addClass('fixed');
            $('.single-reviews-castom-row_verdict').css('margin-top', 255);
        } else {
            $('.nav-content').removeClass('fixed');
            $('.single-reviews-info').removeClass('fixed');
            $('.single-reviews-castom-row_verdict').css('margin-top', 15);
        }
    });

    $('.component__read-more').on('click', function() {
        $('.component').toggleClass('active');
        $(this).toggleClass('active');

        if (!$(this).hasClass('active')) {
            let id = $(this).attr('data-anchor');
            if ($(document).find('#' + id).length > 0) {
                let posY = $(document).find('#' + id).offset().top;
                $('html,body').animate({
                    scrollTop: posY
                }, 1000);
            }
            return false;
        }
    });

    //btn up page
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('#scroller').fadeIn();
        } else {
            $('#scroller').fadeOut();
        }
    });
    $('#scroller').click(function() {
        $('body,html').animate({ scrollTop: 0 }, 400);
        return false;
    });

    //cookies
    $('.cookies-popup__btn').click(function() {
        $('.cookies-popup').fadeOut(1000);
        localStorage.setItem('cookies', 1);
    });
    if (localStorage.getItem('cookies') == 1) {
        $('.cookies-popup').css('display', 'none');
    } else {
        setTimeout(() => $('.cookies-popup').fadeIn(1000), 2000);
    }

    //adoric popup
    $('.btn-popup__close-btn').click(function() {
        $('.btn-popup').fadeOut(1000);
    });
    $( document ).on( 'click', '.adoric_popup', function () {
        $('.btn-popup').fadeIn(1000);
        let titleReviews = $(this).parent().parent().parent().find('.witget-all__title').html();
        // $('.btn-popup input[placeholder="Web address"]').val(titleReviews).prop('disabled', true);
        $('.btn-popup__reviews').val(titleReviews).prop('readonly', true);
    });
});