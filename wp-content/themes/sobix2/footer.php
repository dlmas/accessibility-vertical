<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sobix
 */

?>
<footer>
    <div class="container">
        <div class="footer-expert__title"><?php the_field('team_of_experts', 'option')?></div>
        <div class="footer-expert__row">

            <?php if( have_rows('team_of_experts_footer', 'options') ): 
                while( have_rows('team_of_experts_footer', 'options') ): the_row(); ?>
                    <div class="footer-expert__item">
                        <div class="footer-expert__img">
                        <?php 
                            $image = get_sub_field('images_team_footer');
                            $size = array(250, 250); // (thumbnail, medium, large, full or custom size)
                            if( $image ) {
                                echo wp_get_attachment_image( $image, $size );
                            }
                        ?>
                        </div>
                        <div class="footer-expert__name">
                            <div class="footer-expert__name-famely"><?php the_sub_field('name_team_footer'); ?></div>
                            <div class="footer-expert__name-prof"><?php the_sub_field('position_team_footer'); ?></div>
                            <div class="footer-expert__education"><strong><?php the_sub_field('education_team_footer'); ?></strong></div>
                            <div class="footer-expert__experience"><em><?php the_sub_field('experience_team_footer'); ?></em></div>
                        </div>
                    </div>
            <?php endwhile; endif;?>

        </div>
        <div class="footer-top">
            <div class="footer-left logo"><?php $image = get_field('footer_logo', 'options'); echo wp_get_attachment_image( $image, array(166, 36) ); ?></div>
            <div class="footer-right">

            </div>
        </div>
        <div class="footer-bottm">
            <div class="footer-bottm__item footer-bottm__text">
                <div class="footer-text">
                    <?php the_field('footer_content', 'options'); ?>
                </div>
            </div>
            <div class="footer-bottm__item footer-bottm__item_menu">
                <div class="footer-bottm__item-one">
                    <?php wp_nav_menu( [ 
                                    'theme_location' => 'menu-2',
                                    'menu_class' => 'footer-menu-top',
                                ] ); ?>
                    <?php if(domain_user() != 'topwebaccessibilitychecker.com') { ?>
                    <div class="footer-left__social">
                        <a href="https://www.facebook.com/BestWebAccessibility" target="_blank">
                            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-facebook-f fa-w-10 fa-2x">
                                <path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z" class=""></path>
                            </svg>
                        </a>
                        <a href="https://twitter.com/BestWebAcc" target="_blank">
                            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-twitter fa-w-16 fa-2x">
                                <path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" class=""></path>
                            </svg>
                        </a>
                        <a href="https://www.linkedin.com/company/best-web-accessibility" target="_blank">
                            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-linkedin-in fa-w-14 fa-2x">
                                <path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z" class=""></path>
                            </svg>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <div class="footer-bottm__item-two">
                    <?php wp_nav_menu( [ 
                                            'theme_location' => 'menu-3',
                                            'menu_class' => 'footer-menu-bottom',
                                        ] ); ?>
                                        			<?php 
			if(is_page('home-page')) {
				do_action('wpml_add_language_selector'); 
			}
			?>
                </div>
            </div>
        </div>
        <div class="copyright"><?php the_field('footer_copyright', 'options'); ?></div>
    </div>
</footer>
<div class="fone-popup"></div>
    <div class="cookies-popup" style="display: none;">
        <div class="cookies-popup__text"><?php the_field('cookies_popup_text', 'options'); ?> <a href="<?php the_field('cookies_popup_page', 'options'); ?>"><?php the_field('homepage_top3_text', 'options'); ?></a></div>
        <div class="cookies-popup__btn"><?php the_field('cookies_popup_button_text', 'options'); ?></div>
    </div>

    <?php if(is_home() || is_singular('reviews') || is_post_type_archive('reviews')){ ?>
        <div class="close-popup close-popup-reviews" style="display: none;">
           <?php if(ICL_LANGUAGE_CODE == 'fr'){
               echo '<img src="/wp-content/themes/sobix2/assets/img/EQUALLY-FR.jpg" alt="">';
           }elseif(ICL_LANGUAGE_CODE == 'de'){
            echo '<img src="/wp-content/themes/sobix2/assets/img/EQUALLY-DE.jpg" alt="">';
           }elseif(ICL_LANGUAGE_CODE == 'it'){
            echo '<img src="/wp-content/themes/sobix2/assets/img/EQUALLY-IT.jpg" alt="">';
           }else{
            echo '<img src="/wp-content/themes/sobix2/assets/img/EQUALLY.jpeg" alt="">';
           }?>
            <svg class="close-popup__close-btn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25"  xml:space="preserve" class="inner-element" role="img" width="100%" height="100%" preserveAspectRatio="none" data-event-name="Conversion" data-shadow-distance="0" data-init-color="#ffffff"> <path style="fill:#ffffff" d="M12.5,0C5.596,0,0,5.596,0,12.5S5.596,25,12.5,25S25,19.404,25,12.5S19.404,0,12.5,0z M18.157,16.035 	l-2.121,2.121L12.5,14.621l-3.536,3.536l-2.121-2.121l3.535-3.536L6.843,8.964l2.121-2.121l3.536,3.536l3.536-3.536l2.121,2.121 	L14.621,12.5L18.157,16.035z" data-init-color="#cccccc"></path> </svg>
            <div class="close-popup__block"><a href="<?php echo get_field('close_popup_url', 'options');?>" class="brave_element__inner_link" target="_blank"><?php the_field('claim_offer', 'options');?></a></div>
        </div>
    <?php }else{?>
    <div class="close-popup" style="display: none;">
        <div class="close-popup__title"><img class="inner-element" width="20" height="20" src="/wp-content/themes/sobix2/assets/img/1b5726cf-4c32-4df4-8790-615db99e70b0.png" alt=""  ><?php the_field('close_popup_title_popup', 'options'); ?></div>
        <svg class="close-popup__close-btn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25"  xml:space="preserve" class="inner-element" role="img" width="100%" height="100%" preserveAspectRatio="none" data-event-name="Conversion" data-shadow-distance="0" data-init-color="#ffffff"> <path style="fill:#ffffff" d="M12.5,0C5.596,0,0,5.596,0,12.5S5.596,25,12.5,25S25,19.404,25,12.5S19.404,0,12.5,0z M18.157,16.035 	l-2.121,2.121L12.5,14.621l-3.536,3.536l-2.121-2.121l3.535-3.536L6.843,8.964l2.121-2.121l3.536,3.536l3.536-3.536l2.121,2.121 	L14.621,12.5L18.157,16.035z" data-init-color="#cccccc"></path> </svg>
        <div class="close-popup__content-row">
            <div class="close-popup__content-item">
                <div class="close-popup__logo"><img src="/wp-content/themes/sobix2/assets/img/68716c8a-001a-48bd-ab27-f15b4f41ee4a.png" alt=""></div>
                <div class="close-popup__screen"><img src="/wp-content/themes/sobix2/assets/img/523a28b7-e3f9-4f61-aecd-4d200ddeabd2.png" alt=""></div>
            </div>
            <div class="close-popup__content-item" style="margin-left: 30px">
                <div class="close-popup__content"><?php the_field('close_popup_text', 'options'); ?></div>
                <a href="<?php the_field('close_popup_url', 'options'); ?>" class="close-popup__btn" target="_blank"><?php the_field('close_popup_text_btn', 'options'); ?></a>
                <div class="close-popup__bottom-img"><img src="/wp-content/themes/sobix2/assets/img/113cffe1-6fd8-44a7-93b6-c46b8acbb3fa.png" alt=""></div>
            </div>
        </div>
    </div>
    <?php }?>
<?php $my_current_lang = apply_filters( 'wpml_current_language', NULL ); ?>

<div class="btn-popup" style="display: none;">
    <div class="close-popup__title"><?php the_field('request_demo_popup_title', 'options'); ?></div>
    <svg class="btn-popup__close-btn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25"  xml:space="preserve" class="inner-element" role="img" width="100%" height="100%" preserveAspectRatio="none" data-event-name="Conversion" data-shadow-distance="0" data-init-color="#ffffff"> <path style="fill:#ffffff" d="M12.5,0C5.596,0,0,5.596,0,12.5S5.596,25,12.5,25S25,19.404,25,12.5S19.404,0,12.5,0z M18.157,16.035 	l-2.121,2.121L12.5,14.621l-3.536,3.536l-2.121-2.121l3.535-3.536L6.843,8.964l2.121-2.121l3.536,3.536l3.536-3.536l2.121,2.121 	L14.621,12.5L18.157,16.035z" data-init-color="#cccccc"></path> </svg>
    <div class="close-popup__content-row">
        <div class="btn-popup__logo"><?php $image = get_field('header_logo', 'options'); echo wp_get_attachment_image( $image, array(166,170) ); ?></div>
        <?php if(ICL_LANGUAGE_CODE == 'fr'){
                    echo do_shortcode('[contact-form-7 id="6362" title="Request demo"]');
        }elseif(ICL_LANGUAGE_CODE == 'de'){
            echo do_shortcode('[contact-form-7 id="6363" title="Request demo"]');
        }elseif(ICL_LANGUAGE_CODE == 'it'){
            echo do_shortcode('[contact-form-7 id="6361" title="Request demo"]');
        } else { ?>
            <?php echo do_shortcode('[contact-form-7 id="5702" title="Request demo"]') ?>
        <?php } ?>
    </div>
</div>
<div class="close-popup_accessibe" style="display: none;">
<?php if(wp_is_mobile()){
    echo '<img src="/wp-content/themes/sobix2/assets/img/Mobile-image.png" alt="">';
} ?>
           <?php if(ICL_LANGUAGE_CODE == 'fr'){
               if( !wp_is_mobile() ) {
                    echo '<img src="/wp-content/themes/sobix2/assets/img/Form-fr.jpeg" alt="">';
               }
               echo do_shortcode('[contact-form-7 id="7000" title="popup accessibe"]');
           }elseif(ICL_LANGUAGE_CODE == 'de'){
            if( !wp_is_mobile() ) {
                echo '<img src="/wp-content/themes/sobix2/assets/img/Form-de.jpeg" alt="">';
            }
            echo do_shortcode('[contact-form-7 id="7001" title="popup accessibe"]');
           }elseif(ICL_LANGUAGE_CODE == 'it'){
            if( !wp_is_mobile() ) {
                echo '<img src="/wp-content/themes/sobix2/assets/img/Form-it.jpeg" alt="">';
            }
            echo do_shortcode('[contact-form-7 id="7002" title="popup accessibe"]');
           }else{
            if( !wp_is_mobile() ) {
                echo '<img src="/wp-content/themes/sobix2/assets/img/Form-en.jpeg" alt="">';
            }
            echo do_shortcode('[contact-form-7 id="6991" title="popup accessibe"]');
           } ?>
            <svg class="close-popup__close-btn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25"  xml:space="preserve" class="inner-element" role="img" width="100%" height="100%" preserveAspectRatio="none" data-event-name="Conversion" data-shadow-distance="0" data-init-color="#ffffff"> <path style="fill:#ffffff" d="M12.5,0C5.596,0,0,5.596,0,12.5S5.596,25,12.5,25S25,19.404,25,12.5S19.404,0,12.5,0z M18.157,16.035 	l-2.121,2.121L12.5,14.621l-3.536,3.536l-2.121-2.121l3.535-3.536L6.843,8.964l2.121-2.121l3.536,3.536l3.536-3.536l2.121,2.121 	L14.621,12.5L18.157,16.035z" data-init-color="#cccccc"></path> </svg>
        </div>
<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but"><svg width="40" height="40" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-chevron-circle-up fa-w-16 fa-2x"><path fill="rgb(255 151 37)" d="M8 256C8 119 119 8 256 8s248 111 248 248-111 248-248 248S8 393 8 256zm231-113.9L103.5 277.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L256 226.9l101.6 101.6c9.4 9.4 24.6 9.4 33.9 0l17-17c9.4-9.4 9.4-24.6 0-33.9L273 142.1c-9.4-9.4-24.6-9.4-34 0z" class=""></path></svg></span></div>
<?php if(domain_user() == 'bestwebaccessibility.com') : ?>
    <img src="/wp-content/themes/sobix2/assets/img/conv-image.gif?offer_id=<?php the_field('offer_id', 'options'); ?>&goal_id=<?php the_field('goal_id', 'options'); ?>" style="" class="img-pixel" id="img-pixel">
<?php endif;?>
<?php if(domain_user() != 'topwebaccessibilitychecker.com') : ?>
<?php endif; ?>
<script>
jQuery(document).ready(function($) {
//exit site popup
    let lanuages = '<?php echo $my_current_lang; ?>';
        $('.close-popup__close-btn').click(function () {
            $('.close-popup').fadeOut(1000);
            $('.fone-popup').fadeOut(1000);
            localStorage.setItem('popupClose'+lanuages, 1);
        });
        
        $(document).mouseup(function(e) {
            var div = $('.close-popup');
            if (!div.is(e.target) && div.has(e.target).length === 0 && div.hasClass('active')) {
                    $('.close-popup').fadeOut(1000);
                    $('.fone-popup').fadeOut(1000);
                    localStorage.setItem('popupClose'+lanuages, 1);
            }
        });
        
        $(document).mouseleave(function(e){
            if (e.clientY < 10 && localStorage.getItem('popupClose'+lanuages) != 1) {
                setTimeout(() => $('.close-popup').fadeIn(1000), 1500);
                setTimeout(() => $('.fone-popup').fadeIn(1000), 1500);
                $('.close-popup').addClass('active');
            }
        });

        $('.popup-btn-footer').click(function () {
            $('.close-popup_accessibe').fadeIn(1000);
            $('.fone-popup').fadeIn(1000);
            $('.close-popup_accessibe').addClass('active');
        });

        $(document).mouseup(function(e) {
            var divN = $('.close-popup_accessibe');
            if (!divN.is(e.target) && divN.has(e.target).length === 0 && divN.hasClass('active')) {
                divN.fadeOut(1000);
                    $('.fone-popup').fadeOut(1000);
                    divN.removeClass('active');
            }
        });

        $('.close-popup__close-btn').click(function () {
            $('.close-popup_accessibe').fadeOut(1000);
            $('.fone-popup').fadeOut(1000);
        });
    });
</script>
<?php wp_footer(); ?>
