<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
		<?php
 		if(domain_user() == 'bestwebsiteaccessibility.com') {
             echo "<!-- Google Tag Manager -->
                 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                 new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                 })(window,document,'script','dataLayer','GTM-W5DD86V');</script>
                 <!-- End Google Tag Manager -->";
          }else if(domain_user() == 'bestwebaccessibility.com') {
             echo "<!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
 new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
 })(window,document,'script','dataLayer','GTM-WCPFXL4');</script>
 <!-- End Google Tag Manager -->";
         }
		?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<meta name="google-site-verification" content="-cMFc9sSQ8MyEFA0o7h7P_m619JVGcXjYUQhWpXWUk8" />
	<meta name="facebook-domain-verification" content="wvhv7t17es7zkrrdik1t8jx0lxlf9e" />
	<?php wp_head(); ?>
	<style>
	@media (max-width: 991px) {
		.witget-top3__item:first-child::before {
			content: '<?php the_field('mobile_title_most_popular', 'options');?>';
		}
	}
	@media (max-width: 767px) {
		.footer-bottm__item-one::before {
			content: '<?php the_field('mobile_title_contact_us', 'options');?>';
		}
		/* .footer-bottm__item-two::before {
			content: '<?php //the_field('mobile_title_quick_links', 'options');?>';
		} */
		.nav-content::before {
			content: '<?php the_field('mobile_menu_title', 'options');?>';
		}
	}
</style>
</head>
<body <?php body_class(); ?>>

<?php wp_body_open(); ?>
		<?php if(domain_user() == 'bestwebsiteaccessibility.com') {
            echo '<!-- Google Tag Manager (noscript) -->
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5DD86V"
                height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <!-- End Google Tag Manager (noscript) -->';
         }else if(domain_user() == 'bestwebaccessibility.com') {
            echo '<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WCPFXL4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->';
        } ?>

	<header>
		<div class="container">
			<a href="<?php echo home_url('/'); ?>" class="logo">
				<?php $image = get_field('header_logo', 'options'); echo wp_get_attachment_image( $image, array(166,170) ); ?>
			</a>
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
            	<div class="menu-icon"></div>
            </button>
			<?php
            wp_nav_menu( [
				'theme_location' => 'menu-1',
				'container' => 'nav',
				'menu_class' => 'nav__row container',
			] );
            ?>

			<?php
			// if(is_page('home-page')) {
				do_action('wpml_add_language_selector');
			// }
			?>

		</div>
	</header>
