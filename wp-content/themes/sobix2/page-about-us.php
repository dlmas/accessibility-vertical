<?php
/*
Template Name: about-us
*/

get_header();
?>
<div class="full-page-bg">
    <main id="primary" class="site-main reviews article">

        <div class="container">
            <div class="breadcrumbs-disclosure">
                <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
                <!-- <div class="header_home__disclosure disclosure-all-page">Advertising Disclosure</div> -->
            </div>
            <div class="single-page-bg">
                <div class="full-page__top-title">
                    <?php the_title( '<h1 class="full-page__title">', '</h1>' ); ?>
                </div>

                <div class="entry-content component">
                    <?php the_content(); ?>
                </div>
                <div class="component__read-more" data-anchor="our-values">Read more ></div>
            </div>
        </div>
        <div class="about_us_blocks_repiter">
            <?php 
        // Check rows exists.
        if( have_rows('about_us_blocks_repiter') ):

            // Loop through rows.
            while( have_rows('about_us_blocks_repiter') ) : the_row(); ?>
            <div id="our-values" class="about_us_repiter_item" style="background-color: <?php the_sub_field('about_us_blocks_repiter_color'); ?>;">
                <div class="container">
                    <div class="about-repiter__title"><?php the_sub_field('about_us_blocks_repiter_title'); ?></div>
                </div>
                <div class="container about_us_repiter_item__container">
                    <div class="about-repiter__text">
                        <div class="about-repiter__content"><?php the_sub_field('about_us_blocks_repiter_text'); ?></div>
                    </div>
                    <div class="about-repiter__img">
                        <?php $image = get_sub_field('about_us_blocks_repiter_image'); ?>
                        <?php echo wp_get_attachment_image( $image, 'full' ); ?></div>
                </div>
            </div>
            <?php endwhile;

        // No value.
        else :
            // Do something...
        endif;
    ?>
       
       
        </div>
        <div class="container about-repiter_bottom-content contact-us">
            <div class="about-repiter__title_bottom"><?php the_field('bottom_content_title'); ?></div>
            <div class="about-repiter__content_bottom"><?php the_field('bottom_ content'); ?></div>
            <?php echo do_shortcode('[contact-form-7 id="825" title="Contact us"]'); ?>
        </div>
        
    </main><!-- #main -->
</div>
<?php get_footer(); ?>
<?php 
if( domain_user() == 'bestwebaccessibility.com' ) { 
    the_field('сode_end_body_bestwebaccessibility', 'options'); 
}else{
    the_field('сode_end_body', 'options'); 
}
?>
</body>

</html>
