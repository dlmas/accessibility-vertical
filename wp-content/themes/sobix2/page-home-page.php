<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

get_header();
gettype(get_sites());
// echo widget_lineup_builder('lineup_3_top', 'reviews', 3);
?>
<style>
.home-one-post {
    height: 435px;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
}
</style>
<div class="header-block">
    <div class="container">
        <div class="header_home">
            <div class="header_home__block">
                <h1 class="header_home__title"><?php the_field('header_content_title', 'options') ?></h1>
                <!-- <div class="header_home__text">
                    <?php the_field('header_content', 'options') ?>
                </div> -->
                <?php if( have_rows('header_bullet_points', 'options') ): ?>
                    <div class="header_home__check-row">
                    <?php while( have_rows('header_bullet_points', 'options') ): the_row(); ?>
                        <div class="header_home__check-item"><?php the_sub_field('header_bullet_points_text'); ?></div>
                    <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <?php if( $button_compare_now = get_field('button_compare_now', 'options') ){ ?>
                <a href="/comparison/<?php if( wp_is_mobile()) { echo '?step=two'; } ?>" class="header_btn-compare"><?php echo $button_compare_now; ?></a>
                <?php } ?>
            </div>
            <div class="header_home__animate-icon">
            <script
                src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script><lottie-player src="https://assets2.lottiefiles.com/packages/lf20_dqgdwbzi.json" background="transparent" speed="1" style="width: 100%; height: 100%;" loop autoplay></lottie-player>
            </div>
        </div>
    </div>
</div>
<div class="hh-block">
    <div class="container">
        <div class="header_home__global-disclosure">
            <div class="header_home__last-updated-sep">
                <?php the_field('last_updated', 'options');?>
                <?php $timestamp = strtotime(date('F Y')); ?>
                <span><?php echo date_i18n( 'F Y', $timestamp); ?></span>
        </div>
            <div class="header_home__referal-disclosure">
                <div class="header_home__referal"><?php the_field('we_receive_referral_fees_from_partners', 'options');?></div>
                <div class="header_home__disclosure"><?php the_field('advertising_disclosure', 'options'); ?></div>
            </div>
        </div>
    </div>
</div>
<div class="popap-bg">
    <div class="container">
        <div class="advertiser-disclosure-text">
            <div class="advertiser-close"></div>
            <?php the_field('advertising_disclosure_text', 'options'); ?>
        </div>
    </div>
</div>

<div class="container">
<div class="main-content main-content_home-page">
<main>
<?php 
    // $args = array(
    //     'posts_per_page' => 1,
    //     'post_type'   => 'article'
    // );
    // $query = $query = new WP_Query( $args );
    // if( $query->have_posts() ):
    //     while( $query->have_posts()) : $query->the_post();
     ?>
            <!-- <article class="home-one-post" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                 <a href="<?php the_permalink() ?>">
                     <div class="home-one-post__h1"><?php the_title() ?></div>
                 </a>
             </article> -->
    <?php 
    // endwhile;
    // endif; 
    // wp_reset_postdata();
    ?>
<?php 
    $args = array(
        'posts_per_page' => 3,
        'post_type'   => 'news'
    );
    $query = $query = new WP_Query( $args );

    if( $query->have_posts() ): ?>
    <div class="home-two-post">
    <?php while( $query->have_posts() ): $query->the_post(); ?>

            <article class="home-two-post__item">
                <a href="<?php the_permalink() ?>" class="home-two-post__img"><?php the_post_thumbnail(array(366, 200)) ?></a>

                <div class="home-two-post__cat-row">
                    <?php
                    foreach ( get_the_category() as $category ) {
                        printf(
                            '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                            esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                            esc_html( $category->name ) // Название рубрики
                        );
                    }
                    ?>
                </div>

                <a href="<?php the_permalink() ?>" class="home-two-post__title"><?php the_title() ?></a>
            </article>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>


    
    <?php 
        $args = array(
            'posts_per_page' => 2,
            'post_type'   => ['article', 'bestsolution'],
            'post__in' => [255, 770],
            'orderby'        => 'post__in'
        );
        $query = $query = new WP_Query( $args );
       if( $query->have_posts() ): 
    ?>
    <div class="home-bottom-block-post">
        <div class="home-bottom-block-post__titile"><span><?php the_field('popular_block_title', 'options'); ?></span></div>
        <div class="home-bottom-block-post__row">
        <?php while( $query->have_posts() ): $query->the_post(); ?>
                    <div class="home-bottom-block-post__item">
                        <a href="<?php the_permalink() ?>" class="home-bottom-block-post__img">
                        <?php the_post_thumbnail(array(262, 200)) ?>
                        </a>
                        <div class="home-bottom-block-post__text">
                            <div class="home-two-post__cat-row">
                            <?php
                                foreach ( get_the_category() as $category ) {
                                    printf(
                                        '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                                        esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                                        esc_html( $category->name ) // Название рубрики
                                    );
                                }
                            ?>
                            </div>
                            <h2 class="home-bottom-block-post__h2">
                                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                            </h2>
                            <div class="home-bottom-block-post__desc"><?php the_field('populartrending_short_content'); ?></div>
                            <div class="home-one-post__autor-data">
                            <?php //$author_email = get_the_author_email(); echo get_avatar($author_email, '33'); 
                                                                        $image = get_field('article_autor_img');
                                                                        $size = 33; // (thumbnail, medium, large, full or custom size)
                                                                        if( $image ) { ?>
                                <div class="home-one-post__autor-ava"><?php echo wp_get_attachment_image( $image, $size ); ?></div>
                                <?php } ?>

                                <?php if($article_autor_name = get_field('article_autor_name')) { ?>
                                <div class="home-one-post__autor"><?php echo $article_autor_name; ?></div>
                                <?php } ?>

                                <div class="home-one-post__data"><?php echo get_the_date('F d, Y'); ?></div>
                            </div>
                        </div>
                    </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
       <?php 
        $args = array(
            'posts_per_page' => 3,
            'post_type'   => 'article',
            'article_category' => 'compare',
            // 'post__in' => [758, 257, 170],
            // 'orderby'        => 'post__in'
        );
        $query = $query = new WP_Query( $args );
       if( $query->have_posts() ): 
    ?>
    <div class="home-bottom-block-post">
        <div class="home-bottom-block-post__titile"><span><?php the_field('trending_block_title', 'options'); ?></span></div>
        <div class="home-bottom-block-post__row">
        <?php while( $query->have_posts() ): $query->the_post(); ?>
                    <div class="home-bottom-block-post__item">
                        <a href="<?php the_permalink() ?>" class="home-bottom-block-post__img">
                        <?php the_post_thumbnail(array(262, 200)) ?>
                        </a>
                        <div class="home-bottom-block-post__text">
                            <div class="home-two-post__cat-row">
                            <?php
                                foreach ( get_the_category() as $category ) {
                                    printf(
                                        '<a href="%s" class="home-two-post__cat-item">%s</a>', // Шаблон вывода ссылки
                                        esc_url( get_category_link( $category ) ), // Ссылка на рубрику
                                        esc_html( $category->name ) // Название рубрики
                                    );
                                }
                            ?>
                            </div>
                            <h2 class="home-bottom-block-post__h2">
                                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                            </h2>
                            <div class="home-bottom-block-post__desc"><?php the_field('populartrending_short_content'); ?></div>
                            <div class="home-one-post__autor-data">
                            <?php //$author_email = get_the_author_email(); echo get_avatar($author_email, '33'); 
                                                                        $image = get_field('article_autor_img');
                                                                        $size = 33; // (thumbnail, medium, large, full or custom size)
                                                                        if( $image ) { ?>
                                <div class="home-one-post__autor-ava"><?php echo wp_get_attachment_image( $image, $size ); ?></div>
                                <?php } ?>

                                <?php if($article_autor_name = get_field('article_autor_name')) { ?>
                                <div class="home-one-post__autor"><?php echo $article_autor_name; ?></div>
                                <?php } ?>

                                <div class="home-one-post__data"><?php echo get_the_date('F d, Y'); ?></div>
                            </div>
                        </div>
                    </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
                            </main>
    <div class="sidebar-full-page">
            <div class="sidebar_block sidebar_block_home">
                <div class="sidebar__inner">
                    <?php get_template_part( 'template-parts/witget', 'reviews-top5' ); ?>
                    <?php get_template_part( 'template-parts/witget', 'article-top5' ); ?>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    
</div>
</div>
    <div class="witget-top3 witget-top3__home-page">
    <div class="container">
        <h2><?php the_field('home_title_top_3', 'options'); ?></h2>
        <a href="/comparison/" class="witget-top3__btn-comp"><?php the_field('btn_compare_now'); ?></a>
        <div class="witget-top3__row">
<style>
.witget-top3__item:first-child::before {
    content: '<?php the_field('most_popular', 'options')?>';
}
</style>
            <?php
			// переключаемся на блог 1
			//switch_to_blog( 1 );
			
			// Выводим данные блога на который переключились
			// Получаем посты с блога 1
			

            do_action( 'lineup_3_top', 'lineup_3_top', 'reviews', 3);
			if ( $query->have_posts() ) : 
            $i = 1;
			while ( $query->have_posts() ) : $query->the_post(); ?>
            <?php if($i == 3){ ?>
                    <div href="<?php the_field('visit_site_url'); ?>" target="_blank" class="witget-top3__item adoric_popup<?php if(ICL_LANGUAGE_CODE=='fr'): echo '-fr'; elseif(ICL_LANGUAGE_CODE=='de'): echo '-de'; endif;?>" id="witget-top3__item" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_3_top_##_' . $i; ?>">
                <?php }else{ ?>
                    <a href="<?php the_field('visit_site_url'); ?>" target="_blank" class="witget-top3__item" id="witget-top3__item" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_3_top_##_' . $i; ?>">
                 <?php } ?>
                <div class="witget-top3__img"><?php the_post_thumbnail(); ?></div>
                <div class="witget-top3__text"><?php the_field('top_3_witget_text');?></div>
                <div class="witget-top3__raiting"><span><?php the_field('raiting'); ?></span></div>
                <?php if($i == 3){ ?>
                <div class="witget-top3__link">
                    <?php //the_field('top_3_learn_more', 'options')?>
                    <?php the_field('btn_request_demo', 'options'); ?>
                </div>
                </div>
                <?php }else{ ?>
                    <div class="witget-top3__link witget-top3__link_none-after">
                    <?php //the_field('btn_visit' , 'options')?> <?php echo str_replace('Review', '', get_the_title()); ?>
                </div>
                </a>
                <?php } ?>
            
            <?php $i++;
				endwhile; 
			endif; 
			wp_reset_query(); 

			// возвращемся к текущему блогу
			//restore_current_blog();

			?>
        </div>
    </div>
    <div style="clear:both;"></div>



    </div>


<div class="container">
    
	<div class="clearfix" style="    position: relative;">
		<div id="sidebar"  class="home-sidebar">
			<div class="sidebar__inner">
            <h2 class="witget-all__h2"><?php the_field('home_title_top_10_seo', 'options'); ?></h2>
            <div class="witget-all__row">
            <?php $query = new WP_Query( array(
                                'post_type' => 'article',
                                'posts_per_page' => 4,
                                //'nopaging' => 'true',
                                'post_status'=> 'publish', 

	                           ) ); 
                
                if ( $query->have_posts() ) : ?>
            <?php
                            /* Start the Loop */
                            while ( $query->have_posts() ) :
                                $query->the_post();
            
                                /*
                                * Include the Post-Type-specific template for the content.
                                * If you want to override this in a child theme, then include a file
                                * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                                */
                                get_template_part( 'template-parts/content', 'article' );
            
                            endwhile;
            
                            //the_posts_navigation();
            
                        else :
            
                            get_template_part( 'template-parts/content', 'none' );
            
                        endif;
                
                        ?>
                </div>
			</div>
		</div>
		<div id="content">
        <?php get_template_part( 'template-parts/witget', 'best-results-top5' ); ?>
		</div>		
	</div>
</div>


<script>
    jQuery(document).ready(function($) {
        $('#true_loadmore').click(function() {
            $(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
            var data = {
                'action': 'loadmore',
                'query': true_posts,
                'page': current_page,
                'template_part': 'home-page',
            };
            $.ajax({
                url: ajaxurl, // обработчик
                data: data, // данные
                type: 'POST', // тип запроса
                success: function(data) {
                    if (data) {
                        $('#true_loadmore').text('Load more').before(data); // вставляем новые посты
                        current_page++; // увеличиваем номер страницы на единицу
                        if (current_page == max_pages) $("#true_loadmore").remove(); // если последняя страница, удаляем кнопку
                    } else {
                        $('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
                    }
                }
            });
        });
    });

</script>
    <script type="text/javascript" src="/wp-content/themes/sobix2/assets/js/sticky-sidebar.js"></script>
	<script type="text/javascript">

		var stickySidebar = new StickySidebar('#sidebar', {
			topSpacing: 20,
			bottomSpacing: 20,
			containerSelector: '.container',
			innerWrapperSelector: '.sidebar__inner'
		});
	</script>
<?php get_footer(); ?>
<?php 
    if( domain_user() == 'bestwebaccessibility.com' ) {
        the_field('сode_end_body_homepage_copy_bestwebaccessibility', 'options'); 
    }else{
        the_field('сode_end_body_homepage', 'options'); 
    }
?>
</body>

</html>
