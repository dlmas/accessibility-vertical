<?php
/*
Template Name: News
*/

get_header();
?>
<div class="full-page-bg">
    <div class="container">
        <div class="breadcrumbs-disclosure">
            <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
            <div class="header_home__disclosure disclosure-all-page"><?php the_field('advertising_disclosure', 'options'); ?></div>
        </div>
        <div class="popap-bg">
            <div class="container">
                <div class="advertiser-disclosure-text">
                    <div class="advertiser-close"></div>
                    <?php the_field('advertising_disclosure_text', 'options'); ?>
                </div>
            </div>
        </div>


        <h1 class="archive-title"><?php the_field('title_news', 'option'); ?></h1>
        <div class="archive-descption">
            <?php echo get_field('text_news', 'option'); ?>
        </div>
    </div>

    <div style="clear:both;"></div>
    <div class="container  single-page-bg">
        <!--
    <div class="single-page-bg__cat">
            
                <?php 
//                 $taxonomy= get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); //массив текущий таксаномии  
//                 $tags = get_terms('article_category', [
// 	'hide_empty' => false, //Скрывать ли термины в которых нет записей. 1(true) - скрывать пустые, 0(false) - показывать пустые.
// ]);

// 			$html = '<div class="arch_tags"><a href="/guides/" data-tag_slug="news" data-taxonomy_slug="123">All categories</a> ';
// 			foreach ( $tags as $tag ) {
// 				$tag_link = get_tag_link( $tag->term_id );		
// 				$html .= " <a class='tags-links' href='{$tag_link}' title='{$tag->name} рубрика' data-tag_slug='{$tag->slug}' data-taxonomy_slug='$taxonomy->slug'>"; 
// 				$html .= "{$tag->name}</a>";
// 			}
// 			$html .= '</div>';
// 			echo $html; 
            ?>
            
    </div>
           -->
        <main id="article" class="site-main reviews article">
            <?php 
                $query = new WP_Query( array(
                                'post_type' => 'news',
                                'posts_per_page' => 10,
                                //'nopaging' => 'true',
                                'post_status'=> 'publish', 

	                           ) ); 
                
                if ( $query->have_posts() ) : ?>
            <?php
                            /* Start the Loop */
                            while ( $query->have_posts() ) :
                                $query->the_post();
            
                                /*
                                * Include the Post-Type-specific template for the content.
                                * If you want to override this in a child theme, then include a file
                                * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                                */
                                get_template_part( 'template-parts/content', 'news' );
            
                            endwhile;
            
                            //the_posts_navigation();
            
                        else :
            
                            get_template_part( 'template-parts/content', 'none' );
            
                        endif;
                
                        ?>
            <?php // AJAX loading posts ?>
            <?php if (  $query->max_num_pages > 1 ) : ?>
            <script>
                var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                var true_posts = '<?php echo serialize($query->query_vars); ?>';
                var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                var max_pages = '<?php echo $query->max_num_pages; ?>';

            </script>
            <div id="true_loadmore">Show more options</div>
            <?php endif; 
            wp_reset_query(); 
            ?>
        </main><!-- #main -->

        <div id="aside1" class="sidebar-full-page">
            <div class="sidebar_block">
                <div class="sidebar__inner">
                    <?php get_template_part( 'template-parts/witget', 'reviews-top5' ); ?>
                    <?php get_template_part( 'template-parts/witget', 'article-top5' ); ?>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<script>
    jQuery(document).ready(function($) {
        $('#true_loadmore').click(function() {
            $(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
            var data = {
                'action': 'guodes_loadmore',
                'query': true_posts,
                'page': current_page,
            };
            $.ajax({
                url: ajaxurl, // обработчик
                data: data, // данные
                type: 'POST', // тип запроса
                success: function(data) {
                    if (data) {
                        $('#true_loadmore').text('Load more').before(data); // вставляем новые посты
                        current_page++; // увеличиваем номер страницы на единицу
                        if (current_page == max_pages) $("#true_loadmore").remove(); // если последняя страница, удаляем кнопку
                    } else {
                        $('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
                    }
                }
            });
        });
    });

</script>
<?php
get_footer(); ?>
<?php 
if( domain_user() == 'bestwebaccessibility.com' ) {
    the_field('сode_end_body_articlepage_bestwebaccessibility', 'options'); 
}else{
    the_field('сode_end_body_articlepage', 'options');  
}
?>
</body>

</html>
