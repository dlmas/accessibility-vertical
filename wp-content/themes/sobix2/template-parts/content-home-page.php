<div class="witget-all__item <?php if(get_field('click_views_on_off')) { echo 'witget-all__item_mobile'; } ?>">
               <?php if($ribbon_top_text = get_field('ribbon_top_text')) { ?>
                    <div class="ribbon-top" style="background-color: <?php the_field('ribbon_top_background_color'); ?>"><?php echo $ribbon_top_text; ?></div>
                <?php } ?>
					<div class="witget-all__block">
						<div>
							<div class="witget-all__img-block">
								<a href="<?php the_permalink(); ?>" class="witget-all__img">
									<?php the_post_thumbnail(); ?>
								</a>
								<div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('stars'); ?>"><span style=""></span></span></div>
								<a href="<?php the_permalink(); ?>" class="witget-all__read-more"><span><?php the_field('bottom_read_more', 'options'); ?></span></a>
							</div>
							<div class="witget-all__info">
								<div class="witget-all__title"><?php the_title(); ?></div>
								
									<div class="witget-all__list-features-row">
									<?php if( get_field('advantages') ) : ?>
										<?php while( have_rows('advantages') ): the_row(); 
											if(get_sub_field('advantages_boolean') != false) { ?>
												<div class="list-features-row__item" ><?php echo get_sub_field('advantages_text'); ?></div>
											<?php } ?>
										<?php endwhile; ?>
										<?php endif; ?>
										
									</div>
								
								</div>
						</div>
						<div>
							<div class="witget-all__raiting">
								<span>
                                   <?php 
                                    $raiting = get_field('raiting'); 
                                    echo $raiting;
                                    ?>
                                </span>
								<?php if( $tool_tip_score = get_field('tool_tip_score', 'option') ): ?><i class="tlt-icon">?</i><?php endif;?>
								
								<div class="witget-all__raiting__desc">
                                    <?php if($raiting >= 9.5) {
												the_field('outstanding', 'options');
                                            }else if($raiting >= 9.0 && $raiting < 9.5) {
												the_field('exceptional', 'options');
                                            }else if($raiting >= 8.5 && $raiting < 9) {
												the_field('very_good', 'options');
                                            }else if($raiting >= 8 && $raiting < 8.5) {
                                                the_field('good', 'options');
                                            }else if($raiting >= 7.5 && $raiting < 8) {
												the_field('promising', 'options');
                                            }else{
                                                the_field('normal', 'options');
                                            }
                                    
                                    ?>
                                </div>
								<?php if( $tool_tip_score ): ?>
                                    <div class="tlt-wrap" data-open="false">
                                        <div class="tlt-text">
                                            <?php echo $tool_tip_score; ?>
                                        </div>
                                        <div class="tlt-close">Close</div>
                                        <div class="tlt-arrow"></div>
                                    </div>
								<?php endif;?>
							</div>
							<div class="witget-all__btn">
								<div id="spb-rts-461" class="bb-bubble-rts" style="<?php if(get_field('click_views_on_off')) { echo 'display: block;'; } ?>"><?php the_field('one_over_people', 'options');?> <?php the_field('click_views_quantity'); ?> <?php the_field('tow_over_people', 'options');?></div>
								<?php
									if(!get_field('adoric_popup_active')){ ?>
										<div class="witget-all__link adoric_popup<?php //if(ICL_LANGUAGE_CODE=='fr'): echo '-fr'; elseif(ICL_LANGUAGE_CODE=='de'): echo '-de'; endif;?>"><?php the_field('bottom_visit_site', 'options'); ?></div>
									<?php }else{ ?>
										<a href="<?php the_field('visit_site_url');?>" target="_blank" class="witget-all__link" id="home-visit-site" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_100_reviewspage_##_' . $i; ?>">
										<?php the_field('btn_visit', 'options')?> <?php echo str_replace(['Review', 'Rezension', 'Revue'], '', get_the_title()); ?>
										<!-- <i class="baseline_trending_withe"></i> -->
									</a>
									<?php } ?>
									<?php if(get_field('brend_logo_images')){ ?>
								  		<div class="witget-all__read-more witget-all__read-more_show-more"><?php the_field('show_more', 'options'); ?></div>
									<?php } ?>
							</div>
						</div>
					</div>
					<?php if( get_field('highlights_title') && get_field('highlights') ) :?>
					<!-- <div class="expandable">
						<div class="expandable-row">
							<div class="expandable-row__text">
								<div class="">
									<div class="expandable-row__title">Highlights</div>
									<?php if(get_field('highlights_title')) :?><div class="expandable-row__title"><?php the_field('highlights_title')?></div><?php endif;?>
									<?php if( get_field('highlights') ) : ?>
										<ul class="expandable-row__list-features">
										<?php while( have_rows('highlights') ): the_row(); ?>
											<li><?php echo get_sub_field('highlights_text'); ?></li>
										<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
								<a href="<?php the_permalink(); ?>" class="expandable__link">Read review >></a>
							</div>
							<div class="expandable-row__img">
									<?php 
									$image = get_field('image_home');
									$size = 'full'; // (thumbnail, medium, large, full или ваш размер)

									if( $image ) {
										echo wp_get_attachment_image( $image, $size );
									}
									?>
								<a href="" target="_blank" class="expandable__link">Visit <?php the_title(); ?> >></a>
							</div>
						</div>
					</div>
					<div class="witget-all__more-info">More info</div> -->
					<?php endif; ?>
					
					<?php if(get_field('adoric_popup_active') && $i <= 3): ?>
						<div class="witget-all__images-brend">
									<?php
										// Check rows exists.
										if( have_rows('brend_logo_images') ):

											// Loop through rows.
											while( have_rows('brend_logo_images') ) : the_row();

												// Load sub field value.
												$sub_value_img = get_sub_field('brend_logo_images_images');
												$sub_value_url = get_sub_field('brend_logo_images_url');
												$size = 'full';
												?>
												<a href="<?php echo $sub_value_url; ?>" class="brend_logo_images_images" target="_blank"><?php echo wp_get_attachment_image( $sub_value_img, $size ); ?></a>
												<?php

											// End loop.
											endwhile;

										// No value.
										else :
											// Do something...
										endif;
									?>
						</div>
					<?php endif; ?>
					<div class="witget-all__bottom-down" style="display: none;">
						<div class="bottom-down__item bottom-down__item_one">
									<div class="bottom-down__img">
										<!-- <img src="https://top10adatools.com/wp-content/uploads/2020/12/accessibe-screenshot-400x146.png" alt=""> -->
									</div>
									<div class="bottom-down__content"><?php the_field('select_content'); ?></div>
									<a href="<?php the_permalink(); ?>" class="bottom-down__link"><?php the_field('read_full_review', 'options'); ?> ></a>
						</div>
						<div class="bottom-down__item">
							<div class="bottom-down__brend-logo-row">
							<?php


									// Check rows exists.bottom-down__brend-logo-row
									if( have_rows('brend_logo_images_tow') ):

										// Loop through rows.
										while( have_rows('brend_logo_images_tow') ) : the_row();

											// Load sub field value.
											$sub_value_img = get_sub_field('brend_logo_images_tow');
											$sub_value_url = get_sub_field('brend_logo_images_url_two');
											$size = 'full';
											?>
											<a href="<?php echo $sub_value_url; ?>" class="bottom-down__brend-logo-items" target="_blank"><?php echo wp_get_attachment_image( $sub_value_img, $size ); ?></a>
											<?php

										// End loop.
										endwhile;

									// No value.
									else :
										// Do something...
									endif;




									// Check rows exists.bottom-down__brend-logo-row
									if( have_rows('brend_logo_images') ):

										// Loop through rows.
										while( have_rows('brend_logo_images') ) : the_row();

											// Load sub field value.
											$sub_value_img = get_sub_field('brend_logo_images_images');
											$sub_value_url = get_sub_field('brend_logo_images_url');
											$size = 'full';
											?>
											<a href="<?php echo $sub_value_url; ?>" class="bottom-down__brend-logo-items" target="_blank"><?php echo wp_get_attachment_image( $sub_value_img, $size ); ?></a>
											<?php

										// End loop.
										endwhile;

									// No value.
									else :
										// Do something...
									endif;
								?>
									
							</div>
						</div>
					</div>
				</div>