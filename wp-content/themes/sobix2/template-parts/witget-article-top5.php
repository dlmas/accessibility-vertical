				<div class="top-5-witget-article">
					<div class="top-5-witget__title"><?php the_field('title_witget_top_5_atticle', 'options')?></div>
					<div class="top-5-witget__row">
						<?php $query = new WP_Query( array(
									'post_type' => array('article'),
									'posts_per_page'	=> '5',
									'order' => 'DESC'
								));
						if ( $query->have_posts() ) : 
							while ( $query->have_posts() ) : $query->the_post(); ?>
								<div class="top-5-witget-article__item">
								    <?php if(get_the_post_thumbnail()): ?>
									    <a href="<?php the_permalink(); ?>" class="top-5-witget-article__img"><?php the_post_thumbnail('large'); ?></a>
									   <?php endif; ?>
									<div class="top-5-witget-article__title"><?php the_title(); ?></div>
									<a href="<?php the_permalink(); ?>" class="top-5-witget-article__links-reviews">
									    <?php the_field('button_title_witget_top_5_atticle', 'options'); ?>
									</a>
								</div>
							<?php endwhile; 
						endif; wp_reset_query(); ?>
					</div>
				</div>