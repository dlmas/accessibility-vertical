jQuery(document).ready(function($) {
    $('.sub-menu')
        .parent().parent()
        .addClass('has-sub-menu-caption');

    //Price hint button
    $('.witget-all__row .tlt-icon, .arсhive-rewiews .tlt-icon, .top-5-witget__row .tlt-icon').on('click', function() {
        $('.witget-all__raiting, .top-5-witget__rating').removeClass('open');
        $(this).next().parent().addClass('open');
    });

    $(document).mouseup(function(e) { // click event on web document
        var div = $(".witget-all__raiting, .top-5-witget__rating"); // here we specify the element ID
        if (!div.is(e.target) //if the click was not on our block
            &&
            div.has(e.target).length === 0) { // and not by its children
            div.removeClass('open'); //  hide it
        }
    });

    $('.tlt-close').on('click', function() {
        $('.witget-all__raiting, .top-5-witget__rating').removeClass('open');
    });


    //Burger menu
    $('.menu-toggle').click(function() {
        $(this).toggleClass('active');
        $('nav').toggleClass('active');
        $('body').toggleClass('active');
    });

    //rating star
    jQuery(".wpcr_averageStars").each(function() {
        // Get the value
        var val1 = jQuery(this).attr("data-wpcravg");
        // val1 /= 2; 
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size1 = Math.max(0, (Math.min(10, val1)));
        size1 = rating_star(8, 15, 32, 39, 56, 64, 86, 88, 104, 112, size1);
        // Create stars holder
        var $span1 = jQuery('<span />').width(size1);
        // Replace the numerical value with stars
        jQuery(this).html($span1);
    });
    //rating star large-stars
    jQuery(".single-reviews-info .wpcr_averageStars").each(function() {
        // Get the value
        var val1 = jQuery(this).attr("data-wpcravg");
        // val1 /= 2; 
        // Make sure that the value is in 0 - 5 range, multiply to get width
        // var size1 = Math.max(0, (Math.min(5, val1))) * 30;
        var size1 = Math.max(0, (Math.min(5, val1)));
        size1 = rating_star(10, 20, 40, 50, 70, 80, 100, 110, 130, 140, size1);
        // Create stars holder
        var $span1 = jQuery('<span />').width(size1);
        // Replace the numerical value with stars
        jQuery(this).html($span1);
    });
    //rating star
    jQuery(".top-5-witget .wpcr_averageStars").each(function() {
        // Get the value
        var val1 = jQuery(this).attr("data-wpcravg");
        // val1 /= 2; 
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size1 = Math.max(0, (Math.min(10, val1)));
        size1 = rating_star(6, 12, 24, 30, 42, 48, 60, 66, 78, 90, size1);
        // Create stars holder
        var $span1 = jQuery('<span />').width(size1);
        // Replace the numerical value with stars
        jQuery(this).html($span1);
    });
    //rating star filter 
    jQuery(".check__item .wpcr_averageStars").each(function() {
        // Get the value
        var val1 = jQuery(this).attr("data-wpcravg");
        // val1 /= 2; 
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size1 = Math.max(0, (Math.min(10, val1)));
        size1 = rating_star(6, 12, 24, 30, 42, 48, 60, 66, 78, 90, size1);
        // Create stars holder
        var $span1 = jQuery('<span />').width(size1);
        // Replace the numerical value with stars
        jQuery(this).html($span1);
    });

    function rating_star(sizeWidth1, sizeWidth2, sizeWidth3, sizeWidth4, sizeWidth5, sizeWidth6, sizeWidth7, sizeWidth8, sizeWidth9, sizeWidth10, size1) {


        if (size1 >= 0.1 && size1 <= 0.6) {
            return sizeWidth1;
        } else if (size1 >= 0.7 && size1 <= 1) {
            return sizeWidth2;
        } else if (size1 >= 1.1 && size1 <= 1.6) {
            return sizeWidth3;
        } else if (size1 >= 1.7 && size1 <= 2) {
            return sizeWidth4;
        } else if (size1 >= 2.1 && size1 <= 2.6) {
            return sizeWidth5;
        } else if (size1 >= 2.7 && size1 <= 3) {
            return sizeWidth6;
        } else if (size1 >= 3.1 && size1 <= 3.6) {
            return sizeWidth7;
        } else if (size1 >= 3.7 && size1 <= 4) {
            return sizeWidth8;
        } else if (size1 >= 4.1 && size1 <= 4.6) {
            return sizeWidth9;
        } else if (size1 >= 4.7) {
            return sizeWidth10;
        } else {
            return 0;
        }
    }
    //Brand open button
    $('.witget-all__row .witget-all__more-info').on('click', function() {
        $(this).toggleClass('active');
        $(this).siblings('.expandable').toggleClass('active');
    });

    $('.menu-item-has-children').append('<div class="mobile-menu-children"></div>');

    $('.nav__row .mobile-menu-children').on('click', function() {
        $(this).toggleClass('active');
        $(this).siblings('.sub-menu').toggleClass('active');
    });

    //hint button
    $('.header_home__disclosure').on('click', function() {
        $('.popap-bg').toggleClass('active');
    });
    $(document).mouseup(function(e) { // click event on web document
        var div = $(".advertiser-disclosure-text"); // here we specify the element ID
        if (!div.is(e.target) // if the click was not on our block
            &&
            div.has(e.target).length === 0) { // and not by its children
            $('.popap-bg').removeClass('active'); // hide it
        }
    });
    $('.advertiser-close').on('click', function() {
        $('.popap-bg').removeClass('active');
    });

    $(document).mouseup(function(e) {
        var div = $('.bb-bubble-rts');
        if (!div.is(e.target) &&
            div.has(e.target).length === 0) {
            $('.bb-bubble-rts').css('display', 'none');
        }
    });

    //sidebar 
    // (function() {
    //     var a = document.querySelector('#aside1'),
    //         b = null,
    //         K = null,
    //         Z = 0,
    //         P = 0,
    //         N = 0; // if P replaces zero with a number, then the block will stick before the top edge of the browser window reaches the top edge of the element, if for N, the bottom edge reaches the bottom edge of the element. Can be negative
    //     window.addEventListener('scroll', Ascroll, false);
    //     document.body.addEventListener('scroll', Ascroll, false);

    //     function Ascroll() {
    //         var Ra = a.getBoundingClientRect(),
    //             R1bottom = document.querySelector('#article').getBoundingClientRect().bottom;
    //         if (Ra.bottom < R1bottom) {
    //             if (b == null) {
    //                 var Sa = getComputedStyle(a, ''),
    //                     s = '';
    //                 for (var i = 0; i < Sa.length; i++) {
    //                     if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
    //                         s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
    //                     }
    //                 }
    //                 b = document.createElement('div');
    //                 b.className = "stop";
    //                 b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
    //                 a.insertBefore(b, a.firstChild);
    //                 var l = a.childNodes.length;
    //                 for (var i = 1; i < l; i++) {
    //                     b.appendChild(a.childNodes[1]);
    //                 }
    //                 a.style.height = b.getBoundingClientRect().height + 'px';
    //                 a.style.padding = '0';
    //                 a.style.border = '0';
    //             }
    //             var Rb = b.getBoundingClientRect(),
    //                 Rh = Ra.top + Rb.height,
    //                 W = document.documentElement.clientHeight,
    //                 R1 = Math.round(Rh - R1bottom),
    //                 R2 = Math.round(Rh - W);
    //             if (Rb.height > W) {
    //                 if (Ra.top < K) { // scroll down
    //                     if (R2 + N > R1) { // do not reach the bottom
    //                         if (Rb.bottom - W + N <= 0) { // pick up
    //                             b.className = 'sticky';
    //                             b.style.top = W - Rb.height - N + 'px';
    //                             Z = N + Ra.top + Rb.height - W;
    //                         } else {
    //                             b.className = 'stop';
    //                             b.style.top = -Z + 'px';
    //                         }
    //                     } else {
    //                         b.className = 'stop';
    //                         b.style.top = -R1 + 'px';
    //                         Z = R1;
    //                     }
    //                 } else {
    //                     if (Ra.top - P < 0) {
    //                         if (Rb.top - P >= 0) {
    //                             b.className = 'sticky';
    //                             b.style.top = P + 'px';
    //                             Z = Ra.top - P;
    //                         } else {
    //                             b.className = 'stop';
    //                             b.style.top = -Z + 'px';
    //                         }
    //                     } else {
    //                         b.className = '';
    //                         b.style.top = '';
    //                         Z = 0;
    //                     }
    //                 }
    //                 K = Ra.top;
    //             } else {
    //                 if ((Ra.top - P) <= 0) {
    //                     if ((Ra.top - P) <= R1) {
    //                         b.className = 'stop';
    //                         b.style.top = -R1 + 'px';
    //                     } else {
    //                         b.className = 'sticky';
    //                         b.style.top = P + 'px';
    //                     }
    //                 } else {
    //                     b.className = '';
    //                     b.style.top = '';
    //                 }
    //             }
    //             window.addEventListener('resize', function() {
    //                 a.children[0].style.width = getComputedStyle(a, '').width
    //             }, false);
    //         }
    //     }
    // })();

    //menu content
    //    $('.best-solutions .sub-menu').append('<a href="" class="sub-menu-btn-all-cat">See all categories</li>');
    //    $('.guides .sub-menu').append('<a href="" class="sub-menu-btn-all-cat">See all guides</li>');
    //    $('.reviews>.sub-menu-btn>.sub-menu').append('<a href="" class="sub-menu-btn-all-cat">See all reviews</li>');

    $('.nav-content').on('click', function() {
        $(this).toggleClass('active');
    });

    $('.nav-content a').on('click', function() {
        $('.nav-content').removeClass('active');
    });

    $(window).scroll(function() {
        var the_top = $(document).scrollTop();
        if (the_top > 480) {
            $('.nav-content').addClass('fixed');
            $('.single-reviews-info').addClass('fixed');
            $('.single-reviews-castom-row_verdict').css('margin-top', 255);
        } else {
            $('.nav-content').removeClass('fixed');
            $('.single-reviews-info').removeClass('fixed');
            $('.single-reviews-castom-row_verdict').css('margin-top', 15);
        }
    });

    //home component read more
    $('.component__read-more').on('click', function() {
        $('.component').toggleClass('active');
        $(this).toggleClass('active');

        if (!$(this).hasClass('active')) {
            let id = $(this).attr('data-anchor');
            if ($(document).find('#' + id).length > 0) {
                let posY = $(document).find('#' + id).offset().top;
                $('html,body').animate({
                    scrollTop: posY
                }, 1000);
            }
            return false;
        }
    });

    //btn up page
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('#scroller').fadeIn();
        } else {
            $('#scroller').fadeOut();
        }
    });
    $('#scroller').click(function() {
        $('body,html').animate({ scrollTop: 0 }, 400);
        return false;
    });

    //cookies
    $('.cookies-popup__btn').click(function() {
        $('.cookies-popup').css('display', 'none');
        localStorage.setItem('cookies', 1);
    });
    if (localStorage.getItem('cookies') == 1) {
        $('.cookies-popup').css('display', 'none');
    } else {
        setTimeout(() => $('.cookies-popup').fadeIn(1000), 2000);
    }

    //adoric popup
    $('.btn-popup__close-btn').click(function() {
        $('.btn-popup').fadeOut(1000);
    });
    //page rewiews
    $('.arсhive-rewiews .adoric-popup').click(function(e) {
        $('.btn-popup').fadeIn(1000);
        let titleReviews = $(this).parent().parent().parent().find('.witget-all__title').html();
        $('.btn-popup__reviews').val(titleReviews).prop('readonly', true);
        console.log(titleReviews);
    });
    //single reviews
    $('.single-reviews-info .adoric-popup').click(function(e) {
        $('.btn-popup').fadeIn(1000);
        let titleReviews = $('.full-page__title').html();
        $('.btn-popup__reviews').val(titleReviews).prop('readonly', true);
    });
    //witget
    $('.top-5-witget__links-visit').click(function(e) {
        $('.btn-popup').fadeIn(1000);
        let titleReviews = $(this).parent().parent().find('.top-5-witget__title-brend').html();
        $('.btn-popup__reviews').val(titleReviews).prop('readonly', true);
    });

    //exit site popup
    $('.close-popup__close-btn').click(function() {
        $('.close-popup').fadeOut(1000);
        localStorage.setItem('popupClose', 1);
    });


    $(document).mouseleave(function(e) {
        if (e.clientY < 10 && localStorage.getItem('popupClose') != 1) {
            setTimeout(() => $('.close-popup').fadeIn(1000), 1500);
        }
    });
});


/*
 Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/
(function() {
    var b, f;
    b = this.jQuery || window.jQuery;
    f = b(window);
    b.fn.stick_in_parent = function(d) {
        var A, w, J, n, B, K, p, q, k, E, t;
        null == d && (d = {});
        t = d.sticky_class;
        B = d.inner_scrolling;
        E = d.recalc_every;
        k = d.parent;
        q = d.offset_top;
        p = d.spacer;
        w = d.bottoming;
        null == q && (q = 0);
        null == k && (k = void 0);
        null == B && (B = !0);
        null == t && (t = "is_stuck");
        A = b(document);
        null == w && (w = !0);
        J = function(a, d, n, C, F, u, r, G) {
            var v, H, m, D, I, c, g, x, y, z, h, l;
            if (!a.data("sticky_kit")) {
                a.data("sticky_kit", !0);
                I = A.height();
                g = a.parent();
                null != k && (g = g.closest(k));
                if (!g.length) throw "failed to find stick parent";
                v = m = !1;
                (h = null != p ? p && a.closest(p) : b("<div />")) && h.css("position", a.css("position"));
                x = function() {
                    var c, f, e;
                    if (!G && (I = A.height(), c = parseInt(g.css("border-top-width"), 10), f = parseInt(g.css("padding-top"), 10), d = parseInt(g.css("padding-bottom"), 10), n = g.offset().top + c + f, C = g.height(), m && (v = m = !1, null == p && (a.insertAfter(h), h.detach()), a.css({ position: "", top: "", width: "", bottom: "" }).removeClass(t), e = !0), F = a.offset().top - (parseInt(a.css("margin-top"), 10) || 0) - q,
                            u = a.outerHeight(!0), r = a.css("float"), h && h.css({ width: a.outerWidth(!0), height: u, display: a.css("display"), "vertical-align": a.css("vertical-align"), "float": r }), e)) return l()
                };
                x();
                if (u !== C) return D = void 0, c = q, z = E, l = function() {
                    var b, l, e, k;
                    if (!G && (e = !1, null != z && (--z, 0 >= z && (z = E, x(), e = !0)), e || A.height() === I || x(), e = f.scrollTop(), null != D && (l = e - D), D = e, m ? (w && (k = e + u + c > C + n, v && !k && (v = !1, a.css({ position: "fixed", bottom: "", top: c }).trigger("sticky_kit:unbottom"))), e < F && (m = !1, c = q, null == p && ("left" !== r && "right" !== r || a.insertAfter(h),
                            h.detach()), b = { position: "", width: "", top: "" }, a.css(b).removeClass(t).trigger("sticky_kit:unstick")), B && (b = f.height(), u + q > b && !v && (c -= l, c = Math.max(b - u, c), c = Math.min(q, c), m && a.css({ top: c + "px" })))) : e > F && (m = !0, b = { position: "fixed", top: c }, b.width = "border-box" === a.css("box-sizing") ? a.outerWidth() + "px" : a.width() + "px", a.css(b).addClass(t), null == p && (a.after(h), "left" !== r && "right" !== r || h.append(a)), a.trigger("sticky_kit:stick")), m && w && (null == k && (k = e + u + c > C + n), !v && k))) return v = !0, "static" === g.css("position") && g.css({ position: "relative" }),
                        a.css({ position: "absolute", bottom: d, top: "auto" }).trigger("sticky_kit:bottom")
                }, y = function() { x(); return l() }, H = function() {
                    G = !0;
                    f.off("touchmove", l);
                    f.off("scroll", l);
                    f.off("resize", y);
                    b(document.body).off("sticky_kit:recalc", y);
                    a.off("sticky_kit:detach", H);
                    a.removeData("sticky_kit");
                    a.css({ position: "", bottom: "", top: "", width: "" });
                    g.position("position", "");
                    if (m) return null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.remove()), a.removeClass(t)
                }, f.on("touchmove", l), f.on("scroll", l), f.on("resize",
                    y), b(document.body).on("sticky_kit:recalc", y), a.on("sticky_kit:detach", H), setTimeout(l, 0)
            }
        };
        n = 0;
        for (K = this.length; n < K; n++) d = this[n], J(b(d));
        return this
    }
}).call(this);