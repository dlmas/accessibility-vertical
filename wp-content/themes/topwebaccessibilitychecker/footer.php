<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sobix
 */

?>
<footer>
    <div class="container">
        <div class="footer-top">
            <div class="footer-logo"><?php $image = get_field('footer_logo', 'options'); echo wp_get_attachment_image( $image, array(160, 40) ); ?></div>
                <div class="footer-expert">
                    <div class="footer-expert__title"><?php the_field('team_of_experts', 'option')?></div>
                    <div class="footer-expert__row">
                        <div class="footer-expert__item">
                            <div class="footer-expert__img">
                            <?php 
                                $image = get_field('nuraen_isa_photo', 'options');
                                $size = array(75, 93); // (thumbnail, medium, large, full or custom size)
                                if( $image ) {
                                    echo wp_get_attachment_image( $image, $size );
                                }
                            ?>
                            </div>
                            <div class="footer-expert__name">
                                <div class="footer-expert__name-famely"><?php the_field('nuraen_isa', 'options'); ?></div>
                                <div class="footer-expert__name-prof"><?php the_field('head_of_research', 'option')?></div>
                            </div>
                        </div>
                        <div class="footer-expert__item">
                            <div class="footer-expert__img">
                            <?php 
                                $image = get_field('hester_photo', 'options');
                                $size = array(75, 93); // (thumbnail, medium, large, full or custom size)
                                if( $image ) {
                                    echo wp_get_attachment_image( $image, $size );
                                }
                            ?>
                            </div>
                            <div class="footer-expert__name">
                                <div class="footer-expert__name-famely"><?php the_field('hester_hoog', 'options'); ?></div>
                                <div class="footer-expert__name-prof"><?php the_field('head_of_digital_content', 'option')?></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="footer-bottm">
            <div class="footer-bottm__item footer-bottm__text">
                <div class="footer-text">
                    <?php the_field('footer_content', 'options'); ?>
                </div>
                <div class="copyright"><?php the_field('footer_copyright', 'options'); ?></div>
            </div>
            <div class="footer-bottm__item footer-bottm__item_menu">
                <div class="footer-bottm__item-one">
                    <?php wp_nav_menu( [ 
                                    'theme_location' => 'menu-2',
                                    'menu_class' => 'footer-menu-top',
                                ] ); ?>
                    <?php if(domain_user() != 'www.topwebaccessibilitychecker.com') { ?>
                    <div class="footer-left__social">
                        <a href="https://www.facebook.com/BestWebAccessibility" target="_blank">
                            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-facebook-f fa-w-10 fa-2x">
                                <path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z" class=""></path>
                            </svg>
                        </a>
                        <a href="https://twitter.com/BestWebAcc" target="_blank">
                            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-twitter fa-w-16 fa-2x">
                                <path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" class=""></path>
                            </svg>
                        </a>
                        <a href="https://www.linkedin.com/company/best-web-accessibility" target="_blank">
                            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-linkedin-in fa-w-14 fa-2x">
                                <path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z" class=""></path>
                            </svg>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <div class="footer-bottm__item-two">
                    <?php wp_nav_menu( [ 
                                            'theme_location' => 'menu-3',
                                            'menu_class' => 'footer-menu-bottom',
                                        ] ); ?>
                </div>
            </div>
        </div>
        
    </div>
</footer>
    <div class="cookies-popup" style="display: none;">
        <div class="cookies-popup__text"><?php the_field('cookies_popup_text', 'options'); ?> <a href="<?php the_field('cookies_popup_page', 'options'); ?>"><?php the_field('cookies_popup_learn_more', 'options') ?></a></div>
        <div class="cookies-popup__btn"><?php the_field('cookies_popup_button_text', 'options'); ?></div>
    </div>

    <div class="btn-popup" style="display: none;">
    <div class="close-popup__title"><?php the_field('btn_popup_title', 'options'); ?></div>
    <div class="close-popup__sub-title"><?php the_field('btn_popup_subtitle', 'options'); ?></div>
    <svg class="btn-popup__close-btn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25"  xml:space="preserve" class="inner-element" role="img" width="100%" height="100%" preserveAspectRatio="none" data-event-name="Conversion" data-shadow-distance="0" data-init-color="#ffffff"> <path style="fill:#ffffff" d="M12.5,0C5.596,0,0,5.596,0,12.5S5.596,25,12.5,25S25,19.404,25,12.5S19.404,0,12.5,0z M18.157,16.035 	l-2.121,2.121L12.5,14.621l-3.536,3.536l-2.121-2.121l3.535-3.536L6.843,8.964l2.121-2.121l3.536,3.536l3.536-3.536l2.121,2.121 	L14.621,12.5L18.157,16.035z" data-init-color="#cccccc"></path> </svg>
    <div class="">
        <div class="btn-popup__logo"><img src="https://static.adoric.com/9573bc6d-a97c-4727-a838-259c7f46a87d.jpg"></div>
        <?php echo do_shortcode('[contact-form-7 id="958" title="REQUEST DEMO popup"]');?>
    </div>
</div>


<div class="close-popup" style="display: none;">
    <div class="close-popup__title"><?php the_field('close-popup__title', 'options'); ?></div>
    <svg class="close-popup__close-btn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 25"  xml:space="preserve" class="inner-element" role="img" width="100%" height="100%" preserveAspectRatio="none" data-event-name="Conversion" data-shadow-distance="0" data-init-color="#ffffff"> <path style="fill:#ffffff" d="M12.5,0C5.596,0,0,5.596,0,12.5S5.596,25,12.5,25S25,19.404,25,12.5S19.404,0,12.5,0z M18.157,16.035 	l-2.121,2.121L12.5,14.621l-3.536,3.536l-2.121-2.121l3.535-3.536L6.843,8.964l2.121-2.121l3.536,3.536l3.536-3.536l2.121,2.121 	L14.621,12.5L18.157,16.035z" data-init-color="#cccccc"></path> </svg>
    <div class="close-popup__content-row">
    <div class="btn-popup__logo"> <?php $image = get_field('header_logo', 'options'); echo wp_get_attachment_image( $image, array(160, 40) ); ?></div>
        <?php echo do_shortcode('[contact-form-7 id="957" title="Close site popup"]');?>
    </div>
</div>

<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but"><svg width="40" height="40" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-circle-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-chevron-circle-up fa-w-16 fa-2x"><path fill="rgb(51 151 255)" d="M8 256C8 119 119 8 256 8s248 111 248 248-111 248-248 248S8 393 8 256zm231-113.9L103.5 277.6c-9.4 9.4-9.4 24.6 0 33.9l17 17c9.4 9.4 24.6 9.4 33.9 0L256 226.9l101.6 101.6c9.4 9.4 24.6 9.4 33.9 0l17-17c9.4-9.4 9.4-24.6 0-33.9L273 142.1c-9.4-9.4-24.6-9.4-34 0z" class=""></path></svg></span></div>
<?php wp_footer(); ?>
