<?php
/**
 * sobix functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sobix
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'sobix_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sobix_setup() {


		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Menu header', 'sobix' ),
				'menu-2' => esc_html__( 'Menu footer top', 'sobix' ),
                'menu-3' => esc_html__( 'Menu footer bottom', 'sobix' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'sobix_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'sobix_setup' );

add_theme_support( 'post-thumbnails' ); // для всех типов постов подключаем миниатюры


		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sobix_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sobix_content_width', 640 );
}
add_action( 'after_setup_theme', 'sobix_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sobix_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'sobix' ),
			'id'            => 'sidebar-2',
			'description'   => esc_html__( 'Add widgets here.', 'sobix' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
			'description' => __( "This is the description" ),

		)
	);
}
add_action( 'widgets_init', 'sobix_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method() {
	// отменяем зарегистрированный jQuery
	// вместо "jquery-core", можно вписать "jquery", тогда будет отменен еще и jquery-migrate
//	wp_deregister_script( 'jquery-core' );
//	wp_register_script( 'jquery-core', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
	wp_enqueue_script( 'jquery' );
}

function sobix_scripts() {
	// регистрируем стили
	wp_register_style( 'sobix-style-main', get_template_directory_uri() . '/assets/css/style.min.css' );
	// Подключаю стили
	wp_enqueue_style( 'sobix-style-main' );

	// wp_enqueue_style( 'sobix-style', get_stylesheet_uri(), array(), _S_VERSION );
	// wp_style_add_data( 'sobix-style', 'rtl', 'replace' );

//    wp_register_style( 'sobix-style-poppins', get_template_directory_uri() . '/assets/fonts/poppins/stylesheet.css' );
//    wp_enqueue_style( 'sobix-style-poppins' );
//
//    wp_register_style( 'sobix-style-sarabun', get_template_directory_uri() . '/assets/fonts/sarabun/stylesheet.css' );
//    wp_enqueue_style( 'sobix-style-sarabun' );

	// wp_enqueue_script( 'sobix-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
//	wp_enqueue_script( 'jquery-cookie', get_stylesheet_directory_uri() . '/assets/js/jquery.cookie.js', array('jquery') );
	wp_enqueue_script( 'main-script', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery') );
    //wp_enqueue_script( 'stickUp', get_stylesheet_directory_uri() . '/assets/js/stickUp.min.js', array('jquery') );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sobix_scripts' );

//подключение шрифтов Open Sans и Roboto от Google start
function wph_add_google_fonts() {
    if ( !is_admin() ) {
        wp_register_style('google-montserrat', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;600;700&display=swap', array(), null, 'all');
        wp_enqueue_style('google-montserrat');
    }
}
add_action('wp_enqueue_scripts', 'wph_add_google_fonts');

/* favicon */
function add_favicon() {
	$image = get_field('favicon_image', 'options');
	$size = array(32, 32); // (thumbnail, medium, large, full или ваш размер)

	echo '<link rel="shortcut icon" type="image/png" href="'. wp_get_attachment_image_url( $image, $size ) .'" />';
   }
add_action('wp_head', 'add_favicon');


/* Регистрация виджета */

function wpb_load_widget() {
	register_widget( 'lineup_5_top' );
  }
  add_action( 'widgets_init', 'wpb_load_widget' );

  class lineup_5_top extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_5_top', 'lineup_5_top', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

/* Регистрация виджета */

function wpb_load_widget_top3() {
	register_widget( 'lineup_3_top' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_top3' );

  class lineup_3_top extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_3_top', 'lineup_3_top3', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

  /* Регистрация виджета */

function wpb_load_widget_homepage() {
	register_widget( 'lineup_20_homepage' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_homepage' );

  class lineup_20_homepage extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_20_homepage', 'lineup_20_homepage', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title frontend</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

    /* Регистрация виджета */

function wpb_load_widget_popular() {
	register_widget( 'lineup_4_popular' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_popular' );

  class lineup_4_popular extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_4_popular', 'lineup_4_popular', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title frontend</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

    /* Регистрация виджета */

	function wpb_load_widget_topfree() {
		register_widget( 'lineup_4_topfree' );
	  }
	  add_action( 'widgets_init', 'wpb_load_widget_topfree' );

	  class lineup_4_topfree extends WP_Widget {

		function __construct() {
			$widget_ops = array(
				'classname' => 'my_widget',
				'description' => 'My Widget is awesome',
			);
		  parent::__construct('lineup_4_topfree', 'lineup_4_topfree', $widget_ops );
		}

		public function widget( $args, $instance ) {


		  $title = apply_filters( 'widget_title', $instance['title'] );
		  $widget_id = $args['widget_id'];

			/* Булет */
		if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
		  echo $args['before_widget'];


		  /* Цвет */

		//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
		// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
		//   } else {
		// 	$color = 'transparent';
		//   }
		//   echo '<div style="background-color:' . $color . '">';

		  /* Заголовок */
		  if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		  /* Текст */

		  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
			echo get_field('widget_text', 'widget_' . $widget_id);
		  }

		  /* Изображение */

		//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
		// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
		//   }

		  //echo '</div>';
		  echo $args['after_widget'];
		}
	}

		public function form( $instance ) {
		  if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		  } else {
			$title = 'Widget title';
		  }
		  ?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>">title frontend</label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<?php
		}

		public function update( $new_instance, $old_instance ) {
		  $instance = array();
		  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		  return $instance;
		}

	  }
   /* Регистрация виджета */

   function wpb_load_widget_comparison() {
	register_widget( 'lineup_4_comparison' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_comparison' );

  class lineup_4_comparison extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_4_comparison', 'lineup_4_comparison', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>">title frontend</label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

  /* Регистрация reviews */

function wpb_load_widget_reviewspage() {
	register_widget( 'lineup_100_reviewspage' );
  }
  add_action( 'widgets_init', 'wpb_load_widget_reviewspage' );

  class lineup_100_reviewspage extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'my_widget',
			'description' => 'My Widget is awesome',
		);
	  parent::__construct('lineup_100_reviewspage', 'lineup_100_reviewspage', $widget_ops );
	}

	public function widget( $args, $instance ) {


	  $title = apply_filters( 'widget_title', $instance['title'] );
	  $widget_id = $args['widget_id'];

		/* Булет */
	if ( get_field('widget_off', 'widget_' . $widget_id) == 0 ) {
	  echo $args['before_widget'];


	  /* Цвет */

	//   if ( get_field('danilin_testwidget_color', 'widget_' . $widget_id) ) {
	// 	$color = get_field('danilin_testwidget_color', 'widget_' . $widget_id);
	//   } else {
	// 	$color = 'transparent';
	//   }
	//   echo '<div style="background-color:' . $color . '">';

	  /* Заголовок */
	  if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	  /* Текст */

	  if ( get_field('widget_text', 'widget_' . $widget_id) ) {
		echo get_field('widget_text', 'widget_' . $widget_id);
	  }

	  /* Изображение */

	//   if ( get_field('danilin_testwidget_image', 'widget_' . $widget_id) ) {
	// 	echo wp_get_attachment_image( get_field('danilin_testwidget_image', 'widget_' . $widget_id), 'large' );
	//   }

	  //echo '</div>';
	  echo $args['after_widget'];
	}
}

	public function form( $instance ) {
	  if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	  } else {
		$title = 'Widget title';
	  }
	  ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>">title frontend</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
	  $instance = array();
	  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	  return $instance;
	}

  }

  /* Сайтбар топ 10 */
function register_sidebar_top(){
	register_sidebar( array(
		'name'          => "sidebar_homepage",
		'id'            => 'homepage-sidebar',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => "</li>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
	) );
}
add_action( 'widgets_init', 'register_sidebar_top' );

  /* Сайтбар reviews page */
function register_sidebar_reviews(){
	register_sidebar( array(
		'name'          => "sidebar_reviewspage",
		'id'            => 'reviewspage-sidebar',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => "</li>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
	) );
}
add_action( 'widgets_init', 'register_sidebar_reviews' );

// Выводим IP пользователя в WordPress
function getIp() {
	$keys = [
	  'HTTP_CLIENT_IP',
	  'HTTP_X_FORWARDED_FOR',
	  'REMOTE_ADDR'
	];
	foreach ($keys as $key) {
	  if (!empty($_SERVER[$key])) {
		$tmp = explode(',', $_SERVER[$key]);
        $file_extension = end($tmp);
        $ip = trim($file_extension);
		if (filter_var($ip, FILTER_VALIDATE_IP)) {
		  return $ip;
		}
	  }
	}
  }

// url user
function url_user() {
	$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	return $url;
}

//domain user
function domain_user() {
	// $host_names = explode(".", $current_domain);
	// $bottom_host_name = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];

	$current_domain = $_SERVER['HTTP_HOST'];
	return $current_domain;
}

//Screen resolution
//function screen_resolutionn() {
//	$width = $_COOKIE['sw'];
//	$height = $_COOKIE['sh'];
//
//	return 'width: '.$width.', height: '.$height;
//}

//reset api no function
function isRestUrl() {
    $bIsRest = false;
    if ( function_exists( 'rest_url' ) && !empty( $_SERVER[ 'REQUEST_URI' ] ) ) {
        $sRestUrlBase = get_rest_url( get_current_blog_id(), '/' );
        $sRestPath = trim( parse_url( $sRestUrlBase, PHP_URL_PATH ), '/' );
        $sRequestPath = trim( $_SERVER[ 'REQUEST_URI' ], '/' );
        $bIsRest = ( strpos( $sRequestPath, $sRestPath ) === 0 );
    }
    return $bIsRest;
}

//setcookie( 'lineup_3_top', '', time() - 1, COOKIEPATH, COOKIE_DOMAIN );
//setcookie( 'lineup_5_top', '', time() - 1, COOKIEPATH, COOKIE_DOMAIN );
//setcookie( 'lineup_20_homepage', '', time() - 1, COOKIEPATH, COOKIE_DOMAIN );

$array_result = [];
$user_agent = $_SERVER['HTTP_USER_AGENT'];

/**
 * @param $widget_name
 */
function widget_lineup_builder_v2($widget_name) {

    global $array_result, $user_agent;

    if (!isset($_COOKIE[$widget_name])) {

        // массив для переменных, которые будут переданы с запросом
//        if(domain_user() == 'bestwebsiteaccessibility.com'){
//            $paramsArray = [
//                'ip'         => getIp(),
//                'host'       => 'bestwebsiteaccessibility.com',
//                'widget'     => $widget_name,
//                'user_agent' => $user_agent
//            ];
//        }else if(domain_user() == 'bestwebaccessibility.com'){
//            $paramsArray = [
//                'ip'         => getIp(),
//                'host'       => 'bestwebaccessibility.com',
//                'widget'     => $widget_name,
//                'user_agent' => $user_agent
//            ];
//        }else{
            $paramsArray = [
                'ip'         => getIp(),
                'host'       => domain_user(),
                'widget'     => $widget_name,
                'user_agent' => $user_agent
            ];

//        }
        // преобразуем массив в URL-кодированную строку
        $vars = json_encode($paramsArray, JSON_UNESCAPED_UNICODE);

        // создаем параметры контекста
        $options = [
            'http' => [
                'method'  => 'POST', // метод передачи данных
                'header'  => 'Content-Type: application/json', // заголовок
                'content' => $vars, // переменные
				'timeout' => 1  //Seconds
            ]
        ];

        $context = stream_context_create($options); // создаём контекст потока


            $result = file_get_contents(
            'https://lb.bestwebsiteaccessibility.com/lines',
            false,
            $context
        ); //отправляем запрос



        $result = json_decode($result);
        //echo '<pre>'; print_r($result); echo '</pre>';
        if($result){
            $array_result[$widget_name] = $result;
        }
    }
}

if(!isRestUrl()){
    // widget_lineup_builder_v2('lineup_3_top');
    // widget_lineup_builder_v2('lineup_20_homepage');
    widget_lineup_builder_v2('lineup_5_top');
	widget_lineup_builder_v2('lineup_4_popular');
	widget_lineup_builder_v2('lineup_4_topfree');
	widget_lineup_builder_v2('lineup_4_comparison');
	widget_lineup_builder_v2('lineup_100_reviewspage');


}
/**
 * @param $widget_name
 * @param $post_type
 * @param $posts_per_page
 *
 * @return \WP_Query
 */
function widget_lineup_builder_v3($widget_name, $post_type, $posts_per_page)
{
    global $query, $array_result;

    //mobile posts_per_page for homepage lineup_20_homepage witget
    if( $widget_name == 'lineup_20_homepage' && wp_is_mobile() || $widget_name == 'lineup_100_reviewspage' && wp_is_mobile()) {
        $posts_per_page = 5;
    }

    if (array_key_exists($widget_name, $array_result)) {
        return $query = new WP_Query(
            [
                'post_type'      => $post_type,
                'posts_per_page' => $posts_per_page,
                'post__in'       => $array_result[$widget_name],
                'orderby'        => 'post__in'
            ]
        );
    }

    if (isset($_COOKIE[$widget_name])) {
        return $query = new WP_Query(
            [
                'post_type'      => $post_type,
                'posts_per_page' => $posts_per_page,
                'post__in'       => unserialize($_COOKIE[$widget_name]),
                'orderby'        => 'post__in'
            ]
        );
    }

    // return $query = new WP_Query(
    //     [
    //         'post_type'      => $post_type,
    //         'posts_per_page' => $posts_per_page,
    //         'meta_key'       => 'raiting',
    //         'orderby'        => 'meta_value',
    //         'order'          => 'DESC'
    //     ]
    // );
}

// add_action('lineup_3_top', 'widget_lineup_builder_v3', 10, 3);
// add_action('lineup_20_homepage', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_100_reviewspage', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_5_top', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_4_popular', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_4_topfree', 'widget_lineup_builder_v3', 10, 3);
add_action('lineup_4_comparison', 'widget_lineup_builder_v3', 10, 3);

//cookie lineups
 add_action( 'init', 'setcookie_lineups' );
 function setcookie_lineups() {
     global $array_result;
     foreach($array_result as $key => $val) {
	       setcookie( $key, serialize($val), time() + (86400 * 30), COOKIEPATH, COOKIE_DOMAIN ); //1 день time() + (86400 * 30)
	 }
 }

//load more ajax home page
function true_load_posts(){
    $posts_per_page = 10;
    if( wp_is_mobile()) {
        $posts_per_page = 5;
    }

    $postIn = unserialize( stripslashes( $_POST['query'] ) );
	$args = $postIn;
	$args['paged'] = $_POST['page'] + 1; // следующая страница
	$args['post_status'] = 'publish';
	$args['posts_per_page'] = $posts_per_page; // по сколько записей подгружать
    $args['post_type'] = 'reviews';
    if($postIn['post__in']) {
        $args['orderby'] = 'post__in';
        //$args['post__in'] = $result,
    }else {
        //$args['cat'] = array(5,6,7);
        $args['orderby'] = 'meta_value';
        $args['meta_key'] = 'raiting';
        $args['order'] = 'DESC';
    }


	// обычно лучше использовать WP_Query, но не здесь
	query_posts( $args );
	// если посты есть
	if( have_posts() ) :
		// запускаем цикл
		while( have_posts() ): the_post();
                get_template_part( 'template-parts/content', $_POST['template_part'] );
		endwhile;

	endif;
	die();
}


add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');


//load more ajax guodes
function guodes_load_posts(){
    $posts_per_page = 10;
    // if( wp_is_mobile()) {
    //     $posts_per_page = 5;
    // }

    $postIn = unserialize( stripslashes( $_POST['query'] ) );
	$args = $postIn;
	$args['paged'] = $_POST['page'] + 1; // следующая страница
	$args['post_status'] = 'publish';
	$args['posts_per_page'] = $posts_per_page; // по сколько записей подгружать
    $args['post_type'] = 'guides';



	// обычно лучше использовать WP_Query, но не здесь
	query_posts( $args );
	// если посты есть
	if( have_posts() ) :
		// запускаем цикл
		while( have_posts() ): the_post();
                get_template_part( 'template-parts/content', 'article' );
		endwhile;

	endif;
	die();
}


add_action('wp_ajax_guodes_loadmore', 'guodes_load_posts');
add_action('wp_ajax_nopriv_guodes_loadmore', 'guodes_load_posts');

 /* Новый тип записи */
 add_action('init', 'register_postType_reviews');
 function register_postType_reviews() {
	 $labels = array(
		 'name' => 'Reviews',
		 'singular_name' => 'Reviews',
		 'add_new' => 'Add Reviews',
		 'add_new_item' => 'Add new Reviews',
		 'edit_item' => 'Edit Reviews',
		 'new_item' => 'New Reviews',
		 'all_items' => 'All Reviews',
		 'view_item' => 'View Reviews',
		 'search_items' => 'Search Reviews',
		 'not_found' => 'Reviews not found',
		 'not_found_in_trash' => 'No in basket Reviews',
		 'menu_name' => 'Reviews'
	 );
	 $args   = array(
		 'labels' => $labels,
		 'rewrite' => array( 'slug' => 'reviews'),
		 'public' => true,
		 'show_ui' => true,
		 'has_archive' => 'reviews',
		 'publicly_queryable' => true,
		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		 'menu_position' => 5,
		 'menu_icon' => 'dashicons-feedback',
		 'show_in_nav_menus' => true, //выводить в меню
		 'show_in_rest'       => true, //вкл. гутенберг
		 'supports' => array(
			 'title',
			 'editor',
			 'thumbnail',
			 'excerpt',
             'comments'
		 )
	 );
	 register_post_type('reviews', $args);
 }

 function register_taxonomies_reviews() {
	 $labels = array(
		 'name'              => _x( 'Categories Reviews', 'taxonomy general name' ),
		 'singular_name'     => _x( 'Categories Reviews', 'taxonomy singular name' ),
		 'search_items'      => __( 'Search ' ),
		 'all_items'         => __( 'All categories' ),
		 'parent_item'       => __( 'Parental categories' ),
		 'parent_item_colon' => __( 'Parental categories:' ),
		 'edit_item'         => __( 'Edit categories' ),
		 'update_item'       => __( 'Refresh categories' ),
		 'add_new_item'      => __( 'Add new categories' ),
		 'new_item_name'     => __( 'New name categories' ),
		 'menu_name'         => __( 'Categories Reviews' ),
	 );

	 $args =  array(
		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		 'labels'            => $labels,
		 'show_ui'           => true,
		 'show_admin_column' => true,
		 'query_var'         => true,
         'show_in_rest' => true,
		 //'rewrite' => array( 'slug' => 'housing', // ярлык
							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	 );

	 register_taxonomy( 'reviews_category', 'reviews', $args );
 }
 add_action( 'init', 'register_taxonomies_reviews', 0 );



 /* Новый тип записи */
 add_action('init', 'register_postType_guides');
 function register_postType_guides() {
	 $labels = array(
		 'name' => 'Guides',
		 'singular_name' => 'Guides',
		 'add_new' => 'Add Guides',
		 'add_new_item' => 'Add new Guides',
		 'edit_item' => 'Edit Guides',
		 'new_item' => 'New Guides',
		 'all_items' => 'All Guides',
		 'view_item' => 'View Guides',
		 'search_items' => 'Search Guides',
		 'not_found' => 'Guides not found',
		 'not_found_in_trash' => 'No in basket Guides',
		 'menu_name' => 'Guides'
	 );
	 $args   = array(
		 'labels' => $labels,
		 'rewrite' => array( 'slug' => 'guides'),
		 'public' => true,
		 'show_ui' => true,
		 'has_archive' => false,
		 'publicly_queryable' => true,
		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		 'menu_position' => 6,
		 'menu_icon' => 'dashicons-admin-post',
		 'show_in_rest'       => true, //вкл. гутенберг
		 'supports' => array(
			 'title',
			 'editor',
			 'thumbnail'
		 )
	 );
	 register_post_type('guides', $args);
 }

 function register_taxonomies_guides() {
	 $labels = array(
		 'name'              => _x( 'Categories Guides', 'taxonomy general name' ),
		 'singular_name'     => _x( 'Categories Guides', 'taxonomy singular name' ),
		 'search_items'      => __( 'Search ' ),
		 'all_items'         => __( 'All categories' ),
		 'parent_item'       => __( 'Parental categories' ),
		 'parent_item_colon' => __( 'Parental categories:' ),
		 'edit_item'         => __( 'Edit categories' ),
		 'update_item'       => __( 'Refresh categories' ),
		 'add_new_item'      => __( 'Add new categories' ),
		 'new_item_name'     => __( 'New name categories' ),
		 'menu_name'         => __( 'Categories Guides' ),
	 );

	 $args =  array(
		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		 'labels'            => $labels,
		 'show_ui'           => true,
		 'show_admin_column' => true,
		 'query_var'         => true,
         'show_in_rest' => true,
		 //'rewrite' => array( 'slug' => 'guides'),
		 //'rewrite' => array( 'slug' => 'housing', // ярлык
							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	 );

	 register_taxonomy( 'guides_category', 'guides', $args );
 }
 add_action( 'init', 'register_taxonomies_guides', 0 );

 /* Новый тип записи */
 add_action('init', 'register_postType_article');
 function register_postType_article() {
	 $labels = array(
		 'name' => 'Article',
		 'singular_name' => 'Article',
		 'add_new' => 'Add Article',
		 'add_new_item' => 'Add new Article',
		 'edit_item' => 'Edit Article',
		 'new_item' => 'New Article',
		 'all_items' => 'All Article',
		 'view_item' => 'View Article',
		 'search_items' => 'Search Article',
		 'not_found' => 'Article not found',
		 'not_found_in_trash' => 'No in basket Article',
		 'menu_name' => 'Article'
	 );
	 $args   = array(
		 'labels' => $labels,
		 'rewrite' => array( 'slug' => 'article'),
		 'public' => true,
		 'show_ui' => true,
		 'has_archive' => false,
		 'publicly_queryable' => true,
		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		 'menu_position' => 6,
		 'menu_icon' => 'dashicons-admin-post',
		 'show_in_rest'       => true, //вкл. гутенберг
		 'supports' => array(
			 'title',
			 'editor',
			 'thumbnail'
		 )
	 );
	 register_post_type('article', $args);
 }

 function register_taxonomies_article() {
	 $labels = array(
		 'name'              => _x( 'Categories Article', 'taxonomy general name' ),
		 'singular_name'     => _x( 'Categories Article', 'taxonomy singular name' ),
		 'search_items'      => __( 'Search ' ),
		 'all_items'         => __( 'All categories' ),
		 'parent_item'       => __( 'Parental categories' ),
		 'parent_item_colon' => __( 'Parental categories:' ),
		 'edit_item'         => __( 'Edit categories' ),
		 'update_item'       => __( 'Refresh categories' ),
		 'add_new_item'      => __( 'Add new categories' ),
		 'new_item_name'     => __( 'New name categories' ),
		 'menu_name'         => __( 'Categories Article' ),
	 );

	 $args =  array(
		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		 'labels'            => $labels,
		 'show_ui'           => true,
		 'show_admin_column' => true,
		 'query_var'         => true,
         'show_in_rest' => true,
		 //'rewrite' => array( 'slug' => 'housing', // ярлык
							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	 );

	 register_taxonomy( 'article_category', 'article', $args );
 }
 add_action( 'init', 'register_taxonomies_article', 0 );

// if($_SERVER['HTTP_HOST'] != 'topwebaccessibilitychecker.com') {
 /* Новый тип записи */
//  add_action('init', 'register_postType_bestsolution');
//  function register_postType_bestsolution() {
// 	 $labels = array(
// 		 'name' => 'Best Solution',
// 		 'singular_name' => 'Best Solution',
// 		 'add_new' => 'Add Best Solution',
// 		 'add_new_item' => 'Add new Best Solution',
// 		 'edit_item' => 'Edit Best Solution',
// 		 'new_item' => 'New Best Solution',
// 		 'all_items' => 'All Best Solution',
// 		 'view_item' => 'View Best Solution',
// 		 'search_items' => 'Search Best Solution',
// 		 'not_found' => 'Article not found',
// 		 'not_found_in_trash' => 'No in basket Best Solution',
// 		 'menu_name' => 'Best Solution'
// 	 );
// 	 $args   = array(
// 		 'labels' => $labels,
// 		 'rewrite' => array( 'slug' => 'bestsolution'),
// 		 'public' => true,
// 		 'show_ui' => true,
// 		 'has_archive' => false,
// 		 'publicly_queryable' => true,
// 		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
// 		 'menu_position' => 6,
// 		 'menu_icon' => 'dashicons-lightbulb',
// 		 'show_in_rest'       => true, //вкл. гутенберг
// 		 'supports' => array(
// 			 'title',
// 			 'editor',
// 			 'thumbnail'
// 		 )
// 	 );
// 	 register_post_type('bestsolution', $args);
//  }


//  function register_taxonomies_bestsolution() {
// 	 $labels = array(
// 		 'name'              => _x( 'Categories bestsolution', 'taxonomy general name' ),
// 		 'singular_name'     => _x( 'Categories bestsolution', 'taxonomy singular name' ),
// 		 'search_items'      => __( 'Search ' ),
// 		 'all_items'         => __( 'All categories' ),
// 		 'parent_item'       => __( 'Parental categories' ),
// 		 'parent_item_colon' => __( 'Parental categories:' ),
// 		 'edit_item'         => __( 'Edit categories' ),
// 		 'update_item'       => __( 'Refresh categories' ),
// 		 'add_new_item'      => __( 'Add new categories' ),
// 		 'new_item_name'     => __( 'New name categories' ),
// 		 'menu_name'         => __( 'Categories bestsolution' ),
// 	 );

// 	 $args =  array(
// 		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
// 		 'labels'            => $labels,
// 		 'show_ui'           => true,
// 		 'show_admin_column' => true,
// 		 'query_var'         => true,
//          'show_in_rest' => true,
// 		 //'rewrite' => array( 'slug' => 'housing', // ярлык
// 							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
// 	 );

// 	 register_taxonomy( 'bestsolution_category', 'bestsolution', $args );
//  }
//  add_action( 'init', 'register_taxonomies_bestsolution', 0 );

//убираем slug bestsolution
// function bestsolution_remove_slug( $post_link, $post, $leavename ) {
//     if ( 'bestsolution' != $post->post_type || 'publish' != $post->post_status ) {
//         return $post_link;
//     }
//     $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
//     return $post_link;
// }
// add_filter( 'post_type_link', 'bestsolution_remove_slug', 10, 3 );

// function bestsolution_parse_request( $query ) {
//     if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
//         return;
//     }
//     if ( ! empty( $query->query['name'] ) ) {
//         $query->set( 'post_type', array( 'post', 'bestsolution', 'page' ) );
//     }
// }
// add_action( 'pre_get_posts', 'bestsolution_parse_request' );
// }

// if($_SERVER['HTTP_HOST'] == 'topwebaccessibilitychecker.com') {

 /* Новый тип записи */
 add_action('init', 'register_postType_topchecker');
 function register_postType_topchecker() {
	 $labels = array(
		 'name' => 'Top Checker',
		 'singular_name' => 'Top Checker',
		 'add_new' => 'Add Top Checker',
		 'add_new_item' => 'Add new Top Checker',
		 'edit_item' => 'Edit Top Checker',
		 'new_item' => 'New Top Checker',
		 'all_items' => 'All Top Checker',
		 'view_item' => 'View Top Checker',
		 'search_items' => 'Search Top Checker',
		 'not_found' => 'Top Checker not found',
		 'not_found_in_trash' => 'No in basket Top Checker',
		 'menu_name' => 'Top Checker'
	 );
	 $args   = array(
		 'labels' => $labels,
		 'rewrite' => array( 'slug' => 'topchecker'),
		 'public' => true,
		 'show_ui' => true,
		 'has_archive' => false,
		 'publicly_queryable' => true,
		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		 'menu_position' => 6,
		 'menu_icon' => 'dashicons-lightbulb',
		 'show_in_rest'       => true, //вкл. гутенберг
		 'supports' => array(
			 'title',
			 'editor',
			 'thumbnail',
			 'excerpt'
		 )
	 );
	 register_post_type('topchecker', $args);
 }


 function register_taxonomies_topchecker() {
	 $labels = array(
		 'name'              => _x( 'Categories Top Checker', 'taxonomy general name' ),
		 'singular_name'     => _x( 'Categories Top Checker', 'taxonomy singular name' ),
		 'search_items'      => __( 'Search ' ),
		 'all_items'         => __( 'All categories' ),
		 'parent_item'       => __( 'Parental categories' ),
		 'parent_item_colon' => __( 'Parental categories:' ),
		 'edit_item'         => __( 'Edit categories' ),
		 'update_item'       => __( 'Refresh categories' ),
		 'add_new_item'      => __( 'Add new categories' ),
		 'new_item_name'     => __( 'New name categories' ),
		 'menu_name'         => __( 'Categories Top Checker' ),
	 );

	 $args =  array(
		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		 'labels'            => $labels,
		 'show_ui'           => true,
		 'show_admin_column' => true,
		 'query_var'         => true,
         'show_in_rest' => true,
		 //'rewrite' => array( 'slug' => 'housing', // ярлык
							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	 );

	 register_taxonomy( 'topchecker_category', 'topchecker', $args );
 }
 add_action( 'init', 'register_taxonomies_topchecker', 0 );

//убираем slug bestsolution
// function topchecker_remove_slug( $post_link, $post, $leavename ) {
//     if ( 'topchecker' != $post->post_type || 'publish' != $post->post_status ) {
//         return $post_link;
//     }
//     $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
//     return $post_link;
// }
// add_filter( 'post_type_link', 'topchecker_remove_slug', 10, 3 );

// function topchecker_parse_request( $query ) {
//     if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
//         return;
//     }
//     if ( ! empty( $query->query['name'] ) ) {
//         $query->set( 'post_type', array( 'post', 'topchecker', 'page' ) );
//     }
// }
// add_action( 'pre_get_posts', 'topchecker_parse_request' );

 /* Новый тип записи */
 add_action('init', 'register_postType_research');
 function register_postType_research() {
	 $labels = array(
		 'name' => 'Research',
		 'singular_name' => 'Research',
		 'add_new' => 'Add Research',
		 'add_new_item' => 'Add new Research',
		 'edit_item' => 'Edit Research',
		 'new_item' => 'New Research',
		 'all_items' => 'All Research',
		 'view_item' => 'View Research',
		 'search_items' => 'Search Research',
		 'not_found' => 'Top Research',
		 'not_found_in_trash' => 'No in basket Research',
		 'menu_name' => 'Research'
	 );
	 $args   = array(
		 'labels' => $labels,
		 'rewrite' => array( 'slug' => 'research'),
		 'public' => true,
		 'show_ui' => true,
		 'has_archive' => false,
		 'publicly_queryable' => true,
		 //'taxonomies' => array( 'category' ), //Массив зарегистрированных таксономий, которые будут связанны с этим типом записей, например: category или post_tag.
		 'menu_position' => 6,
		 'menu_icon' => 'dashicons-admin-site-alt3',
		 'show_in_rest'       => true, //вкл. гутенберг
		 'supports' => array(
			 'title',
			 'editor',
			 'thumbnail'
		 )
	 );
	 register_post_type('research', $args);
 }


 function register_taxonomies_research() {
	 $labels = array(
		 'name'              => _x( 'Categories Research', 'taxonomy general name' ),
		 'singular_name'     => _x( 'Categories Research', 'taxonomy singular name' ),
		 'search_items'      => __( 'Search ' ),
		 'all_items'         => __( 'All categories' ),
		 'parent_item'       => __( 'Parental categories' ),
		 'parent_item_colon' => __( 'Parental categories:' ),
		 'edit_item'         => __( 'Edit categories' ),
		 'update_item'       => __( 'Refresh categories' ),
		 'add_new_item'      => __( 'Add new categories' ),
		 'new_item_name'     => __( 'New name categories' ),
		 'menu_name'         => __( 'Categories Research' ),
	 );

	 $args =  array(
		 'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		 'labels'            => $labels,
		 'show_ui'           => true,
		 'show_admin_column' => true,
		 'query_var'         => true,
         'show_in_rest' => true,
		 //'rewrite' => array( 'slug' => 'housing', // ярлык
							//'with_front' => false ), // Позволяет ссылку добавить к базовому URL.
	 );

	 register_taxonomy( 'research_category', 'research', $args );
 }
 add_action( 'init', 'register_taxonomies_research', 0 );

//убираем slug bestsolution
function research_remove_slug( $post_link, $post, $leavename ) {
    if ( 'research' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }
    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    return $post_link;
}
add_filter( 'post_type_link', 'research_remove_slug', 10, 3 );

function research_parse_request( $query ) {
    if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }
    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'research', 'page' ) );
    }
}
add_action( 'pre_get_posts', 'research_parse_request' );
// }

 /*
 * "Хлебные крошки" для WordPress
 * автор: Dimox
 * версия: 2019.03.03
 * лицензия: MIT
*/
function dimox_breadcrumbs() {

	/* === ОПЦИИ === */
	$text['home']     = 'Home'; // текст ссылки "Главная"
	$text['category'] = '%s'; // текст для страницы рубрики
	$text['search']   = 'Search results for the query "%s"'; // текст для страницы с результатами поиска
	$text['tag']      = 'Posts tagged "%s"'; // текст для страницы тега
	$text['author']   = 'Authors articles %s'; // текст для страницы автора
	$text['404']      = 'Error 404'; // текст для страницы 404
	$text['page']     = 'Page %s'; // текст 'Страница N'
	$text['cpage']    = 'Comments page %s'; // текст 'Страница комментариев N'

	$wrap_before    = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'; // открывающий тег обертки
	$wrap_after     = '</div><!-- .breadcrumbs -->'; // закрывающий тег обертки
	$sep            = '<span class="breadcrumbs__separator"> > </span>'; // разделитель между "крошками"
	$before         = '<span class="breadcrumbs__current">'; // тег перед текущей "крошкой"
	$after          = '</span>'; // тег после текущей "крошки"

	$show_on_home   = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
	$show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
	$show_current   = 1; // 1 - показывать название текущей страницы, 0 - не показывать
	$show_last_sep  = 1; // 1 - показывать последний разделитель, когда название текущей страницы не отображается, 0 - не показывать
	/* === КОНЕЦ ОПЦИЙ === */

	global $post;
	$home_url       = home_url('/');
	$link           = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
	$link          .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
	$link          .= '<meta itemprop="position" content="%3$s" />';
	$link          .= '</span>';
	$parent_id      = ( $post ) ? $post->post_parent : '';
	$home_link      = sprintf( $link, $home_url, $text['home'], 1 );

	if ( is_home() || is_front_page() ) {

		if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;

	} else {

		$position = 0;

		echo $wrap_before;

		if ( $show_home_link ) {
			$position += 1;
			echo $home_link;
		}

		if ( is_category() ) {
			$parents = get_ancestors( get_query_var('cat'), 'category' );
			foreach ( array_reverse( $parents ) as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				$cat = get_query_var('cat');
				echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
				} elseif ( $show_last_sep ) echo $sep;
			}

		} elseif ( is_search() ) {
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				if ( $show_home_link ) echo $sep;
				echo sprintf( $link, $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['search'], get_search_query() ) . $after;
				} elseif ( $show_last_sep ) echo $sep;
			}

		} elseif ( is_year() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_time('Y') . $after;
			elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_month() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_day() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position ) . $sep;
			$position += 1;
			echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_single() && ! is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$position += 1;
				$post_type = get_post_type_object( get_post_type() );
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
				if ( $show_current ) echo $sep . $before . get_the_title() . $after;
				elseif ( $show_last_sep ) echo $sep;
			} else {
				$cat = get_the_category(); $catID = $cat[0]->cat_ID;
				$parents = get_ancestors( $catID, 'category' );
				$parents = array_reverse( $parents );
				$parents[] = $catID;
				foreach ( $parents as $cat ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				}
				if ( get_query_var( 'cpage' ) ) {
					$position += 1;
					echo $sep . sprintf( $link, get_permalink(), get_the_title(), $position );
					echo $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
				} else {
					if ( $show_current ) echo $sep . $before . get_the_title() . $after;
					elseif ( $show_last_sep ) echo $sep;
				}
			}

		} elseif ( is_post_type_archive() ) {
			$post_type = get_post_type_object( get_post_type() );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . $post_type->label . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_attachment() ) {
			$parent = get_post( $parent_id );
			$cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
			$parents = get_ancestors( $catID, 'category' );
			$parents = array_reverse( $parents );
			$parents[] = $catID;
			foreach ( $parents as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			$position += 1;
			echo $sep . sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_page() && ! $parent_id ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_title() . $after;
			elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_page() && $parent_id ) {
			$parents = get_post_ancestors( get_the_ID() );
			foreach ( array_reverse( $parents ) as $pageID ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
			}
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_tag() ) {
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				$tagID = get_query_var( 'tag_id' );
				echo $sep . sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_author() ) {
			$author = get_userdata( get_query_var( 'author' ) );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				echo $sep . sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_404() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . $text['404'] . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( has_post_format() && ! is_singular() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			echo get_post_format_string( get_post_format() );
		}

		echo $wrap_after;

	}
} // end of dimox_breadcrumbs()


## Удаляет "Рубрика: ", "Метка: " и т.д. из заголовка архива
add_filter( 'get_the_archive_title', function( $title ){
	return preg_replace('~^[^:]+: ~', '', $title );
});


// This function runs after your post is saved
// Эта функция запускается после сохранения записи
// function my_acf_save_post( $post_id ) {

//     $value1 = (double) get_field( 'user-friendliness', $post_id );
// 	$value2 = (double) get_field( 'affordability', $post_id );
// 	$value3 = (double) get_field( 'customer_support', $post_id );
// 	$value4 = (double) get_field( 'functionality', $post_id );


//     // Merged values with ; on the end
//     //$merge = implode(" ",$value1).' '.implode(" ",$value2);
// 	$merge = ($value1 + $value2 + $value3 + $value4) / 4;
// 	$merge = floor($merge * 10) / 10;
//     // Update field 3 with the new value which should be

//     update_field( 'raiting', $merge, $post_id );
// }
// add_action('acf/save_post', 'my_acf_save_post', 20);

//Новая роль пользователя
// $result = add_role( 'wpschool_custom_admin', __(
// 	'Moderator' ),
// 	array(
// 		'read_post'     => true,
// 		'edit_posts'    => true,
// 		'delete_posts'  => true,
// 		'publish_posts' => true,
// 		'edit_themes' => false, // редактирование тем
// 		'install_plugins' => false, // установка плагинов
// 		'update_plugin' => false, // обновление плагинов
// 		'update_core' => false // обновление ядра WordPress

// 	)
// 	);

//options page admin
if( function_exists('acf_add_options_page') ) {
	//acf_add_options_page('Theme settings');
    acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Homepage Settings',
		'menu_title'	=> 'Homepage',
		'parent_slug'	=> 'theme-general-settings',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Article Settings',
		'menu_title'	=> 'Article',
		'parent_slug'	=> 'theme-general-settings',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Reviews Settings',
		'menu_title'	=> 'Reviews',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Guides Settings',
		'menu_title'	=> 'Guides',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Topchecker Settings',
		'menu_title'	=> 'Topchecker',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Research Settings',
		'menu_title'	=> 'Research',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Cookies Settings',
		'menu_title'	=> 'Cookies',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Forms text',
		'menu_title'	=> 'Forms text',
		'parent_slug'	=> 'theme-general-settings',
	));
    if($_SERVER['HTTP_HOST'] != 'topwebaccessibilitychecker.com') {
        acf_add_options_sub_page(array(
            'page_title' 	=> 'Theme Best Solution Settings',
            'menu_title'	=> 'Best Solution',
            'parent_slug'	=> 'theme-general-settings',
        ));
    }

//    if($_SERVER['HTTP_HOST'] == 'topwebaccessibilitychecker.com') {
//        acf_add_options_sub_page(array(
//            'page_title' 	=> 'Theme Top Checker Settings',
//            'menu_title'	=> 'Top Checker',
//            'parent_slug'	=> 'edit.php?post_type=bestsolution',
//        ));
//        acf_add_options_sub_page(array(
//            'page_title' 	=> 'Theme Research Settings',
//            'menu_title'	=> 'Research',
//            'parent_slug'	=> 'edit.php?post_type=bestsolution',
//        ));
//    }
}




# Добавляет SVG в список разрешенных для загрузки файлов.
add_filter( 'upload_mimes', 'svg_upload_allow' );
function svg_upload_allow( $mimes ) {
	$mimes['svg']  = 'image/svg+xml';
	return $mimes;
}
add_filter( 'wp_check_filetype_and_ext', 'fix_svg_mime_type', 10, 5 );

# Исправление MIME типа для SVG файлов.
function fix_svg_mime_type( $data, $file, $filename, $mimes, $real_mime = '' ){

	// WP 5.1 +
	if( version_compare( $GLOBALS['wp_version'], '5.1.0', '>=' ) )
		$dosvg = in_array( $real_mime, [ 'image/svg', 'image/svg+xml' ] );
	else
		$dosvg = ( '.svg' === strtolower( substr($filename, -4) ) );

	// mime тип был обнулен, поправим его
	// а также проверим право пользователя
	if( $dosvg ){

		// разрешим
		if( current_user_can('manage_options') ){

			$data['ext']  = 'svg';
			$data['type'] = 'image/svg+xml';
		}
		// запретим
		else {
			$data['ext'] = $type_and_ext['type'] = false;
		}

	}

	return $data;
}

// ************* Remove default Posts type since no blog *************
// Remove side menu
add_action( 'admin_menu', 'remove_default_post_type' );

function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}

// Remove +New post in top Admin Menu Bar
add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );

function remove_default_post_type_menu_bar( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'new-post' );
}

// Remove Quick Draft Dashboard Widget
add_action( 'wp_dashboard_setup', 'remove_draft_widget', 999 );

function remove_draft_widget(){
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}

// End remove post type

//shortcode url
function shortcode_url( $atts ) {
	$params = shortcode_atts( array(
		'id' => '',
        'text' => 'link',
        'blank' => true,
	), $atts );

    $url = get_permalink($params['id']);
    if($params['blank'] === true) {
        $blank = "target='_blank'";
    }
    if($params['text']){
        $text = $params['text'];
    }else{
        $text = $url;
    }
	return "<a href='{$url}' {$blank}>{$text}</a>";
}
add_shortcode( 'url_shortcode', 'shortcode_url' );

//shortcode btn
function shortcode_btn( $atts ) {
	$params = shortcode_atts( array(
		'url' => '',
        'text' => 'link',
        'blank' => true,
	), $atts );

    $url = $params['url'];
    if($params['blank'] === true) {
        $blank = "target='_blank'";
    }
    if($params['text']){
        $text = $params['text'];
    }else{
        $text = $url;
    }
	return "<a href='{$url}' {$blank} class='btn-visit-site'>{$text}</a>";
}
add_shortcode( 'btn_shortcode', 'shortcode_btn' );



//shortcode url brand content
function shortcode_url_brand_content( $atts ) {
	$params = shortcode_atts( array(
		'url' => '',
        'blank' => true,
	), $atts );


    $url = $params['url'];
    if($params['blank'] === true) {
        $blank = "target='_blank'";
    }
    if($params['text']){
        $text = $params['text'];
    }else{
        $text = $url;
    }
	return "<a href='{$url}' class='shortcode_url_brand_content' {$blank} data-gtm-url=''>{$text}</a>";
}
add_shortcode( 'url_brand_content', 'shortcode_url_brand_content' );


//shortcode domen
function shortcode_domen( $atts ) {
	$params = shortcode_atts( array(
		'id' => 0
	), $atts );
    $id = $params['id'];
    $id--;
    $rows = get_field('domain_repiter', 'option');
    if( $rows ) {
        $first_row = $rows[$id];
        if($_SERVER['HTTP_HOST'] == 'bestwebsiteaccessibility.com') {
            $text_bestwebsiteaccessibility = $first_row['domain_text_for_bestwebsiteaccessibility'];
            $url_bestwebsiteaccessibility = $first_row['domain_url_bestwebsiteaccessibility'];
        }
        if($_SERVER['HTTP_HOST'] == 'bestwebaccessibility.com') {
            $text_bestwebsiteaccessibility = $first_row['domain_text_for_bestwebaccessibility'];
            $url_bestwebsiteaccessibility = $first_row['domain_url_for_bestwebaccessibility'];
        }
        if($url_bestwebsiteaccessibility){
            $output = '<a href="' . $url_bestwebsiteaccessibility . '" >' . $text_bestwebsiteaccessibility . '</a>';
        }else{
            $output = $text_bestwebsiteaccessibility;
        }
    }


return $output;
}
add_shortcode( 'domen_shortcode', 'shortcode_domen' );


// Удалить каноническую ссылку - SEO by Yoast
function at_remove_dup_canonical_link() {
return false;
}
add_filter( 'wpseo_canonical', 'at_remove_dup_canonical_link' );


function wpschool_remove_yoast_jsonld( $data ){
    $data = array();
    return $data;
}
add_filter( 'wpseo_json_ld_output', 'wpschool_remove_yoast_jsonld', 10, 1 );

add_action( 'after_setup_theme', 'prefix_remove_unnecessary_tags' );
function prefix_remove_unnecessary_tags(){

    // REMOVE WP EMOJI
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );


    // remove all tags from header


    // Удаляет ссылки RSS-лент записи и комментариев
    remove_action( 'wp_head', 'feed_links', 2 );
    // Удаляет ссылки RSS-лент категорий и архивов
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    // Удаляет RSD ссылку для удаленной публикации
    remove_action( 'wp_head', 'rsd_link' );
    // Удаляет ссылку Windows для Live Writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // Удаляет короткую ссылку
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0);
    // Удаляет информацию о версии WordPress
    remove_action( 'wp_head', 'wp_generator' );
    // Удаляет ссылки на предыдущую и следующую статьи
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
    remove_action( 'wp_head',      'rest_output_link_wp_head'              );
    remove_action( 'wp_head',      'wp_oembed_add_discovery_links'         );
    remove_action( 'template_redirect', 'rest_output_link_header', 11 );
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
    // language
    add_filter('multilingualpress.hreflang_type', '__return_false');
    add_filter('multilingualpress.render_hreflang', function() { return false; });

}
add_filter( 'wpseo_opengraph_url' , '__return_false' );


//add description menu
function add_description_to_menu($item_output, $item, $depth, $args) {

   if (strlen($item->description) > 0 ) {
      // append description after link
      $item_output .= sprintf('<span class="description">%s</span>', esc_html($item->description));

      // or.. insert description as last item inside the link ($item_output ends with "</a>{$args->after}")
      // $item_output = substr($item_output, 0, -strlen("</a>{$args->after}")) . sprintf('<span class="description">%s</span >', esc_html($item->description)) . "</a>{$args->after}";
   }
   return $item_output;
}
add_filter('walker_nav_menu_start_el', 'add_description_to_menu', 10, 4);



function filter_projects() {
    $services_value = json_decode(stripslashes($_POST['acfValue']));
    $sortingMetaKey = $_POST['sortingMetaKey'];
    $sortingMetaKeyOrder = $_POST['sortingMetaKeyOrder'];

  $services_array = [
      'relation' => 'AND',
  ];

    foreach($services_value as $service_value) {
        $key = explode(':', $service_value)[0];
        $value = explode(':', $service_value)[1];

        $services_value_arrey[$key][] = $value;
    }


  $i = 0;
  foreach($services_value_arrey as $key => $value) {
        array_push($services_array, ['relation' => 'OR']);

        foreach($value as $value_sub){ //прокрутиль по кол-ву value

                    array_push($services_array[$i], [
                            'key'       => $key,
                            'value'     => $value_sub,
                            'compare'   => 'LIKE',
                    ]);
      }
      $i++;
  }



    //echo '<pre>'; print_r($services_array); echo '</pre>';

    //$args['meta_query'] = $services_array;
    $args = [
      'post_type' => 'reviews',
      'post_status' => 'publish',
      'posts_per_page' => 5,
      'meta_query' => $services_array,
            'meta_key'       => 'raiting',
            'orderby'        => 'meta_value',
            'order'          => 'DESC'

    ];
    if(isset( $sortingMetaKey) && isset( $sortingMetaKey)){
        $args['meta_key'] = $sortingMetaKey;
        $args['orderby'] = 'meta_value';
        $args['order'] = $sortingMetaKeyOrder;
    }
  $ajaxposts = new WP_Query( $args );

    //echo '<pre>'; print_r($ajaxposts); echo '</pre>';
  if($ajaxposts->have_posts()) {
    while($ajaxposts->have_posts()) : $ajaxposts->the_post();
        get_template_part( 'template-parts/content', 'comparison' );
    endwhile;
  } else {
    echo '<div class="filter-no-match">We are sorry, but there is no match according to your preferences</div>';
  }

  exit;
}
add_action('wp_ajax_filter_projects', 'filter_projects');
add_action('wp_ajax_nopriv_filter_projects', 'filter_projects');

//compare
function compare_reviews() {
    $comparePostId = $_POST['allcomparePostId'];
    $args = [
      'post_type' => 'reviews',
      'post_status' => 'publish',
      'post__in'       => $comparePostId,
      'orderby'        => 'post__in',
    ];
    $ajaxposts = new WP_Query( $args );
    if($ajaxposts->have_posts()) {
        $postCount = 0;
        while($ajaxposts->have_posts()) : $ajaxposts->the_post();
//            get_template_part( 'template-parts/content', 'compare' );
            include(locate_template('template-parts/content-compare.php'));
        $postCount++;
        endwhile;
    }
    exit;
}
add_action('wp_ajax_compare_reviews', 'compare_reviews');
add_action('wp_ajax_nopriv_compare_reviews', 'compare_reviews');

function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        foreach($languages as $l){
            if(!$l['active']) echo '<a class="wpml-ls-item" href="'.$l['url'].'">';
            echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
            if(!$l['active']) echo '</a>';
        }
    }
}

// Delete input site to comments form
function true_remove_url_field( $fields ) {
	unset( $fields['url'] );
	return $fields;
}

add_filter( 'comment_form_default_fields', 'true_remove_url_field', 10, 1);

//remove url category
add_filter('request', 'rudr_change_term_request', 1, 1 );
function rudr_change_term_request($query){

  $tax_name = 'guides_category'; // specify you taxonomy name here, it can be also 'category' or 'post_tag'

  // Request for child terms differs, we should make an additional check
  if( $query['attachment'] ) :
    $include_children = true;
  $name = $query['attachment'];
  else:
    $include_children = false;
  $name = $query['name'];
  endif;


  $term = get_term_by('slug', $name, $tax_name); // get the current term to make sure it exists

  if (isset($name) && $term && !is_wp_error($term)): // check it here

  if( $include_children ) {
    unset($query['attachment']);
    $parent = $term->parent;
    while( $parent ) {
      $parent_term = get_term( $parent, $tax_name);
      $name = $parent_term->slug . '/' . $name;
      $parent = $parent_term->parent;
    }
  } else {
    unset($query['name']);
  }

  switch( $tax_name ):
  case 'category':{
        $query['category_name'] = $name; // for categories
        break;
      }
      case 'post_tag':{
        $query['tag'] = $name; // for post tags
        break;
      }
      default:{
        $query[$tax_name] = $name; // for another taxonomies
        break;
      }
      endswitch;

      endif;

      return $query;
    }

    add_filter( 'term_link', 'rudr_term_permalink', 10, 3 );

    function rudr_term_permalink( $url, $term, $taxonomy ){

  $taxonomy_name = 'guides_category'; // your taxonomy name here
  $taxonomy_slug = 'guides_category'; // the taxonomy slug can be different with the taxonomy name (like 'post_tag' and 'tag' )

  // exit the function if taxonomy slug is not in URL
  if ( strpos($url, $taxonomy_slug) === FALSE || $taxonomy != $taxonomy_name ) return $url;

  $url = str_replace('/' . $taxonomy_slug, '', $url);

  return $url;
}
