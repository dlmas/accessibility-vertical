<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<meta name="google-site-verification" content="-cMFc9sSQ8MyEFA0o7h7P_m619JVGcXjYUQhWpXWUk8" />
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-194318495-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-194318495-1');
</script>
<script>
(function (a, d, o, r, i, c, u, p, w, m) {
m = d.getElementsByTagName(o)[0], a[c] = a[c]
|| {}, a[c].trigger = a[c].trigger || function () {
(a[c].trigger.arg = a[c].trigger.arg || []).push(arguments)},
a[c].on = a[c].on || function () {(a[c].on.arg = a[c].on.arg || []).push(arguments)},
a[c].off = a[c].off || function () {(a[c].off.arg = a[c].off.arg || []).push(arguments)
}, w = d.createElement(o), w.id = i, w.src = r, w.async = 1, w.setAttribute(p, u),
m.parentNode.insertBefore(w, m), w = null}
)(window, document, "script", "https://68188890.adoric-om.com/adoric.js", "Adoric_Script", "adoric","7d45b1cc1315f7ce8e1ac301fdc3be55", "data-key");
</script>
</head>


<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<header>
		<div class="container">
			<a href="<?php echo home_url(); ?>" class="logo">
				<?php $image = get_field('header_logo', 'options'); echo wp_get_attachment_image( $image, array(160, 40) ); ?>
			</a>
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
            	<div class="menu-icon"></div>
            </button>
			<?php
            wp_nav_menu( [
				'theme_location' => 'menu-1',
				'container' => 'nav',
				'menu_class' => 'nav__row container',
			] );
            ?>
			<?php do_action('wpml_add_language_selector'); ?>
		</div>

	</header>
