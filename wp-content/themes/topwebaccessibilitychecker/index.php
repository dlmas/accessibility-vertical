<?php
get_header();
gettype(get_sites());
?>
<div class="header-block">
    <div class="container">
        <div class="header_home">
            <div class="header_home__block">
                <h1 class="header_home__title"><?php the_field('header_content_title', 'options') ?></h1>
                <div class="header_home__text">
                    <?php the_field('header_content', 'options') ?>
                </div>
            </div>
            <div class="header_home__form">
                <form id="station">
                    <input class="input-url" type="text" placeholder="<?php the_field('homepage_input_text', 'options'); ?>" required>
                    <input type="submit" value="<?php the_field('homepage_submit_text', 'options'); ?>">
                </form>
            </div>
        </div>
    </div>
</div>
<div class="popap-bg">
    <div class="container">
        <div class="advertiser-disclosure-text">
            <div class="advertiser-close"></div>
            <?php the_field('advertising_disclosure_text', 'options'); ?>
        </div>
    </div>
</div>



<div class="content content_home container">
    <main id="primary" class="site-main">

        <?php if ( function_exists('dynamic_sidebar') )
			//dynamic_sidebar('homepage-sidebar');
		?>
        <div class="component">
            <h2 class="component-title"><?php the_field('home_content_title_top', 'options'); ?></h2>
            <div class="component-content"><?php the_field('home_content_top', 'options'); ?></div>
            <div class="component-img">
                            <?php 
                                $image = get_field('home_content_image_top', 'options');
                                $size = array(1160, 696); // (thumbnail, medium, large, full or custom size)
                                if( $image ) {
                                    echo wp_get_attachment_image( $image, $size );
                                }
                            ?>
            </div>

        </div>
        <?php  if( have_rows('why_web_row', 'options') ): ?>
            <div class="why-web__row">
                <?php while( have_rows('why_web_row', 'options') ) : the_row(); ?>
                    <div class="why-web__item">
                        <div class="why-web__text">
                            <div class="why-web__title"><?php echo get_sub_field('why_web_title');?></div>
                            <div class="why-web__content"><?php the_sub_field('why_web_content');?></div>
                            <?php $link = get_sub_field('why_web_url');
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" class="why-web__btn"><?php echo esc_html( $link_title ); ?></a>
                        </div>
                        <div class="why-web__img">
                            <?php 
                                $image = get_sub_field('why_web_img');
                                $size = array(1160, 696); // (thumbnail, medium, large, full or custom size)
                                if( $image ) {
                                    echo wp_get_attachment_image( $image, $size );
                                }
                            ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>


    </main><!-- #main -->


</div>
<div class="section-free-platforms">
    <div class="container">
            <h2 class="section-free-platforms__title"><?php the_field('title_block_need_to_check', 'options'); ?></h2>
            <?php 
                $args = array(
                    'post_type' => 'reviews',
                    'posts_per_page' => 4,
                    'post__in' => [368,237,410,366],
                    'orderby'        => 'post__in'
                );
                
                $query = new WP_Query( $args );
                do_action( 'lineup_4_popular', 'lineup_4_popular', 'reviews', 4);
                if ( $query->have_posts() ) {
            ?>
            <h3 class="section-free-platforms_h3"><?php the_field('popular_checkers', 'options'); ?></h3>
            
            <div class="section-free-platforms__row">
                    <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                <div class="section-free-platforms__item">
                    <div class="section-free-platforms__img"><?php the_post_thumbnail(array(234, 60)); ?></div>
                    <div class="section-free-platforms__title-review"><?php the_title(); ?></div>
                    <div class="section-free-platforms__star"><span class="wpcr_averageStars" data-wpcravg="4.8"><span style="width: 140px;"></span></span></div>
                    <a href="<?php the_permalink(); ?>" class="section-free-platforms__linck"><?php the_field('full_review_btn', 'options'); ?></a>
                </div>
            <?php } 
            }
            wp_reset_postdata(); ?>
            </div>
            <?php 
                $args = array(
                    'post_type' => 'reviews',
                    'posts_per_page' => 4,
                    'post__in' => [320,407,402,324],
                    'orderby'        => 'post__in'
                );
                
                $query = new WP_Query( $args );
                do_action( 'lineup_4_topfree', 'lineup_4_topfree', 'reviews', 4);
                if ( $query->have_posts() ) {
            ?>
            <h3 class="section-free-platforms_h3"><?php the_field('top_free_checkers', 'options'); ?></h3>
            <div class="section-free-platforms__row">
            <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                <div class="section-free-platforms__item">
                    <div class="section-free-platforms__img"><?php the_post_thumbnail(array(234, 60)); ?></div>
                    <div class="section-free-platforms__title-review"><?php the_title(); ?></div>
                    <div class="section-free-platforms__star"><span class="wpcr_averageStars" data-wpcravg="4.8"><span style="width: 140px;"></span></span></div>
                    <a href="<?php the_permalink(); ?>" class="section-free-platforms__linck"><?php the_field('full_review_btn', 'options'); ?></a>
                </div>
                <?php } 
                }
            wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
    <?php $my_current_lang = apply_filters( 'wpml_current_language', NULL ); ?>
<script>
    jQuery(document).ready(function($) {
        var asdasd = '<?php echo $my_current_lang; ?>';

        $('form#station').submit(function(e){
            e.preventDefault(); // e = event
        });
        $('input[type="submit"]').click(function () {
            if($('.input-url').val() != ''){
                
                location.href = '/' + asdasd + '/comparison/?url=' + $('.input-url').val();
                // console.log( $('.input-url').val() );
            }
        });
    });
    
</script>
<?php get_footer(); ?>
<?php the_field('сode_end_body_homepage', 'options'); ?>

</body>
</html>
