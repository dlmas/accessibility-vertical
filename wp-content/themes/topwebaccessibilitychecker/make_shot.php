   <?php

  if(!empty($_POST['url'])){

    // адрес сайта
    $url = $_POST['url'];

    if(filter_var($url, FILTER_VALIDATE_URL)){
      // вызов Google PageSpeed Insights API
      $api_data = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$url&screenshot=true");

      // расшифровка json данных  
      $api_data = json_decode($api_data, true);

      // данные снимка
      $screenshot = $api_data['screenshot']['data'];
      $screenshot = str_replace(array('_','-'),array('/','+'),$screenshot);

      // отобразить изображение
      echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";
    } else {
      echo "Пожалуйста, введите правильный URL.";
    }
  }
  ?>