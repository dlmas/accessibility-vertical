<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

get_header();
?>
<div class="full-page-bg">
	<div class="container">
		<main  class="site-main">

			<?php if ( have_posts() ) : ?>

				
					

					<div class="breadcrumbs-disclosure">
						<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
						<!-- <div class="header_home__disclosure disclosure-all-page">Advertising Disclosure</div> -->
					</div>

					<!-- <div class="popap-bg">
						<div class="container">
							<div class="advertiser-disclosure-text">
							<div class="advertiser-close"></div>
								This site is a free online resource that strives to offer helpful content and comparison features to its visitors. Please be advised that the operator of this site accepts advertising compensation from companies that appear on the site, and such compensation impacts the location and order in which the companies (and/or their products) are presented, and in some cases may also impact the rating that is assigned to them. To the extent that ratings appear on this site, such rating is determined by our subjective opinion and based on a methodology that aggregates our analysis of brand market share and reputation, each brand's conversion rates, compensation paid to us and general consumer interest. Company listings on this page DO NOT imply endorsement. We do not feature all providers on the market. Except as expressly set forth in our Terms of Use, all representations and warranties regarding the information presented on this page are disclaimed. The information, including pricing, which appears on this site is subject to change at any time.
								
							</div>
						</div>
					</div> -->

					<h1 class="review-title"><?php the_title(); ?></h1>
					<div class="archive-descption">
						<?php the_content(); ?>
					</div>
					<ul class="arсhive-rewiews">
				<?php
                $args = array(
                    'post_type' => 'reviews'
                );
                $query = new WP_Query( $args );

                $i = 1;
				/* Start the Loop */
				while ( $query->have_posts() ) :
					$query->the_post();

					/*
					* Include the Post-Type-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Type name) and that will be used instead.
					*/ 
					
				    include (locate_template( 'template-parts/content-reviews.php' ));
					$i++;
				endwhile;

				//the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
            <?php // AJAX загрузка постов ?> 
			<?php if (  $query->max_num_pages > 1 ) : ?>
                <script>
                    var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                    var true_posts = '<?php echo serialize($query->query_vars); ?>';
                    var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                    var max_pages = '<?php echo $query->max_num_pages; ?>';
                </script>
                <div id="true_loadmore"><?php the_field('show_more_options', 'options'); ?></div>
            <?php endif; ?>
			</ul>
		</main><!-- #main -->
	</div>
</div>
<script>
jQuery(function($){
	$('#true_loadmore').click(function(){
		$(this).text('loading...'); // изменяем текст кнопки, вы также можете добавить прелоадер
		var data = {
			'action': 'loadmore',
			'query': true_posts,
			'page' : current_page,
            'template_part' : 'reviews',
		};
		$.ajax({
			url:ajaxurl, // обработчик
			data:data, // данные
			type:'POST', // тип запроса
			success:function(data){
				if( data ) { 
					$('#true_loadmore').text('Load more').before(data); // вставляем новые посты
					current_page++; // увеличиваем номер страницы на единицу
					if (current_page == max_pages) $("#true_loadmore").remove(); // если последняя страница, удаляем кнопку
				} else {
					$('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
				}
			}
		});
	});
});
</script>
<?php get_footer(); ?>
<?php 
if( domain_user() == 'bestwebaccessibility.com' ) { 
    the_field('сode_end_body_reviewpage_bestwebaccessibility', 'options'); 
}else{
    the_field('сode_end_body_reviewpage', 'options'); 
}
?>
</body>
</html>
