<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

get_header();
?>
<div class="full-page-bg">
    <div class="container">
        <div class="breadcrumbs-disclosure">
            <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
            <div class="header_home__disclosure disclosure-all-page"><?php the_field('advertising_disclosure', 'options'); ?></div>
         </div>   
            <div class="popap-bg">
                <div class="container">
                    <div class="advertiser-disclosure-text">
                        <div class="advertiser-close"></div>
                        <?php the_field('advertising_disclosure_text', 'options'); ?>
                    </div>
                </div>
            </div>
        

        <h1 class="archive-title"><?php the_field('title_best_solution', 'option'); ?></h1>
        <div class="archive-descption">
            <?php echo get_field('text_best_solution', 'option'); ?>
        </div>
    </div>

<div style="clear:both;"></div>
    <div class="container  single-page-bg">
    <!--
    <div class="single-page-bg__cat">
            
                <?php $taxonomy= get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); //массив текущий таксаномии ?>
                <?php  $tags = get_terms('article_category', [
	'hide_empty' => false, //Скрывать ли термины в которых нет записей. 1(true) - скрывать пустые, 0(false) - показывать пустые.
]);

			$html = '<div class="arch_tags"><a href="/guides/" data-tag_slug="news" data-taxonomy_slug="123">All categories</a> ';
			foreach ( $tags as $tag ) {
				$tag_link = get_tag_link( $tag->term_id );		
				$html .= " <a class='tags-links' href='{$tag_link}' title='{$tag->name} рубрика' data-tag_slug='{$tag->slug}' data-taxonomy_slug='$taxonomy->slug'>"; 
				$html .= "{$tag->name}</a>";
			}
			$html .= '</div>';
			echo $html; ?>
            
    </div>
           -->
            <main id="article" class="site-main reviews article">
            <div class="bestsolution-cat__row">

                <!-- ---------------- Список всех постов ---------------- -->
                <?php 
                
                
                $taxonomy = 'bestsolution_category';
                $taxonomies = get_terms( array(
                    'taxonomy' => $taxonomy,
                    'orderby' => 'term_id',
                    'order' => 'ASC',
                    //'hide_empty' => false
                ) );

                if ( !empty($taxonomies) ) :
                    foreach( $taxonomies as $category ) { ?>
                <!--Цикл-->
                        <?php 
                            $query = new WP_Query( array(
                                'post_type' => 'bestsolution',
//                                'posts_per_page' => -1,
                                'nopaging' => 'true',
                                'post_status'=> 'publish', 
                                'order'=>'DESC',
                                'bestsolution_category' => $category->slug,

	                           ) ); 
                        ?>


                        <?php if( $query->have_posts() ) : ?>
                        
                <section class="bestsolution-cat__item">
                    <h2 class="bestsolution-cat__title"><?php echo $category->name; ?></h2>
                    <div class="bestsolution-cat__desc"><?php echo $category->description; ?></div>
                    <ul class="bestsolution-cat__all-bloks">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <li><a href="<?php the_permalink(); ?>" class="leisure__blok_title"><?php the_title() ?></a></li>
                        <?php endwhile; ?>
                    </ul>
                    
                </section>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>

                <!--end Цикл-->
                <?php
                    }
                endif; ?>

            </div> <!-- left content -->
            
            </main><!-- #main -->
            
            <div id="aside1" class="sidebar-full-page" >
               <div class="sidebar_block">
                    <div class="sidebar__inner">
                        <?php get_template_part( 'template-parts/witget', 'reviews-top5' ); ?>
                        <?php get_template_part( 'template-parts/witget', 'article-top5' ); ?>
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
<div style="clear:both;"></div>
    </div>
</div>
    <?php
get_footer(); ?>
<?php 
if( domain_user() == 'bestwebaccessibility.com' ) {
    the_field('сode_snippets_to_the_end_of_the_body_best_solution_page_bestwebaccessibility', 'options'); 
}else{
    the_field('сode_snippets_to_the_end_of_the_body_bestsolutionpage', 'options');  
}
?>
    </body>

    </html>
