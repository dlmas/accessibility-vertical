<?php
get_header(); ?>

<div class="comparison-header">
    <div class="container">
        <div class="breadcrumbs-disclosure">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                    }
                ?>
        </div>
        <h1 class="archive-title"><?php the_field('Comparison_title_page'); ?></h1>
        <div class="comparison-header__form">
            <form id="station">
                <input class="input-name" required type="text" placeholder="<?php the_field('comparison_name_text', 'options'); ?>">
                <input class="input-email" required type="email" placeholder="<?php the_field('comparison_email_text', 'options'); ?>">
                <input type="submit" value="<?php the_field('comparison_submit_text', 'options'); ?>">
            </form>
        </div>
    </div>
</div>
<div class="full-page-bg">
    <div class="container">
                    <h2 class="comparison-h2"><?php the_field('top_web_accessibility_checkers_-_blocks'); ?></h2>
                    <div class="comparison-cont"><?php the_field('top_web_accessibility_checkers_-_blocks_content'); ?></div>
                    <div class="comparison-page-row">
                        <a href="/reviews/" target="_blank" class="comparison-page-item">
                            <div class="comparison-page-item__img"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/page2CategotyIconA.png" alt=""></div>
                            <div class="comparison-page-item__title"><?php the_field('top_web_accessibility_checkers_-_btn_one_title'); ?></div>
                        </a>
                        <a href="/best-free-checker-reviews/" target="_blank" class="comparison-page-item">
                            <div class="comparison-page-item__img"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/page3CategotyIconA.png" alt=""></div>
                            <div class="comparison-page-item__title"><?php the_field('top_web_accessibility_checkers_-_btn_two_title'); ?></div>
                        </a>
                        <a href="/guides-category/checklist/" target="_blank" class="comparison-page-item">
                            <div class="comparison-page-item__img"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/page4CategotyIconA.png" alt=""></div>
                            <div class="comparison-page-item__title"><?php the_field('top_web_accessibility_checkers_-_btn_three_title'); ?></div>
                        </a>
                    </div>
                    <h2 class="comparison-h2"><?php the_title(); ?></h2>
                    <div class="comparison-reviews">
                        <div class="comparison-fieds__titles-row">
                            <div class="comparison-fieds__titles-item">
                                <div class="comparison-fieds__item-title"><?php the_field('comparison_stars'); ?></div>
                                <div class="comparison-fieds__item-comt"></div>
                            </div>
                            <div class="comparison-fieds__titles-item">
                                <div class="comparison-fieds__item-title"><?php the_field('comparison_rating'); ?></div>
                                <div class="comparison-fieds__item-comt"></div>
                            </div>
                            <div class="comparison-fieds__titles-item">
                                <div class="comparison-fieds__item-title"><?php the_field('comparison_advanced_tools'); ?></div>
                                <div class="comparison-fieds__item-comt"></div>
                            </div>
                            <div class="comparison-fieds__titles-item">
                                <div class="comparison-fieds__item-title"><?php the_field('comparison_free_scan'); ?></div>
                                <div class="comparison-fieds__item-comt"></div>
                            </div>
                            <div class="comparison-fieds__titles-item">
                                <div class="comparison-fieds__item-title"><?php the_field('Comparison_wcag_compliance_covered'); ?></div>
                                <div class="comparison-fieds__item-comt"></div>
                            </div>
                            <div class="comparison-fieds__titles-item">
                                <div class="comparison-fieds__item-title"><?php the_field('comparison_multiple_page_scan'); ?></div>
                                <div class="comparison-fieds__item-comt"></div>
                            </div>
                            <div class="comparison-fieds__titles-item">
                                <div class="comparison-fieds__item-title"><?php the_field('comparison_automated_technology'); ?></div>
                                <div class="comparison-fieds__item-comt"></div>
                            </div>
                        </div>
                        <div class="comparison-reviews__row">
                        <?php
                        $args = array(
                            'post_type' => 'reviews',
                            'posts_per_page' => 4,
                            'post__in' => [237,322,395,381],
                            'orderby'        => 'post__in'
                        );

                            $query = new WP_Query( $args );
                            do_action( 'lineup_4_comparison', 'lineup_4_comparison', 'reviews', 4);
                            while ( $query->have_posts() ) { $query->the_post(); ?>
                            <div class="comparison-reviews__item">
                                <div class="comparison-reviews__img"><?php the_post_thumbnail(); ?></div>
                                <!-- <div class="comparison-reviews__title"><?php the_title(); ?></div> -->
                                <div class="comparison-reviews__check-row">
                                    <div class="comparison-reviews__check-item">
                                        <span class="wpcr_averageStars" data-wpcravg="<?php the_field('stars'); ?>"><span style=""></span></span>
                                    </div>
                                    <div class="comparison-reviews__check-item">
                                        <div class="comparison-rating-num"><?php the_field('raiting'); ?></div>
                                    </div>
                                    <div class="comparison-reviews__check-item">
                                        <?php if(get_field('advanced_tools')){ ?>
                                            <div class="check__icon"></div>
                                        <?php }else{
                                            echo '<div class="check__icon check__icon_none"></div>';
                                        } ?>
                                    </div>
                                    <div class="comparison-reviews__check-item">
                                    <?php if(get_field('free_scan')){ ?>
                                            <div class="check__icon"></div>
                                        <?php }else{
                                            echo '<div class="check__icon check__icon_none"></div>';
                                        } ?>
                                    </div>
                                    <div class="comparison-reviews__check-item">
                                    <?php if(get_field('wcag_compliance_covered')){ ?>
                                            <div class="check__icon"></div>
                                        <?php }else{
                                            echo '<div class="check__icon check__icon_none"></div>';
                                        } ?>
                                    </div>
                                    <div class="comparison-reviews__check-item">
                                    <?php if(get_field('multiple_page_scan')){ ?>
                                            <div class="check__icon"></div>
                                        <?php }else{
                                            echo '<div class="check__icon check__icon_none"></div>';
                                        } ?>
                                    </div>
                                    <div class="comparison-reviews__check-item">
                                    <?php if(get_field('automated_technology')){ ?>
                                            <div class="check__icon"></div>
                                        <?php }else{
                                            echo '<div class="check__icon check__icon_none"></div>';
                                        } ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" class="comparison-reviews__btn"><?php the_field('read_review', 'options'); ?></a>
                                </div>
                            </div>
                            <?php }
                            wp_reset_postdata(); ?>

                        </div>
                    </div>
                <div class="comparison-section">
                    <h2 class="comparison-h2"><?php the_field('guides_reviews_and_resources'); ?></h2>
                    <div class="comparison-grr__row">
                        <div class="comparison-grr__item">
                            <div class="comparison-grr__img"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/shutterstock_67042552.jpg" alt=""></div>
                            <div class="comparison-grr__block-text">
                                <div class="comparison-grr__block-title"><?php the_field('popular_checker_review'); ?></div>
                                <div class="comparison-grr__block-row">
                                <?php
                                // $args = array(
                                //     'post_type' => 'topchecker',
                                //     'posts_per_page' => 4,
                                //     'post__in' => [580,512,581,598],
                                //     'orderby'        => 'post__in'
                                // );

                                // $query = new WP_Query( $args );
                                // while ( $query->have_posts() ) { $query->the_post();
                                ?>
                                    <a href="/topchecker/automated-accessibility-testing-tools-axe-vs-a11y/" class="comparison-grr__block-item"><?php the_field('axe_vs_a11y'); ?></a>
                                    <a href="/topchecker/compare-siteimprove-vs-axe-web-accessibility-checkers/" class="comparison-grr__block-item"><?php the_field('siteimprove_vs_axe'); ?></a>
                                    <a href="/topchecker/monsido-vs-ckeditor/" class="comparison-grr__block-item"><?php the_field('monsido_vs_ckeditor'); ?></a>
                                    <a href="/topchecker/comparison-siteimprove-webaim-accessibility/" class="comparison-grr__block-item"><?php the_field('siteimprove_vs_webaim'); ?></a>
                                <?php //}
                                //wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="comparison-grr__item">
                            <div class="comparison-grr__img"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/shutterstock_701900725.jpg" alt=""></div>
                            <div class="comparison-grr__block-text">
                                <div class="comparison-grr__block-title"><?php the_field('best_free_scans'); ?></div>
                                <div class="comparison-grr__block-row">
                                <?php
                                $args = array(
                                    'post_type' => 'reviews',
                                    'posts_per_page' => 4,
                                    'post__in' => [320,407,402,324],
                                    'orderby'        => 'post__in'
                                );

                                $query = new WP_Query( $args );
                                while ( $query->have_posts() ) { $query->the_post(); ?>
                                    <a href="<?php the_permalink(); ?>" class="comparison-grr__block-item"><?php the_title(); ?></a>
                                <?php }
                                wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="comparison-grr__item">
                            <div class="comparison-grr__img"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/shutterstock_345481694.jpg" alt=""></div>
                            <div class="comparison-grr__block-text">
                                <div class="comparison-grr__block-title"><?php the_field('best_accessibility_guides'); ?></div>
                                <div class="comparison-grr__block-row">
                                <?php
                                $args = array(
                                    'post_type' => 'reviews',
                                    'posts_per_page' => 4,
                                    'orderby' => 'date'
                                );

                                $query = new WP_Query( $args );
                                while ( $query->have_posts() ) { $query->the_post(); ?>
                                    <a href="<?php the_permalink(); ?>" class="comparison-grr__block-item"><?php the_title(); ?></a>
                                <?php }
                                wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="comparison-grr__item">
                            <div class="comparison-grr__img"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/shutterstock_557355178.jpg" alt=""></div>
                            <div class="comparison-grr__block-text">
                                <div class="comparison-grr__block-title"><?php the_field('top_checklists'); ?></div>
                                <div class="comparison-grr__block-row">
                                    <?php
                                    $args = array(
                                        'post_type' => 'guides',
                                        'posts_per_page' => 4,
                                        'guides_category' => 'checklist'                                   );

                                    $query = new WP_Query( $args );
                                    while ( $query->have_posts() ) { $query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="comparison-grr__block-item"><?php the_title(); ?></a>
                                    <?php }
                                    wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>      </div>
</div>



<?php $my_current_lang = apply_filters( 'wpml_current_language', NULL ); ?>

<script>
    jQuery(document).ready(function($) {
        var asdasd = '/<?php echo $my_current_lang; ?>';
        if(asdasd == '/en'){
            asdasd = '';
        }
        $('form#station').submit(function(e){
            e.preventDefault(); // e = event
        });

        var mail = $('.input-email');
        var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
        $('input[type="submit"]').click(function () {
            // location.href = $(location).attr('href') + '?name=' + $('.input-name').val() + '?email=' + $('.input-email').val();
            if( $('.input-name').val() != '' || $('.input-email').val() != '' ){
                if(mail.val().search(pattern) == 0){
                    location.href = asdasd + '/reviews/';
                }
            }
        });
    });

</script>
<?php get_footer(); ?>
</body>

</html>
