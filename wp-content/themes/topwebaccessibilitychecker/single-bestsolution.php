<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sobix
 */

get_header();
while ( have_posts() ) :
	the_post();
?>
<div class="full-page-bg">
    <div class="container">
        <div class="breadcrumbs-disclosure">
            <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
            <div class="header_home__disclosure disclosure-all-page"><?php the_field('advertising_disclosure', 'options'); ?></div>
        </div>

        <div class="popap-bg">
            <div class="container">
                <div class="advertiser-disclosure-text">
                    <div class="advertiser-close"></div>
                    <?php the_field('advertising_disclosure_text', 'options'); ?>
                </div>
            </div>
        </div>
    </div>
<div style="clear:both;"></div>
    <div class="container single-page-bg">

        <main id="article" class="site-main reviews article">
            <div class="full-page__top-title">
                <?php the_title( '<h1 class="full-page__title">', '</h1>' ); ?>
            </div>
            <div class="full-page__autor-info">
                <?php if( $article_autor_name = get_field('article_autor_name') ) :?>
                <div class="full-page__autor">By <?php echo $article_autor_name; ?></div>
                <span>•</span>
                <?php endif; ?>
                <div class="full-page__date-post"><?php the_date('M d, Y'); ?></div>
            </div>
            <div class="full-page-content-item">
                <div class="full-page-content-item__img"><?php the_post_thumbnail(); ?></div>


                <?php the_content(); ?>
            </div>
            <div class="about-author">
                <div class="about-author_row">
                    <div class="about-author__title">About The Author</div>
                    <div class="about-author__line"></div>
                </div>
                <div class="about-author__ligo-row">
                    <div class="about-author__ligo"><img src="/wp-content/themes/sobix2/assets/img/group4.png" alt=""></div>
                    <div class="about-author__text-row">
                        <div class="about-author__text-title">Best Website Accessibility Editorial Team</div>
                        <div class="about-author__text-content">Our experienced editorial staff is comprised of experts and specialists in digital technology services. With our comprehensive research, we specialize in writing in-depth articles and reviews to simplify the process of choosing the right website accessibility solution for your business or private website.</div>
                    </div>
                </div>
            </div>
        </main>
        <!-- #main -->

        <div id="aside1" class="sidebar-full-page">
            <div class="sidebar_block">
                <div class="sidebar__inner">
                    <?php get_template_part( 'template-parts/witget', 'reviews-top5' ); ?>
                    <?php get_template_part( 'template-parts/witget', 'article-top5' ); ?>
                    <?php //get_sidebar(); ?>

                </div>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>

<?php
endwhile; // End of the loop.
get_footer(); ?>
<?php 
if( domain_user() == 'bestwebaccessibility.com' ) { 
    the_field('сode_end_body_bestwebaccessibility', 'options'); 
}else{
    the_field('сode_end_body', 'options'); 
}
?>
</body>

</html>
