<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sobix
 */

get_header();
while ( have_posts() ) :
	the_post();
?>
<main class="full-page-bg">
    <div class="container">
        <div class="breadcrumbs-disclosure">
						<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
						<!-- <div class="header_home__disclosure disclosure-all-page">Advertising Disclosure</div> -->
        </div>

        <div class="popap-bg">
            <div class="container">
                <div class="advertiser-disclosure-text">
                    <div class="advertiser-close"></div>
                    <?php the_field('advertising_disclosure_text', 'options'); ?>
                </div>
            </div>
        </div>
    </div>
<div style="clear:both;"></div>
    <div class="container single-page-bg">

        <main id="article" class="site-main reviews article">
            <div class="full-page__top-title">
                <?php the_title( '<h1 class="full-page__title">', '</h1>' ); ?>
            </div>
            <div class="full-page__autor-info">
                <?php if( $article_autor_name = get_field('article_autor_name') ) :?>
                <div class="full-page__autor">By <?php echo $article_autor_name; ?></div>
                <span>•</span>
                <?php endif; ?>
                <div class="full-page__date-post"><?php the_date('M d, Y'); ?></div>
            </div>
            <div class="full-page-content-item">
                <div class="full-page-content-item__img"><?php the_post_thumbnail(); ?></div>


                <?php the_content(); ?>
            </div>
            <div class="about-author">
                <div class="about-author_row">
                    <div class="about-author__title"><?php the_field('about_the_author', 'option'); ?></div>
                    <div class="about-author__line"></div>
                </div>
                <div class="about-author__ligo-row">
                <div class="about-author__ligo"><img src="/wp-content/themes/topwebaccessibilitychecker/assets/img/pagoGuides.png" alt=""></div>
                    <div class="about-author__text-row">
                    <div class="about-author__text-title"><?php the_field('author_titile', 'option'); ?></div>
                        <div class="about-author__text-content"><?php the_field('author_content', 'option'); ?></div>
                    </div>
                </div>
            </div>
        </main>
        <!-- #main -->

        <div id="aside1" class="sidebar-full-page">
            <div class="sidebar_block">
                <div class="sidebar__inner">
                    <?php get_template_part( 'template-parts/witget', 'reviews-top5' ); ?>
                    <?php get_template_part( 'template-parts/witget', 'article-top5' ); ?>
                    <?php //get_sidebar(); ?>

                </div>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
</main>
<br><br><br>
<div class="section-free-platforms">
    <div class="container">
            <h2 class="section-free-platforms__title">Need to check your web accessibility? Check these free and paid checker platforms. </h2>
            <?php 
                $args = array(
                    'post_type' => 'reviews',
                    'posts_per_page' => 4,
                    'post__in' => [368,237,410,366],
                    'orderby'        => 'post__in'
                );
                
                $query = new WP_Query( $args );

                if ( $query->have_posts() ) {
            ?>
            <h3 class="section-free-platforms_h3">Top checker web accessibility</h3>
            
            <div class="section-free-platforms__row">
                    <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                <div class="section-free-platforms__item">
                    <div class="section-free-platforms__img"><?php the_post_thumbnail(array(234, 60)); ?></div>
                    <div class="section-free-platforms__title-review"><?php the_title(); ?></div>
                    <div class="section-free-platforms__star"><span class="wpcr_averageStars" data-wpcravg="4.8"><span style="width: 140px;"></span></span></div>
                    <a href="<?php the_permalink(); ?>" class="section-free-platforms__linck">full review</a>
                </div>
            <?php } 
            }
            wp_reset_postdata(); ?>
            </div>
            <?php 
                $args = array(
                    'post_type' => 'reviews',
                    'posts_per_page' => 4,
                    'post__in' => [320,407,402,324],
                    'orderby'        => 'post__in'
                );
                
                $query = new WP_Query( $args );

                if ( $query->have_posts() ) {
            ?>
            <h3 class="section-free-platforms_h3">Top free checker web accessibility</h3>
            <div class="section-free-platforms__row">
            <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                <div class="section-free-platforms__item">
                    <div class="section-free-platforms__img"><?php the_post_thumbnail(array(234, 60)); ?></div>
                    <div class="section-free-platforms__title-review"><?php the_title(); ?></div>
                    <div class="section-free-platforms__star"><span class="wpcr_averageStars" data-wpcravg="4.8"><span style="width: 140px;"></span></span></div>
                    <a href="<?php the_permalink(); ?>" class="section-free-platforms__linck">full review</a>
                </div>
                <?php } 
                }
            wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php
endwhile; // End of the loop.
get_footer(); ?>
<?php 
if( domain_user() == 'bestwebaccessibility.com' ) { 
    the_field('сode_end_body_bestwebaccessibility', 'options'); 
}else{
    the_field('сode_end_body', 'options'); 
}
?>
</body>

</html>
