<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

get_header();
?>
<div class="full-page-bg">
    <div class="container">
    <div class="breadcrumbs-disclosure">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                    }
                ?>
        </div>
        

        <h1 class="archive-title"><?php single_term_title(); ?></h1>
        <div class="archive-descption">
            <?php echo term_description(); ?>
        </div>
    </div>

<div style="clear:both;"></div>
    <div class="container  single-page-bg">
    <!--
    <div class="single-page-bg__cat">
            
                <?php $taxonomy= get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); //массив текущий таксаномии ?>
                <?php  $tags = get_terms('article_category', [
	'hide_empty' => false, //Скрывать ли термины в которых нет записей. 1(true) - скрывать пустые, 0(false) - показывать пустые.
]);

			$html = '<div class="arch_tags"><a href="/guides/" data-tag_slug="news" data-taxonomy_slug="123">All categories</a> ';
			foreach ( $tags as $tag ) {
				$tag_link = get_tag_link( $tag->term_id );		
				$html .= " <a class='tags-links' href='{$tag_link}' title='{$tag->name} рубрика' data-tag_slug='{$tag->slug}' data-taxonomy_slug='$taxonomy->slug'>"; 
				$html .= "{$tag->name}</a>";
			}
			$html .= '</div>';
			echo $html; ?>
            
    </div>
           -->
            <main id="article" class="site-main reviews article main_column">
                <?php 
                
                if ( have_posts() ) : ?>
                <?php
                            /* Start the Loop */
                            while ( have_posts() ) :
                                the_post();
            
                                /*
                                * Include the Post-Type-specific template for the content.
                                * If you want to override this in a child theme, then include a file
                                * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                                */
                                get_template_part( 'template-parts/content', 'article' );
            
                            endwhile;
            
                            //the_posts_navigation();
            
                        else :
            
                            get_template_part( 'template-parts/content', 'none' );
            
                        endif;
                        ?>
            
            </main><!-- #main -->
            
            <div id="aside1" class="sidebar-full-page" >
               <div class="sidebar_block">
                    <div class="sidebar__inner">
                        <?php get_template_part( 'template-parts/witget', 'reviews-top5' ); ?>
                        <?php get_template_part( 'template-parts/witget', 'article-top5' ); ?>
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
<div style="clear:both;"></div>
    </div>
</div>
    <?php
get_footer(); ?>
<?php 
if( domain_user() == 'bestwebaccessibility.com' ) {
    the_field('сode_end_body_articlepage_bestwebaccessibility', 'options'); 
}else{
    the_field('сode_end_body_articlepage', 'options');  
}
?>
<script>

    jQuery(".main_column").stick_in_parent({
        offset_top: 10
    });
</script>
    </body>

    </html>
