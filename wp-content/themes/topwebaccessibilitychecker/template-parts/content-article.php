<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

?>

<article id="post-<?php the_ID(); ?>" class="article-content__item <?php if(!get_the_post_thumbnail()) echo 'article-content__item_no-img' ?>">
	<div class="article-content__img">
        <?php if(get_the_post_thumbnail()):?>
	        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
	    <?php endif; ?>

	</div>
	<div class="article-content__text">
		<a href="<?php the_permalink(); ?>" class="article-content__title"><?php the_title( '<h2 class="article-content__title">', '</h2>' ); ?></a>
		<span><?php echo get_the_excerpt(); ?></span>
		<div class="article-content__content"><?php $content = get_field('short_content_article'); echo mb_strimwidth($content, 0, 280, '...'); ?></div>
		<div class="article-content__date-btn">
			<a href="<?php the_permalink(); ?>" class="article-content__link"><?php the_field('bottom_read_more', 'options'); ?> ></a>
			<div class="article-content__date"><?php the_date(); ?></div>
		</div>

	</div>
</article><!-- #post-<?php the_ID(); ?> -->
