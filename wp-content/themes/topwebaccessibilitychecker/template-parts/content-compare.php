<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

?>


<div class="project-tiles__item">
    <div class="project-tiles__info">
        <div class="project-tiles__logo"><?php the_post_thumbnail('medium'); ?></div>

        <div class="project-tiles__rating">
            <div class="project-tiles__rating-number"><?php the_field('raiting'); ?></div>
            <div class="project-tiles__rating-star"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('stars'); ?>"><span style=""></span></span></div>
        </div>

        <div class="project-tiles__url">
            <a href="<?php the_field('visit_site_url');?>" target="_blank" class="project-tiles__url-link">Visit Site</a>
            <a href="<?php the_permalink(); ?>" class="project-tiles__url-read-more"><span><?php the_field('read_review', 'options'); ?> »</span></a>
        </div>
    </div>
    <div class="compare-block">
     <!-- <div class="check__item check__item_title">Rating</div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('functionality'); ?>"><span style=""></span></span></div></div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('user-friendliness'); ?>"><span style=""></span></span></div></div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('customer_support'); ?>"><span style=""></span></span></div></div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('affordability'); ?>"><span style=""></span></span></div></div> -->
       
       
       
       
       
       
        <?php $acf_fields = acf_get_fields_by_id( 897 ); 
              $filter_all_fileds = [];
                            for( $i = 0; $i < count($acf_fields); $i++ ){
                                $checked_name = $acf_fields[$i]['name']; //slag поля
                                
                                if($postCount == 0){
                                    echo '<div id="compare-name" class="compare-name">'.$acf_fields[$i]['label'].': </div>';
                                }else{
                                    echo'<div id="compare-name" class="compare-name"></div>';
                                }
                                
                                echo '<div class="compare-name__filter">';
                                $filter_all_fileds[] = $checked_name; //получаем список всех полей для фильтра
                                foreach($filter_all_fileds as $filter_all_fileds_key){
                                    $hero[$filter_all_fileds_key] = get_field($filter_all_fileds_key);
                                }
                                
                                $array = $acf_fields[$i]['choices'];
                                $len = count($array);
                                foreach($array as $key => $value) {
                                    
                                    if(is_array($hero[$checked_name])) {
                                        if($hero[$checked_name] && in_array($key, $hero[$checked_name])){
                                              echo '<span class="">'.$key.'</span> ';
                                        }
                                    }else{
                                        if($hero[$checked_name] == $key){
                                                  echo '<span class="">'.$key.'</span>';
                                        }
                                    }
                                }
                                echo '</div>';
                        }
//        echo '<pre>'; print_r($hero); echo '</pre>';
        ?>


    </div>
</div>