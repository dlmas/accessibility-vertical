<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sobix
 */

?>


<div class="project-tiles__item">
    <div class="project-tiles__info">
        
            <a href="<?php the_field('visit_site_url');?>" target="_blank" class="project-tiles__logo">
                <?php the_post_thumbnail('medium'); ?>
            </a>
        
        <div class="project-tiles__rating">
            <div class="project-tiles__rating-number"><?php the_field('raiting'); ?></div>
            <div class="project-tiles__rating-star"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('stars'); ?>"><span style=""></span></span></div>
        </div>
        <div class="project-tiles__url">
            <a href="<?php the_field('visit_site_url');?>" target="_blank" class="project-tiles__url-link"><?php the_field('bottom_visit_site', 'options'); ?></a>
            <a href="<?php the_permalink(); ?>" class="project-tiles__url-read-more"><span><?php the_field('read_review', 'options'); ?> »</span></a>
        </div>
    </div>
    <div class="check__row">
     <div class="check__item check__item_title"></div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('functionality'); ?>"><span style=""></span></span></div></div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('user-friendliness'); ?>"><span style=""></span></span></div></div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('customer_support'); ?>"><span style=""></span></span></div></div>
      <div class="check__item"><div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('affordability'); ?>"><span style=""></span></span></div></div>
       
       
       
       
       
       
        <?php $acf_fields = acf_get_fields( 897 ); //acf_get_fields_by_id( 897 )
              $filter_all_fileds = [];
                            for( $i = 0; $i < count($acf_fields); $i++ ){
                                echo '<div class="check__item check__item_title"></div>';
                                $checked_name = $acf_fields[$i]['name']; //slag поля
                                
                                $filter_all_fileds[] = $checked_name; //получаем список всех полей для фильтра
                                foreach($filter_all_fileds as $filter_all_fileds_key){
                                    $hero[$filter_all_fileds_key] = get_field($filter_all_fileds_key);
                                }
                                
                                foreach($acf_fields[$i]['choices'] as $key) {
                                    

                                    if(is_array($hero[$checked_name])) {
                                        if($hero[$checked_name] && in_array($key, $hero[$checked_name])){
                                              echo '<div class="check__item"><div class="check__icon"></div></div>';
                                        }else{
                                              echo '<div class="check__item"></div>';
                                        }
                                    }else{
                                        if($hero[$checked_name] == $key){
                                              echo '<div class="check__item"><div class="check__icon"></div></div>';
                                        }else{
                                              echo '<div class="check__item"></div>';
                                        }
                                    }
                                }
                        }
//        echo '<pre>'; print_r($hero); echo '</pre>';
        ?>


    </div>
</div>
