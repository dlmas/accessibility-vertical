<div class="witget-all__item <?php if(get_field('click_views_on_off')) { echo 'witget-all__item_mobile'; } ?>">
               <?php if($ribbon_top_text = get_field('ribbon_top_text')) { ?>
                    <div class="ribbon-top" style="background-color: <?php the_field('ribbon_top_background_color'); ?>"><?php echo $ribbon_top_text; ?></div>
                <?php } ?>
					<div class="witget-all__block">
						<div>
							<div class="witget-all__img-block">
								<a href="<?php the_permalink(); ?>" class="witget-all__img">
									<?php the_post_thumbnail(); ?>
								</a>
								<div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('stars'); ?>"><span style=""></span></span></div>
							</div>
							<div class="witget-all__info">
								<div class="witget-all__title"><?php the_title(); ?></div>
								
									<div class="witget-all__list-features-row">
									<?php if( get_field('advantages') ) : ?>
										<?php while( have_rows('advantages') ): the_row(); ?>
											<div class="list-features-row__item"><?php echo get_sub_field('advantages_text'); ?></div>
										<?php endwhile; ?>
										<?php endif; ?>
										<a href="<?php the_permalink(); ?>" class="witget-all__read-more"><span><?php the_field('bottom_read_more', 'options'); ?></span></a>
									</div>
								
								</div>
						</div>
						<div>
							<div class="witget-all__raiting">
								<span>
                                   <?php 
                                    $raiting = get_field('raiting'); 
                                    echo $raiting;
                                    ?>
                                </span>
								<?php if( $tool_tip_score = get_field('tool_tip_score', 'option') ): ?><i class="tlt-icon">?</i><?php endif;?>
								
								<div class="witget-all__raiting__desc">
                                    <?php if($raiting >= 9.5) {
												the_field('outstanding', 'options');
                                            }else if($raiting >= 9.0 && $raiting < 9.5) {
												the_field('exceptional', 'options');
                                            }else if($raiting >= 8.5 && $raiting < 9) {
												the_field('very_good', 'options');
                                            }else if($raiting >= 8 && $raiting < 8.5) {
                                                the_field('good', 'options');
                                            }else if($raiting >= 7.5 && $raiting < 8) {
												the_field('promising', 'options');
                                            }else{
                                                the_field('normal', 'options');
                                            }
                                    
                                    ?>
                                </div>
								<?php if( $tool_tip_score ): ?>
                                    <div class="tlt-wrap" data-open="false">
                                        <div class="tlt-text">
                                            <?php echo $tool_tip_score; ?>
                                        </div>
                                        <div class="tlt-close"><?php the_field('close', 'options'); ?></div>
                                        <div class="tlt-arrow"></div>
                                    </div>
								<?php endif;?>
							</div>
							<div class="witget-all__btn">
								<div id="spb-rts-461" class="bb-bubble-rts" style="<?php if(get_field('click_views_on_off')) { echo 'display: block;'; } ?>">Over <?php the_field('click_views_quantity'); ?> people choose this site today</div>
								<a href="<?php the_field('visit_site_url');?>" target="_blank" class="witget-all__link" id="home-visit-site" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_20_homepage_##_' . $i; ?>"><?php the_field('bottom_visit_site', 'options'); ?> <i class="baseline_trending_withe"></i></a>
							</div>
						</div>
					</div>
					<?php if( get_field('highlights_title') && get_field('highlights') ) :?>
					<!-- <div class="expandable">
						<div class="expandable-row">
							<div class="expandable-row__text">
								<div class="">
									<div class="expandable-row__title">Highlights</div>
									<?php if(get_field('highlights_title')) :?><div class="expandable-row__title"><?php the_field('highlights_title')?></div><?php endif;?>
									<?php if( get_field('highlights') ) : ?>
										<ul class="expandable-row__list-features">
										<?php while( have_rows('highlights') ): the_row(); ?>
											<li><?php echo get_sub_field('highlights_text'); ?></li>
										<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
								<a href="<?php the_permalink(); ?>" class="expandable__link">Read review >></a>
							</div>
							<div class="expandable-row__img">
									<?php 
									$image = get_field('image_home');
									$size = 'full'; // (thumbnail, medium, large, full или ваш размер)

									if( $image ) {
										echo wp_get_attachment_image( $image, $size );
									}
									?>
								<a href="" target="_blank" class="expandable__link">Visit <?php the_title(); ?> >></a>
							</div>
						</div>
					</div>
					<div class="witget-all__more-info">More info</div> -->
					<?php endif;?>
				</div>