<li class="witget-all__item">
    <div class="witget-all__block">
        <div>
            <div class="witget-all__img-block">
                <a href="<?php the_permalink(); ?>" class="witget-all__img">
                    <?php the_post_thumbnail(); ?>
                </a>
                <div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('stars'); ?>"><span style=""></span></span></div>
            </div>
            <div class="witget-all__info">
                <div class="witget-all__title"><?php the_title(); ?></div>

                <div class="witget-all__list-features-row">
                    <div class="rewiews__text"><?php the_field('short_description_reviews_page'); ?></div>
                    <a href="<?php the_permalink(); ?>" class="witget-all__read-more"><?php the_field('read_full_review', 'options'); ?></a>
                </div>

            </div>
        </div>
        <div>
            <div class="witget-all__raiting">
                <span><?php 
                    $raiting = get_field('raiting'); 
                    echo $raiting;
                    ?></span>
                <?php if( $tool_tip_score = get_field('tool_tip_score', 'option') ): ?><i class="tlt-icon">?</i><?php endif;?>
                <div class="witget-all__raiting__desc">
                    <?php if($raiting >= 9.5) {
												the_field('outstanding', 'options');
                                            }else if($raiting >= 9.0 && $raiting < 9.5) {
												the_field('exceptional', 'options');
                                            }else if($raiting >= 8.5 && $raiting < 9) {
												the_field('very_good', 'options');
                                            }else if($raiting >= 8 && $raiting < 8.5) {
                                                the_field('good', 'options');
                                            }else if($raiting >= 7.5 && $raiting < 8) {
												the_field('promising', 'options');
                                            }else{
                                                the_field('normal', 'options');
                                            }
                                    
                                    ?>
                </div>
                <?php if( $tool_tip_score ): ?>
                <div class="tlt-wrap" data-open="false">
                    <div class="tlt-text">
                        <?php echo $tool_tip_score; ?>
                    </div>
                    <div class="tlt-close"><?php the_field('close', 'options'); ?></div>
                    <div class="tlt-arrow"></div>
                </div>
                <?php endif;?>
            </div>
            <div class="witget-all__btn">
                <div href="<?php the_field('visit_site_url');?>" target="_blank" class="witget-all__link adoric-popup" id="home-visit-site" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_100_reviewspage_##_' . $i; ?>"><?php //the_field('bottom_visit_site', 'options'); ?><?php the_field('scan_me', 'options'); ?> <i class="baseline_trending_withe"></i></div>
            </div>
        </div>
    </div>
                </li>
