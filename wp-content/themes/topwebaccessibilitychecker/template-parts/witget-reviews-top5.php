<div class="top-5-witget">
    <div class="top-5-witget__title"><?php the_field('title_witget_top_5_all_pages', 'options')?></div>
    <div class="top-5-witget__row">
        <?php global $query;
                        $args = array(
                            'post_type' => 'reviews',
                            'posts_per_page' => 4,
                            'post__in' => [320,407,402,324],
                            'orderby'        => 'post__in'
                        );
                        
                        $query = new WP_Query( $args );
              do_action( 'lineup_5_top', 'lineup_5_top', 'reviews', 5);
              if ( $query->have_posts() ) : 
                  $i = 1;
				  while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="top-5-witget__item">
                        <div class="top-5-witget__title-brend" style="display: none"><?php the_title();?></div>
                        <div class="top-5-witget__img-star">
                            <a href="<?php the_permalink(); ?>" class="top-5-witget__img"><?php the_post_thumbnail('medium'); ?></a>
                            <div class="witget-all__star_row"><span class="wpcr_averageStars" data-wpcravg="<?php the_field('raiting'); ?>"><span style=""></span></span></div>
                        </div>
                        <div class="top-5-witget__rating">
                            <div class="top-5-witget__number">
                                <?php 
                                    $raiting = get_field('raiting'); 
                                    echo $raiting;
                                ?>
                            </div>

                            <span>
                                    <?php if($raiting >= 9.5) {
 												the_field('outstanding', 'options');
                                            }else if($raiting >= 9.0 && $raiting < 9.5) {
												the_field('exceptional', 'options');
                                            }else if($raiting >= 8.5 && $raiting < 9) {
												the_field('very_good', 'options');
                                            }else if($raiting >= 8 && $raiting < 8.5) {
                                                the_field('good', 'options');
                                            }else if($raiting >= 7.5 && $raiting < 8) {
												the_field('promising', 'options');
                                            }else{
                                                the_field('normal', 'options');
                                            }
                                    
                                    ?>
                            </span>
                            <?php if( $tool_tip_score = get_field('tool_tip_score', 'option') ): ?><i class="tlt-icon">?</i><?php endif;?>
                            <?php if( $tool_tip_score ): ?>
                            <div class="tlt-wrap" data-open="false">
                                <div class="tlt-text">
                                    <?php echo $tool_tip_score; ?>
                                </div>
                                <div class="tlt-close">Close</div>
                                <div class="tlt-arrow"></div>
                            </div>
                            <?php endif;?>
                        </div>
                        <div class="top-5-witget__links">
                            <div href="<?php the_field('visit_site_url');?>" target="_blank" class="top-5-witget__links-visit adoric-popup" data-gtm-url="<?php echo get_field('visit_site_url') . '##_lineup_5_top_##_' . $i; ?>">
                            <?php //the_field('bottom_visit_site', 'options'); ?>
                            <?php the_field('scan_me', 'options'); ?>
                        </div>
                            <a href="<?php the_permalink(); ?>" class="top-5-witget__links-reviews"><?php the_field('read_review', 'options'); ?> ></a>
                        </div>
                    </div>
            <?php $i++;
            endwhile; 
        endif; wp_reset_query(); ?>
    </div>
</div>
